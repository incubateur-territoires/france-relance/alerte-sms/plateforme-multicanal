/**
 * Provide useful methods commonly used by several Gironde Numerique services apps
 */
class GirNum {

    initialised = false;

    constructor() {
        if (['interactive', 'complete'].includes(document.readyState)) {
            this.addMessageTemplateToDom();
            this.bindsFeedbackOnAjaxModalFormSubmission();
            this.initialised = true;
        } else {
            document.onreadystatechange = function () {
                if (['interactive', 'complete'].includes(document.readyState) && !this.initialised) {
                    this.addMessageTemplateToDom();
                    this.bindsFeedbackOnAjaxModalFormSubmission();
                    this.initialised = true;
                }
            }.bind(this)
        }
    }

    //Binds feedback handing on modal ajax form submission
    bindsFeedbackOnAjaxModalFormSubmission = () => {
        $(document).ajaxSend(function (event, jqxhr, settings) {
            if ('gnSubmitFeedbackModal' in settings && settings.gnSubmitFeedbackModal) {
                this.handleSubmitFeedbackStart(event);
            }
        }.bind(this));
        $(document).ajaxSuccess(function (event, jqxhr, settings) {
            this.handleSubmitFeedbackStop(settings);
        }.bind(this));
        $(document).ajaxComplete(function (event, jqxhr, settings) {
            this.handleSubmitFeedbackStop(settings);
        }.bind(this));

        $(document).ajaxError(function (event, jqxhr, settings) {
            this.handleSubmitFeedbackStop(settings);
        }.bind(this));
    }


    /**
     * To fix left and right key navigation inside input and textarea.
     * Because it is trapped by dsfr tab component on .fr-tabs container
     * @see https://gouvfr.atlassian.net/servicedesk/customer/portal/1/DSFR-741
     */
    fixDsfrInputNavigationInTabComponents = () => {
        const inputElements = document.querySelectorAll('.fr-tabs input[type], .fr-tabs textarea');

        const keyCodesToFix = [
            window.dsfr.core.KeyCodes.LEFT,
            window.dsfr.core.KeyCodes.RIGHT,
            window.dsfr.core.KeyCodes.HOME,
            window.dsfr.core.KeyCodes.END
        ];

        inputElements.forEach((inputElement) => {
            inputElement.addEventListener('keydown', (e) => {
                if (keyCodesToFix.includes(e.keyCode)) {
                    e.stopPropagation();
                }
            });
        });
    }

    /**
     * To fix modal behaviour since DSFR 1.6.0, where <dialog> inside tab-panel are displayed inside tab-panel and not screen wide.
     * @see https://gouvfr.atlassian.net/servicedesk/customer/portal/1/DSFR-820
     */
    fixDsfrModalInTabComponents = () => {
        const tabPanelDialogs = document.querySelectorAll('.fr-tabs__panel dialog');
        tabPanelDialogs.forEach((dialog) => {
            document.body.appendChild(dialog);
        });
    }

    /* Calculate most appropriate navLink to set it aria-current in main nav*/
    setCurrentMainNavLink = () => {
        try {
            const navLinks = Array.from(document.querySelectorAll('#navigation-main-menu a.fr-nav__link'));
            const navUrls = navLinks.map(l => {
                return l.getAttribute('href');
            });

            navUrls.sort(function (a, b) {
                return a.length - b.length || // sort by length, if equal then
                    a.localeCompare(b);    // sort by dictionary order
            }
            ).reverse();

            for (const u in navUrls) {
                if (window.document.location.href.includes(navUrls[u])) {
                    document.querySelectorAll('#navigation-main-menu a[href="' + navUrls[u] + '"]').item(0).setAttribute('aria-current', 'true');

                    const inSubMenu = document.querySelector('#navigation-main-menu a[aria-current="true"]').closest('.fr-menu');
                    if (inSubMenu?.id && document.querySelectorAll('button[aria-controls="' + inSubMenu.id + '"').length) {
                        document.querySelector('button[aria-controls="' + inSubMenu.id + '"').setAttribute('aria-current', 'true');
                    }
                    break;
                }
            }
        } catch (error) {
            console.error(error);
        }
    }

    /**
     * Prevents non "primary button" to submit form.
     *
     * Typiquement dans :
     *  - toutes les modales, le bouton annuler ne doit pas soumettre le formulaire,
     *  - Gaapse (Mon compte), le formulaire encapsule les 3 onglets où la navigation est faite par des boutons,
     *  - API Open Data (Mon compte), le buton "copier" en bout de ligne ne soit pas soumettre le formulaire de régéneration des Identifiants API token
     **/
    preventNonPrimaryButtonToSubmitForm = () => {
        document.querySelectorAll('button.fr-btn--secondary, button.fr-btn--tertiary, button.fr-tabs__tab, button[data-toggle="tooltip"]').forEach((nonPrimaryButton) => {
            nonPrimaryButton.addEventListener('click', (event) => {
                event.preventDefault();
                event.stopPropagation();
            });
        });
    }

    handleActiveTabFocusOnReload = () => {
        const tabPanelsElements = document.querySelectorAll('.fr-tabs__panel');

        tabPanelsElements.forEach(element => {
            // if tabPanel element is in dialog, don't had eventListner
            if (element.closest('dialog')) {
                return;
            }

            element.addEventListener('dsfr.disclose', (e) => {
                localStorage.setItem('activeTab', e.target.getAttribute('id'));
                /* refs #26 : prevent hiding messages when disclosing defaut tab on load */
                if (!('gnSettingDefaultTab' in document.querySelector('body').dataset)) {
                    this.hideMessages();
                }
            });
        });
        const activeTab = localStorage.getItem('activeTab');
        if (activeTab && activeTab.length) {
            const activeTabElem = document.getElementById(activeTab);
            if (activeTabElem && activeTabElem.classList.contains('fr-tabs__panel')) {
                document.querySelector('body').dataset.gnSettingDefaultTab = true;
                window.dsfr(activeTabElem).tabPanel.disclose();
                delete document.querySelector('body').dataset.gnSettingDefaultTab;
            }
        }
    }

    getFieldValidationContext = (el) => {
        const formElemGroup = el.closest('div.fr-input-group, div.fr-select-group, fieldset.fr-fieldset');

        let elType = 'input';
        let elTypeGroup = 'input-group';
        try {
            if (formElemGroup.classList.contains('fr-select-group')) {
                elType = 'select';
                elTypeGroup = 'select-group';
            }

            if (formElemGroup.classList.contains('fr-fieldset')) {
                elType = false;
                elTypeGroup = 'fieldset';
            }
        } catch (e) {
            console.log('Groupe d\'input non géré.', e, formElemGroup, el);
        }
        return [formElemGroup, elType, elTypeGroup];
    }

    handleFieldValidationClasses(htmlElem, elementType, validity) {
        const removeSuffix = validity ? 'error' : 'valid';
        const addSuffix = validity ? 'valid' : 'error';
        if (elementType) {
            htmlElem.classList.remove('fr-' + elementType + '--' + removeSuffix);
            htmlElem.classList.add('fr-' + elementType + '--' + addSuffix);
        }
    }

    handleFieldValidationMessage = (elemGroup, validity) => {
        const displayValue = validity ? 'none' : 'block';
        for (const error of elemGroup.getElementsByClassName('fr-error-text')) {
            error.style.display = displayValue;
        }
    }

    handleFieldValidation = (el) => {
        let formElemGroup, elType, elTypeGroup;
        [formElemGroup, elType, elTypeGroup] = this.getFieldValidationContext(el)
        this.handleFieldValidationClasses(el, elType, el.checkValidity());
        this.handleFieldValidationClasses(formElemGroup, elTypeGroup, el.checkValidity());
        this.handleFieldValidationMessage(formElemGroup, el.checkValidity());
    }


    // Form validation control
    handleFormValidationControl = () => {
        'use strict';
        const forms = document.querySelectorAll('.needs-validation');
        forms.forEach((form) => {

            form.addEventListener('submit', (event) => {
                if (!form.checkValidity()) {
                    event.preventDefault();
                    event.stopPropagation();
                } else {
                    if (!form.classList.contains('ajax-form') && form.hasAttribute('action')) {
                        this.handleSubmitFeedbackStart(event);
                    }
                }

                for (const el of form.querySelectorAll('[required], [gn-validate]')) {
                    this.handleFieldValidation(el);
                }
                this.displayTabbedFormValidationStatus(form);
            }, false);
        });
        // Ajax form validation control
        const ajaxForms = document.querySelectorAll('.ajax-form');
        ajaxForms.forEach((form) => {
            form.addEventListener('submit', (event) => {
                if (form.checkValidity()) {
                    $.ajax({
                        url: $(form).attr('action'),
                        method: $(form).attr('method'),
                        gnSubmitFeedbackModal: true,
                        data: $(form).serialize(),
                        success: function (data) {
                            this.displayMessage(data.state, data.message);
                            this.closeModal($(form).closest('.fr-modal').attr('id'));

                            // Clear form
                            $(form).removeClass('was-validated');
                            $(form).find(':text, select').val('').end().find(':checked').prop('checked', false);
                            $(form).find('textarea').val('');

                            // If a boostrap-table (or more, comma separated) is associated to the form
                            if ($(form).data('table-refresh')) {
                                $(form).data('table-refresh').split(',').map(el => el.trim()).forEach(refreshId => {
                                    $('#' + refreshId).bootstrapTable('refresh');
                                })
                            }
                        }.bind(this),
                        error: function (jqXHR, textStatus, errorThrown) {
                            this.displayMessage(textStatus, errorThrown);
                            this.closeModal($(form).closest('.fr-modal').attr('id'));
                        }.bind(this)
                    });
                }
                event.preventDefault();
                event.stopPropagation();
            }, false);
        });

        const formsNoAjaxNoValidationNeed = document.querySelectorAll("form:not(.ajax-form):not(.needs-validation)");
        formsNoAjaxNoValidationNeed.forEach((form) => {
            form.addEventListener('submit', (event) => {
                this.handleSubmitFeedbackStart(event);
            });
        });

    }

    /* disables element (button) and keep initial 'disabled' state */
    handleInitialDisabledState = (index, elem) => {
        if (elem.hasAttribute('disabled')) {
            elem.setAttribute('data-gn-initial-disabled', 'disabled');
        }
        elem.setAttribute('disabled', true);
    }

    handleSubmitFeedbackStart = (event) => {
        const $opendModalFooterButtons = $('.fr-modal--opened').find('.fr-modal__footer .fr-btns-group');
        const $targetButtons = $(event.submitter).parents('.fr-btns-group');
        if ($opendModalFooterButtons.length) { //Cas d'une modale
            if (!$opendModalFooterButtons.find('.gn-ajax-loading').length) {
                $('.fr-modal--opened').attr('data-gn-submitted', 'true');
                $('.fr-modal--opened').find('.fr-btn').each(this.handleInitialDisabledState);
                $('.fr-modal--opened').find('.fr-modal__footer .fr-btns-group').append('<li class="gn-ajax-loading"><div class="gn-spinner" role="status" aria-hidden="true"></div><span class="gn-ajax-loading--message">Action en cours, merci de patienter.</span></li>');
            }
        } else if ($(event.target).prop('nodeName') === 'FORM' && $targetButtons.length) {
            $(event.target).find('.fr-btn').each(this.handleInitialDisabledState);
            $targetButtons.prepend('<li class="gn-ajax-loading"><div class="gn-spinner" role="status" aria-hidden="true"></div><span class="gn-ajax-loading--message">Action en cours, merci de patienter.</span></li>');
        } else { // les autres cas
            this.openModal('gnSubmitFeedbackModal');
        }
    }

    handleSubmitFeedbackStop = (settings) => {
        if ('gnSubmitFeedbackModal' in settings && settings.gnSubmitFeedbackModal) {
            if ($('.gn-ajax-loading').length) {
                $('.gn-ajax-loading').remove();
            } else {
                this.closeModal('gnSubmitFeedbackModal');
            }
        }
    }

    /** Binds resetModalForm on Modal opening */
    customizeModalOpening = () => {
        const modalElements = document.querySelectorAll('dialog.fr-modal');
        modalElements.forEach(element => {
            element.addEventListener('dsfr.disclose', (e) => {
                // prevent closing modal on clicking outside
                e.target.setAttribute('data-fr-concealing-backdrop', 'false');

                this.resetModalForm(e.target);
                this.resetModalFormValidation(e.target);
                this.resetSubmittedModalForm(e.target);
                this.resetTabbedFormValidationStatus();
            });
        });
    }

    /**
     * @description Resets form validation inside a modal identified by its HTML element.
     * @param {Html Element} modalElement
     */
    resetModalFormValidation = (modalElement) => {
        if (modalElement.getElementsByClassName('needs-validation')[0]) {
            for (const elType of ['input', 'select']) {
                for (const inputGroup of modalElement.getElementsByClassName('fr-' + elType + '-group')) {
                    inputGroup.classList.remove('fr-' + elType + '-group--error');
                    inputGroup.classList.remove('fr-' + elType + '-group--valid');
                }
                for (const input of modalElement.getElementsByClassName('fr-' + elType)) {
                    input.classList.remove('fr-' + elType + '--error');
                    input.classList.remove('fr-' + elType + '--valid');
                }
            }

            for (const error of modalElement.getElementsByClassName('fr-error-text')) {
                error.style.display = 'none';
            }
        }
    }

    /**
     * @description Resets "Submitted" state (concealing-backdrop, disabled buttons, spinner / waiting message) on previously submitted modal identified by its HTML element.
     * @param {Html Element} modalElement
     */
    resetSubmittedModalForm = (modalElement) => {
        if (modalElement.getAttribute('data-gn-submitted')) {
            modalElement.querySelectorAll('.fr-btn').forEach(item => {
                if (!item.hasAttribute('data-gn-initial-disabled')) {
                    item.removeAttribute('disabled');
                    item.removeAttribute('data-gn-initial-disabled');
                }
            });
        }
    }

    /**
     * @description Resets form fields value inside a modal identified by its HTML element.
     * @param {Html Element} modalElement
     */
    resetModalForm = (modalElement) => {
        for (const formElement of modalElement.getElementsByTagName('form')) {
            if ('gnPreventFormReset' in formElement.dataset && formElement.dataset.gnPreventFormReset) {
                continue;
            }
            formElement.reset();
        }
    }

    /**
     * add a "show password" toggle button after input[type=password] having the .gnPasswordVisibilityToggle class
     */
    handleVisibilityToggleOnPasswordFields = () => {

        // find the password fields we want to add the functionality to
        const passwordFields = document.querySelectorAll('input[type=password].gnPasswordVisibilityToggle');

        //and leave early if nothing to do
        if (!passwordFields.length) { return; }


        //first create the event handler for toogle button click so we can bind it later (if created after, it would be undefined)
        const togglePassword = (event) => {
            event.preventDefault();
            event.stopPropagation();

            const passwordField = document.getElementById(event.target.dataset.gnTogglePassword);
            const showPassword = event.target.getAttribute('aria-pressed') !== 'true';

            passwordField.setAttribute('type', showPassword ? 'text' : 'password');
            event.target.setAttribute('aria-pressed', showPassword ? 'true' : 'false');
            event.target.title = (showPassword ? 'Cacher' : 'Montrer') + ' le mot de passe';
            event.target.firstChild.classList.remove('fa-eye', 'fa-eye-slash')
            const classToAdd = showPassword ? 'fa-eye-slash' : 'fa-eye';
            event.target.firstChild.classList.add(classToAdd);
        }

        passwordFields.forEach(passwordField => {

            //creation d'un wrapper pour l'input passord et le boutton de toggle
            const passwordWrapper = document.createElement('div');
            passwordWrapper.classList.add('fr-input-wrap', 'fr-input-wrap--addon');
            passwordField.parentNode.insertBefore(passwordWrapper, passwordField);

            // move password field into wrapper
            passwordWrapper.appendChild(passwordField);

            // create visibility toggle button
            const buttonToggler = document.createElement('button');
            buttonToggler.classList.add('fr-btn', 'fr-btn--tertiary');
            buttonToggler.setAttribute('type', 'button');
            buttonToggler.setAttribute('role', 'switch');
            buttonToggler.setAttribute('aria-pressed', 'false');
            buttonToggler.dataset.gnTogglePassword = passwordField.id;
            buttonToggler.title = 'Montrer le mot de passe';
            buttonToggler.id = 'show-passord-for-field-' + passwordField.id
            buttonToggler.addEventListener('click', togglePassword);
            // if form in modal, form is reset on modal open, we need to simulate to reset toogleButton
            passwordField.closest('form').addEventListener('reset', function (e) {
                buttonToggler.setAttribute('aria-pressed', 'true');
                buttonToggler.click();
            })

            // adding fontawesome icon inside toggle button
            const buttonTogglerContent = document.createElement('i');
            buttonTogglerContent.classList.add('fas', 'fa-eye');
            buttonTogglerContent.style = 'pointer-events: none;'; // important so that click on the icon still fires button click
            buttonToggler.appendChild(buttonTogglerContent);

            //put the toggle button after passwordField
            passwordField.after(buttonToggler)
        });
    };

    /**
     * @description Display a modal view identified by its HTML id.
     * @param {String} idModal Modal bloc HTML id.
     */
    openModal = (idModal) => {
        const modalElement = document.getElementById(idModal);
        if (modalElement && modalElement.classList.contains('fr-modal') && window.dsfr(modalElement) && ('modal' in window.dsfr(modalElement))) {
            window.dsfr(modalElement).modal.disclose();
        }
    }

    /**
     * @description Close a modal view identified by its HTML id.
     * @param {String} idModal Modal bloc HTML id.
     * @see #openModal
     */
    closeModal = (idModal) => {
        const modalElement = document.getElementById(idModal);
        if (modalElement && modalElement.classList.contains('fr-modal') && window.dsfr(modalElement) && ('modal' in window.dsfr(modalElement))) {
            window.dsfr(modalElement).modal.conceal();
        }
    }

    addMessageTemplateToDom = () => {

        let messageTemplateContainer = document.createElement('template');
        messageTemplateContainer.id = "message-template";

        const messageTemplate = '<div class="fr-mb-2v fr-collapse">' +
            '<div class="fr-alert" role="alert">' +
            '<p class="fr-alert__title"></p>' +
            '<button class="fr-btn--close fr-btn" aria-expanded="true" title="Masquer le message" data-fr-js-collapse-button="true">Masquer le message</button>' +
            '</div>' +
            '</div>';
        messageTemplateContainer.innerHTML = messageTemplate;
        document.body.appendChild(messageTemplateContainer)

    }

    hideMessages = () => {
        const messages = document.querySelectorAll('#message-area .fr-collapse');
        messages.forEach(message => message.remove());
    }

    /**
     * @description Display an information message with a DSFR Alert composern with specific level color.
     * @param {string} level Alert level, possible value : info, success, warning, error
     * @param {string} message The message to display.
     */
    displayMessage = (level, message) => {
        const divUniqueId = 'fr-alert-' + level + '-' + this.generateHtmlRandomId();
        const levelLabel = this.getLevelLabel(level);
        const messageTemplateContainer = document.querySelector("#message-template");
        const messageTemplate = document.importNode(messageTemplateContainer.content, true).firstChild;
        messageTemplate.setAttribute('id', divUniqueId);
        messageTemplate.querySelector('.fr-btn--close').setAttribute('aria-controls', divUniqueId);
        messageTemplate.querySelector('.fr-alert').classList.add('fr-alert--' + level);
        messageTemplate.querySelector('.fr-alert p.fr-alert__title').innerHTML = levelLabel + message;
        document.querySelector('#message-area').prepend(messageTemplate);
    }

    /**
     * Generate a 7 chars pseudo random (non-crypto) string composed of letters and numbers.
     *
     * @returns {string} usually used to produce a unique id for HTML elements.
     */
    generateHtmlRandomId = () => {
        return Math.random().toString(36).substring(2, 9);
    }

    /**
     * Return the label for a given alert message level.
     *
     * @param {string} level should be one of info, success, warning or error
     * @returns {string} a human friendly formatted flavour of level
     */
    getLevelLabel = (level) => {
        let levelLabel = '';
        switch (level) {
            case 'info':
            case 'success':
                levelLabel = 'Information : ';
                break;
            case 'warning':
                levelLabel = 'Attention : ';
                break;
            case 'error':
                levelLabel = 'Erreur : ';
                break;
        }
        return levelLabel;
    }

    /**
     * Apply usefull defaults for GN
     */
    customizeBootstrapTableDefaults = () => {
        if (typeof $.fn?.bootstrapTable?.defaults === 'undefined') {
            //if bootstraptable not loaded, get out
            return;
        }

        $.extend($.fn.bootstrapTable.defaults, {
            buttonsPrefix: 'fr-btn', // for DSFR , .fr-btn replaces bootstrap .btn
            classes: 'table table-sm table-bordered table-hover table-striped',
            iconSize: 'sm',
            locale: 'fr-FR',
            pagination: true,
            showExtendedPagination: true,
            pageSize: 25,
            pageList: [25, 50, 100, 'all'],
            search: true,
            height: 'auto'
        });

        // unfortunately, small button in DSFR are set with a .fr-btn--sm and not .fr-btn-sm
        // so replace it after bootstrap-table is rendered.
        const gnTables = document.querySelectorAll('table');
        gnTables.forEach(gnTable => {
            $(gnTable).on('post-body.bs.table', (e, name, args) => {
                const smBtn = gnTable.closest('.bootstrap-table').querySelectorAll('.fr-btn-sm');
                smBtn.forEach(elem => { elem.classList.replace('fr-btn-sm', 'fr-btn--sm') });
            })


            // when setting height: 'auto', the pagination is quite too low, we fix it
            $(gnTable).on('post-header.bs.table', (e, name, args) => {
                const tableContainer = gnTable.closest('.fixed-table-container');
                tableContainer.style.paddingBottom = '1em';
                tableContainer.style.borderBottom = 'none';
            })
        })
    }
    /**
     * add a gn-label--mandatory class on required fields' labels
     */
    handleRequiredVisibilityOnLabels = () => {
        document.querySelectorAll('[required]:not([type=radio])').forEach(elem => {
            document.querySelector('label[for=' + elem.getAttribute('id') + ']')?.classList.add('gn-label--mandatory');
        })

    }

    displayTabbedFormValidationStatus = (formElement) => {

        this.resetTabbedFormValidationStatus();

        const parentDialog = formElement.closest('dialog');
        const tabPanels = formElement.querySelectorAll('.fr-tabs__panel');
        let hasError = false;

        tabPanels.forEach((tabPanel) => {
            const tabId = tabPanel.getAttribute('aria-labelledby')
            const errors = tabPanel.querySelectorAll('.fr-input--error, .fr-select--error, .fr-fieldset--error');

            if (errors.length) {
                hasError = true;
                document.getElementById(tabId)?.classList.add('gn-tabs__tab--error', 'fr-tabs__tab--icon-left', 'fr-fi-error-fill');
            }
        });

        if (parentDialog && hasError) {
            parentDialog.querySelector('.fr-modal__header').scrollIntoView({ behavior: "smooth" });
        }
    }

    resetTabbedFormValidationStatus = () => {
        document.querySelectorAll('.gn-tabs__tab--error').forEach((tabButton) => {
            tabButton.classList.remove('gn-tabs__tab--error', 'fr-tabs__tab--icon-left', 'fr-fi-error-fill');
            // the evaluation of  "tabButton.offsetWidth" is needed to re-run css animation
            // this just a way to evalutate the expression without using void(), which is raised as a critial code smell by  sonarqube
            tabButton.setAttribute('offsetWidthVoid', tabButton.offsetWidth);
        });
    }

    /** init a tooltip on copy button, and handle input source text copy with Clipboard API */
    handleCopyToClipboardButton = () => {

        const copyButtonSelector = '.copy-btn';
        const sourceCopySelector = '.input-btn';
        const copyInClipboardText = 'Copier dans le presse-papier';
        const copiedInClipboardText = 'Copié !';
        const copyManualyText = 'Copier avec Ctrl+C';

        const buttons = document.querySelectorAll(copyButtonSelector);

        buttons.forEach((button) => {
            const container = button.closest('dialog') || document.body;

            $(button).tooltip({
                container: container,
                trigger: 'hover',
                placement: 'top'
            });

            if (container.tagName === 'DIALOG') {
                container.addEventListener('dsfr.disclose', (e) => {
                    button.setAttribute('data-original-title', copyInClipboardText);
                });
            }

            button.addEventListener('copied', function (event, message) {
                $(this).tooltip('hide').attr('data-original-title', event.detail).tooltip('show');
            });

            button.addEventListener('click', function () {
                const sourceInput = button.parentElement.querySelector(sourceCopySelector);
                sourceInput.select();

                navigator.clipboard.writeText(sourceInput.value).then(() => {
                    // Clipboard successfully set*
                    this.dispatchEvent(new CustomEvent('copied', { detail: copiedInClipboardText }));
                }, () => {
                    // Clipboard write failed
                    this.dispatchEvent(new CustomEvent('copied', { detail: copyManualyText }));
                }).catch(err => {
                    this.dispatchEvent(new CustomEvent('copied', { detail: copyManualyText }));
                });
            });
        });
    }
}

window.GN = new GirNum();
