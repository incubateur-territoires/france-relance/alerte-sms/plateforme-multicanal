$(document).ready(function () {
    /**
     * To fix left and right key navigation inside input and textarea.
     * Because it is trapped by dsfr tab component on .fr-tabs container
     * @see https://gouvfr.atlassian.net/servicedesk/customer/portal/1/DSFR-741
     */
    GN.fixDsfrInputNavigationInTabComponents();

    /**
     * To fix modal behaviour since DSFR 1.6.0, where <dialog> inside tab-panel are displayed inside tab-panel and not screen wide.
     * @see https://gouvfr.atlassian.net/servicedesk/customer/portal/1/DSFR-820
     */
    GN.fixDsfrModalInTabComponents();

    /* Calculate most appropriate navLink to set it aria-current in main nav*/
    GN.setCurrentMainNavLink();

    /* Prevent non "primary button" to submit form */
    GN.preventNonPrimaryButtonToSubmitForm();

    /** Permet le re-focus d'onglet actif au rechargement avec l'api dsfr */
    GN.handleActiveTabFocusOnReload();

    /** Gère la validation des formulaire et la soumission des formulaires .ajax-form */
    GN.handleFormValidationControl();

    /** Ajout un bouton de toggle password visibility sur les input de type password qui on la classe .gnPasswordVisibilityToggle */
    GN.handleVisibilityToggleOnPasswordFields();

    /** Permet de configurer le comportement à l'ouverture d'une modale (empeche le click en-dehors, resetForm, ...) */
    GN.customizeModalOpening();

    /** Permet de configurer certaines valeurs par défaut de bootstrap-table */
    GN.customizeBootstrapTableDefaults();

    /** Ajoute l'indicatif obligatoire sur les champs requis */
    GN.handleRequiredVisibilityOnLabels();

    /** fait fonctionner le tooltip et le copyToClipboard sur les boutons copier */
    GN.handleCopyToClipboardButton();

});
