$(document).ready(function () {
    $('button.edit-category').on('click', function () {
        let categoryId = $(this).data('category-id');
        GN.openModal('editCategoryModal');
        $('#editCategoryModal input[name="category-id"]').val(categoryId);
        $('#editCategoryModal input[name="category-name"]').val($('li#category' + categoryId + ' a').html());
    });

    $('button.delete-category').on('click', function () {
        let categoryId = $(this).data('category-id');
        GN.openModal('deleteCategoryModal');
        $('#deleteCategoryModal span.category-name').html($('li#category' + categoryId + ' a').html());
        $('#deleteCategoryModal input[name="category-id"]').val(categoryId);
    });
});

function loadShareContentModal(contentId) {
    GN.openModal('shareContentModal');
    const contentViewUrl = document.querySelector('#share-base-url').getAttribute('value') + contentId;
    const message = "Bonjour,\nVoici un contenu que je souhaite vous partager avant de procéder à sa communication.";
    document.querySelector('[name="share-content-id"]').setAttribute('value', contentId);
    document.querySelector('#share-url').setAttribute('value', contentViewUrl);
    document.querySelector('#share-email-message').textContent = message;
}

function loadDuplicateContentModal(contentId, contentTitle) {
    GN.openModal('duplicateContentModal');
    $('#duplicateContentModal input[name="content-id"]').val(contentId);
    $('#duplicateContentModal span.content-name').html(contentTitle);
}

function loadViewDistributionListModal(distributionListId) {
    GN.openModal('viewDistributionListModal');
    $.ajax({
        url: '/action/ajax/preview/distributionList',
        method: 'post',
        data: {
            'csrf_name': csrfName,
            'csrf_value': csrfValue,
            'distribution-id': distributionListId
        },
        success: function (data) {
            $('#viewDistributionListModal .fr-follow').html(data);
        },
        error: function (er) {
            params.error(er);
        }
    });
}

function loadEditDistributionListModal(distributionListId) {
    GN.openModal('editDistributionListModal');
    $('#editDistributionListModal img.distribution-image').hide();
    $('#editDistributionListModal img.distribution-image').attr('src', '');
    $.ajax({
        url: '/action/ajax/get/distributionList',
        method: 'post',
        data: {
            'csrf_name': csrfName,
            'csrf_value': csrfValue,
            'distribution-id': distributionListId
        },
        success: function (data) {
            $('#editDistributionListModal input[name="distribution-id"]').val(data.id);
            $('#editDistributionListModal input[name="distribution-title"]').val(data.title);
            $('#editDistributionListModal textarea[name="distribution-description"]').val(data.description);
            $('#editDistributionListModal select[name="distribution-category"]').val(data.category.id).change();
            if (data.image != null && data.image.url !== '') {
                $('#editDistributionListModal img.distribution-image').attr('src', data.image.url);
                $('#editDistributionListModal img.distribution-image').show();
            }
        },
        error: function (er) {
            params.error(er);
        }
    });
}

const MultiCanal = class {

    initDmUploader = (dragAndDropSelector, messageAreaSelector) => {

        const $dragAndDropElement = jQuery(dragAndDropSelector);
        const allowedExtensions = window.gnSettings.dmUploader.allowed_extensions; // Like ['jpeg', 'jpg', 'png', 'pdf', 'mp4', 'webm'];
        const allowedMimeTypes = window.gnSettings.dmUploader.allowed_mimetypes;
        const maxFileSize = window.gnSettings.dmUploader.max_filesize; // in bytes, max 10 Mo

        let message = '';
        let filename = null;
        $(function () {
            $dragAndDropElement.dmUploader({
                url: '/action/ajax/upload/file',
                extraData: function (id) {
                    return {
                        'csrf_name': csrfName,
                        'csrf_value': csrfValue,
                        'upload_id': id,
                    };
                },
                maxFileSize: maxFileSize,
                extFilter: allowedExtensions,
                auto: true,
                queue: true,
                onInit: function () {
                    $(this).find('.gnUploaderMaxFileSize').text(parseFloat((maxFileSize / (1024 * 1024))).toFixed(0) + " Mo");
                    $(this).find('.gnUploaderAllowerFormats').text(allowedExtensions.join(', '));

                    // pupulate real input[type="file"] accept attribute
                    const extensionsForAcceptAttribute = allowedExtensions.map(s => '.' + s)
                    $(this).find('input[type="file"]').attr('accept', [...extensionsForAcceptAttribute, ...allowedMimeTypes].join(','));

                },
                onFileExtError: function (file) {
                    GN.displayMessage('warning',
                        'Ce type de fichier n\'est pas pris en charge.',
                        messageAreaSelector);
                },
                onDragEnter: function () {
                    // Happens when dragging something over the DnD area
                    this.addClass('active');
                },
                onDragLeave: function () {
                    // Happens when dragging something OUT of the DnD area
                    this.removeClass('active');
                },
                onNewFile: function (id, file) {
                    ui_multi_add_file(id, file);
                    if (!file.type.startsWith('image/') || typeof FileReader === "undefined") {
                        $('#uploaderFile' + id).find('.gn-upload-img-preview img').remove();
                        return
                    }

                    let reader = new FileReader();
                    let img = $('#uploaderFile' + id).find('img');

                    reader.onload = function (e) {
                        img.attr('src', e.target.result);
                    }
                    reader.readAsDataURL(file);
                },
                onBeforeUpload: function (id) {
                    // about tho start uploading a file
                    ui_multi_update_file_status(id, 'uploading', 'téléversement...');
                    ui_multi_update_file_progress(id, 0, '', true);
                },
                onUploadCanceled: function (id) {
                    // Happens when a file is directly canceled by the user.
                    ui_multi_update_file_status(id, 'warning', 'Annul&eacute; par l\'utilisateur');
                    ui_multi_update_file_progress(id, 0, 'warning', false);
                },
                onUploadProgress: function (id, percent) {
                    // Updating file progress
                    ui_multi_update_file_progress(id, percent);
                },
                onUploadSuccess: function (id, data) {
                    // A file was successfully uploaded
                    try {
                        const jsondata = JSON.parse(data);
                        message = jsondata?.message;
                        filename = jsondata.filename;
                    } catch (e) {
                        message = 'error';
                    }
                    ui_multi_update_file_status(id, 'success', message);
                    ui_multi_update_file_realname(id, filename);
                    ui_multi_update_file_progress(id, 100, 'success', false);
                },
                onUploadError: function (id, xhr, status, message) {
                    try {
                        const data = JSON.parse(xhr.responseText);
                        message = data?.message;
                        if (window.gnSettings.env.development) {
                            console.error(data?.error);
                        }
                    } catch (e) {
                        message = xhr.responseText;
                    }

                    ui_multi_update_file_status(id, 'danger', message);
                    ui_multi_update_file_realname(id, false);
                    ui_multi_update_file_progress(id, 0, 'danger', false);
                },
                onFallbackMode: function () {
                    GN.displayMessage('error', 'Le module de t&eacute;l&eacute;charg&eacute; n\'est pas pris en charge par votre navigateur.<br />Veuillez changer de navigateur ou mettre &agrave; jour celui-ci.');
                },
                onFileSizeError: function (file) {
                    GN.displayMessage('warning', 'Le fichier ' + file.name + ' est trop grand pour &ecirc;tre t&eacute;l&eacute;charg&eacute;.<br />Taille maximale autoris&eacute; par fichier : 10 Mo.');
                }
            });
        });


        /** @description Creates a new file and add it to our list.
        * @param {string} id File identifier
        * @param {string} status File upload status
        */
        const ui_multi_add_file = (id, file) => {
            let template = $('#files-template').text();
            template = template.replace('%%filename%%', file.name);
            template = template.replaceAll('%%fileid%%', id);

            template = $(template);
            template.prop('id', 'uploaderFile' + id);
            template.data('file-id', id);

            $('#files').find('li.empty').fadeOut(); // remove the 'no files yet'
            $('#files').prepend(template);
        }

        /** @description Changes the status messages on upload files list.
         * @param {string} id File identifier
         * @param {string} status File upload status
         * @param {string} message Message to display
         */
        const ui_multi_update_file_status = (id, status, message) => {
            $('#uploaderFile' + id).find('span').html(message).prop('class', 'status text-' + status);
        }

        const ui_multi_update_file_realname = (id, realfilename) => {
            if (realfilename) {
                $('#uploaderFile' + id).find('input.uploaded-input-filename').val(realfilename);
                $('#uploaderFile' + id).find('strong.uploaded-filename').text(realfilename);
            } else {
                $('#uploaderFile' + id).find('input.uploaded-input-filename').remove();
                $('#uploaderFile' + id).find('input.fileids').remove();
                $('#uploaderFile' + id).find('.form-group').remove();
            }
        }

        /** @description Updates a file progress, depending on the parameters it may animate it or change the color.
         * @param {string} id File identifier
         * @param {float} percent File upload percentage
         * @param {string} color File upload progress bar color
         * @param {boolean} active File media active flag
         */
        const ui_multi_update_file_progress = (id, percent, color, active) => {
            color = (typeof color === 'undefined' ? false : color);
            active = (typeof active === 'undefined' ? true : active);
            let bar = $('#uploaderFile' + id).find('div.progress-bar');
            bar.width(percent + '%').attr('aria-valuenow', percent);
            bar.toggleClass('progress-bar-striped progress-bar-animated', active);

            if (percent === 0) {
                bar.html('');
            } else {
                bar.html(percent + '%');
            }

            if (color) {
                bar.removeClass('bg-success bg-info bg-warning bg-danger');
                bar.addClass('bg-' + color);
            }
        }

    };
}

window.Multicanal = new MultiCanal();
