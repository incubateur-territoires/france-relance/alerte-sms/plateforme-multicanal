<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

declare(strict_types=1);

require_once __DIR__ . '/../vendor/autoload.php';

use Multicanal\App\Helper;
use Multicanal\App\Settings\SettingsInterface;
use Multicanal\Controller\View\ErrorViewController;
use Multicanal\Entity\ApiResponse;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Log\LoggerInterface;
use Slim\App;


// Bootstrapping settings and main container dependencies definitions
$container = require_once dirname(__DIR__, 1) . '/src/App/bootstrap/bootstrap.php';

// Get the App (automatically created from container (see dependencies factory definition))
$app = $container->get(App::class);

// Register middleware
$middleware = require_once dirname(__DIR__, 1) . '/src/App/bootstrap/middleware.php';
$middleware($app);

// Register routes
$routes = require_once dirname(__DIR__, 1) . '/src/App/bootstrap/routes.php';
$routes($app);

// Define Custom Error Handler
$errorHandler = function (Request $request, Throwable $exception) use ($app) {
    // Display custom error page
    if (!Helper::isApiRouteRequest($request)) {
        $request = $request->withAttribute('exception', $exception);
        return $app->get($request->getUri()->getPath(), ErrorViewController::class)->run($request);
    } else {
        // For API, return error as JSON
        $response = $app->getResponseFactory()->createResponse();
        $response = $response->withStatus($exception->getCode());
        $response->withHeader('Content-type', 'application/json');
        $apiResponse = new ApiResponse($exception->getCode(), $exception->getMessage(), false);
        $apiResponse->setTime(intval(1000 * (microtime(true) - $request->getServerParams()['REQUEST_TIME_FLOAT'])));
        $response->getBody()->write(json_encode($apiResponse, JSON_UNESCAPED_UNICODE));
        return $response;
    }
};
// Add Error Middleware
$settings = $container->get(SettingsInterface::class);
$logger = $container->get(LoggerInterface::class);
$errorMiddleware = $app->addErrorMiddleware(
    $settings->get('env.development'),
    $settings->get('env.logging'),
    $settings->get('env.debug'),
    $logger
);
if (!$settings->get('env.development')) {
    $errorMiddleware->setDefaultErrorHandler($errorHandler);
}

$app->run();
