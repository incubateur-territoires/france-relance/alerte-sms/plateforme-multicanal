# docker_factory
MAINTAINER Thierry CEREYON <t.cereyon@groupeonepoint.com>

Projet de dockerisation des usines web


## Pré-requis

### Make

Vérifier si `Make` est installé en lancant la commande : `make -v`
* Si `make` est installé vous devriez voir affiché sur votre terminal :

```bash
GNU Make 4.2.1
Built for x86_64-pc-linux-gnu
Copyright (C) 1988-2016 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
```

* Si `make` n'est pas installé sur votre poste :
  * Sur Ubuntu / Debian taper la commande :
```bash
sudo apt install make
```

  * Sur Mac taper la commande :
```bash
brew install make
```

  * Sur Red Hat taper la commande :
```bash
yum install make
```

### Docker
https://docs.docker.com/engine/install

### Docker-compose
https://docs.docker.com/compose/install/


## Configuration
* Copier ou renommer `.env.example` en `.env`
* Ouvrir le fichier `.env`
* A la ligne di-dessous, remplacer `/projects` par le chemin de la racine de vos projets
``export PROJECT_PATH_ON_VM=/projects``
* A la ligne di-dessous, remplacer `/girondenumerique` par le nom du réprtoire de votre projet contenant la solution
  export PROJECT_ROOT_DIRECTORY=girondenumerique
* Au besoin, adapter le reste de la configuration notament les identifiants et nom de la base de données

## Démarrage de l'usine
* Pour la première installation, exécuter la commande `make build`
* Pour relancer , exécuter la commande `make rebuild`
* Pour redémarrer, exécuter la commande `make restart`
Les données de la BDD sont persistées

## Activation / Désactivation Xdebug
* modifier les paramètres "Xdebug conf" dans `docker_factory/.env`,
* relancer l'instance avec `make restart`
* Sur VSCode, cliquer sur l'icone verte en bas à gauche, "Attach to running container" > choisir le conainer php
* installer l'extension "PHP Debug" dans la fenêtre VSCode du container.
* lancer la session de debug vscode et sur le navigateur activer la session de debug avec l'extension Xdebug Helper
 si besoin (en fonction de la configuration de XDEBUG_START_WITH_REQUEST).

## En cas de problème sous windows (wsl)
Décommenter ma ligne ``sudo sysctl -w vm.max_map_count=262144``

## Pour
## Nota bene
La base de données est disponible sur la boucle local, dans l'application, son host est `database`










