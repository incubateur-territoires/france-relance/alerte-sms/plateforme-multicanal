include .env

export LOCAL_USER_ID=$(shell id -u)
export LOCAL_GROUP_ID=$(shell id -g)
DOCKER_PHP=docker exec -i -u root --workdir=/var/www/html ${PROJECT_NAMESPACE}-${PROJECT_NAME}-php-${PROJECT_ENVIRONNEMENT} bash;
DOCKER_PHP_TEST=docker exec -i -u root --workdir=/var/www/html ${PROJECT_NAMESPACE}-${PROJECT_NAME}-php-test bash;

.PHONY: build
build:
	cd docker && docker-compose build

.PHONY: clear
clear:
	docker-compose down --remove-orphans
	cd docker && docker-compose down --remove-orphans
	docker network prune -f

.PHONY: rebuild
rebuild:
	docker-compose down --remove-orphans
	cd docker && docker-compose down --remove-orphans
	docker network prune -f
	#sudo sysctl -w vm.max_map_count=262144
	docker network prune -f
	docker network create web
	docker network create internal
	cd docker && docker-compose build
	docker-compose build
	docker-compose up -d
	echo "composer install" | $(DOCKER_PHP)

.PHONY: tests
tests:
	docker-compose down --remove-orphans
	cd docker && docker-compose down --remove-orphans
	docker network prune -f
	#sudo sysctl -w vm.max_map_count=262144
	docker network prune -f
	docker network create web
	docker network create internal
	cd docker && docker-compose build
	docker-compose --file docker-compose-test.yml build
	docker-compose --file docker-compose-test.yml up -d
	echo "composer install" | $(DOCKER_PHP_TEST)
	echo "cp .env.test .env" | $(DOCKER_PHP_TEST)
	sleep 20
	echo "php vendor/bin/doctrine orm:schema-tool:create" | $(DOCKER_PHP_TEST)
	echo "vendor/bin/phpunit" | $(DOCKER_PHP_TEST)


.PHONY: retests
retests:
	docker-compose down --remove-orphans
	cd docker && docker-compose down --remove-orphans
	docker network prune -f
	#sudo sysctl -w vm.max_map_count=262144
	docker network prune -f
	docker network create web
	docker network create internal
	docker-compose --file docker-compose-test.yml build
	docker-compose --file docker-compose-test.yml up -d
	echo "composer install" | $(DOCKER_PHP_TEST)
	echo "cp .env.test .env" | $(DOCKER_PHP_TEST)
	sleep 20
	echo "php vendor/bin/doctrine orm:schema-tool:create" | $(DOCKER_PHP_TEST)
	echo "vendor/bin/phpunit" | $(DOCKER_PHP_TEST)

.PHONY: site_install
site_install:
	echo "composer install" | $(DOCKER_PHP)

.PHONY: start
start:
	#sudo sysctl -w vm.max_map_count=262144
	docker network create web
	docker network create internal
	docker-compose build
	docker-compose up -d
	echo "composer install" | $(DOCKER_PHP)

.PHONY: up
up:
	docker-compose up -d

.PHONY: stop
stop:
	docker-compose down --remove-orphans
	docker network prune -f

.PHONY: restart
restart:
	docker-compose down --remove-orphans
	docker network prune -f
	#sudo sysctl -w vm.max_map_count=262144
	docker network create web
	docker network create internal
	docker-compose build
	docker-compose up -d
	echo "composer install" | $(DOCKER_PHP)


.PHONY: ssh-root
ssh-root:
	docker-compose exec -u root php bash

.PHONY: ssh-user
ssh-user:
	docker-compose exec -u  ${LOCAL_USER_ID}:${LOCAL_GROUP_ID} php bash

.PHONY: php-logs
logs:
	docker-compose logs -f php
