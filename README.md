[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

# Application multicanal, interface de gestion de contenus pour communication multicanal (email, SMS, site Internet et réseaux sociaux)

## Description
Cette application permet la gestion de contenus de manière collaborative au sein d'une même collectivité avant communication sur plusieurs canaux de transmission. Elle intégre aussi une API pour emploi de ces fonctionanlités de communication au sein d'autres applications Gironde Numérique.
La gestion intégrale est permise aux administrateurs généraux du domaine girondenumerique.fr uniquement.

## Pré-requis
- Serveur Apache avec PHP > 8.1
- Serveur MySQL
- Extension PHP LDAP
- Extension IMAP

## Installation
- Pour obtenir les dépendances du projet, lancer dans un terminal la commande (composer doit préalablement être installé) : `composer install`
- Générer l'autoload des classes du projet avec la commande : `composer dump-autoload -o`
- Copier le fichier .htaccess à la racine du projet en prenant soin de commenter la mise en place du protocole HTTPS
- Paramètrer ensuite le fichier de propriétés .env à partir de l'exemple situé dans le répertoire src/conf/.env.example

### Base de données gérées par ORM Doctrine
La persistence des données de ce projet est gérée par le biais d'un mapping objet-relationnel (en anglais *object-relational mapping* ou ORM) fourni par la librairie ORM Doctrine. La structure du schéma de la base de données est dictée par la couche entité du projet permettant l'interface entre l'application et la base de données relationnelle persistée sur un serveur MySQL. Toute modification de la structure des entités du projet entraine donc une potentiel modification du schéma.
#### Documentation
https://www.doctrine-project.org/projects/doctrine-orm/en/2.13/reference/attributes-reference.html
#### Création du schéma
Spécificité MAMP Pro, ne pas oublier de cocher l'option "Allow network access to Mysql" pour éviter l'erreur de type 'SQLSTATE[HY000] [2002] Connection refused'
- Pour extraire le script de création du schéma de la base de données, exécuter la commande suivante à la racine du projet :
  `php bin/doctrine orm:schema-tool:create --dump-sql`

- Pour mettre à jour le script de création du schéma orm-schema-create.sql, employer la commande suivante :
  `php bin/doctrine orm:schema-tool:create --dump-sql > resources/schema/orm-schema-create.sql`

- Pour créer le schéma de la base de données, exécuter la commande suivante :
  `php bin/doctrine orm:schema-tool:create`

#### Mise à jour du schéma
- Pour mettre à jour le schéma de la base de données, exécuter la commande suivante à la racine du projet :
  `php bin/doctrine orm:schema-tool:update --dump-sql`

- Pour mettre à jour le script de mise à jour du schéma orm-schema-update.sql, employer la commande suivante :
  `php bin/doctrine orm:schema-tool:update --dump-sql > resources/schema/orm-schema-update.sql`

- Pour mettre à jour la base de données, exécuter la commande suivante (attention, ne fonctionne pas toujours en fonction de l'impact des modifications à apporter) :
  `php bin/doctrine orm:schema-tool:update --force`

## Modification de l'interface de l'API
Un rechargement du fichier swagger.json est nécessaire après toutes modifications du contrat d'interface de l'API.
Ces modifications doivent être commentées par annotation Open API (OA) en entête de l'index de l'API et/ou des méthodes impactées.

Le rechargement de ce fichier s'effectue par le biais du projet https://github.com/zircote/swagger-php importé en dépendance de développement en lançant la commande suivante à la racine du projet :

```
./vendor/zircote/swagger-php/bin/openapi ./ -o ./public/resources/api/v1/swagger.json --exclude vendor --exclude templates --exclude tests --debug

ou

composer gn:swagger-update
```
