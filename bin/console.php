<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

use Multicanal\App\Helper;
use Multicanal\Console\ClearTemporaryUploadsCommand;
use Multicanal\Console\PublishContentCommand;
use Psr\Container\ContainerInterface;
use Slim\Factory\AppFactory;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Input\InputOption;

require_once __DIR__ . '/../vendor/autoload.php';

$env = (new ArgvInput())->getParameterOption(['--env', '-e'], 'dev');

if ($env) {
    $_ENV['APP_ENV'] = $env;
}

// Bootstrapping settings and main container dependencies definitions
$containerBuilder = require __DIR__ . '/../src/App/bootstrap/bootstrap.php';

// add "Console" specific dependencies definitions in containerBuilder
$containerBuilder->addDefinitions([
    'commands' => [
        ClearTemporaryUploadsCommand::class,
        PublishContentCommand::class
    ],
    Application::class => function (ContainerInterface $container) {
        $application = new Application();
        $application->getDefinition()->addOption(
            new InputOption('--env', '-e', InputOption::VALUE_REQUIRED, 'The Environment name.', 'dev')
        );
        foreach ($container->get('commands') as $class) {
            $application->add($container->get($class));
        }
        return $application;
    }
]);

// Build PHP-DI Container instance
$container = $containerBuilder->build();
Helper::setContainer($container);

try {
    $application = AppFactory::createFromContainer($container);
    $console = $application->getContainer()->get(Application::class);
    exit($console->run());
} catch (Throwable $exception) {
    echo $exception->getMessage();
    exit(1);
}
