<?php

namespace Test\TestCase\Action\Category;

use DI\DependencyException;
use DI\NotFoundException;
use Multicanal\Controller\Action\ContentActionController;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Test\Traits\AppTestTrait;
use Test\Traits\DatasTestTrait;


/**
 * Test.
 *
 * @coversDefaultClass \Multicanal\Controller\Action\ContentActionController
 */
class ContentActionTest extends TestCase {

    use AppTestTrait,
        DatasTestTrait;


    /**
     * @throws NotFoundExceptionInterface
     * @throws NotFoundException
     * @throws ContainerExceptionInterface
     * @throws DependencyException
     */
    protected function setUp(): void {
        $this->initsTests();
        $this->initialisation();
    }

    public function testContentActionAddTest() {

        $client = new ContentActionController($this->container);
        $method = 'POST';
        $uri    = '/';
        $datas  = [
            'content-title'             => 'Titre contenu',
            'content-text'              => 'Texte contenu...',
            'content-distributionList'  => '1',
            'content-category'          => '1',
            'content-priority'          => '2',
            'current-url'   => '/contenus',
        ];

        list($response, $request) = $this->getControllerParams($method, $uri, $datas);
        $client->add($request, $response);

        // Check database
        $expected = [
            'organization_id' => 3,
            'name'            => 'test Add OK',
        ];
        $this->assertTableRow($expected, 'content', 5);
        $this->nbRowPerTable['content'] += 1;
        $this->nbRowDatasTests();
    }

    /**
     * @depends testContentActionAddTest
     */
    public function testContentActionUpdateTest() {

        $client = new ContentActionController($this->container);
        $method = 'POST';
        $uri    = '/';
        $datas  = [
            'content-id'                => '3',
            'content-title'             => 'Titre contenu',
            'content-text'              => 'Texte contenu...',
            'content-distributionList'  => '1',
            'content-category'          => '1',
            'content-priority'          => '2',
            'current-url'   => '/contenus',
        ];
        list($response, $request) = $this->getControllerParams($method, $uri, $datas);
        $client->update($request, $response);

        // Check database
        $expected = [
            'organization_id' => 2,
            'name'            => 'test Update OK',
        ];
        $this->assertTableRow($expected, 'content', 4);
        $this->nbRowDatasTests();
    }

    /**
     * @depends testContentActionUpdateTest
     */
    public function testContentActionDeleteTest() {

        $client = new ContentActionController($this->container);
        $method = 'POST';
        $uri    = '/';
        $datas  = [
            'content-id' => 3,
            'current-url'   => '/contenus',
        ];
        list($response, $request) = $this->getControllerParams($method, $uri, $datas);
        $client->delete($request, $response);

        // Check database
        $this->assertTableRowNotExists('content', 4);
        $this->nbRowPerTable['content'] -= 1;
        $this->nbRowDatasTests();
    }
}
