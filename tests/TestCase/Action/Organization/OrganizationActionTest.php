<?php

namespace Test\TestCase\Action\Organization;

use DI\DependencyException;
use DI\NotFoundException;
use Multicanal\Controller\Action\OrganizationActionController;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Test\Traits\AppTestTrait;
use Test\Traits\DatasTestTrait;


/**
 * Test.
 *
 * @coversDefaultClass \Multicanal\Controller\Action\OrganizationActionController
 */
class OrganizationActionTest extends TestCase {

    use AppTestTrait,
        DatasTestTrait;


    /**
     * @throws NotFoundExceptionInterface
     * @throws NotFoundException
     * @throws ContainerExceptionInterface
     * @throws DependencyException
     */
    protected function setUp(): void {
        $this->initsTests();
        $this->initialisation();
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function testOrganizationActionCountTest() {

        $client = new OrganizationActionController($this->container);
        $method = 'POST';
        $uri    = '/';
        $datas  = [
            'organization-name' => 'groupeonepoint.com',
            'current-url'       => '/administration',
        ];

        list($response, $request) = $this->getControllerParams($method, $uri, $datas);
        $return = $client->count($request, $response, ['organization' => 'multicanal1.test']);

        $this->assertJsonContentType($return);
        $this->assertJsonData(
            [
                'total'            => 1,
                'totalNotFiltered' => 1,
                'rows'             => [
                    0 => [
                        'domain' => 'multicanal1.test',
                        'sms' => 134350,
                        'email' => 765320,
                    ]
                ],
            ],
            $response
        );
        $this->nbRowDatasTests();
    }
}
