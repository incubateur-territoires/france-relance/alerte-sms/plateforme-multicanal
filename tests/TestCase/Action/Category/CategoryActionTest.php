<?php

namespace Test\TestCase\Action\Category;

use DI\DependencyException;
use DI\NotFoundException;
use Multicanal\Controller\Action\CategoryActionController;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Test\Traits\AppTestTrait;
use Test\Traits\DatasTestTrait;


/**
 * Test.
 *
 * @coversDefaultClass \Multicanal\Controller\Action\CategoryActionController
 */
class CategoryActionTest extends TestCase {

    use AppTestTrait,
        DatasTestTrait;


    /**
     * @throws NotFoundExceptionInterface
     * @throws NotFoundException
     * @throws ContainerExceptionInterface
     * @throws DependencyException
     */
    protected function setUp(): void {
        $this->initsTests();
        $this->initialisation();
    }

    public function testCategoryActionAddTest() {

        $client = new CategoryActionController($this->container);
        $method = 'POST';
        $uri    = '/';
        $datas  = [
            'category-name' => 'test Add OK',
            'current-url'   => '/contenus',
        ];

        list($response, $request) = $this->getControllerParams($method, $uri, $datas);
        $client->add($request, $response);

        // Check database
        $expected = [
            'organization_id' => 3,
            'name'            => 'test Add OK',
        ];
        $this->assertTableRow($expected, 'category', 5);
        $this->nbRowPerTable['category'] += 1;
        $this->nbRowDatasTests();
    }

    /**
     * @depends testCategoryActionAddTest
     */
    public function testCategoryActionUpdateTest() {

        $client = new CategoryActionController($this->container);
        $method = 'POST';
        $uri    = '/';
        $datas  = [
            'category-id' => 4,
            'category-name' => 'test Update OK',
            'current-url'   => '/contenus',
        ];
        list($response, $request) = $this->getControllerParams($method, $uri, $datas);
        $client->update($request, $response);

        // Check database
        $expected = [
            'organization_id' => 2,
            'name'            => 'test Update OK',
        ];
        $this->assertTableRow($expected, 'category', 4);
        $this->nbRowDatasTests();
    }

    /**
     * @depends testCategoryActionUpdateTest
     */
    public function testCategoryActionDeleteTest() {

        $client = new CategoryActionController($this->container);
        $method = 'POST';
        $uri    = '/';
        $datas  = [
            'category-id' => 4,
            'current-url'   => '/contenus',
        ];
        list($response, $request) = $this->getControllerParams($method, $uri, $datas);
        $client->delete($request, $response);

        // Check database
        $this->assertTableRowNotExists('category', 4);
        $this->nbRowPerTable['category'] -= 1;
        $this->nbRowDatasTests();
    }
}
