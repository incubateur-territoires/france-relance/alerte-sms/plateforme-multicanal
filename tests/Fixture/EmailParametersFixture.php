<?php

namespace Test\Fixture;

/**
 * Fixture.
 */
class EmailParametersFixture
{
    public string $table = 'email_parameters';

    public array $records = [
        [
            'id' => 1,
            'nominator' => 'user 1',
            'nominator_email' => 'user1@multicanal1.test',
            'reply_to' => 'user reply 1',
            'reply_to_email' => 'user_reply1@multicanal1.test',
        ],[
            'id' => 2,
            'nominator' => 'user 2',
            'nominator_email' => 'user2@multicanal2.test',
            'reply_to' => 'user reply 2',
            'reply_to_email' => 'user_reply2@multicanal2.test',
        ],
    ];
}
