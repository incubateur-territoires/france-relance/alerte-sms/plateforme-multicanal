<?php

namespace Test\Fixture;

/**
 * Fixture.
 */
class EmailSubscriptionFixture
{
    public string $table = 'email_subscription';

    public array $records = [
        [
            'id' => 1,
            'email' => 't.cereyon@groupeonepoint.com',
            'subscription_date' => '2022-10-07 07:55:53',
            'distributionList_id' => 1,
        ],[
            'id' => 2,
            'email' => 'Thierry.cereyon@gmail.com',
            'subscription_date' => '2022-10-07 07:55:53',
            'distributionList_id' => 1,
        ],[
            'id' => 3,
            'email' => 't.cereyon@groupeonepoint.com',
            'subscription_date' => '2022-10-07 07:55:53',
            'distributionList_id' => 2,
        ],[
            'id' => 4,
            'email' => 'Thierry.cereyon@gmail.com',
            'subscription_date' => '2022-10-07 07:55:53',
            'distributionList_id' => 2,
        ]
    ];
}
