<?php

namespace Test\Fixture;

use Doctrine\Persistence\ObjectManager;
use Multicanal\Entity\GeneralParameters;

/**
 * Fixture.
 */
class GeneralParametersFixture
{
    public string $table = 'general_parameters';

    public array $records = [
        [
            'id' => 1,
            'logo_file_id' => 1,
            'domainName' => 'multicanal1.test',
            'label' => 'multicanal test 1',
            'background_color' => '#a2c4c9',
            'gradient_color' => '#2986cc',
            'text_color' => '#16537e',
            'email' => 'user1@multicanal1.test',
            'phone_number' =>'0123456789',
            'web_site_url' => 'http://multicanal1.test',
            'contact_url' => 'http://multicanal1.test/contact',
        ],[
            'id' => 2,
            'logo_file_id' => 2,
            'domainName' => 'multicanal2.test',
            'label' => 'multicanal test 2',
            'background_color' => '#a2c2c9',
            'gradient_color' => '#2982cc',
            'text_color' => '#16237e',
            'email' => 'user2@multicanal2.test',
            'phone_number' =>'0223456789',
            'web_site_url' => 'http://multicanal2.test',
            'contact_url' => 'http://multicanal2.test/contact',
        ],
    ];

}
