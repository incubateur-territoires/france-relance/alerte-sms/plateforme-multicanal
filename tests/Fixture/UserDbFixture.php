<?php

namespace Test\Fixture;

/**
 * Fixture.
 */
class UserDbFixture
{
    public string $table = 'user';

    public array $records = [
        [
            'id' => 1,
            'organization_id' => 1,
            'login' => 'user1@multicanal1.test',
            'email' => 'user1@multicanal1.test',
        ],[
            'id' => 2,
            'organization_id' => 2,
            'login' => 'user2@multicanal2.test',
            'email' => 'user_reply2@multicanal2.test',
        ],
    ];
}
