<?php

namespace Test\Fixture;

/**
 * Fixture.
 */
class CategoryFixture
{
    public string $table = 'category';

    public array $records = [
        [
            'id' => 1,
            'creator_id' => 1,
            'organization_id' => 1,
            'name' => 'Alertes',
            'creation_date' => '2022-10-07 07:55:53',
        ],[
            'id' => 2,
            'creator_id' => 1,
            'organization_id' => 1,
            'name' => 'Ecoles primaires',
            'creation_date' => '2022-10-07 07:55:53',
        ],[
            'id' => 3,
            'creator_id' => 1,
            'organization_id' => 1,
            'name' => 'Informations générales',
            'creation_date' => '2022-10-07 07:55:53',
        ],[
            'id' => 4,
            'creator_id' => 2,
            'organization_id' => 2,
            'name' => 'Ecoles primaires',
            'creation_date' => '2022-10-07 07:55:53',
        ]
    ];
}
