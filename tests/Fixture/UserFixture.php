<?php

namespace Test\Fixture;

/**
 * Fixture.
 */
class UserFixture
{
    public string $table = 'user';

    public array $records = [
        [
            'id' => 1,
            'organization_id' => 1,
            'login' => 'user1@multicanal1.test',
            'email' => 'user1@multicanal1.test',
            'password' => 'password',
            'firstname' => 'Thierry',
            'lastname' => 'CEREYON',
            'rank' => 'Administrateur',
            'applications_array' => ['multicanal'],
            'organization_name' => 'multicanal1.test',
        ],[
            'id' => 2,
            'organization_id' => 2,
            'login' => 'user2@multicanal2.test',
            'email' => 'user_reply2@multicanal2.test',
            'password' => 'password',
            'firstname' => 'Xavier',
            'lastname' => 'MADOIT',
            'rank' => 'Administrateur GN',
            'applications_array' => ['multicanal'],
            'organization_name' => 'multicanal2.test',
        ],
    ];
}
