<?php

namespace Test\Fixture;

/**
 * Fixture.
 */
class MailboxParametersFixture
{
    public string $table = 'email_box_parameters';

    public array $records = [
        [
            'id' => 1,
            'mailbox' => 'imap.gmail.com:993/imap/ssl',
            'folder' => 'INBOX',
            'user' => 'techlead.cereyon@gmail.com',
            'password' => 'dhcmshlxkkegpduf',
        ],
        [
            'id' => 2,
            'mailbox' => 'imap.gmail.com:993/imap/ssl',
            'folder' => 'INBOX',
            'user' => 'techlead.cereyon@gmail.com',
            'password' => 'dhcmshlxkkegpduf',
        ],
    ];
}
