<?php

namespace Test\Fixture;

/**
 * Fixture.
 */
class MulticanalFileFixture
{
    public string $table = 'multicanal_file';

    public array $records = [
        [
            'id' => 1,
            'content_id' => null,
            'name' => 'monfichier63442e0fe221a.png',
            'location' => '/var/www/html/public/upload',
            'description' => null,
            'url' => '/upload/monfichier63442e0fe221a.png',
        ],[
            'id' => 2,
            'content_id' => null,
            'name' => 'alerte63442e0fe221a.png',
            'location' => '/var/www/html/public/upload',
            'description' => null,
            'url' => '/upload/alerte63442e0fe221a.png',
        ],[
            'id' => 3,
            'content_id' => null,
            'name' => 'ecole63442e0fe221a.png',
            'location' => '/var/www/html/public/upload',
            'description' => null,
            'url' => '/upload/ecole63442e0fe221a.png',
        ],[
            'id' => 4,
            'content_id' => null,
            'name' => 'informations63442e0fe221a.png',
            'location' => '/var/www/html/public/upload',
            'description' => null,
            'url' => '/upload/informations63442e0fe221a.png',
        ],[
            'id' => 5,
            'content_id' => 1,
            'name' => 'illustration63442e0fe221a.png',
            'location' => '/var/www/html/public/upload',
            'description' => null,
            'url' => '/upload/illustration63442e0fe221a.png',
        ]
    ];
}
