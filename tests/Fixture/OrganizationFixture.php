<?php

namespace Test\Fixture;

use Doctrine\Bundle\FixturesBundle\Fixture;

/**
 * Fixture.
 */
class OrganizationFixture {
    public string $table = 'organization';

    public array $records = [
        [
            'general_parameters_id' => 1,
            'email_parameters_id' => 1,
            'social_network_parameters_id' => 1,
            'mailbox_parameters_id' => 1,
            'domain' => 'multicanal1.test',
        ], [
            'general_parameters_id' => 2,
            'email_parameters_id' => 2,
            'social_network_parameters_id' => 2,
            'mailbox_parameters_id' => 2,
            'domain' => 'multicanal2.test',
        ],
    ];
}
