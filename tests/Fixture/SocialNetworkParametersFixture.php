<?php

namespace Test\Fixture;

/**
 * Fixture.
 */
class SocialNetworkParametersFixture
{
    public string $table = 'social_network_parameters';

    public array $records = [
        [
            'id' => 1,
            'facebook_url' => 'https://www.facebook.com/1',
            'twitter_url' => 'https://twitter.com/1',
            'instagram_url' => 'https://instagram.com/1',
        ],[
            'id' => 2,
            'facebook_url' => 'https://www.facebook.com/2',
            'twitter_url' => 'https://twitter.com/2',
            'instagram_url' => 'https://instagram.com/2',
        ],
    ];
}
