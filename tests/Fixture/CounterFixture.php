<?php

namespace Test\Fixture;

use Doctrine\Persistence\ObjectManager;
use Multicanal\Entity\Counter;

/**
 * Fixture.
 */
class CounterFixture
{
    public string $table = 'counter';

    public array $records = [
        [
            'year' => 2021,
            'organization_id' => 1,
            'nb_of_sms' => 5645436540,
            'nb_of_email' => 87643256570,
        ],[
            'year' => 2022,
            'organization_id' => 1,
            'nb_of_sms' => 134350,
            'nb_of_email' => 765320,
        ],[
            'year' => 2021,
            'organization_id' => 2,
            'nb_of_sms' => 526656540,
            'nb_of_email' => 97657543680,
        ],[
            'year' => 2022,
            'organization_id' => 2,
            'nb_of_sms' => 668236540,
            'nb_of_email' => 8786546540,
        ],
    ];

}
