<?php

namespace Test\Fixture;

/**
 * Fixture.
 */
class ContentFixture
{
    public string $table = 'content';

    public array $records = [
        [
            'id' => 1,
            'illustration_file_id' => 4,
            'creator_id' => 1,
            'category_id' => 1,
            'title' => 'Et prima post',
            'text' => 'Principium autem unde latius se funditabat,
            emersit ex negotio tali. <br />
            Chilo ex vicario et coniux eius Maxima nomine, questi apud Olybrium ea <br />
            tempestate urbi praefectum, vitamque suam venenis petitam adseverantes inpetrarunt ut hi, <br />
            quos suspectati sunt, ilico rapti conpingerentur in vincula, organarius Sericus et Asbolius palaestrita et aruspex Campensis.',
            'short_url' => 'https://getReduceUrl',
            'creation_date' => '2022-10-07 07:55:53',
            'update_date' => '2022-10-08 07:55:53',
            'status' => 'TO_PUBLISH',
            'priority' => 1,
            'deadline' => '2022-10-15 07:55:53',
            'distributionList_id' => 1,
        ],[
            'id' => 2,
            'illustration_file_id' => null,
            'creator_id' => 1,
            'category_id' => 2,
            'title' => 'Soleo saepe ante',
            'text' => 'Ego vero sic intellego, Patres conscripti, nos hoc
                        tempore in provinciis decernendis perpetuae pacis habere oportere rationem.
                        Nam quis hoc non sentit omnia alia esse
                        nobis vacua ab omni periculo atque etiam suspicione belli?.',
            'short_url' => 'https://getReduceUrl',
            'creation_date' => '2022-10-07 07:55:53',
            'update_date' => '2022-10-08 07:55:53',
            'status' => 'PUBLISHED',
            'priority' => 1,
            'deadline' => '2022-10-15 07:55:53',
            'distributionList_id' => 2,
        ]
    ];
}
