<?php

namespace Test\Fixture;

/**
 * Fixture.
 */
class CommunicationFixture
{
    public string $table = 'communication';

    public array $records = [
        [
            'id' => 1,
            'publisher_id' => 1,
            'content_id' => 1,
            'object' => 'Duplexque isdem',
            'text' => 'Alertes',
            'sender_mail' => 't.cereyon@groupeonepoint.com',
            'sent' => 0,
            'channel' => 'SMS',
            'schedule_date' => '2022-10-07 07:55:53',
            'sending_date' => '2022-10-07 07:55:53',
        ],[
            'id' => 2,
            'publisher_id' => 1,
            'content_id' => 1,
            'object' => 'Duplexque isdem',
            'text' => 'Quanta autem vis amicitiae sit, ex hoc intellegi maxime potest, quod ex infinita societate gene.',
            'sender_mail' => 't.cereyon@groupeonepoint.com',
            'sent' => 1,
            'channel' => 'SMS',
            'schedule_date' => '2022-10-07 07:55:53',
            'sending_date' => '2022-10-07 07:55:53',
        ],[
            'id' => 3,
            'publisher_id' => 1,
            'content_id' => 2,
            'object' => 'Nunc vero inanes',
            'text' => 'Post quorum necem nihilo lenius ferociens Gallus ut leo cadaveribus pastus multa.',
            'sender_mail' => null,
            'sent' => 1,
            'channel' => 'SMS',
            'schedule_date' => '2022-10-07 07:55:53',
            'sending_date' => null,
        ],[
            'id' => 4,
            'publisher_id' => 1,
            'content_id' => 2,
            'object' => 'Nunc vero inanes',
            'text' => 'Post quorum necem nihilo lenius ferociens Gallus ut leo cadaveribus pastus multa.',
            'sender_mail' => null,
            'sent' => 1,
            'channel' => 'EMAIL',
            'schedule_date' => '2022-10-07 07:55:53',
            'sending_date' => null,
        ],
    ];
}
