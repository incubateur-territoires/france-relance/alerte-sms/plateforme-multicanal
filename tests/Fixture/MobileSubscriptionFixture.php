<?php

namespace Test\Fixture;

/**
 * Fixture.
 */
class MobileSubscriptionFixture
{
    public string $table = 'mobile_subscription';

    public array $records = [
        [
            'id' => 1,
            'mobile' => '0783364463',
            'subscription_date' => '2022-10-07 07:55:53',
            'distributionList_id' => 1,
        ],[
            'id' => 2,
            'mobile' => '00000000000',
            'subscription_date' => '2022-10-07 07:55:53',
            'distributionList_id' => 1,
        ],[
            'id' => 3,
            'mobile' => '0783364463',
            'subscription_date' => '2022-10-07 07:55:53',
            'distributionList_id' => 2,
        ],[
            'id' => 4,
            'mobile' => '00000000000',
            'subscription_date' => '2022-10-07 07:55:53',
            'distributionList_id' => 2,
        ]
    ];
}
