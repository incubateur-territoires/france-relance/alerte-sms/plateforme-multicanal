<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

declare(strict_types=1);

use Multicanal\Middleware\EntryMiddleware;
use Slim\App;
use Slim\Views\TwigMiddleware;

return function (App $app) {

    // Add Routing Middleware
    //@see https://discourse.slimframework.com/t/about-addroutingmiddleware-routing-middleware-slim-4/3957/4
    $app->addRoutingMiddleware();

    $app->add(TwigMiddleware::createFromContainer($app));
    $app->add(new EntryMiddleware($app->getContainer()));
};
