<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

declare(strict_types=1);

use DI\ContainerBuilder;

/**
 * Bootstrap to Instantiate PHP-DI ContainerBuilder.
 *
 * @package Multicanal
 * @author  William BLANC DIT JOLICOEUR <w.blanc-dit-jolicoeur@girondenumerique.fr>
 */

$containerBuilder = new ContainerBuilder();

// Set up settings
$settings = require __DIR__ . '/settings.php';
$settings($containerBuilder);

// Set up dependencies
$dependencies = require __DIR__ . '/dependencies.php';
$dependencies($containerBuilder);


/**
 * Don't return the "built" container, but containerBuilder
 * because we may need to add some specific definitions (for bin/console for example )
 */
return $containerBuilder;
