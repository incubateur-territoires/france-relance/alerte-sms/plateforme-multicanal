<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

declare(strict_types=1);

use DI\ContainerBuilder;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMSetup;
use Jumbojett\OpenIDConnectClient;
use Multicanal\App\Settings\SettingsInterface;
use Odan\Session\PhpSession;
use Odan\Session\SessionInterface;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Csrf\Guard;
use Slim\Exception\HttpBadRequestException;
use Slim\Flash\Messages;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\Views\Twig;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Twig\Extension\DebugExtension;
use Twig\Extra\String\StringExtension;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        SessionInterface::class => DI\autowire(PhpSession::class),
        'oidc' => function (SettingsInterface $settings) {
            $oidc = new OpenIDConnectClient(
                $settings->get('oidc.provider_url'),
                $settings->get('oidc.client_id'),
                $settings->get('oidc.client_secret')
            );
            $oidc->addScope($settings->get('oidc.scopes'));
            $oidc->setVerifyHost($settings->get('oidc.ssl_verify'));
            $oidc->setVerifyPeer($settings->get('oidc.ssl_verify'));
            return $oidc;
        },
        ResponseFactoryInterface::class => DI\create(ResponseFactory::class),
        'csrf' => DI\autowire(Guard::class)
            ->method('setPersistentTokenMode', true)
            ->method('setFailureHandler', DI\value(function () {
                return function (Request $request) {
                    throw new HttpBadRequestException($request);
                };
            })),
        'view' => function (SettingsInterface $settings) {
            if (!$settings->get('env.development')) {
                $view = Twig::create(
                    $settings->get('twig.templates_path'),
                    ['cache' => $settings->get('twig.cache_path')]
                );
            } else {
                $view = Twig::create($settings->get('twig.templates_path'), ['debug' => $settings->get('env.debug')]);
                $view->addExtension(new DebugExtension());
            }
            $view->addExtension(new StringExtension());
            $view->getEnvironment()->addGlobal('domain', $settings->get('app.domain'));
            $view->getEnvironment()->addGlobal('TRUNCATE_DEFAULT_LENGTH', $settings->get('app.truncate_default_length'));
            return $view;
        },
        PHPMailer::class => function (SettingsInterface $settings) {
            $mailer = new PHPMailer($settings->get('mail.throw_external_exception'));
            /* Server settings
            Warning, with debug enabled data is outputed and breaks redirect response */
            $mailer->CharSet    =  $mailer::CHARSET_UTF8;
            $mailer->SMTPDebug  = $settings->get('mail.debug') ? SMTP::DEBUG_SERVER : SMTP::DEBUG_OFF;
            $mailer->isSMTP();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            //Send using SMTP
            $mailer->Host       = $settings->get('mail.smtp_host'); // Set the SMTP server to send through
            $mailer->SMTPAuth   = $settings->get('mail.smtp_auth'); // Enable SMTP authentication
            $mailer->Username   = $settings->get('mail.usermail');  // SMTP username
            $mailer->Password   = $settings->get('mail.password');  // SMTP password
            $mailer->SMTPSecure = $settings->get('mail.smtp_secure'); // is '', 'tls' or 'ssl ?
            $mailer->Port       = $settings->get('mail.smtp_port'); // TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

            // Recipients
            $mailer->setFrom($settings->get('mail.from_email'), $settings->get('mail.from_name'));

            return $mailer;
        },
        EntityManager::class => function (SettingsInterface $settings): EntityManager {
            $doctrine = $settings->get('doctrine');
            $cache = $doctrine['dev_mode'] ?
                new ArrayAdapter() :
                new FilesystemAdapter(directory: $doctrine['cache_dir']);
            /*
            Type::addType('enum_content_status', 'Multicanal\Entity\Enum\ContentStatusType');
            Type::addType('enum_channel_type', 'Multicanal\Entity\Enum\ChannelType');
*/
            $config = ORMSetup::createAttributeMetadataConfiguration(
                $doctrine['metadata_dirs'],
                $doctrine['dev_mode'],
                $doctrine['proxies_dir'],
                $cache
            );

            $em   = EntityManager::create($doctrine['connection'], $config);
            $conn = $em->getConnection();
            $conn->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
            return $em;
        },
        'flash' => DI\create(Messages::class),
        'user' => function () {
            return null;
        },
        'upload_directory' => function (SettingsInterface $settings) {
            return $settings->get('app.public_dir') . '/upload';
        },
        'upload_directory_tmp' => function (SettingsInterface $settings) {
            return $settings->get('app.root_dir') . '/upload_tmp';
        },
        'Multicanal\Lib\*' => DI\autowire(),
        'Multicanal\Repository\*' => DI\autowire(),
    ]);
};
