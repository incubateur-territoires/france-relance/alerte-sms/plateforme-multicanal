# Tests de l'Interface de gestion des alertes SMS et administration (Alertes SMS)

## Desescription Tests des CRUDS et API Multicanal


## Prérequis
- PHP > 8.1

# Mettre à jour le schéma
Se placer à la racine du projet et exécuter la commande :
- `php vendor/bin/doctrine orm:schema-tool:update --dump-sql > resources/schema/orm-schema-create.sql`

## Commands
- Se placer à la racine du projet
- Exécuter la commande ` vendor/bin/phpunit `

Pour lancer un test unitaire :
``vendor/bin/phpunit tests/TestCase/Action/Category/CategoryAddActionTest.php``

