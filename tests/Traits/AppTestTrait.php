<?php

namespace Test\Traits;

use DI\DependencyException;
use DI\NotFoundException;
use Doctrine\ORM\EntityManager;
use Multicanal\App\Helper;
use Multicanal\App\Settings\SettingsInterface;
use Multicanal\Controller\View\ErrorViewController;
use Multicanal\Entity\Enum\Rank;
use Multicanal\Entity\User;
use Multicanal\Lib\UserDAO;
use Multicanal\Repository\OrganizationRepository;
use Multicanal\Repository\UserRepository;
use Nyholm\Psr7\Factory\Psr17Factory;
use Odan\Session\PhpSession;
use Odan\Session\SessionInterface;
use PDO;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ServerRequestFactoryInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\UploadedFileInterface;
use Selective\TestTrait\Traits\ArrayTestTrait;
use Selective\TestTrait\Traits\ContainerTestTrait;
use Selective\TestTrait\Traits\DatabaseTestTrait;
use Selective\TestTrait\Traits\HttpJsonTestTrait;
use Selective\TestTrait\Traits\HttpTestTrait;
use Selective\TestTrait\Traits\MockTestTrait;
use Slim\App;
use Slim\Csrf\Guard;
use Slim\Factory\AppFactory;
use Slim\Psr7\Factory\StreamFactory;
use Symfony\Component\BrowserKit\HttpBrowser;
use Symfony\Component\HttpClient\HttpClient;
use Throwable;

/**
 * App Test Trait.
 */
trait AppTestTrait {
    use ArrayTestTrait,
        ContainerTestTrait,
        HttpTestTrait,
        HttpJsonTestTrait,
        MockTestTrait,
        HttpJsonTestTrait,
        FixturesTestTrait,
        DatabaseTestTrait;

    /** @var App */
    protected App $app;

    /** @var Guard */
    protected Guard $csrf;

    protected UserDAO $dao;

    protected PhpSession             $session;
    protected OrganizationRepository $organizationRepository;

    protected HttpBrowser $browser;

    /**
     * @return void
     * @throws DependencyException
     * @throws NotFoundException
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function initialisation(): void {
        $containerBuilder = require __DIR__ . '/../bootstrap/bootstrap.php';

        $containerBuilder->addDefinitions([
            PDO::class                           => function (ContainerInterface $container) {
                $host     = $_SERVER['DOCTRINE_HOST'];
                $dbname   = $_SERVER['DOCTRINE_DBNAME'];
                $username = $_SERVER['DOCTRINE_USER'];
                $password = $_SERVER['DOCTRINE_PASSWORD'];
                $charset  = 'utf8mb4';
                $flags    = [
                    // Turn off persistent connections
                    PDO::ATTR_PERSISTENT         => false,
                    // Enable exceptions
                    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                    // Emulate prepared statements
                    PDO::ATTR_EMULATE_PREPARES   => true,
                    // Set default fetch mode to array
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    // Set character set
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8mb4 COLLATE utf8mb4_unicode_ci'
                ];
                $dsn      = "mysql:host=$host;dbname=$dbname;charset=$charset";

                return new PDO($dsn, $username, $password, $flags);
            },
            // HTTP factories
            ResponseFactoryInterface::class      => function (ContainerInterface $container) {
                return $container->get(Psr17Factory::class);
            },
            // HTTP factories
            StreamFactory::class      => function (ContainerInterface $container) {
                return $container->get(Psr17Factory::class);
            },
            ServerRequestFactoryInterface::class => function (ContainerInterface $container) {
                return $container->get(Psr17Factory::class);
            },
            UploadedFileInterface::class => function (ContainerInterface $container) {
                return $container->get(Psr17Factory::class);
            },
            'user' => function (ContainerInterface $container) {
                $session        = $container->get(SessionInterface::class);
                $userRepository = $container->get(UserRepository::class);
                $entityManager  = $container->get(EntityManager::class);
                $organizationRepository = $container->get(OrganizationRepository::class);
                $user           = new User('t.cereyon@phpunit.com');
                $user->setFirstname('cereyon');
                $user->setLastname('password');
                $user->setEmail("phpunit@phpunit.com");
                $user->setRank(Rank::ADMIN());
                $user->setRank(Rank::AGENT());
                $user->setRank(Rank::ADMINGN());
                $user->setDescription('super pouvoir');
                $user->setOrganizationName("phpunit.com");
                $dbUser = $userRepository->findByEmail($user->getEmail());
                if (is_null($dbUser)) {
                    $organization = $organizationRepository->createIfNotExist($user->getOrganizationName());
                    $user->setOrganization($organization);
                    $entityManager->persist($user);
                    $entityManager->flush();
                }
                $session->set('user', \serialize($user));
                $user = unserialize($session->get('user'));
                return $user;
            },
        ]);
        $container = $containerBuilder->build();

        //sets the container in a static accessible Helper class,may be useful to access settings from non DI stuff
        Helper::setContainer($container);

        $app = AppFactory::createFromContainer($container);

        $routes = require __DIR__ . '/../bootstrap/routes.php';
        $routes($app);

        // Define Custom Error Handler
        $errorHandler = function (Request $request, Throwable $exception) use ($app) {
            // For GUI application, display custom error page
            if (strpos($request->getUri()->getPath(), '/api') !== 0) {
                $request = $request->withAttribute('exception', $exception);
                return $app->get($request->getUri()->getPath(), ErrorViewController::class)->run($request);
            } else {
                // For API, return error as JSON
                $response = $app->getResponseFactory()->createResponse();
                $response->withHeader('Content-type', 'application/json');
                $response = $response->withStatus($exception->getCode());
                $response->getBody()->write(
                    json_encode([
                        'success' => false,
                        'message' => $exception->getMessage()
                    ], JSON_UNESCAPED_UNICODE)
                );
                return $response;
            }
        };

        $settings = $container->get(SettingsInterface::class);

        $errorMiddleware = $app->addErrorMiddleware($settings->get('env.development'), $settings->get('env.logging'), $settings->get('env.debug'));
        if (!$settings->get('env.development')) {
            $errorMiddleware->setDefaultErrorHandler($errorHandler);
        }
        $this->app = $app;
        $this->setUpContainer($container);
        $this->container = $container;
        $this->truncateTables();

        $pdo = $this->getConnection();
        $pdo->exec('SET unique_checks=0; SET foreign_key_checks=0;');
        $this->insertMulticanalTestFixtures();
        $pdo->exec('SET unique_checks=1; SET foreign_key_checks=1;');
        //
        $container->get('user');


        $this->assertTableRowValue('phpunit.com', 'organization', 3, 'domain');
        $this->nbRowPerTable['user'] += 1;
        $this->nbRowPerTable['organization'] += 1;
        $this->nbRowPerTable['general_parameters'] += 1;
        $this->nbRowPerTable['email_parameters'] += 1;
        $this->nbRowPerTable['email_box_parameters'] += 1;
        $this->nbRowPerTable['social_network_parameters'] += 1;
        $this->nbRowDatasTests();
    }


    protected function initHttpBrowser() {
        $this->browser    = new HttpBrowser(HttpClient::create());
        $crawler          = $this->browser->request('GET', '/connexion');
        $form             = $crawler->filter('.needs-validation')->form();
        $form['login']    = 'browser@browser.com';
        $form['password'] = 'password';
        $this->browser->submit($form);
        $this->nbRowPerTable['user'] += 1;
        $this->nbRowPerTable['organization'] += 1;
        $this->nbRowPerTable['general_parameters'] += 1;
        $this->nbRowPerTable['email_parameters'] += 1;
        $this->nbRowPerTable['email_box_parameters'] += 1;
        $this->nbRowPerTable['social_network_parameters'] += 1;
    }
    /**
     * @param string $method
     * @param string $uri
     * @param array $datas
     * @return array
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function getControllerParams(string $method, string $uri, array $datas): array {
        $response = $this->container->get(ResponseFactoryInterface::class);
        $response = $response->createResponse();

        $request = $this->createRequest($method, $uri, $datas);
        $request = $request->withParsedBody($datas);
        return array($response, $request);
    }
}
