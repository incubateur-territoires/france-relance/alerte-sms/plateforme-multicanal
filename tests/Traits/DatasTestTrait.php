<?php

namespace Test\Traits;


use Test\Fixture\CategoryFixture;
use Test\Fixture\CommunicationFixture;
use Test\Fixture\ContentFixture;
use Test\Fixture\CounterFixture;
use Test\Fixture\DistributionListFixture;
use Test\Fixture\EmailParametersFixture;
use Test\Fixture\EmailSubscriptionFixture;
use Test\Fixture\GeneralParametersFixture;
use Test\Fixture\MailboxParametersFixture;
use Test\Fixture\MobileSubscriptionFixture;
use Test\Fixture\MulticanalFileFixture;
use Test\Fixture\OrganizationFixture;
use Test\Fixture\SocialNetworkParametersFixture;
use Test\Fixture\UserDbFixture;

trait DatasTestTrait {

    public array $nbRowPerTable = [];
    private      $datas         = [
        OrganizationFixture::class,
        GeneralParametersFixture::class,
        EmailParametersFixture::class,
        MailboxParametersFixture::class,
        SocialNetworkParametersFixture::class,
        CounterFixture::class,
        CategoryFixture::class,
        DistributionListFixture::class,
        EmailSubscriptionFixture::class,
        MobileSubscriptionFixture::class,
        ContentFixture::class,
        MulticanalFileFixture::class,
        CommunicationFixture::class,
        UserDbFixture::class,
    ];
    protected function initsTests(): void {
        foreach ($this->datas as $fixtures) {
            $fixture = new $fixtures;
            $this->nbRowPerTable[$fixture->table] = count($fixture->records);

        }

    }
    /**
     * @return void
     */
    protected function datasTests(): void {
        foreach ($this->datas as $fixtures) {
            $fixture = new $fixtures;
            $records = $fixture->records;
            $table   = $fixture->table;
            foreach ($records as $id => $record) {
                $this->assertTableRow($record, $table, $id + 1);
            }
        }
    }

    /**
     * @return void
     */
    protected function nbRowDatasTests(): void {
        foreach ($this->nbRowPerTable as $table => $nbRows) {
            $this->assertTableRowCount($nbRows, $table);
        }
    }
}
