<?php

namespace Test\Traits;

use Test\Fixture\CategoryFixture;
use Test\Fixture\CommunicationFixture;
use Test\Fixture\ContentFixture;
use Test\Fixture\CounterFixture;
use Test\Fixture\DistributionListFixture;
use Test\Fixture\EmailParametersFixture;
use Test\Fixture\EmailSubscriptionFixture;
use Test\Fixture\GeneralParametersFixture;
use Test\Fixture\MailboxParametersFixture;
use Test\Fixture\MobileSubscriptionFixture;
use Test\Fixture\MulticanalFileFixture;
use Test\Fixture\OrganizationFixture;
use Test\Fixture\SocialNetworkParametersFixture;
use Test\Fixture\UserDbFixture;

trait FixturesTestTrait {
    /**
     * @return void
     */
    protected function insertMulticanalTestFixtures(): void {
        $this->insertFixtures([
                                  OrganizationFixture::class,
                                  GeneralParametersFixture::class,
                                  EmailParametersFixture::class,
                                  MailboxParametersFixture::class,
                                  SocialNetworkParametersFixture::class,
                                  CounterFixture::class,
                                  CategoryFixture::class,
                                  DistributionListFixture::class,
                                  EmailSubscriptionFixture::class,
                                  MobileSubscriptionFixture::class,
                                  ContentFixture::class,
                                  MulticanalFileFixture::class,
                                  CommunicationFixture::class,
                                  UserDbFixture::class,
                              ]);
    }

}
