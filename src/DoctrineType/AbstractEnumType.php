<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\DoctrineType;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

/**
 * Abstract class to manipulate enum in doctrine context
 *
 * @package Multicanal\DoctrineType
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
abstract class AbstractEnumType extends Type {

    abstract public static function getEnumsClass(): string;

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform) {
        return 'TEXT';
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform) {
        if ($value instanceof \BackedEnum) {
            return $value->value;
        }
        return null;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform) {
        if (false === enum_exists($this->getEnumsClass(), true)) {
            throw new \LogicException("This class should be an enum");
        }
        // 🔥 https://www.php.net/manual/en/backedenum.tryfrom.php
        return $this::getEnumsClass()::tryFrom($value);
    }
}
