<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Factory;

use Nyholm\Psr7\Factory\Psr17Factory;
use PDO;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ServerRequestFactoryInterface;

final class ContainerFactory {
    /**
     * Create a new container instance.
     *
     * @return ContainerInterface The container
     */
    public static function createInstance(): ContainerInterface {
        // Set up settings
        $containerBuilder = require_once __DIR__ . '/../../src/App/bootstrap/bootstrap.php';
        $containerBuilder->addDefinitions([
            PDO::class => function () {
                $host = $_SERVER['DOCTRINE_HOST'];
                $dbname = 'tests';
                $username = $_SERVER['DOCTRINE_USER'];
                $password = $_SERVER['DOCTRINE_PASSWORD'];
                $charset = 'utf8mb4';
                $flags =  [
                    // Turn off persistent connections
                    PDO::ATTR_PERSISTENT => false,
                    // Enable exceptions
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    // Emulate prepared statements
                    PDO::ATTR_EMULATE_PREPARES => true,
                    // Set default fetch mode to array
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    // Set character set
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8mb4 COLLATE utf8mb4_unicode_ci'
                ];
                $dsn = "mysql:host=$host;dbname=$dbname;charset=$charset";

                return new PDO($dsn, $username, $password, $flags);
            },
            // HTTP factories
            ResponseFactoryInterface::class => function (ContainerInterface $container) {
                return $container->get(Psr17Factory::class);
            },
            ServerRequestFactoryInterface::class => function (ContainerInterface $container) {
                return $container->get(Psr17Factory::class);
            },
        ]);
        // Build PHP-DI Container instance
        return $containerBuilder->build();
    }
}
