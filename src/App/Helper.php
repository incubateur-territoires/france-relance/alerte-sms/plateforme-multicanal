<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

declare(strict_types=1);

namespace Multicanal\App;

use Multicanal\App\Settings\SettingsInterface;
use Multicanal\Entity\Exception\MulticanalException;
use Multicanal\Utils\BytesUtils;
use PHPMailer\PHPMailer\PHPMailer;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface;

class Helper {
    protected static ?ContainerInterface $container;

    public function __construct(ContainerInterface $container) {
        Helper::setContainer($container);
    }

    /**
     * Sets a new global container.
     *
     * @param ContainerInterface $container
     *   A new container instance to replace the current.
     */
    public static function setContainer(ContainerInterface $container): void {
        static::$container = $container;
    }

    /**
     * Unsets the global container.
     */
    public static function unsetContainer(): void {
        static::$container = null;
    }

    /**
     * Returns the currently active global container.
     *
     * @return ContainerInterface
     * @throws \Exception
     * @access public
     */
    public static function getContainer() {
        if (static::$container === null) {
            throw new MulticanalException('\GN::$container is not initialized yet. \GN::setContainer() must be called with a real container.');
        }
        return static::$container;
    }

    /**
     * Returns TRUE if the container has been initialized, FALSE otherwise.
     *
     * @return bool
     */
    public static function hasContainer() {
        return static::$container !== null;
    }

    /**
     * Retrieves a service from the container.
     *
     * Use this method if the desired service is not one of those with a dedicated
     * accessor method below. If it is listed below, those methods are preferred
     * as they can return useful type hints.
     *
     * @param string $id
     *   The ID of the service to retrieve.
     *
     * @return mixed
     *   The specified service.
     */
    public static function service($id) {
        return static::getContainer()->get($id);
    }

    public static function config($name) {
        return static::getContainer()->get(SettingsInterface::class)->get($name);
    }

    public static function getAppDomain() {
        return static::config('app.domain');
    }

    public static function isDebugEnabled() {
        return static::config('env.debug');
    }

    public static function getMailer() {
        return static::service(PHPMailer::class);
    }

    public static function getUploadDirectory() {
        return static::config('app.public_dir') . '/upload';
    }

    public static function getUploadDirectoryTmp() {
        return static::config('app.root_dir') . '/upload_tmp';
    }

    /**
     * Determines the maximum file upload size by querying the PHP settings.
     *
     * @return int
     *   A file size limit in bytes based on the PHP upload_max_filesize and
     *   post_max_size settings.
     */
    public static function getUploadMaxSize() {
        static $maxSize = -1;

        if ($maxSize < 0) {
            // Start with post_max_size.
            $maxSize = BytesUtils::toNumber(ini_get('post_max_size'));

            // If upload_max_size is less, then reduce. Except if upload_max_size is
            // zero, which indicates no limit.
            $uploadMax = BytesUtils::toNumber(ini_get('upload_max_filesize'));
            if ($uploadMax > 0 && $uploadMax < $maxSize) {
                $maxSize = $uploadMax;
            }
        }
        return $maxSize;
    }

    public static function isApiRouteRequest(ServerRequestInterface $request): bool {
        return (bool) preg_match('/^\/api\/v\d+/', $request->getUri()->getPath());
    }
}
