<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

declare(strict_types=1);

use Multicanal\App\Settings\SettingsInterface;
use Multicanal\Middleware\EntryMiddleware;
use RKA\Middleware\ProxyDetection;
use Slim\App;
use Slim\Views\TwigMiddleware;

return function (App $app) {
    $app->add(TwigMiddleware::createFromContainer($app));
    // Add Routing Middleware
    //@see https://discourse.slimframework.com/t/about-addroutingmiddleware-routing-middleware-slim-4/3957/4
    $app->addRoutingMiddleware();
    $app->add(EntryMiddleware::class);
    $trustedProxies = $app->getContainer()->get(SettingsInterface::class)->get('app.trusted_proxies');
    $app->add(new ProxyDetection($trustedProxies));
};
