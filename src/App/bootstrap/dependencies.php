<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

declare(strict_types=1);

use DI\ContainerBuilder;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMSetup;
use IvoValchev\TruncateHtmlExtension\TruncateHtmlExtension;
use Jumbojett\OpenIDConnectClient;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Monolog\Processor\PsrLogMessageProcessor;
use Multicanal\App\Helper;
use Multicanal\App\Settings\SettingsInterface;
use Multicanal\Lib\SessionUser;
use Odan\Session\PhpSession;
use Odan\Session\SessionInterface;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Log\LoggerInterface;
use Slim\App;
use Slim\Csrf\Guard;
use Slim\Exception\HttpBadRequestException;
use Slim\Factory\AppFactory;
use Slim\Flash\Messages;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\Views\Twig;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Twig\Extension\DebugExtension;
use Twig\Extra\String\StringExtension;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        App::class => DI\Factory([AppFactory::class, 'createFromContainer']),
        Helper::class => DI\autowire(),
        SessionInterface::class => DI\factory(function (SettingsInterface $settings) {
            $session = new PhpSession();
            $session->setOptions($settings->get('session'));
            return $session;
        }),

        /**
         * Middlewares with autowireable args are automatically autowired when using `->add(AutoWireableArgsMiddleware::class)`,
         * no need to explicitly defined it for example
         * //SessionMiddleware::class => DI\autowire(),
         * //EntryMiddleware::class => DI\autowire(),
         */
        LoggerInterface::class => DI\factory(function (SettingsInterface $settings) {
            $logger = new Logger($settings->get('logger.name'));

            $filename = sprintf('%s/%s.log', $settings->get('logger.path'), $settings->get('logger.name'));
            $rotatingFileHandler = new RotatingFileHandler($filename, 0, $settings->get('logger.level'), true, 0777);

            // The last "true" here tells monolog to remove empty []'s
            $rotatingFileHandler->setFormatter(new LineFormatter(null, null, false, true));

            $logger->pushHandler($rotatingFileHandler);

            // Processes a log record's message according to PSR-3 rules, replacing {foo} with the value from $context['foo']
            $psr3LogProcessor = new PsrLogMessageProcessor(null, true);
            $logger->pushProcessor($psr3LogProcessor);

            return $logger;
        }),
        OpenIDConnectClient::class => function (SettingsInterface $settings) {
            $oidc = new OpenIDConnectClient(
                $settings->get('oidc.provider_url'),
                $settings->get('oidc.client_id'),
                $settings->get('oidc.client_secret')
            );
            $oidc->addScope($settings->get('oidc.scopes'));
            $oidc->setVerifyHost($settings->get('oidc.ssl_verify'));
            $oidc->setVerifyPeer($settings->get('oidc.ssl_verify'));
            return $oidc;
        },

        ResponseFactoryInterface::class => DI\create(ResponseFactory::class),
        'csrf' => function (ResponseFactoryInterface $responseFactory) {

            $csrfStorage = null;
            $guard = new Guard($responseFactory, 'csrf', $csrfStorage);
            $guard->setPersistentTokenMode(true);
            $guard->setFailureHandler(
                function (Request $request) {
                    throw new HttpBadRequestException($request);
                }
            );
            return $guard;
        },
        'view' => function (SettingsInterface $settings, Helper $helper) {
            if (!$settings->get('env.development')) {
                $view = Twig::create(
                    $settings->get('twig.templates_path'),
                    ['cache' => $settings->get('twig.cache_path')]
                );
            } else {
                $view = Twig::create(
                    $settings->get('twig.templates_path'),
                    ['debug' => $settings->get('env.debug')]
                );
                $view->addExtension(new DebugExtension());
            }
            $view->addExtension(new StringExtension());
            $view->addExtension(new \Multicanal\Lib\TwigExtension());
            $view->addExtension(new TruncateHtmlExtension());
            $view->getEnvironment()->addGlobal('helper', $helper);
            $view->getEnvironment()->addGlobal('domain', $settings->get('app.domain'));
            $view->getEnvironment()->addGlobal('TRUNCATE_DEFAULT_LENGTH', $settings->get('app.truncate_default_length'));

            return $view;
        },
        Messages::class => function () {
            $flashMessagesStorage = null;
            return new Messages($flashMessagesStorage);
        },

        PHPMailer::class => function (SettingsInterface $settings) {
            $mailer = new PHPMailer($settings->get('mail.throw_external_exception'));
            /*
             * Server settings
             * Warning, with debug enabled data is outputed and breaks redirect response
             */
            $mailer->CharSet    =  $mailer::CHARSET_UTF8;
            $mailer->SMTPDebug  = $settings->get('mail.debug') ? SMTP::DEBUG_SERVER : SMTP::DEBUG_OFF;
            $mailer->isSMTP();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            //Send using SMTP
            $mailer->Host       = $settings->get('mail.smtp_host');         // Set the SMTP server to send through
            $mailer->SMTPAuth   = $settings->get('mail.smtp_auth');         // Enable SMTP authentication
            $mailer->Username   = $settings->get('mail.usermail');          // SMTP username
            $mailer->Password   = $settings->get('mail.password');          // SMTP password
            $mailer->SMTPSecure = $settings->get('mail.smtp_secure');       // is '', 'tls' or 'ssl ?
            $mailer->Port       = $settings->get('mail.smtp_port');         // TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

            // Recipients
            $mailer->setFrom($settings->get('mail.from_email'), $settings->get('mail.from_name'));

            return $mailer;
        },
        EntityManager::class => function (SettingsInterface $settings): EntityManager {
            $doctrine = $settings->get('doctrine');
            $cache = $doctrine['dev_mode'] ?
                new ArrayAdapter() :
                new FilesystemAdapter(directory: $doctrine['cache_dir']);

            Type::addType('enum_content_status', 'Multicanal\Entity\Enum\ContentStatusType');
            Type::addType('enum_channel_type', 'Multicanal\Entity\Enum\ChannelType');

            $config = ORMSetup::createAttributeMetadataConfiguration(
                $doctrine['metadata_dirs'],
                $doctrine['dev_mode'],
                $doctrine['proxies_dir'],
                $cache
            );

            $em   = EntityManager::create($doctrine['connection'], $config);
            $conn = $em->getConnection();
            $conn->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
            return $em;
        },
        'user' => function (SessionUser $sessionUser) {
            return $sessionUser->getUser();
        },
        'Multicanal\Lib\*' => DI\autowire(),
        'Multicanal\Repository\*' => DI\autowire()
    ]);
};
