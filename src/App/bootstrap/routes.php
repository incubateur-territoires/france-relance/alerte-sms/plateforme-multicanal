<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

declare(strict_types=1);

use Multicanal\Controller\Action\AuthenticationActionController;
use Multicanal\Controller\Action\CategoryActionController;
use Multicanal\Controller\Action\ChannelActionController;
use Multicanal\Controller\Action\CommunicationActionController;
use Multicanal\Controller\Action\ContentActionController;
use Multicanal\Controller\Action\DistributionListActionController;
use Multicanal\Controller\Action\DmUploaderActionController;
use Multicanal\Controller\Action\InformationActionController;
use Multicanal\Controller\Action\OrganizationActionController;
use Multicanal\Controller\Action\SubscriptionActionController;
use Multicanal\Controller\Api\v1\DistributionListController;
use Multicanal\Controller\Api\v1\EmailController;
use Multicanal\Controller\Api\v1\SMSController;
use Multicanal\Controller\View\APIViewController;
use Multicanal\Controller\View\AdministrationOrganizationViewController;
use Multicanal\Controller\View\AdministrationViewController;
use Multicanal\Controller\View\ConnectionViewController;
use Multicanal\Controller\View\ContentViewController;
use Multicanal\Controller\View\DistributionListViewController;
use Multicanal\Controller\View\HomeViewController;
use Multicanal\Controller\View\InformationViewController;
use Multicanal\Controller\View\LegalDisclaimerViewController;
use Multicanal\Controller\View\OrganizationViewController;
use Multicanal\Middleware\ApiAuthorizationMiddleware;
use Multicanal\Middleware\Authentication\AdminGNAuthMiddleware;
use Multicanal\Middleware\Authentication\DomainAdminAuthMiddleware;
use Multicanal\Middleware\Authentication\LoggedInUserAuthMiddleware;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {

    $app->group('', function (Group $group) {

        $group->get('/',                                                HomeViewController::class);
        $group->get('/mentions-legales',                                LegalDisclaimerViewController::class);
        $group->get('/connexion',                                       ConnectionViewController::class);
        $group->get('/connexion/{organization}',                        ConnectionViewController::class);
        $group->post('/action/login',                                   [AuthenticationActionController::class, 'login']);
        $group->get('/action/logout',                                   [AuthenticationActionController::class, 'logout']);
        $group->get('/action/openid-connect',                           [AuthenticationActionController::class, 'openIdConnect']);
        $group->get('/action/openid-connect/{targetOrganization}',      [AuthenticationActionController::class, 'openIdConnect']);
        $group->get('/action/check-sso',                                [AuthenticationActionController::class, 'checkSso']);
        $group->get('/action/refresh-sso',                              [AuthenticationActionController::class, 'refreshSso']);
        $group->post('/action/subscribe',                               [SubscriptionActionController::class, 'subscribe']);
        $group->post('/action/unsubscribe',                             [SubscriptionActionController::class, 'unsubscribe']);
        $group->get('/communications/{organization}',                   [DistributionListViewController::class, 'viewCommunicationsList']);
        $group->get('/communication/{communicationId}',                 [DistributionListViewController::class, 'viewCommunication']);
        $group->get('/contenu/{contentId}',                             [ContentViewController::class, 'viewContent']);

        $group->group('', function (Group $group) {
            $group->get('/administration',                                      AdministrationViewController::class);
            $group->get('/administration/{organization}',                       AdministrationOrganizationViewController::class);
            $group->get('/api',                                                 APIViewController::class);
            $group->post('/action/ajax/list/organizations',                     [OrganizationActionController::class, 'load']);
            $group->post('/action/add/organization',                            [OrganizationActionController::class, 'add']);
            $group->post('/action/delete/organization',                         [OrganizationActionController::class, 'delete']);
        })->add(AdminGNAuthMiddleware::class);

        $group->group('', function (Group $group) {
            $group->post('/action/organization/general-parameters/save',        [OrganizationActionController::class, 'saveGeneralParameters']);
            $group->post('/action/organization/emailling-parameters/save',      [OrganizationActionController::class, 'saveEmailingParameters']);
            $group->post('/action/organization/mailbox-parameters/save',        [OrganizationActionController::class, 'saveMailboxParameters']);
            $group->post('/action/ajax/count[/{organization}]',                 [OrganizationActionController::class, 'count']);
            $group->get('/collectivite',                                        OrganizationViewController::class);
            $group->get('/collectivite/{facebookToken}',                        OrganizationViewController::class);
            $group->post('/action/delete/channel',                              [ChannelActionController::class, 'deleteChannel']);

            $group->group('/channels', function (Group $group) {
                $group->get('/auth/twitter',                                    [ChannelActionController::class, 'redirectTwitter']);
                $group->get('/auth/facebook',                                   [ChannelActionController::class, 'redirectFacebook']);
            });
        })->add(DomainAdminAuthMiddleware::class);

        $group->group('', function (Group $group) {
            $group->post('/action/communications/unsubscribe',                  [SubscriptionActionController::class, 'unsubscribeById']);
            $group->post('/action/communications/subscribe/email',              [SubscriptionActionController::class, 'subscribeEmails']);
            $group->post('/action/communications/subscribe/sms',                [SubscriptionActionController::class, 'subscribeMobiles']);
            $group->post('/action/ajax/list/communications/sms',                [SubscriptionActionController::class, 'loadMobileSubscriptions']);
            $group->post('/action/ajax/list/communications/email',              [SubscriptionActionController::class, 'loadEmailSubscriptions']);
            $group->post('/action/ajax/list/communications',                    [DistributionListActionController::class, 'load']);
            $group->post('/action/ajax/list/communicationsByOrganization',      [DistributionListActionController::class, 'loadByOrganisation']);
            $group->get('/contenus/editer[/{categoryId}]',                      [ContentViewController::class, 'editContents']);
            $group->get('/contenus/liste-diffusion[/{categoryId}]',             [ContentViewController::class, 'distribution']);
            $group->get('/contenus/publier[/{contentId}]',                      [ContentViewController::class, 'publish']);
            $group->get('/contenus/nouveau',                                    [ContentViewController::class, 'editContentForm']);
            $group->get('/contenus/modifier/{contentId}',                       [ContentViewController::class, 'editContentForm']);
            $group->get('/contenus[/{categoryId}]',                             [ContentViewController::class, 'listContent']);
            $group->get('/contenus/{organization}/{distributionListId}',        [ContentViewController::class, 'viewContentsList']);
            $group->get('/informations',                                        InformationViewController::class);
            $group->get('/action/ajax/list/information/details/{emailNumber}',  [InformationActionController::class, 'details']);
            $group->get('/attachment/{emailNumber}/{attachmentId}',             [InformationActionController::class, 'attachment']);
            $group->post('/action/ajax/list/information',                       [InformationActionController::class, 'load']);
            $group->post('/action/ajax/view/information',                       [InformationActionController::class, 'view']);
            $group->post('/action/transform/information',                       [InformationActionController::class, 'transform']);
            $group->post('/action/concat/information',                          [InformationActionController::class, 'concat']);
            $group->post('/action/add/category',                                [CategoryActionController::class, 'add']);
            $group->post('/action/update/category',                             [CategoryActionController::class, 'update']);
            $group->post('/action/delete/category',                             [CategoryActionController::class, 'delete']);
            $group->post('/action/save/publication',                            [CommunicationActionController::class, 'save']);
            $group->post('/action/schedule/publication',                        [CommunicationActionController::class, 'schedule']);
            $group->post('/action/cancel/scheduling',                           [CommunicationActionController::class, 'cancelScheduling']);
            $group->post('/action/publish/EMAIL',                               [CommunicationActionController::class, 'publishByEmail']);
            $group->post('/action/publish/SMS',                                 [CommunicationActionController::class, 'publishBySMS']);
            $group->post('/action/publish/CHANNEL',                             [CommunicationActionController::class, 'publishByChannel']);
            $group->post('/action/communication/preview/EMAIL',                 [CommunicationActionController::class, 'previewEmail']);
            $group->get('/imprimer/{distributionListId}',                       [DistributionListViewController::class, 'print']);
            $group->post('/action/add/distributionList',                        [DistributionListActionController::class, 'add']);
            $group->post('/action/update/distributionList',                     [DistributionListActionController::class, 'update']);
            $group->post('/action/delete/distributionList',                     [DistributionListActionController::class, 'delete']);
            $group->post('/action/ajax/get/distributionList',                   [DistributionListActionController::class, 'get']);
            $group->post('/action/ajax/preview/distributionList',               [DistributionListActionController::class, 'preview']);
            $group->post('/action/ajax/list/contents',                          [ContentActionController::class, 'load']);
            $group->post('/action/add/content',                                 [ContentActionController::class, 'add']);
            $group->post('/action/archive/content',                             [ContentActionController::class, 'archive']);
            $group->post('/action/duplicate/content',                           [ContentActionController::class, 'duplicate']);
            $group->post('/action/update/content',                              [ContentActionController::class, 'update']);
            $group->post('/action/save-and-publish/content',                    [ContentActionController::class, 'saveAndPublish']);
            $group->post('/action/delete/content',                              [ContentActionController::class, 'delete']);
            $group->post('/action/ajax/get/content',                            [ContentActionController::class, 'get']);
            $group->post('/action/ajax/upload/file',                            [DmUploaderActionController::class, 'uploadFile']);
            $group->post('/action/share/content/mail',                          [ContentActionController::class, 'shareByEmail']);
        })->add(LoggedInUserAuthMiddleware::class);
    })->add('csrf');

    // Routing API methods
    $app->group('/api/v1', function (Group $group) {
        $group->group('/send', function (Group $group) {
            $group->post('/email/{distributionListId}',                     [EmailController::class, 'sendEmailByDistributionList']);
            $group->post('/sms/{distributionListId}',                       [SMSController::class, 'sendSmsByDistributionList']);
            $group->post('/communication/email/{contentId}',                [EmailController::class, 'sendCommunicationByEmail']);
            $group->post('/communication/sms/{contentId}',                  [SMSController::class, 'sendCommunicationBySms']);
            $group->post('/email',                                          [EmailController::class, 'send']);
            $group->post('/sms',                                            [SMSController::class, 'send']);
        });

        $group->group('/distributionList', function (Group $group) {
            $group->get('s[/{organization}]',                       [DistributionListController::class, 'getDistributionListsByOrganization']);
            $group->get('/{distributionListId}',                    [DistributionListController::class, 'getDistributionListById']);
            $group->put('/{distributionListId}',                    [DistributionListController::class, 'updateDistributionList']);
            $group->post('/add/{organization}',                     [DistributionListController::class, 'addDistributionList']);
            $group->delete('/{distributionListId}',                 [DistributionListController::class, 'deleteDistributionList']);
            $group->put('/subscribe/{distributionListId}',          [DistributionListController::class, 'subscribeToDistributionList']);
            $group->put('/unsubscribe/{distributionListId}',        [DistributionListController::class, 'unsubscribeToDistributionList']);
        });
    })->add(ApiAuthorizationMiddleware::class);
};
