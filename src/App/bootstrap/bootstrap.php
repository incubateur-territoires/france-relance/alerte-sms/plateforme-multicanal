<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

declare(strict_types=1);

use DI\ContainerBuilder;
use Multicanal\App\Helper;

require_once dirname(__DIR__, 3)  . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * Bootstrap to Instantiate PHP-DI ContainerBuilder.
 *
 * @package Multicanal
 * @author  William BLANC DIT JOLICOEUR <w.blanc-dit-jolicoeur@girondenumerique.fr>
 */

$containerBuilder = new ContainerBuilder();

/**
 * To compile container, we could write something like that :
 * @example : $containerBuilder->enableCompilation(dirname(__DIR__, 3) . '/log/compiledcontainer');
 */

// Set up settings
$settings = require_once __DIR__ . '/settings.php';
$settings($containerBuilder);

// Set up dependencies
$dependencies = require_once __DIR__ . '/dependencies.php';
$dependencies($containerBuilder);

$container = $containerBuilder->build();
$container->get(Helper::class); // inject container in global static Helper class (see definition in dependencies.php)

return $container;
