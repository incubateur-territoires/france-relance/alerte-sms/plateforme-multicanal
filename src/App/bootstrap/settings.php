<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

declare(strict_types=1);

use DI\ContainerBuilder;
use Dotenv\Dotenv;
use Monolog\Logger;
use Multicanal\App\Helper;
use Multicanal\App\Settings\Settings;
use Multicanal\App\Settings\SettingsInterface;
use Multicanal\Utils\EnvUtils;

return function (ContainerBuilder $containerBuilder) {

    $rootDir = realpath(__DIR__ . '/../../..');

    $dotenv = Dotenv::createImmutable($rootDir);
    $dotenv->load();

    $containerBuilder->addDefinitions([
        SettingsInterface::class => function () use ($rootDir) {

            $settings = [];
            // Environment properties
            $settings['app']                         = [];
            $settings['app']['name']                 = 'multicanal';
            $settings['app']['level']                = $_SERVER['LEVEL'];
            $settings['app']['domain']               = $_SERVER['DOMAIN'];

            $settings['app']['assets_cache_busting'] = null;
            if (array_key_exists('ASSETS_CACHE_BUSTING', $_SERVER)) {
                $settings['app']['assets_cache_busting'] = (strpos($_SERVER['ASSETS_CACHE_BUSTING'] ?? '', '#') === false) ?
                    $_SERVER['ASSETS_CACHE_BUSTING'] :
                    null;
            }

            // Important directories;
            $settings['app']['root_dir']    = $rootDir;
            $settings['app']['bin_dir']     = $rootDir . DIRECTORY_SEPARATOR . 'bin';
            $settings['app']['public_dir']  = $rootDir . DIRECTORY_SEPARATOR . 'public';
            $settings['app']['log_dir']     = $rootDir . DIRECTORY_SEPARATOR . 'logs/';

            // Eventual trusted proxies
            $settings['app']['trusted_proxies']  = EnvUtils::toValueArray($_SERVER['TRUSTED_PROXIES'] ?? null);

            // Truncate length
            $settings['app']['truncate_default_length'] = !empty($_SERVER['TRUNCATE_DEFAULT_LENGTH']) ?
                (int) $_SERVER['TRUNCATE_DEFAULT_LENGTH'] : 150;

            // URL shortener API
            $settings['app']['shortener_api_url'] = $_SERVER['SHORTENER_API_URL'];
            $settings['app']['api_key'] = $_SERVER['API_KEY'];

            $settings['env'] = [
                'development' => EnvUtils::evalBool($_SERVER['DEVELOPMENT']),
                'production'  => EnvUtils::evalBool($_SERVER['PRODUCTION']),
                'debug'       => EnvUtils::evalBool($_SERVER['DEBUG']),
                'logging'     => EnvUtils::evalBool($_SERVER['LOGGING']),
            ];

            /**
             * Add some settings to configure sessions (used in dependencies.php for Odan\Session\PhpSession)
             * @see https://www.php.net/manual/fr/session.security.ini.php
             * @see https://cheatsheetseries.owasp.org/cheatsheets/Session_Management_Cheat_Sheet.html
             */
            $settings['session'] = [
                'name' => $settings['app']['name'] . '-' . $settings['app']['level'],
                'cookie_samesite' => 'Lax',
                'cookie_httponly' => true,
                'cookie_secure' => true,
            ];

            try {
                $monologConstantReflex = new \ReflectionClassConstant(Logger::class, $_SERVER['LOG_LEVEL'] ?? 'INFO');
                $monologLevel = $monologConstantReflex->getValue();
            } catch (\ReflectionException $e) {
                $monologLevel = Logger::ERROR;
            }
            $settings['logger'] = [
                'name'       => $settings['app']['name'],
                'path'       => $settings['app']['log_dir'],
                'level'  => $monologLevel,
            ];

            // Twig properties
            $settings['twig']['templates_path'] = $rootDir . DIRECTORY_SEPARATOR . 'templates';
            $settings['twig']['cache_path']     = $settings['twig']['templates_path'] . DIRECTORY_SEPARATOR . 'cache';

            // Doctrine
            $settings['doctrine'] = [
                // Enables or disables Doctrine metadata caching
                // for either performance or convenience during development.
                'dev_mode' => EnvUtils::evalBool($_SERVER['DEVELOPMENT']),

                // Path where Doctrine will cache the processed metadata
                // when 'dev_mode' is false.
                'cache_dir' => $settings['app']['root_dir'] . '/var/doctrine',

                // Path where Doctrine will generate proxy classes
                // when 'dev_mode' is false.
                'proxies_dir' => $settings['app']['root_dir'] . '/var/doctrine/proxies',

                // List of paths where Doctrine will search for metadata.
                // Metadata can be either YML/XML files or PHP classes annotated
                // with comments or PHP8 attributes.
                'metadata_dirs' => [$settings['app']['root_dir'] . '/src/Entity'],

                // The parameters Doctrine needs to connect to your database.
                // These parameters depend on the driver (for instance the 'pdo_sqlite' driver
                // needs a 'path' parameter and doesn't use most of the ones shown in this example).
                // Refer to the Doctrine documentation to see the full list
                // of valid parameters: https://www.doctrine-project.org/projects/doctrine-dbal/en/current/reference/configuration.html
                'connection' => [
                    'driver'   => $_SERVER['DOCTRINE_DRIVER'],
                    'host'     => $_SERVER['DOCTRINE_HOST'],
                    'port'     => (int) $_SERVER['DOCTRINE_PORT'],
                    'dbname'   => $_SERVER['DOCTRINE_DBNAME'],
                    'user'     => $_SERVER['DOCTRINE_USER'],
                    'password' => $_SERVER['DOCTRINE_PASSWORD'],
                ]
            ];

            // OpenLDAP server properties
            $settings['ldap'] = [
                'server'          => $_SERVER['LDAP_SERVER'],
                'port'            => (int) $_SERVER['LDAP_PORT'],
                'login'           => $_SERVER['LDAP_LOGIN'],
                'password'        => $_SERVER['LDAP_PASSWORD']
            ];

            // OpenIDConnect  properties
            $settings['oidc'] = [
                'connexion_enabled'       => EnvUtils::evalBool($_SERVER['OIDC_CONNEXION_ENABLED']),
                'ssl_verify'              => EnvUtils::evalBool($_SERVER['OIDC_SSL_VERIFY']),
                'allow_insecure_requests' => EnvUtils::evalBool($_SERVER['OIDC_ALLOW_INSECURE_REQUESTS']),
                'provider_url'            => $_SERVER['OIDC_PROVIDER_URL'],
                'client_id'               => $_SERVER['OIDC_CLIENT_ID'],
                'client_secret'           => $_SERVER['OIDC_CLIENT_SECRET'],
                'scopes'                  => EnvUtils::toValueArray($_SERVER['OIDC_SCOPES']),
                'external'                => EnvUtils::evalBool($_SERVER['OIDC_EXTERNAL_SSO'])
            ];

            // OpenIDConnect  properties
            $settings['mail'] = [
                'debug'                    => false,
                'throw_external_exception' => true,
                'smtp_host'                => $_SERVER['PHPMAILER_SMTP_HOST'],
                'smtp_auth'                => EnvUtils::evalBool($_SERVER['PHPMAILER_SMTP_AUTH']),
                'smtp_port'                => (int) $_SERVER['PHPMAILER_PORT'],
                'smtp_secure'              => $_SERVER['PHPMAILER_SMTP_SECURE'],
                'usermail'                 => $_SERVER['PHPMAILER_USERMAIL'],
                'password'                 => $_SERVER['PHPMAILER_PASSWORD'],
                'from_name'                => $_SERVER['PHPMAILER_FROM_NAME'],
                'from_email'               => $_SERVER['PHPMAILER_FROM_EMAIL'],
            ];

            // OVH SMS API properties
            $settings['ovh'] = [
                'endpoint' => $_SERVER['OVH_ENDPOINT'],
                'application_key' => $_SERVER['OVH_APPLICATION_KEY'],
                'application_secret' => $_SERVER['OVH_APPLICATION_SECRET'],
                'consumer_key' => $_SERVER['OVH_CONSUMER_KEY']
            ];

            // DmUploader allowed mime-types and extensions
            $settings['dmUploader'] = [
                'allowed_mimetypes' => [
                    'image/jpeg',
                    'image/png',
                    'application/pdf',
                    'video/mp4',
                    'video/webm',
                ],
                'allowed_extensions' => [
                    'jpeg', 'jpg', 'png', 'pdf', 'mp4', 'webm'
                ],
                'max_filesize' => min(5 * 1024 * 1024, Helper::getUploadMaxSize()), //in bytes
            ];

            // DmUploader allowed mime-types and extensions
            $settings['genericImageUploader'] = [
                'allowed_mimetypes' => [
                    'image/jpeg',
                    'image/png',
                    'image/svg+xml'
                ],
                'allowed_extensions' => [
                    'jpeg', 'jpg', 'png', 'svg'
                ],
                'max_filesize' => min(2 * 1024 * 1024, Helper::getUploadMaxSize()), //in bytes
            ];



            // Twitter API properties
            $settings['twitter'] = [
                'application_key' => $_SERVER['TWITTER_API_KEY'],
                'application_secret' => $_SERVER['TWITTER_API_SECRET_KEY'],
            ];

            // Facebook API properties
            $settings['facebook'] = [
                'application_key' => $_SERVER['FACEBOOK_API_KEY'],
                'application_secret' => $_SERVER['FACEBOOK_APP_SECRET_KEY'],
            ];

            return new Settings($settings);
        }

    ]);
};
