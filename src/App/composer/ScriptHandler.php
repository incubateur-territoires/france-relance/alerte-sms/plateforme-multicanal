<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

declare(strict_types=1);

namespace Multicanal\App\composer;

use Error;
use FilesystemIterator;
use MatthiasMullie\Minify;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;

/**
 * Class to handle custom scripts defined in composer.
 * We use it to minify css and js resources.
 */
class ScriptHandler {
    const RESOURCES_DIR = 'public/resources';
    const CSS_DIR = 'css';
    const JAVASCRIPT_DIR = 'javascript';

    public static function minify(): void {

        self::minifyCssFiles();

        self::minifyJsFiles();
    }

    protected static function minifyCssFiles(): void {
        $cssFiles = self::getFilesByType('css');
        foreach ($cssFiles as $source => $target) {
            $cssMinifier = new Minify\CSS($source);
            /**
             * prevent automatically embed referenced files
             * @see https://github.com/matthiasmullie/minify#setimportextensionsextensions-css-only
             */
            $cssMinifier->setImportExtensions([]);
            $cssMinifier->minify($target);
        }
    }

    protected static function minifyJsFiles(): void {
        $cssFiles = self::getFilesByType('js');
        foreach ($cssFiles as $source => $target) {
            $jsMinifier = new Minify\JS($source);
            $jsMinifier->minify($target);
        }
    }

    protected static function getResourcesDir(): string {
        return dirname(__DIR__, 3) . DIRECTORY_SEPARATOR .  self::RESOURCES_DIR;
    }

    /**
     * getFilesByType
     *
     * @param string $type
     *
     * @return array<string, string>
     */
    protected static function getFilesByType(string $type): array {
        switch ($type) {
            case 'js':
                $typeDir = self::JAVASCRIPT_DIR;
                $typeExt = 'js';
                break;
            case 'css':
                $typeDir = self::CSS_DIR;
                $typeExt = 'css';
                break;
            default:
                throw new Error("Unknown type to process");
        }

        $dirIterator = new RecursiveDirectoryIterator(self::getResourcesDir() . '/' . $typeDir, FilesystemIterator::SKIP_DOTS);
        $iterator = new RecursiveIteratorIterator($dirIterator, RecursiveIteratorIterator::SELF_FIRST);
        // could use CHILD_FIRST if you so wish
        $files = [];
        foreach ($iterator as $file) {
            if ($file->getExtension() == $typeExt && !self::isMinifiedFile($file)) {
                $files[$file->getPathName()] = self::getMinifiedPathName($file);
            }
        }
        return $files;
    }

    protected static function getMinifiedPathName(SplFileInfo $file): string {
        return sprintf(
            '%s/%smin.%s',
            $file->getPathInfo()->getPathName(),
            $file->getBaseName($file->getExtension()),
            $file->getExtension()
        );
    }

    protected static function isMinifiedFile(SplFileInfo $file): bool {
        $endString = '.min.' . $file->getExtension();
        $len = strlen($endString);
        return substr($file->getBasename(), -$len) === $endString;
    }
}
