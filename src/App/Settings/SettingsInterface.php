<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

declare(strict_types=1);

namespace Multicanal\App\Settings;

interface SettingsInterface {
    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key = '');
}
