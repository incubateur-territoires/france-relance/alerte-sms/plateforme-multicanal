<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

declare(strict_types=1);

namespace Multicanal\App\Settings;

/**
 * Classe permettant de stocker le tableau de configuration de l'application
 */
class Settings implements SettingsInterface {
    /**
     * @var array<string, mixed>
     */
    private array $settings;

    /**
     * @param array<string, mixed> $settings
     */
    public function __construct(array $settings) {
        $this->settings = $settings;
    }

    /**
     * Permet d'obtenir la valeur d'une clé de configuration.
     * Permet d'atteindre des sous-éléments en utilisant un point (.) pour séparer le niveau d'arborescence.
     *
     * @example avec la configuration suivante $conf = new Settings([
     *  'cle' => 'valeur',
     *  'key' => [
     *          'subkey' => 'subvalue'
     *      ]
     *  ]);
     *  $conf->get('cle')           returns     valeur
     *  $conf->get('key')           returns     ['subkey' => 'subvalue']
     *  $conf->get('key.subkey')    returns     subvalue
     *
     * @param string $key
     *
     * @return mixed
     */
    public function get($key = '') {

        if (empty($key)) {
            return null;
        } else {
            $parts = explode('.', $key);
            if (count($parts) == 1) {
                return $this->settings[$key] ?? null;
            } else {
                $value = self::getValue($this->settings, $parts, $key_exists);
                return $key_exists ? $value : null;
            }
        }
    }

    /**
     * Retrieves a value from a nested array with variable depth.
     * Inspiré de Drupal\Component\Utility\NestedArray
     * @see https://github.com/drupal/drupal/blob/9.5.x/core/lib/Drupal/Component/Utility/NestedArray.php
     * @see https://github.com/drupal/drupal/blob/9.5.x/core/lib/Drupal/Core/Config/Config.php
     *
     * @param array<string, string> $array
     * @param array<int,string> $parents
     * @param ?bool $key_exists
     * @return mixed
     */
    private static function &getValue(array &$array, array $parents, &$key_exists = null) {
        $ref = &$array;
        foreach ($parents as $parent) {
            if (is_array($ref) && (isset($ref[$parent]) || array_key_exists($parent, $ref))) {
                $ref = &$ref[$parent];
            } else {
                $key_exists = false;
                return null;
            }
        }
        $key_exists = true;
        return $ref;
    }
}
