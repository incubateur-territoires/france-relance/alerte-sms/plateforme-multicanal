<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Repository;

use Exception;
use Multicanal\Entity\Category;
use Multicanal\Entity\Exception\MulticanalException;

/**
 * Class CategoryRepository for category CRUD interaction with database through Doctrine ORM.
 *
 * @package Multicanal\Repository
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class CategoryRepository extends Repository {

    /**
     * Find all category from database.
     *
     * @return array
     * @access public
     */
    public function findAll(): array {
        /** @var Category[] */
        return $this->em
            ->getRepository(Category::class)
            ->findAll();
    }

    /**
     * Find a specific category by its identifier.
     *
     * @param int $id Category identifier
     * @return Category|null
     * @access public
     */
    public function findById(int $id): ?Category {
        return $this->em
            ->getRepository(Category::class)
            ->find($id);
    }

    /**
     * Find a specific organization by its domain name.
     *
     * @param string $name Category name
     * @return Category|null
     * @access public
     */
    public function findByName(string $name): ?Category {
        return $this->em
            ->getRepository(Category::class)
            ->findOneBy(['name' => $name]);
    }

    /**
     * Save a category in database.
     *
     * @param Category $category Category object to save
     * @return bool
     * @throws MulticanalException only in debug context
     * @access public
     */
    public function save(Category &$category): bool {
        try {
            $this->em->persist($category);
            $this->em->flush($category);
            return true;
        } catch (Exception $e) {
            if ($this->debug) {
                throw new MulticanalException($e);
            }
            return false;
        }
    }

    /**
     * Delete a category from database.
     *
     * @param Category $category Category object to delete
     * @return bool
     * @throws MulticanalException only in debug context
     * @access public
     */
    public function delete(Category $category): bool {
        try {
            $this->em->remove($category);
            $this->em->flush($category);
            return true;
        } catch (Exception $e) {
            if ($this->debug) {
                throw new MulticanalException($e);
            }
            return false;
        }
    }
}
