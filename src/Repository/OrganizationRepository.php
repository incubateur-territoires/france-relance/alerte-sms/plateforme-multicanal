<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Repository;

use Doctrine\ORM\Exception\ORMException;
use Exception;
use Multicanal\Entity\Exception\MulticanalException;
use Multicanal\Entity\Organization;

/**
 * Class OrganizationRepository for Organization CRUD interaction with database through Doctrine ORM.
 *
 * @package Multicanal\Repository
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
class OrganizationRepository extends Repository {

    /**
     * Find all organization from database.
     *
     * @return array
     * @access public
     */
    public function findAll(): array {
        /** @var Organization[] */
        return $this->em
            ->getRepository(Organization::class)
            ->findAll();
    }

    /**
     * Find a specific organization by its identifier.
     *
     * @param int $id Organization identifier
     * @return Organization|null
     * @access public
     */
    public function findById(int $id): ?Organization {
        /** @var Organization[] */
        return $this->em
            ->getRepository(Organization::class)
            ->find($id);
    }

    /**
     * Find a specific organization by its domain name.
     *
     * @param string $domain Organization domain name
     * @return Organization|null
     * @access public
     */
    public function findByDomainName(string $domain): ?Organization {
        /** @var Organization[] */
        return $this->em
            ->getRepository(Organization::class)
            ->findOneBy(['domain' => $domain]);
    }

    /**
     * Function to search Organization through Doctrine, create it if not exist.
     *
     * @param string $organizationName Organization domain name
     * @return Organization
     * @throws ORMException
     * @throws Exception
     * @access public
     */
    public function createIfNotExist(string $organizationName): Organization {
        $organization = $this->findByDomainName($organizationName);
        if (empty($organization)) {
            $organization = new Organization($organizationName);
            $this->save($organization);
        }
        return $organization;
    }

    /**
     * Save an organization in database.
     * @param Organization $organization Organization object to save
     * @return bool
     * @throws MulticanalException only in debug context
     * @access public
     */
    public function save(Organization $organization): bool {
        try {
            $this->em->persist($organization);
            $this->em->flush($organization);
            return true;
        } catch (Exception $e) {
            if ($this->debug) {
                throw new MulticanalException($e);
            }
            return false;
        }
    }

    /**
     * Delete an organization from database.
     *
     * @param Organization $organization Organization object to delete
     * @return bool
     * @throws MulticanalException only in debug context
     * @access public
     */
    public function delete(Organization $organization): bool {
        try {
            /** First delete organization categories
             * @var Category $category */
            $categories = $organization->getCategories();
            foreach ($categories as $category) {
                $organization->getCategories()->removeElement($category);
            }
            $this->em->flush();

            /** Then delete organization users
             * @var User $user */
            $users = $organization->getUsers();
            foreach ($users as $user) {
                $organization->getUsers()->removeElement($user);
            }
            $this->em->flush();

            $this->em->remove($organization);
            $this->em->flush($organization);
            return true;
        } catch (Exception $e) {
            if ($this->debug) {
                throw new MulticanalException($e);
            }
            return false;
        }
    }
}
