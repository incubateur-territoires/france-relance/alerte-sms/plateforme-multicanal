<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Repository;

use Exception;
use Multicanal\Entity\Counter;
use Multicanal\Entity\Exception\MulticanalException;
use Multicanal\Entity\Organization;

/**
 * Class CounterRepository for counter CRUD interaction with database through Doctrine ORM.
 *
 * @package Multicanal\Repository
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class CounterRepository extends Repository {

    /**
     * Find counters for a specific organization.
     *
     * @param Organization $organization
     * @return array
     * @access public
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByOrganization(Organization $organization): array {
        return $this->em->createQueryBuilder()
            ->select('o')
            ->from(Counter::class, 'o')
            ->innerJoin('o.organization', 'organization')
            ->andWhere('organization.id = :oid')
            ->setParameter('oid', $organization->getId())
            ->getQuery()
            ->getResult();
    }

    /**
     * Find counter for a specific organization and a specific year.
     *
     * @param int $year
     * @param Organization $organization
     * @return Counter|null
     * @access public
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByYearOrganization(int $year, Organization $organization): ?Counter {
        return $this->em->createQueryBuilder()
            ->select('o')
            ->from(Counter::class, 'o')
            ->innerJoin('o.organization', 'organization')
            ->andWhere('organization.id = :oid')
            ->andWhere('o.year = :year')
            ->setParameter('year', $year)
            ->setParameter('oid', $organization->getId())
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Save a counter in database.
     *
     * @param Counter $counter Counter object to save
     * @return bool
     * @throws MulticanalException only in debug context
     * @access public
     */
    public function save(Counter &$counter): bool {
        try {
            $this->em->persist($counter);
            $this->em->flush($counter);
            return true;
        } catch (Exception $e) {
            if ($this->debug) {
                throw new MulticanalException($e);
            }
            return false;
        }
    }

    /**
     * Delete a counter from database.
     *
     * @param Counter $counter Counter object to delete
     * @return bool
     * @throws MulticanalException only in debug context
     * @access public
     */
    public function delete(Counter $counter): bool {
        try {
            $this->em->remove($counter);
            $this->em->flush($counter);
            return true;
        } catch (Exception $e) {
            if ($this->debug) {
                throw new MulticanalException($e);
            }
            return false;
        }
    }
}
