<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Repository;

use Multicanal\Entity\User;

/**
 * Class UserRepository for User CRUD interaction with database through Doctrine ORM.
 *
 * @package Multicanal\Repository
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
class UserRepository extends Repository {

    /**
     * Find a specific user by its email.
     *
     * @param string $email User email
     * @return User|null
     * @acces public
     */
    public function findByEmail(string $email): ?User {
        /** @var User[] */
        return $this->em
            ->getRepository(User::class)
            ->findOneBy(['email' => $email]);
    }
}
