<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Repository;

use Exception;
use Multicanal\Entity\Category;
use Multicanal\Entity\Content;
use Multicanal\Entity\Exception\MulticanalException;
use Multicanal\Entity\Organization;

/**
 * Class ContentRepository for content CRUD interaction with database through Doctrine ORM.
 *
 * @package Multicanal\Repository
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class ContentRepository extends Repository {

    /**
     * Find all contents from database.
     *
     * @return array
     * @access public
     */
    public function findAll(): array {
        /** @var Content[] */
        return $this->em
            ->getRepository(Content::class)
            ->findAll();
    }

    /**
     * Find a specific content by its identifier.
     *
     * @param int $id Content identifier
     * @return Content|null
     * @access public
     */
    public function findById(int $id): ?Content {
        return $this->em
            ->getRepository(Content::class)
            ->find($id);
    }

    /**
     * Find contents by category.
     *
     * @param Category $category Category object
     * @return array
     * @access public
     */
    public function findByCategory(Category $category): array {
        /** @var Content[] */
        return $this->em->createQueryBuilder()
            ->select('o')
            ->from(Content::class, 'o')
            ->innerJoin('o.category', 'category')
            ->andWhere('category.id = :cid')
            ->setParameter('cid', $category->getId())
            ->orderBy('o.updateDate', 'DESC')
            ->orderBy('o.creationDate', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Find contents by organization's domain name.
     *
     * @param Organization $organisation Organization object
     * @param array $status Content status to filtering
     * @param int|null $limit Optional results size limit
     * @return array
     * @access public
     */
    public function findByDomain(Organization $organisation, array $status, int $limit = null): array {
        /** @var Content[] */
        return $this->em->createQueryBuilder()
            ->select('o')
            ->from(Content::class, 'o')
            ->innerJoin('o.category', 'category')
            ->innerJoin('category.organization', 'organization')
            ->andWhere('organization.id = :oid')
            ->andWhere('o.status IN (:status)')
            ->setParameter('oid', $organisation->getId())
            ->setParameter('status', $status)
            ->setMaxResults($limit)
            ->orderBy('o.updateDate', 'DESC')
            ->orderBy('o.creationDate', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Save a content in database.
     *
     * @param Content $content Content to save
     * @return bool
     * @throws MulticanalException only in debug context
     * @access public
     */
    public function save(Content $content): bool {
        try {
            $this->em->persist($content);
            $this->em->flush($content);
            return true;
        } catch (Exception $e) {
            if ($this->debug) {
                throw new MulticanalException($e);
            }
            return false;
        }
    }

    /**
     * Delete a content from database.
     *
     * @param Content $content Content to delete
     * @return bool
     * @throws MulticanalException only in debug context
     * @access public
     */
    public function delete(Content $content): bool {
        try {
            $this->em->remove($content);
            $this->em->flush($content);
            return true;
        } catch (Exception $e) {
            if ($this->debug) {
                throw new MulticanalException($e);
            }
            return false;
        }
    }
}
