<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Repository;

use Doctrine\ORM\NonUniqueResultException;
use Exception;
use Multicanal\Entity\DistributionList;
use Multicanal\Entity\EmailSubscription;
use Multicanal\Entity\Exception\MulticanalException;
use Multicanal\Entity\MobileSubscription;

/**
 * Class SubscriptionRepository for Subscription CRUD interaction with database through Doctrine ORM.
 *
 * @package Multicanal\Repository
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
class SubscriptionRepository extends Repository {

    /**
     * Find all mobile subscriptions from database.
     *
     * @return array
     * @access public
     */
    public function findAllMobile(): array {
        /** @var MobileSubscription[] */
        return $this->em
            ->getRepository(MobileSubscription::class)
            ->findAll();
    }

    /**
     * Find all email subscriptions from database.
     *
     * @return array
     * @access public
     */
    public function findAllEmail(): array {
        /** @var MobileSubscription[] */
        return $this->em
            ->getRepository(EmailSubscription::class)
            ->findAll();
    }

    /**
     * Find a mobile subscription by its identifier.
     *
     * @param int $id Mobile subscription identifier
     * @return MobileSubscription|null
     * @access public
     */
    public function findMobileById(int $id): ?MobileSubscription {
        return $this->em
            ->getRepository(MobileSubscription::class)
            ->find($id);
    }

    /**
     * Find an email subscription by its identifier.
     *
     * @param int $id Mobile subscription identifier
     * @return EmailSubscription|null
     * @access public
     */
    public function findEmailById(int $id): ?EmailSubscription {
        return $this->em
            ->getRepository(EmailSubscription::class)
            ->find($id);
    }

    /**
     * Find a specific mobile subscription in a distribution list
     * @param string $mobile Mobile phone number
     * @param DistributionList $distributionList Concerned distribution list object
     * @return MobileSubscription|null
     * @throws NonUniqueResultException
     * @access public
     */
    public function findMobileByDistributionList(string $mobile, DistributionList $distributionList): ?MobileSubscription {
        /** @var array */
        return $this->em->createQueryBuilder()
            ->select('o')
            ->from(MobileSubscription::class, 'o')
            ->andWhere('o.mobile = :mobile')
            ->andWhere('o.distributionList = :distributionList')
            ->setParameter('mobile', $mobile)
            ->setParameter('distributionList', $distributionList)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Find a specific em subscription in a distribution list.
     *
     * @param string $email Email
     * @param DistributionList $distributionList Concerned distribution list object
     * @return EmailSubscription|null
     * @throws NonUniqueResultException
     * @access public
     */
    public function findEmailByDistributionList(string $email, DistributionList $distributionList): ?EmailSubscription {
        /** @var array */
        return $this->em->createQueryBuilder()
            ->select('o')
            ->from(EmailSubscription::class, 'o')
            ->andWhere('o.email = :email')
            ->andWhere('o.distributionList = :distributionList')
            ->setParameter('email', $email)
            ->setParameter('distributionList', $distributionList)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Save a mobile subscription in database.
     *
     * @param MobileSubscription $mobileSubscription Mobile subscription to save
     * @return bool
     * @throws MulticanalException only in debug context
     * @access public
     */
    public function saveMobile(MobileSubscription $mobileSubscription): bool {
        try {
            $this->em->persist($mobileSubscription);
            $this->em->flush($mobileSubscription);
            return true;
        } catch (Exception $e) {
            if ($this->debug) {
                throw new MulticanalException($e);
            }
            return false;
        }
    }

    /**
     * Save an email subscription in database.
     *
     * @param EmailSubscription $emailSubscription Email subscription to save
     * @return bool
     * @throws MulticanalException only in debug context
     * @access public
     */
    public function saveEmail(EmailSubscription $emailSubscription): bool {
        try {
            $this->em->persist($emailSubscription);
            $this->em->flush($emailSubscription);
            return true;
        } catch (Exception $e) {
            if ($this->debug) {
                throw new MulticanalException($e);
            }
            return false;
        }
    }

    /**
     * Delete a mobile subscription from database
     * @param MobileSubscription $mobileSubscription Mobile subcription to delete
     * @return bool
     * @throws MulticanalException only in debug context
     * @access public
     */
    public function deleteMobile(MobileSubscription $mobileSubscription): bool {
        try {
            $this->em->remove($mobileSubscription);
            $this->em->flush();
            return true;
        } catch (Exception $e) {
            if ($this->debug) {
                throw new MulticanalException($e);
            }
            return false;
        }
    }

    /**
     * Delete an email subscription from database
     * @param EmailSubscription $emailSubscription Email subcription to delete
     * @return bool
     * @throws MulticanalException only in debug context
     * @access public
     */
    public function deleteEmail(EmailSubscription $emailSubscription): bool {
        try {
            $this->em->remove($emailSubscription);
            $this->em->flush($emailSubscription);
            return true;
        } catch (Exception $e) {
            if ($this->debug) {
                throw new MulticanalException($e);
            }
            return false;
        }
    }
}
