<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Repository;

use Doctrine\ORM\EntityManager;
use Multicanal\App\Settings\SettingsInterface;

/**
 * Main repositiry class.
 *
 * @package Multicanal\Repository
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
class Repository {

    protected EntityManager $em;

    protected bool $debug;

    public function __construct(EntityManager $em, SettingsInterface $settings) {
        $this->em = $em;
        $this->debug = $settings->get('env.debug');
    }
}
