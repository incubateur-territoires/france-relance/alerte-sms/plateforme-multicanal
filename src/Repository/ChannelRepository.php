<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Repository;

use Exception;
use Multicanal\Entity\Exception\MulticanalException;
use Multicanal\Entity\SocialNetwork\Channel;

/**
 * Class ChannelRepository for channel CRUD interaction with database through Doctrine ORM.
 *
 * @package Multicanal\Repository
 * @author  Nathanael KHODL <consulting@khodl.com>
 */
class ChannelRepository extends Repository {

    /**
     * Find a specific channel by its identifier.
     *
     * @param int $id Channel identifier
     * @return Channel|null
     * @access public
     */
    public function findById(int $id): ?Channel {
        return $this->em
            ->getRepository(Channel::class)
            ->find($id);
    }

    /**
     * Save a channel in database.
     *
     * @param Channel $channel Channel object to save
     * @return bool
     * @throws MulticanalException only in debug context
     * @access public
     */
    public function save(Channel &$channel): bool {
        try {
            $this->em->persist($channel);
            $this->em->flush($channel);
            return true;
        } catch (Exception $e) {
            if ($this->debug) {
                throw new MulticanalException($e);
            }
            return false;
        }
    }

    /**
     * Delete a channel from database.
     *
     * @param Channel $channel Channel object to delete
     * @return bool
     * @throws MulticanalException only in debug context
     * @access public
     */
    public function delete(Channel $channel): bool {
        try {
            $this->em->remove($channel);
            $this->em->flush($channel);
            return true;
        } catch (Exception $e) {
            if ($this->debug) {
                throw new MulticanalException($e);
            }
            return false;
        }
    }
}
