<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Repository;

use Exception;
use Multicanal\Entity\DistributionList;
use Multicanal\Entity\Exception\MulticanalException;

/**
 * Class DistributionListRepository for Distribution List CRUD interaction with database through Doctrine ORM.
 *
 * @package Multicanal\Repository
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
class DistributionListRepository extends Repository {

    /**
     * Find all distributionLists from database.
     *
     * @return array
     * @access public
     */
    public function findAll(): array {
        /** @var DistributionList[] */
        return $this->em
            ->getRepository(DistributionList::class)
            ->findAll();
    }

    /**
     * Find distributionLists by variable criteria from database.
     *
     * @param array $fields Fields criteria as array
     * @param string|null $limit Results limit size
     * @param string|null $offset Search offset
     * @return array
     * @access public
     */
    public function findBySize(array $fields, ?string $limit = '0', ?string $offset = '100'): array {
        /** @var DistributionList[] */
        return $this->em
            ->getRepository(DistributionList::class)
            ->findBy($fields, ['id' => 'DESC'], $limit, $offset);
    }

    /**
     * Find a specific distributionList by its identifier.
     *
     * @param int $id DistributionList identifier
     * @return DistributionList|null
     * @access public
     */
    public function findById(int $id): ?DistributionList {
        /** @var DistributionList */
        return $this->em
            ->getRepository(DistributionList::class)
            ->find($id);
    }

    /**
     * Save a distributionList in database.
     *
     * @param DistributionList $distributionList Distribution list object to save
     * @return bool
     * @throws MulticanalException only in debug context
     * @access public
     */
    public function save(DistributionList &$distributionList): bool {
        try {
            $this->em->persist($distributionList);
            $this->em->flush($distributionList);
            return true;
        } catch (Exception $e) {
            if ($this->debug) {
                throw new MulticanalException($e);
            }
            return false;
        }
    }

    /**
     * Delete a distributionList from database.
     *
     * @param DistributionList $distributionList Distribution list object to delete
     * @return bool
     * @throws MulticanalException only in debug context
     * @access public
     */
    public function delete(DistributionList $distributionList): bool {
        try {
            $this->em->remove($distributionList);
            $this->em->flush($distributionList);
            return true;
        } catch (Exception $e) {
            if ($this->debug) {
                throw new MulticanalException($e);
            }
            return false;
        }
    }
}
