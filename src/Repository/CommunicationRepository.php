<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Repository;

use DateTime;
use Exception;
use Multicanal\Entity\Communication;
use Multicanal\Entity\Exception\MulticanalException;

/**
 * Class CommunicationRepository for Communication CRUD interaction with database through Doctrine ORM.
 *
 * @package Multicanal\Repository
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
class CommunicationRepository extends Repository {

    /**
     * Find a specific communication by its identifier.
     *
     * @param int $id Communication identifier
     * @return Communication|null
     * @access public
     */
    public function findById(int $id): ?Communication {
        return $this->em
            ->getRepository(Communication::class)
            ->find($id);
    }

    /**
     * Find all communication from database.
     *
     * @return array
     * @access public
     */
    public function findAll(): array {
        /** @var Communication[] */
        return $this->em
            ->getRepository(Communication::class)
            ->findAll();
    }

    /**
     * Find Communication by email not sent by published content.
     *
     * @return array
     * @access public
     */
    public function findPublishedNotSent(): array {
        /** @var Communication[] */
        return $this->em->createQueryBuilder()
            ->select('o')
            ->from(Communication::class, 'o')
            ->innerJoin('o.content', 'content')
            ->andWhere('o.channel IN (:channel)')
            ->andWhere('o.sent = :sent')
            ->andWhere('content.status = :publish')
            ->setParameter('publish', 'PUBLISHED')
            ->setParameter('channel', ['EMAIL', 'SMS'])
            ->setParameter('sent', false)
            ->getQuery()
            ->getResult();
    }

    /**
     * Flag a communication as sent.
     *
     * @param Communication $communication Communication to update
     * @return bool
     * @throws MulticanalException only in debug context
     * @access public
     */
    public function setSent(Communication $communication): bool {
        try {
            $communication->setSent(true);
            $communication->setSendingDate(new DateTime('NOW'));
            return $this->save($communication);
        } catch (Exception $e) {
            if ($this->debug) {
                throw new MulticanalException($e);
            }
            return false;
        }
    }

    /**
     * Save a communication in database.
     *
     * @param Communication $communication Communication object to save
     * @return bool
     * @throws MulticanalException only in debug context
     * @access public
     */
    public function save(Communication $communication): bool {
        try {
            $this->em->persist($communication);
            $this->em->flush($communication);
            return true;
        } catch (Exception $e) {
            if ($this->debug) {
                throw new MulticanalException($e);
            }
            return false;
        }
    }

    /**
     * Delete a communication from database.
     *
     * @param Communication $communication Communication object to delete
     * @return bool
     * @throws MulticanalException only in debug context
     * @access public
     */
    public function delete(Communication $communication): bool {
        try {
            $this->em->remove($communication);
            $this->em->flush($communication);
            return true;
        } catch (Exception $e) {
            if ($this->debug) {
                throw new MulticanalException($e);
            }
            return false;
        }
    }
}
