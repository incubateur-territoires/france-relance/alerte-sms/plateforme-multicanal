<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Utils;

use DateTime;
use GuzzleHttp\Exception\ClientException;
use Multicanal\App\Helper;
use Multicanal\Entity\Communication;
use Multicanal\Entity\SMS;
use Ovh\Exceptions\InvalidParameterException;
use Ovh\Sms\SmsApi as SMSApi;

/**
 * SMS utils class to send SMS through OVH API.
 *
 * @package Multicanal\Utils
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
class SMSUtils {

    private static SMSApi $api;

    /**
     * Publish a content through a SMS communication.
     *
     * @param Communication $communication SMS communication with its content
     * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return bool
     */
    public static function publishContentBySMS(Communication $communication): bool {
        $sms = new SMS(
            $communication->getContent()->getDistributionList()->getMobileSubscribersList(),
            $communication->getText() . ' ' . $communication->getContent()->getShortUrl()
        );

        if (self::checkSMS($sms)) {
            return self::sendSMS($sms);
        } else {
            return false;
        }
    }

    /**
     * Check if a SMS object is valid, by the way initialize the OVH SMS Api.
     *
     * @param SMS $sms SMS to check
     * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
     * @return bool
     */
    public static function checkSMS(SMS $sms): bool {
        $status = true;
        self::initSMSApi();
        if (empty(self::getSmsAccount())) {
            $status = false;
        }
        if (empty($sms->getMessage())) {
            $status = false;
        }
        if (empty($sms->getMobiles())) {
            $status = false;
        }
        return $status;
    }

    /**
     * Send a SMS through OVH SMS Api.
     *
     * @param SMS $sms SMS to send
     * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
     * @return bool
     */
    public static function sendSMS(SMS $sms): bool {
        if (!self::checkSMS($sms)) {
            return false;
        }
        // Get available SMS accounts
        self::$api->setAccount(self::getSmsAccount());
        $receivers  = $sms->getMobiles();
        $body       = $sms->getMessage();
        $message    = self::$api->createMessage(true);

        try {
            $sender = self::getSmsSenders();
            if (!empty($sender)) {
                $message->setSender($sender);
            }
        } catch (InvalidParameterException $e) {
            return false;
        }
        try {
            foreach ($receivers as $receiver) {
                $message->addReceiver(self::formatMobileNumberToInternationalNumber($receiver));
            }
            $message->setIsMarketing(false);
        } catch (InvalidParameterException $e) {
            return false;
        }

        if (Helper::config('env.development')) {
            $datetime = new DateTime('NOW');
            $datetime->modify('+5 minutes');
            // Plan to send it in the future
            try {
                $message->setDeliveryDate($datetime);
            } catch (InvalidParameterException $e) {
                return false;
            }
        }

        try {
            $message->send($body);
        } catch (ClientException | InvalidParameterException $e) {
            return false;
        }

        if (Helper::config('env.development')) {
            try {
                $plannedMessages = self::$api->getPlannedMessages();
                foreach ((array)$plannedMessages as $planned) {
                    $planned->delete();
                }
            } catch (InvalidParameterException $e) {
                return false;
            }
        }

        return true;
    }

    /**
     * Initialize a SMSApi object to call OVH SMS Api.
     *
     * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return void
     */
    private static function initSMSApi(): void {
        self::$api = new SMSApi(
            Helper::config('ovh.application_key'),
            Helper::config('ovh.application_secret'),
            Helper::config('ovh.endpoint'),
            Helper::config('ovh.consumer_key')
        );
    }

    /**
     * Get the main SMS account.
     *
     * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
     * @return string
     */
    private static function getSmsAccount(): string {
        $accounts = self::$api->getAccounts();
        return $accounts[0] ?? '';
    }

    /**
     * Get the main SMS sender attached to the OVH account.
     *
     * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
     * @return string
     */
    private static function getSmsSenders(): string {
        $senders = self::$api->getSenders();
        return $senders[0] ?? '';
    }

    /**
     * Format a mobile phone number to international format.
     *
     * @param string $phoneNumber Unformatted mobile phone number
     * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
     * @return string
     */
    private static function formatMobileNumberToInternationalNumber(string $phoneNumber): string {
        $phoneNumber = str_replace(' ', '', $phoneNumber);
        if (str_starts_with($phoneNumber, '+33') || str_starts_with($phoneNumber, '0033')) {
            return $phoneNumber;
        }
        if (str_starts_with($phoneNumber, '06') || str_starts_with($phoneNumber, '07')) {
            return substr_replace($phoneNumber, '+33', 0, 1);
        }
        return $phoneNumber;
    }
}
