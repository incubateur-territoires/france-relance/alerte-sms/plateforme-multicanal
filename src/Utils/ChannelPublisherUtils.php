<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Utils;

use Multicanal\Entity\Communication;
use Multicanal\Entity\SocialNetwork\FacebookChannel;
use Multicanal\Entity\SocialNetwork\InstagramChannel;
use Multicanal\Entity\SocialNetwork\TwitterChannel;
use Multicanal\Lib\ChannelPublisher\ChannelPublisher;
use Multicanal\Lib\ChannelPublisher\FacebookChannelPublisher;
use Multicanal\Lib\ChannelPublisher\InstagramChannelPublisher;
use Multicanal\Lib\ChannelPublisher\TwitterChannelPublisher;

/**
 * Utils class to send data.
 *
 * @package Multicanal\Utils
 * @author  Nathanael KHODL <consulting@khodl.com>
 */
class ChannelPublisherUtils {
    public static function sendCommunication(Communication $communication): bool {

        if ($communication->getChannel() !== 'CHANNEL') {
            throw new \LogicException(
                'Channel not supported by the channel publisher: ' . $communication->getChannel()
            );
        }

        $publisher = self::getPublisher($communication);

        return $publisher->publish($communication);
    }

    public static function getPublisher(Communication $communication): ChannelPublisher {

        $channel = $communication->getOrganisationChannel();
        if ($channel instanceof TwitterChannel) {
            return new TwitterChannelPublisher();
        }
        if ($channel instanceof FacebookChannel) {
            return new FacebookChannelPublisher();
        }
        if ($channel instanceof InstagramChannel) {
            return new InstagramChannelPublisher();
        }

        throw new \LogicException('Channel not supported: ' . get_class($channel));
    }
}
