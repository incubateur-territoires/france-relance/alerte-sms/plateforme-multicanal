<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Utils;

use Multicanal\Entity\ApiResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Utils class 4 API.
 *
 * @package Multicanal\Utils
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
class APIUtils {

    protected ServerRequestInterface $request;

    protected ResponseInterface $response;

    /**
     * Call an url
     *
     * @param string $url Endpoint domain URL
     * @param string $authentication Service authentication
     * @param string $method (optional) HTTP method, GET by default
     * @param array $headerParams (optional) parameter in header of the request
     * @param string $params (optional) Parameters as string, null by default
     * @param bool $isParamsJson (optional) Flag to know if paramters are passed as JSON object or string, false by default
     * @return mixed
     * @access public
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     */
    public static function callUrl(string $url, string $authentication = null, ?string $method = 'GET', ?array $headerParams = null, ?string $params = null, ?bool $isParamsJson = false): mixed {
        $headerHttp = [];
        $ch         = curl_init();
        $options    = [
            CURLOPT_URL            => $url,
            CURLOPT_HEADER         => 0,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST  => $method
        ];

        curl_setopt_array($ch, $options);

        if ($isParamsJson) {
            array_push($headerHttp, 'Cache-Control: no-cache');
            array_push($headerHttp, 'content-type:application/json;charset=utf-8');
        }
        if ($headerParams != null) {
            foreach ($headerParams as $headerParam) {
                array_push($headerHttp, $headerParam);
            }
        }
        if (!empty($headerHttp)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headerHttp);
        }
        if ($params != null) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        }
        if ($authentication != null) {
            curl_setopt($ch, CURLOPT_USERPWD, $authentication);
        }

        $response = json_decode(curl_exec($ch));
        if (curl_errno($ch) != 0) {
            return curl_error($ch);
        }
        curl_close($ch);

        return $response;
    }

    /**
     * Prepare response object with status, header and object results in JSON format.
     *
     * @param Response $response Response from Slim framework routing
     * @param integer $code HTTP status code
     * @param string $message Query status message
     * @param float $startTime Query start time to calculate query execution time
     * @param bool $success (optional) Flag to indicate if the query was successful
     * @param string $results (optional) Results JSON encoded as string
     * @author Thierry CEREYON <t.cereyon@groupeonepoint.com>
     * @return ResponseInterface
     * @access public
     */
    public static function writeResponse(
        ResponseInterface $response,
        int $code,
        string $message,
        float $startTime,
        ?bool $success = true,
        ?string $results = null
    ): ResponseInterface {
        $apiResponse = new ApiResponse($code, $message, $success, $results);
        $apiResponse->setTime(round(microtime(true) * 1000 - $startTime));
        $response->getBody()->write(json_encode($apiResponse->jsonSerialize(), JSON_THROW_ON_ERROR));
        return $response->withHeader('Content-Type', 'application/json');
    }
}
