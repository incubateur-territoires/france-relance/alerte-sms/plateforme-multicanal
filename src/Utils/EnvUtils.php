<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Utils;

/**
 * Utils class to manipulate values form dotEnv.
 *
 * @package Multicanal\Utils
 * @author  William Blanc dit Jolicoeur <w.blanc-dit-jolicoeur@girondenumerique.fr>
 */
class EnvUtils {
    public const ITEM_DEFAULT_SEPARATOR = ' ';
    public const KEYVALUE_DEFAULT_SEPARATOR = ':';

    /**
     * Return the boolan of the input handling 'true' and 'false' strings.
     * @param mixed $value
     *
     * @return bool
     */
    public static function evalBool($value): bool {
        if (gettype($value) !== 'string') {
            return (bool) $value;
        }
        return (strcasecmp($value, 'true') ? false : true);
    }

    /**
     * Take a string from .env 'item1 item2 item3' and return a php array of values ['item1', 'item2', 'item3'].
     *
     * @param mixed $value
     * @param string $itemSeparator
     *
     * @return array<int, string>
     */
    public static function toValueArray($value, string $itemSeparator = self::ITEM_DEFAULT_SEPARATOR): array {
        if (is_null($value)) {
            return [];
        }
        $items = explode($itemSeparator, trim($value));
        return array_values(array_filter($items));
    }

    /**
     * Take a string from .env 'key1:value1 key2:value2'
     * and return a php keyed array ['key1' => 'value1', 'key2' => 'value2'].
     *
     * @param mixed $value
     * @param string $itemSeparator
     * @param string $keyValueSeparator
     *
     * @return array<string, string>
     */
    public static function toKeyedArray(
        $value,
        string $itemSeparator = self::ITEM_DEFAULT_SEPARATOR,
        string $keyValueSeparator = self::KEYVALUE_DEFAULT_SEPARATOR
    ): array {
        $items = self::toValueArray($value, $itemSeparator);
        $keyedArray = [];
        foreach ($items as $item) {
            [$key, $val] = explode($keyValueSeparator, $item);
            $keyedArray[$key] = $val;
        }
        return $keyedArray;
    }
}
