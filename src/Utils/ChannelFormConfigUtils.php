<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Utils;

use Multicanal\Entity\SocialNetwork\Channel;
use Multicanal\Entity\SocialNetwork\FacebookChannel;
use Multicanal\Entity\SocialNetwork\InstagramChannel;
use Multicanal\Entity\SocialNetwork\TwitterChannel;
use Multicanal\Lib\ChannelPublisher\ChannelFormConfig;

/**
 * Utils class to generate form config.
 *
 * @package Multicanal\Utils
 * @author  Nathanael KHODL <consulting@khodl.com>
 */
class ChannelFormConfigUtils {

    public static function getChannelFormConfig(Channel $channel): ChannelFormConfig {


        if ($channel instanceof FacebookChannel) {
            return $channel->formConfig = new ChannelFormConfig(
                useContent: true,
            );
        }

        if ($channel instanceof TwitterChannel) {
            return $channel->formConfig = new ChannelFormConfig(
                useTitle: true
            );
        }

        if ($channel instanceof InstagramChannel) {
            return $channel->formConfig = new ChannelFormConfig(
                useMedia: true,
                useContent: true,
            );
        }

        throw new \LogicException('Channel of type ' . get_class($channel) . ' is not supported');
    }
}
