<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Utils;

use Multicanal\App\Helper;
use Multicanal\Entity\Exception\MulticanalException;
use Multicanal\Entity\MulticanalFile;
use Slim\Psr7\UploadedFile;
use finfo;

/**
 * Utils class to manage MulticanalFiles.
 *
 * @package Multicanal\Utils
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
class MulticanalFilesUtils {
    /**
     * Extract files to a Multicanal file.
     *
     * @param mixed $imageFile Image file
     * @param $directory Destination directory
     * @param string $name Image name
     * @return string
     * @throws MulticanalException
     */
    public static function extractFile(mixed &$imageFile, $directory, string $name): string {
        $uploadSettings = Helper::config('genericImageUploader');
        self::validateUploadedFile($imageFile, $uploadSettings);

        return MulticanalFilesUtils::moveUploadedFile($directory, $imageFile, StringUtils::normalize($name, '_', false));
    }


    /**
     * Moves the uploaded file to the upload directory and assigns it a unique name
     * to avoid overwriting an existing uploaded file.
     *
     * @param string $directory The directory to which the file is moved
     * @param UploadedFile $uploadedFile The file uploaded file to move
     * @param string $name Image filename
     * @author Thierry CEREYON <t.cereyon@groupeonepoint.com>
     * @return string The filename of moved file
     * @access public
     */
    public static function moveUploadedFile(string $directory, UploadedFile $uploadedFile, string $name): string {
        $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        $filename  = sprintf('%s.%0.8s', $name, $extension);
        if (!is_dir($directory)) {
            mkdir($directory, 0755, true);
        }
        $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

        return $filename;
    }

    /**
     * Validate uploaded file function.
     *
     * @param UploadedFile $uploadedFile Uploaded file from Slim HTTP request
     * @param array $uploadSettings Settings as array
     * @throws MulticanalException
     * @return void
     */
    public static function validateUploadedFile(UploadedFile $uploadedFile, array $uploadSettings): void {
        $allowedMimeTypes = $uploadSettings['allowed_mimetypes'];
        if (!self::validateUploadedFileMimetype($uploadedFile, $allowedMimeTypes)) {
            throw new MulticanalException('Type MIME du fichier non autorisé, sont acceptés les types suivants [' . implode(', ', $allowedMimeTypes) . '].');
        }

        $allowedExtensions = $uploadSettings['allowed_extensions'];
        if (!self::validateUploadedFileExtension($uploadedFile, $allowedExtensions)) {
            throw new MulticanalException('Extension du fichier non autorisée, sont acceptés les extensions suivantes [' . implode(', ', $allowedExtensions) . '].');
        }

        $maxFileSize = $uploadSettings['max_filesize'];
        if (!self::validateUploadedFileSize($uploadedFile, $uploadSettings['max_filesize'])) {
            throw new MulticanalException('Taille du fichier excédant la limite autorisée de ' . $maxFileSize . ' Mo.');
        }
    }

    /**
     * Validate uploaded file mimetype function.
     *
     * @param UploadedFile $uploadedFile Uploaded file from Slim HTTP request
     * @param array $allowedMimetypes Allowed mimetypes as array
     * @return bool
     */
    public static function validateUploadedFileMimetype(UploadedFile $uploadedFile, array $allowedMimetypes): bool {
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $mime  = $finfo->file($uploadedFile->getFilePath());
        return in_array($mime, $allowedMimetypes);
    }

    /**
     * Validate uploaded file extension function.
     *
     * @param UploadedFile $uploadedFile Uploaded file from Slim HTTP request
     * @param array $allowedExtensions Allowed extensions as array
     * @return bool
     */
    public static function validateUploadedFileExtension(UploadedFile $uploadedFile, array $allowedExtensions): bool {
        $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        return in_array($extension, $allowedExtensions);
    }

    /**
     * Validate uploaded filesize function.
     *
     * @param UploadedFile $uploadedFile Uploaded file from Slim HTTP request
     * @param int $allowedMaxSize Allowed max filesize
     * @return bool
     */
    public static function validateUploadedFileSize(UploadedFile $uploadedFile, int $allowedMaxSize): bool {
        return $uploadedFile->getSize() < $allowedMaxSize;
    }

    /**
     * Check if the file is an image.
     *
     * @param MulticanalFile $file File to check
     * @return bool
     */
    public static function isImage(?MulticanalFile $file): bool {
        if (!$file) {
            return false;
        }
        return strpos($file->getMimeType(), 'image/') === 0;
    }

    /**
     * Check if the file is a video.
     *
     * @param MulticanalFile $file File to check
     * @return bool
     */
    public static function isVideo(?MulticanalFile $file): bool {
        if (!$file) {
            return false;
        }
        return strpos($file->getMimeType(), 'video/') === 0;
    }

    /**
     * Returns the url of the file, including the domain
     *
     * @param MulticanalFile $file
     * @return string The file url
     */
    public static function getFileUrl(MulticanalFile $file): string {
        return Helper::getAppDomain() . $file->getUrl();
    }
}
