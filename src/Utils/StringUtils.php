<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Utils;

use Symfony\Component\String\AbstractUnicodeString;
use Symfony\Component\String\Slugger\AsciiSlugger;

/**
 * Utils class to manipulate string.
 *
 * @package Multicanal\Utils
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class StringUtils {

    /**
     * Normalize string with replacing specials characters with an optional char.
     * If it's a filename with extension, the extension is saved.
     *
     * @param string $text String to normalize
     * @param string $space Optional replacement character ("_" by default)
     * @param boolean $unify Optional flag to unify string with current date
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return string
     * @access public
     */
    public static function normalize(string $text, ?string $space = '_', ?bool $unify = true): string {
        if ($unify) {
            $extension = pathinfo($text, PATHINFO_EXTENSION);   // Save extension
            $text = rtrim($text, $extension);
            $extension = preg_replace('/[\W]+/', '', $extension);
        }
        $text = str_replace(' ', '_', $text);
        $text = self::removeSpecialsChars($text);               // Remove specials characters
        $text = preg_replace('/[\W]+/', '', $text);             // Replace others specials chars
        $text = strtolower($text);                              // In minus
        $text = trim($text);                                    // Delete space before and end
        $text = substr($text, 0, 50);                           // Limit size to 50 !
        $text = ltrim($text, $space);                           // Delete replacement char on left
        $text = rtrim($text, $space);                           // Delete replacement char on right
        $text = str_replace(' ', '', str_replace('-', ' ', $text));

        if ($unify) {
            $text = $text . $space . uniqid();

            // If it's a file with an extension
            if (!empty($extension)) {
                $text = $text . '.' . $extension;
            }
        }
        return $text;
    }

    /**
     * Function to delete accents
     *
     * @param  string $string to delete accents
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return string
     * @access public
     */
    public static function removeSpecialsChars(string $string): string {
        $utf8 = [
            '/[áàâãªä]/u'   =>   'a',
            '/[ÁÀÂÃÄ]/u'    =>   'A',
            '/[ÍÌÎÏ]/u'     =>   'I',
            '/[íìîï]/u'     =>   'i',
            '/[éèêë]/u'     =>   'e',
            '/[ÉÈÊË]/u'     =>   'E',
            '/[óòôõºö]/u'   =>   'o',
            '/[ÓÒÔÕÖ]/u'    =>   'O',
            '/[úùûü]/u'     =>   'u',
            '/[ÚÙÛÜ]/u'     =>   'U',
            '/ç/'           =>   'c',
            '/Ç/'           =>   'C',
            '/ñ/'           =>   'n',
            '/Ñ/'           =>   'N',
            '/–/'           =>   '-', // UTF-8 hyphen to "normal" hyphen
            '/[’‘‹›‚]/u'    =>   ' ', // Literally a single quote
            '/[“”«»„]/u'    =>   ' ', // Double quote
            '/ /'           =>   ' ', // Nonbreaking space (equiv. to 0x160)
        ];

        return preg_replace(array_keys($utf8), array_values($utf8), $string);
    }

    /**
     * Check if a string haystack begins with a specific string needle.
     *
     * @param string $haystack String haystack to test
     * @param string $needle String needle for test
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return bool
     * @access public
     */
    public static function startsWith(string $haystack, string $needle): bool {
        return substr_compare($haystack, $needle, 0, strlen($needle)) === 0;
    }

    /**
     * Check if a string haystack ends with a specific string needle.
     *
     * @param string $haystack String haystack to test
     * @param string $needle String needle for test
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return bool
     * @access public
     */
    public static function endsWith(string $haystack, string $needle): bool {
        return substr_compare($haystack, $needle, -strlen($needle)) === 0;
    }

    /**
     * Shortcut to Symfony Slugger method
     * In some contexts, such as URLs and file/directory names, it's not safe to use any Unicode character.
     * A slugger transforms a given string into another string that only includes safe ASCII characters.
     *
     * @param string $string String to slug
     * @param string $separator Optional separator, - by default
     * @param string $locale Optional locale, null by default
     * @author William BLANC DIT JOLICOEUR <w.blanc-dit-jolicoeur@girondenumerique.fr>
     * @return AbstractUnicodeString
     * @access public
     */
    public static function slug(string $string, string $separator = '-', string $locale = null): AbstractUnicodeString {
        $slugger = new AsciiSlugger();
        return $slugger->slug($string, $separator, $locale);
    }

    /**
     * Check if a string identifier is numeric and has a positive value
     *
     * @param string $id Identifier as string
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return bool
     * @access public
     */
    public static function checkNumericIdentifier(string $id): bool {
        return !empty($id) && is_numeric($id) && (int) $id !== 0;
    }
}
