<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Utils;

use Multicanal\App\Helper;
use Multicanal\Entity\Exception\MulticanalException;
use Multicanal\Middleware\ApiAuthorizationMiddleware;

/**
 * Utils class to use GN URL minifier API.
 *
 * @package Multicanal\Utils
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class UrlMinifierUtils {

    /**
     * Consume GNUrlMinifier API to minify an URL.
     *
     * @param string $url URL to minify
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return string
     * @throws MulticanalException
     * @access public
     */
    public static function getReduceUrl(string $url): string {

        $result = APIUtils::callUrl(
            Helper::config('app.shortener_api_url') . '/reduce',
            null,
            'POST',
            [ApiAuthorizationMiddleware::API_KEY_HEADER_NAME . ': ' . Helper::config('app.api_key')],
            $url,
            true
        );

        if (is_object($result) && $result->code == 200 && $result->success) {
            return $result->results;
        } elseif (is_object($result) && $result->code == 500 && !$result->success) {
            throw new MulticanalException($result->message);
        } else {
            throw new MulticanalException('Une erreur est survenue lors de l\'appel au réducteur d\'URL.');
        }
    }

    /**
     * Consume GNUrlMinifier API to get a base64 encoded QRCode image URL.
     *
     * @param string $url URL to transform on QR code image
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return string
     * @throws MulticanalException
     * @access public
     */
    public static function getQRCode(string $url): string {
        $result = APIUtils::callUrl(
            Helper::config('app.shortener_api_url') . '/qr',
            null,
            'POST',
            [ApiAuthorizationMiddleware::API_KEY_HEADER_NAME . ': ' . Helper::config('app.api_key')],
            $url,
            true
        );

        if (is_object($result) && $result->code == 200 && $result->success) {
            return $result->results;
        } elseif (is_object($result) && $result->code == 500 && !$result->success) {
            throw new MulticanalException($result->message);
        } else {
            throw new MulticanalException('Une erreur est survenue lors de l\'appel au générateur de QR code.');
        }
    }
}
