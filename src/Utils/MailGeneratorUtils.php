<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Utils;

use Multicanal\App\Helper;
use Multicanal\Entity\Communication;
use Multicanal\Entity\Content;
use Multicanal\Entity\Email;
use Multicanal\Entity\Exception\MulticanalException;
use Multicanal\Entity\Organization;
use Multicanal\Entity\User;
use PHPMailer\PHPMailer\Exception;
use Slim\Views\Twig;

/**
 * Utils class to send HTML email with template.
 *
 * @package Multicanal\Utils
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class MailGeneratorUtils {

    protected $twig;

    public function __construct(Twig $twig) {
        $this->twig = $twig;
    }

    /**
     * Send an email with organization template.
     *
     * @param Email $email Email to send
     * @param Organization $organization Current organization from userDb
     * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return bool
     * @access public
     */
    public function sendEmailWithOrganizationTemplate(Email $email, Organization $organization): bool {
        $params = [
            'organization'  => $organization,
            'email'         => $email
        ];

        $email->setBody($this->setHtmlMessage('template', $params));
        $email->setReplyTo($organization->getGeneralParameters()->getLabel());
        $email->setReplyToEmail($organization->getEmailParameters()->getReplyToEmail());

        return $this->sendEmail($email);
    }

    /**
     * Publish a content through an email communication.
     *
     * @param Communication $communication Email communication with its content
     * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return bool
     * @access public
     */
    public function publishContentByEmail(Communication $communication): bool {
        $organization = $communication->getContent()->getCategory()->getOrganization();
        $object       = $communication->getObject();

        $email = new Email(
            $communication->getContent()->getDistributionList()->getEmailSubscribersList(),
            $object,
            $this->setHtmlMessage('content-publication', ['communication' => $communication])
        );
        $email->setReplyTo($organization->getGeneralParameters()->getLabel());
        $email->setReplyToEmail($organization->getEmailParameters()->getReplyToEmail());

        return $this->sendEmail($email);
    }

    /**
     * Publish a content through an email communication.
     *
     * @param array $recipient Recipient of the sharing email
     * @param Organization $organization Current organization from userDb
     * @param User $user User who share the content
     * @param Content $content Content to share by email
     * @param string $message Personnalized message from current user
     * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return bool
     * @access public
     */
    public function shareContentByEmail(array $recipient, Organization $organization, User $user, Content $content, string $message): bool {
        $object = $organization->getGeneralParameters()->getLabel() . ' - ' . $user->getFirstname() . ' ' . $user->getLastname() . ' souhaite vous partager un contenu';

        $params = [
            'organization'  => $organization,
            'user'          => $user,
            'content'       => $content,
            'message'       => $message
        ];

        $email = new Email(
            $recipient,
            $object,
            $this->setHtmlMessage('content-sharing', $params)
        );
        $email->setReplyTo($organization->getGeneralParameters()->getLabel());
        $email->setReplyToEmail($organization->getEmailParameters()->getReplyToEmail());

        return $this->sendEmail($email);
    }

    /**
     * Check if an email object is correctly instancied.
     *
     * @param Email $email Email to check
     * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
     * @return bool
     * @access public
     */
    public static function checkEmail(Email $email): bool {
        $status = true;
        if (empty($email->getEmails())) {
            $status = false;
        }
        if (empty($email->getObject())) {
            $status = false;
        }
        if (empty($email->getBody())) {
            $status = false;
        }

        return $status;
    }

    /**
     * Send an email through PHPMailer.
     *
     * @param Email $email
     * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
     * @return bool
     * @throws MulticanalException
     * @access public
     */
    public static function sendEmail(Email $email): bool {
        if (!self::checkEmail($email)) {
            return false;
        }

        try {
            $mailer = Helper::getMailer();

            // Reply-to
            if (!empty($email->getReplyToEmail())) {
                $mailer->addReplyTo($email->getReplyToEmail(), $email->getReplyTo());
            }

            // Attachments
            foreach ($email->getMedias() as $mediaPath) {
                $mailer->addAttachment($mediaPath);
            }

            // Content body
            $mailer->isHTML(true);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        //Set email format to HTML
            $mailer->Subject = $email->getObject();
            $mailer->Body    = $email->getBody();
            $mailer->AltBody = preg_replace("`(<[^>]*>)(.*)(</[^>]*>)`Ui", "", $email->getBody());

            // Send to each recipient
            foreach ($email->getEmails() as $email) {
                $mailer->addAddress($email);
                $mailer->send();
                $mailer->clearAllRecipients();
            }

            return true;
        } catch (Exception $e) {
            if (Helper::isDebugEnabled()) {
                throw new MulticanalException($e);
            }
            return false;
        }
    }

    /**
     * Generate an HTML email from TWIG template.
     *
     * @param string $template Template identifier
     * @param array $params Values array
     * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return string
     * @access private
     */
    private function setHtmlMessage(string $template, ?array $params): string {
        return $this->twig->fetchBlock('email/' . $template . '.html', 'email', $params);
    }
}
