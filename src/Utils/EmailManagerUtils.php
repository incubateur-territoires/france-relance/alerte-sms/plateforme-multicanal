<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Utils;

use EmailReplyParser\Fragment;
use EmailReplyParser\Parser\EmailParser;
use Exception;
use League\CommonMark\CommonMarkConverter;
use League\HTMLToMarkdown\HtmlConverter;
use PhpImap\IncomingMail;
use PhpImap\IncomingMailAttachment;
use PhpImap\Mailbox;

/**
 * Utils class to manage email information.
 *
 * @package Multicanal\Utils
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class EmailManagerUtils {
    private Mailbox $mailbox;

    /**
     * Constructor.
     *
     * @param string $host {host:port\params}BOX voir http://fr.php.net/imap_open
     * @param string $login
     * @param string $password
     * @throws Exception
     */
    public function __construct(string $host, string $login, string $password, ?string $directory = null) {
        $this->mailbox = new Mailbox(
            $host,
            $login,
            $password,
            $directory,
            'UTF-8',
            true,
            true
        );

        $this->mailbox->setConnectionArgs(CL_EXPUNGE);
    }

    /**
     * Check mailbox configuration.
     *
     * @return object
     */
    public function checkConfiguration(): object {
        return $this->mailbox->checkMailbox();
    }

    /**
     * Get email number of attachments.
     *
     * @param integer $emailNumber Email number
     * @return int
     */
    public function getNbOfAttachments(int $emailNumber): int {
        $email = $this->mailbox->getMail($emailNumber);
        return count($email->getAttachments());
    }

    /**
     * Get email attachments.
     *
     * @param integer $emailNumber Email number
     * @return array
     */
    public function getAttachments(int $emailNumber): array {
        $email = $this->mailbox->getMail($emailNumber);
        return $email->getAttachments();
    }

    /**
     * Get email attachments.
     *
     * @param IncomingMail $emailNumber Email number
     * @return array
     */
    public function getEmailAttachments(IncomingMail $email): array {
        return $email->getAttachments();
    }

    /**
     * Get email list from a mailbox.
     *
     * @return array
     */
    public function getEmailList(): array {
        $emailList = [];
        $emailIds = $this->mailbox->searchMailbox();
        $mails = $this->mailbox->getMailsInfo($emailIds);
        foreach ($mails as $email) {
            $emailList[]         = [
                'subjectHTML'   => (string) htmlspecialchars($this->getEmailSubject($email)),
                'fromHTML'      => htmlspecialchars($this->getEmailSender($email)),
                'toHTML'        => $this->getEmailRecipient($email),
                'emailNumber'   => $email->uid,
                'date'          => $email->date
            ];
        }
        return array_reverse($emailList);
    }

    /**
     * Save an email attachment file in a specific folder with an unique filename.
     *
     * @param mixed $attachmentFile Attachment file data
     * @param string $directory Destination directory
     * @return string
     **/
    public function saveAttachment(IncomingMailAttachment $attachment, string $directory): string {
        if (!is_dir($directory)) {
            mkdir($directory, 0755, true);
        }
        $filename = $attachment->name;
        $filepath = $directory . '/' . $filename;
        $tmp      = explode('.', $filename);
        $ext      = array_pop($tmp);
        $filename = implode('.', $tmp);
        $i        = 1;

        while (file_exists($filepath)) {
            $filepath = $directory . '/' . $filename . $i . '.' . $ext;
            $i++;
        }

        $fp = fopen($filepath, 'w');
        fputs($fp, $attachment->getContents());
        fclose($fp);

        return $filepath;
    }


    /**
     * Returns the attachment based on the email number and an attachment identifier.
     * The attachement id is changing, so the identifier is calculated.
     *
     * @param string $emailNumber
     * @param string $attachmentId The calculated identifier of the attachment.
     * @return IncomingMailAttachment|null
     */
    public function getAttachment(string $emailNumber, string $attachmentId): ?IncomingMailAttachment {
        $email = $this->mailbox->getMail($emailNumber);
        foreach ($email->getAttachments() as $attachment) {
            if ($this->getAttachmentIdentifier($attachment) === $attachmentId) {
                return $attachment;
            }
        }
        return null;
    }

    /**
     * Get email sender.
     *
     * @param integer $emailNumber Email number
     * @return string
     */
    public function getEmailSender($email): string {
        return $email->from ?? "-";
    }

    /**
     * Get email recipient.
     *
     * @param \stdClass $email Email data
     * @return string
     */
    public function getEmailRecipient($email): string {
        return $email->to ?? "-";
    }

    /**
     * Returns a data array with address and name
     *
     * @param string $email
     * @param string|null $name
     * @return array
     */
    private function getUserAddressAndName(string $email, ?string $name): array {
        return [
            'address' => $email,
            'name' => $name
        ];
    }

    /**
     * Put the clean email addresses data in an array.
     *
     * @param array $fromOrTo Email addresses
     * @return array
     */
    private function cleanEmailAddresses(array $fromOrTo): array {
        $addresses = [];
        foreach ($fromOrTo as $email => $name) {
            $addresses[] = $this->getUserAddressAndName($email, $name);
        }
        return $addresses;
    }

    /**
     * Get email recipient(s) data
     *
     * @param IncomingMail $email Email data
     * @return array
     */
    public function getEmailTo(IncomingMail $email): array {
        return $this->cleanEmailAddresses($email->to);
    }

    /**
     * Get email sender(s) data
     *
     * @param  IncomingMail $email Email data
     * @return array
     */
    public function getEmailFrom(IncomingMail $email): array {
        // As an array, in case there is a reply-to in the future
        return [
            $this->getUserAddressAndName($email->fromAddress, $email->fromName)
        ];
    }

    /**
     * Get email subject.
     *
     * @param int $emailNumber Email number
     * @return mixed
     */
    public function getSubject(int $emailNumber): mixed {
        $email = $this->mailbox->getMail($emailNumber);
        return $email->subject;
    }
    public function getEmailSubject($email): mixed {
        return $email->subject ?? "-";
    }

    /**
     * Get email body.
     *
     * @param int $emailNumber Email number
     * @return mixed
     */
    public function getBody(int $emailNumber): mixed {
        $email = $this->mailbox->getMail($emailNumber);

        return $email->textPlain;
    }

    /**
     * Returns the text or html segments of the email.
     *
     * @param IncomingMail $email
     * @return array|string[]
     */
    private function getBodySegments(IncomingMail $email): array {
        if (mb_strlen($email->textHtml) > 0) {
            return $this->splitHTMLBody($email->textHtml);
        } else {
            return $this->splitPlainTextBody($email->textPlain);
        }
    }

    /**
     * Splits an HTML body into multiple HTML segments
     *
     * @param string $html
     * @return array
     */
    private function splitHTMLBody(string $html): array {
        $htmlToMarkdownConverter = new HtmlConverter([
            'strip_tags' => false,
            'header_style' => 'atx',
            'use_autolinks' => false,
        ]);

        $html = $this->cleanHTML($html);

        $text = $htmlToMarkdownConverter->convert($html);

        $email = (new EmailParser())->parse($text);

        $fragments = array_filter($email->getFragments(), function (Fragment $fragment) {
            return !$fragment->isHidden();
        });

        if (count($fragments)) {
            $segments = [];
            $markdownToHtmlConverter = new CommonMarkConverter([
                'html_input' => 'strip',
                'allow_unsafe_links' => true,
            ]);
            foreach ($fragments as $fragment) {
                $segments[] = $markdownToHtmlConverter->convert($fragment->getContent())->getContent();
            }
            return $segments;
        } else {
            return [$html];
        }
    }

    /**
     * Removes HTML that is not useful for the text version of the email
     *
     * @param string $html Raw HTML
     * @return string Clean HTML
     */
    private function cleanHTML(string $html): string {

        // Load HTML in a DOMDocument
        $doc = new \DOMDocument();
        $doc->loadHTML($html);
        $xpath = new \DOMXPath($doc);

        // Delete tags not used for display, also removing their content
        $toDelete = ['//script', '//style', '//meta', '//link', '//head', '//title'];
        foreach ($toDelete as $query) {
            foreach ($xpath->query($query) as $node) {
                $node->parentNode->removeChild($node);
            }
        }

        // Replace images by their alt text
        $images = $xpath->query('//img');
        /** @var \DOMElement $imageNode */
        foreach ($images as $imageNode) {
            $name = $imageNode->getAttribute(trim('alt')) ?? "Image";
            $imageNode->parentNode->replaceChild($doc->createTextNode($name), $imageNode);
        }

        // Clean links
        $links = $xpath->query('//a');
        /** @var \DOMElement $linkNode */
        foreach ($links as $linkNode) {
            // Trim text content
            if ($linkNode->textContent) {
                $value = trim($linkNode->textContent);
                $linkNode->textContent = $value ?: "Lien";
            }
        }

        // Get HTML from DOMDocument
        $html = $doc->saveHTML();

        // Delete tags not used for display, without removing their content
        $html = strip_tags($html, allowed_tags: [
            'a', 'button',
            'b', 'strong', 'em', 'i',
            //'div', 'p', 'span', 'pre', 'code',
            'br',
            'ul', 'ol', 'li',
            'h1', 'h2', 'h3', 'h4', 'h5', 'h6'
        ]);

        return $html;
    }

    /**
     * Splits a plain text body into multiple segments
     *
     * @param string $text
     * @return array
     */
    private function splitPlainTextBody(string $text): array {
        $email = (new EmailParser())->parse($text);
        $fragments = array_filter($email->getFragments(), function (Fragment $fragment) {
            return !$fragment->isHidden();
        });

        return array_map(function (Fragment $fragment) {
            return $fragment->getContent();
        }, $fragments);
    }

    /**
     * Get the main/last segment of the email body.
     *
     * @param int $emailNumber Email number
     * @return string
     */
    public function getTransformedBody(int $emailNumber): string {
        $email = $this->mailbox->getMail($emailNumber);
        $segments = $this->getBodySegments($email);
        return nl2br(array_pop($segments));
    }

    /**
     * The identifier of the attachment.
     * The IncomingMailAttachment::id provided by the server appears to be changing over time.
     * @param IncomingMailAttachment $attachment
     * @return string
     */
    private function getAttachmentIdentifier(IncomingMailAttachment $attachment): string {
        return md5($attachment->name . $attachment->sizeInBytes . $attachment->mimeType);
    }

    /**
     * Get detailed data of attachments.
     *
     * @param IncomingMail $email Email data
     * @return array
     */
    private function getEmailDetailedAttachments(IncomingMail $email): array {
        $attachments = $this->getEmailAttachments($email);
        $list = [];

        $allowedPreview = ['image/png', 'image/jpeg', 'image/gif', 'image/bmp', 'image/webp'];

        foreach ($attachments as $attachment) {
            $list[] = [
                // The id is calculated, as the one provided by the server is changing over time
                'id'            => $this->getAttachmentIdentifier($attachment),
                'name'          => $attachment->name,
                'size'          => BytesUtils::formatSize($attachment->sizeInBytes),
                'type'          => $attachment->mimeType,
                'extension'     => strtok($attachment->fileExtension, '/'),
                'hasThumbnail'  => in_array($attachment->mimeType, $allowedPreview)
            ];
        }

        return $list;
    }

    /**
     * Get email detailed values, with segments and attachments informations
     *
     * @param int $emailNumber Email number
     * @return array
     */
    public function getEmailData(int $emailNumber): array {
        $email = $this->mailbox->getMail($emailNumber);
        return [
            'subject'       => $this->getEmailSubject($email),
            'segments'      => $this->getBodySegments($email),
            'emailNumber'   => $emailNumber,
            'to'            => $this->getEmailTo($email),
            'from'          => $this->getEmailFrom($email),
            'attachments'   => $this->getEmailDetailedAttachments($email)
        ];
    }
}
