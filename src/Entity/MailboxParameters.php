<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use JsonSerializable;

/**
 * Class EmailBoxParameters
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 *
 * @OA\Schema(
 *     description="Spécifications du modèle de la boite email.",
 *     title="Paramètres de la boite email de la collectivité",
 *     required={"mailbox", "folder", "user", "password"},
 *	   @OA\Xml(
 *        name="EmailParameters"
 *     )
 * )
 */
#[Entity, Table(name: 'email_box_parameters')]
class MailboxParameters implements JsonSerializable {

    /**
     * Email parameters identifier as autoincremented integer from database
     *
     * @OA\Property(
     *     description="Identifiant du paramètre d'emailing",
     *     title="Identifier",
     *     property="id",
     *     type="integer"
     * )
     * @var int
     */
    #[Id, Column(type: Types::INTEGER), GeneratedValue(strategy: 'AUTO')]
    private int $id;

    /**
     * Mailbox path on the server
     *
     * @OA\Property(
     *     description="Mailbox path",
     *     title="Mailbox",
     *     property="mailbox",
     *     type="string"
     * )
     * @var string
     */
    #[Column(type: Types::TEXT, nullable: true)]
    private string $mailbox = 'imap.gmail.com:993/imap/ssl';


    /**
     * Mailbox folder on the server
     * @OA\Property(
     *     description="Mailbox folder",
     *     title="Folder",
     *     property="folder",
     *     type="string"
     * )
     * @var string
     */
    #[Column(type: Types::TEXT, nullable: true)]
    private string $folder = 'INBOX';

    /**
     * The mailbox's user
     *
     * @OA\Property(
     *     description="Mailbox's user",
     *     title="User",
     *     property="user",
     *     type="string"
     * )
     * @var string
     */
    #[Column(type: Types::TEXT, nullable: true)]
    private string $user = '';

    /**
     * The password associated with the user
     *
     * @OA\Property(
     *     description="Password associated with the user",
     *     title="Password",
     *     property="password",
     *     type="string"
     * )
     * @var string
     */
    #[Column(type: Types::TEXT, nullable: true)]
    private string $password = '';

    /**
     * Get email parameters identifier
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * Set email parameters identifier
     * @param int $id
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getMailbox(): string {
        return $this->mailbox;
    }

    /**
     * @param string $mailbox
     */
    public function setMailbox(string $mailbox): void {
        $this->mailbox = $mailbox;
    }

    /**
     * @return string
     */
    public function getFolder(): string {
        return $this->folder;
    }

    /**
     * @param string $folder
     */
    public function setFolder(string $folder): void {
        $this->folder = $folder;
    }

    /**
     * @return string
     */
    public function getUser(): string {
        return $this->user;
    }

    /**
     * @param string $user
     */
    public function setUser(string $user): void {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getPassword(): string {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void {
        $this->password = $password;
    }

    /**
     * Return host mailbox parameter
     * @return string
     */
    public function getHost(): string {
        return "{" . $this->mailbox . "}" . $this->folder;
    }

    /**
     * JSON serialization
     * @return mixed|string
     */
    public function jsonSerialize(): array {
        return get_object_vars($this);
    }
}
