<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity;

use DateTime;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\Table;
use Exception;
use JsonSerializable;
use Multicanal\App\Helper;
use Multicanal\Entity\Exception\MulticanalException;
use Multicanal\Entity\Traits\CategoryAwareTrait;
use Multicanal\Entity\Traits\ContentsAwareTrait;
use Multicanal\Entity\Traits\CreatorAwareTrait;
use Multicanal\Entity\Traits\EmailSubscriptionsAwareTrait;
use Multicanal\Entity\Traits\ImageAwareTrait;
use Multicanal\Entity\Traits\MobileSubscriptionsAwareTrait;
use Multicanal\Entity\Traits\OrganizationAwareTrait;
use Multicanal\Utils\UrlMinifierUtils;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpInternalServerErrorException;

/**
 * Class DistributionList
 *
 * @package Multicanal\Entity
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 *
 * @OA\Schema(
 *     description="Spécifications du modèle DistributionList.",
 *     title="Liste de diffusion",
 *     required={"title", "description"},
 *	   @OA\Xml(
 *        name="DistributionList"
 *     )
 * )
 */
#[Entity, Table(name: 'distribution_list'), HasLifecycleCallbacks]
class DistributionList implements JsonSerializable {
    use CreatorAwareTrait,
        ContentsAwareTrait,
        CategoryAwareTrait,
        OrganizationAwareTrait,
        EmailSubscriptionsAwareTrait,
        MobileSubscriptionsAwareTrait,
        ImageAwareTrait;

    /**
     * Diffusion list identifier
     *
     * @OA\Property(
     *     description="Identifiant de la liste de diffusion",
     *     title="Identifier",
     *     property="id",
     *     type="integer"
     * )
     * @var integer
     */
    #[Id, Column(type: Types::INTEGER), GeneratedValue(strategy: 'AUTO')]
    private int $id;

    /**
     * Diffusion list title
     *
     * @OA\Property(
     *     description="Intitulé de la liste de diffusion",
     *     title="Title",
     *     property="title",
     *     type="string"
     * )
     * @var string
     */
    #[Column(type: Types::STRING, length: 240, nullable: false)]
    private string $title;

    /**
     * Diffusion list description
     *
     * @OA\Property(
     *     description="Description de la thématique de la liste de diffusion",
     *     title="Description",
     *     property="description",
     *     type="string"
     * )
     * @var string
     */
    #[Column(type: Types::TEXT, nullable: true)]
    private string $description;

    /**
     * Diffusion list creation date
     *
     * @OA\Property(
     *     description="Date de création de la liste de diffusion",
     *     title="Date de création",
     *     property="creationDate",
     *     type="string",
     *     format="date"
     * )
     * @var DateTime
     */
    #[Column(name: 'creation_date', type: Types::DATETIME_MUTABLE)]
    private DateTime $creationDate;

    /**
     * Creator
     *
     * @OA\Property(
     *     description="Créateur de la liste de diffusion",
     *     title="Creator",
     *     property="creator",
     *     ref="#/components/schemas/User"
     * )
     * @var User
     */
    #[ManyToOne(targetEntity: "User", cascade: ["persist"], inversedBy: "distributionLists")]
    private User $creator;

    /**
     * Diffusion list QR code URL file
     *
     * @OA\Property(
     *     description="URL encodé en base64 du QR code contenant le lien de la liste de diffusion",
     *     title="QR Code",
     *     property="qrCodeFile",
     *     type="string"
     * )
     * @var string
     */
    #[Column(name: 'qr_code_file', type: Types::TEXT, nullable: true)]
    private ?string $qrCodeFile;

    /**
     * Diffusion list minified URL
     *
     * @OA\Property(
     *     description="URL minifié de la liste de diffusion",
     *     title="URL minifié",
     *     property="shortUrl",
     *     type="string"
     * )
     * @var string
     */
    #[Column(name: 'short_url', type: Types::TEXT, nullable: true)]
    private string $shortUrl;

    /**
     * MulticanalFile as illustration
     *
     * @OA\Property(
     *     description="Image d'illustration de la liste de diffusion",
     *     title="Image d'illustration",
     *     property="image",
     *     ref="#/components/schemas/MulticanalFile"
     * )
     * @var string|MulticanalFile|null
     */
    #[
        OneToOne(
            targetEntity: "MulticanalFile",
            cascade: ["all"],
            orphanRemoval: true
        )
    ]
    #[
        JoinColumn(
            name: "illustration_file_id",
            referencedColumnName: "id",
            onDelete: "cascade"
        )
    ]
    private string|MulticanalFile|null $image;

    /**
     * Distribution contents collection
     * @OA\Property(
     *     description="Liste des contenus associés à la liste de diffusion",
     *     title="Contents",
     *     property="contents",
     *     type="array",
     *     @OA\Items(
     *          ref="#/components/schemas/Content"
     *     )
     * )
     * @var Collection
     */
    #[
        OneToMany(
            mappedBy: "distributionList",
            targetEntity: "Content",
            cascade: ["persist"],
            orphanRemoval: true
        )
    ]
    private Collection $contents;

    /**
     * Mobile subscription list
     *
     * @OA\Property(
     *     description="Liste des abonnements mobile",
     *     title="Mobile subscription",
     *     property="mobileSubscriptions",
     *     type="array",
     *     @OA\Items(
     *          ref="#/components/schemas/MobileSubscription"
     *     )
     * )
     * @var Collection
     */
    #[
        OneToMany(
            mappedBy: "distributionList",
            targetEntity: "MobileSubscription",
            cascade: ["persist"],
            orphanRemoval: true
        )
    ]
    private Collection $mobileSubscriptions;

    /**
     * Email subscription list
     *
     * @OA\Property(
     *     description="Liste des abonnements email",
     *     title="Email subscription",
     *     property="emailSubscriptions",
     *     type="array",
     *     @OA\Items(
     *          ref="#/components/schemas/EmailSubscription"
     *     )
     * )
     * @var Collection
     */
    #[
        OneToMany(
            mappedBy: "distributionList",
            targetEntity: "EmailSubscription",
            cascade: ["persist"],
            orphanRemoval: true
        )
    ]
    private Collection $emailSubscriptions;

    /**
     * Organization
     *
     * @OA\Property(
     *     description="Collectivité associée à la liste de diffusion",
     *     title="Organization",
     *     property="organization",
     *     ref="#/components/schemas/Organization"
     * )
     * @var Organization
     */
    #[ManyToOne(targetEntity: "Organization", cascade: ["persist"], inversedBy: "distributionLists")]
    private Organization $organization;

    /**
     * Category
     *
     * @OA\Property(
     *     description="Catégorie associée à la liste de diffusion",
     *     title="Category",
     *     property="category",
     *     ref="#/components/schemas/Category"
     * )
     * @var Category|null
     */
    #[ManyToOne(targetEntity: "Category", cascade: ["persist"], inversedBy: "distributionLists")]
    private ?Category $category;

    /**
     * Number of subscriptions
     * @var int
     */
    private int $numberOfSubscriptions;

    /**
     * Fields construtor
     *
     * @param string $title Distribution list title
     * @param string|null $description Distribution list description
     */
    public function __construct(mixed $title, mixed $description, Organization $organization, ?Category $category = null) {
        $this->setTitle($title);
        $this->setDescription($description);
        $this->setShortUrl('');
        $this->setQrCodeFile('');
        $this->initializeContentsCollection();
        $this->initializeEmailSubscriptionsCollection();
        $this->initializeMobileSubscriptionsCollection();
        $this->setOrganization($organization);
        if ($category instanceof Category) {
            $this->setCategory($category);
        }
    }

    /**
     * Update content
     *
     * @param mixed $title Content title
     * @param mixed $description Content description
     * @param Category|null $category Content category
     * @return void
     */
    public function update(
        mixed            $title,
        mixed            $description,
        ?Category        $category = null
    ): void {
        $this->setTitle($title);
        $this->setDescription($description);
        if ($category instanceof Category) {
            $this->setCategory($category);
        }
    }

    /**
     * Update ShortUrl & QrCodeFile
     * @return void
     * @throws MulticanalException
     */
    public function updateShortUrlQrCodeFile(): void {
        $url = Helper::getAppDomain() . '/communication/' . $this->getId();
        $this->setShortUrl(UrlMinifierUtils::getReduceUrl($url));
        $this->setQrCodeFile(UrlMinifierUtils::getQRCode($url));
    }

    /**
     * Get diffusion list identifier
     * @return  int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * Set diffusion list identifier
     * @param  string  $id  Diffusion list identifier
     * @return  self
     */
    public function setId(string $id): self {
        $this->id = $id;
        return $this;
    }

    /**
     * Get diffusion list title
     * @return  string
     */
    public function getTitle(): string {
        return $this->title;
    }

    /**
     * Set diffusion list title
     * @param  string  $title  Diffusion list title
     * @return  self
     */
    public function setTitle(string $title): self {
        $this->title = $title;
        return $this;
    }

    /**
     * Get diffusion list description
     * @return  string
     */
    public function getDescription(): string {
        return $this->description;
    }

    /**
     * Set diffusion list description
     * @param  string  $description  Diffusion list description
     * @return  self
     */
    public function setDescription(string $description): self {
        $this->description = $description;
        return $this;
    }

    /**
     * Get diffusion list creation date
     * @return  DateTime
     */
    public function getCreationDate(): ?DateTime {
        return $this->creationDate;
    }

    /**
     * Set diffusion list creation date
     * @param  DateTime  $creationDate  Diffusion list creation date
     * @return  self
     */
    public function setCreationDate(DateTime $creationDate): self {
        $this->creationDate = $creationDate;
        return $this;
    }

    /**
     * Get diffusion list QR code URL file
     * @return  string|null
     */
    public function getQrCodeFile(): ?string {
        return $this->qrCodeFile;
    }

    /**
     * Set diffusion list QR code URL file
     * @param  string  $qrCodeFile  Diffusion list QR code URL file
     * @return  self
     */
    public function setQrCodeFile(string $qrCodeFile): self {
        $this->qrCodeFile = $qrCodeFile;
        return $this;
    }

    /**
     * Get diffusion list minified URL
     * @return  string|null
     */
    public function getShortUrl(): ?string {
        return $this->shortUrl;
    }

    /**
     * Set diffusion list minified URL
     * @param  string  $shortUrl  Diffusion list minified URL
     * @return  self
     */
    public function setShortUrl(string $shortUrl): self {
        $this->shortUrl = $shortUrl;
        return $this;
    }

    /**
     * Get diffusion list image
     * @return MulticanalFile|null
     */
    public function getImage(): ?MulticanalFile {
        return $this->image;
    }

    /**
     * Set diffusion list image
     * @param  MulticanalFile  $image  Diffusion list image
     * @return  self
     */
    public function setImage(MulticanalFile $image): self {
        $this->image = $image;
        return $this;
    }

    /**
     * Get number of subscriptions
     * @return  int
     */
    public function getNumberOfSubscriptions(): int {
        return sizeOf($this->emailSubscriptions) + sizeof($this->mobileSubscriptions);
    }

    /**
     * Set creationDate on insert operation
     * @ORM\PrePersist
     */
    #[PrePersist]
    public function onPrePersist() {
        $this->creationDate = new DateTime('NOW');
    }

    /**
     * Override to string function
     * @return string
     */
    public function __toString(): string {
        try {
            return $this->title . ' - ' . $this->description;
        } catch (Exception $e) {
            if (Helper::isDebugEnabled()) {
                return $e->getMessage();
            }
            return '';
        }
    }

    /**
     * To API function to format result for API.
     *
     * @param ServerRequestInterface $request Current HTTP request from Slim framework
     * @return array
     * @throws HttpInternalServerErrorException
     */
    public function __toApi(ServerRequestInterface $request): array {
        try {
            $distributionList = [
                'id' => $this->getId(),
                'title' => $this->getTitle(),
                'description' => $this->getDescription(),
                'creationDate' => $this->getCreationDate()->format('d/m/Y H:i:s'),
                'qrCodeFile' => $this->getQrCodeFile(),
                'shortUrl' => $this->getShortUrl(),
            ];
            if ($this->getCreator() instanceof  User) {
                $distributionList['creator'] = $this->getCreator()->getEmail();
            }
            if ($this->getOrganization() instanceof Organization) {
                $distributionList['contributors'] = $this->getOrganization()->__usersToApi($request);
            }
            if ($this->getImage() instanceof  MulticanalFile) {
                $distributionList['image'] = $this->getImage()->getUrl();
            } elseif (is_string($this->getImage())) {
                $distributionList['image'] = $this->getImage();
            }
            if ($this->getEmailSubscriptions() instanceof Collection) {
                $distributionList['emailSubscriptions'] = $this->getEmailSubscribersList();
            }
            if ($this->getMobileSubscriptions() instanceof Collection) {
                $distributionList['mobileSubscriptions'] = $this->getMobileSubscribersList();
            }
            if ($this->getCategory() instanceof Category) {
                $distributionList['category'] = $this->getCategory()->getName();
            }
            if ($this->getOrganization() instanceof Organization) {
                $distributionList['organization']['id']                = $this->getOrganization()->getId();
                $distributionList['organization']['domain']            = $this->getOrganization()->getDomain();
                $distributionList['organization']['generalParameters'] = $this->getOrganization()->getGeneralParameters()->__toApi($request);
                $distributionList['organization']['emailParameters'] = $this->getOrganization()->getEmailParameters()->__toApi($request);
                $distributionList['organization']['socialNetworkParameters'] = $this->getOrganization()->getSocialNetworkParameters()->__toApi($request);
            }

            return $distributionList;
        } catch (Exception $e) {
            throw new HttpInternalServerErrorException($request, 'Une erreur interne s\'est produite lors de la récupération de la liste de diffusion.', $e);
        }
    }

    /**
     * To array
     * @return array
     */
    public function __toArray(): array {
        try {
            $distributionList = [
                'id' => $this->getId(),
                'title' => $this->getTitle(),
                'description' => $this->getDescription(),
                'creationDate' => $this->getCreationDate(),
                'qrCodeFile' => $this->getQrCodeFile(),
                'shortUrl' => $this->getShortUrl(),
            ];

            if ($this->getCreator() instanceof  User) {
                $distributionList['creator'] = $this->getCreator()->jsonSerialize();
                unset($distributionList['creator']['organization']);
                unset($distributionList['creator']['contents']);
                unset($distributionList['creator']['ownContents']);
                unset($distributionList['creator']['distributionLists']);
                unset($distributionList['creator']['categories']);
                unset($distributionList['creator']['communications']);
            }
            if ($this->getImage() instanceof  MulticanalFile) {
                $distributionList['image'] = $this->getImage()->getUrl();
            } elseif (is_string($this->getImage())) {
                $distributionList['image'] = $this->getImage();
            }
            if ($this->getContents() instanceof Collection) {
                $distributionList['contents'] = $this->getContents()->getValues();
            }
            if ($this->getMobileSubscriptions() instanceof Collection) {
                $distributionList['mobileSubscriptions'] = $this->getMobileSubscriptions()->getValues();
            }
            if ($this->getEmailSubscriptions() instanceof Collection) {
                $distributionList['emailSubscriptions'] = $this->getEmailSubscriptions()->getValues();
            }
            if ($this->getOrganization() instanceof Organization) {
                $distributionList['organization'] = $this->getOrganization()->jsonSerialize();
            }
            if ($this->getCategory() instanceof Category) {
                $distributionList['category'] = $this->getCategory()->jsonSerialize();
            }

            return $distributionList;
        } catch (Exception $e) {
            return [];
        }
    }

    /**
     * JSON serialization
     * @return array
     */
    public function jsonSerialize(): array {
        return get_object_vars($this);
    }
}
