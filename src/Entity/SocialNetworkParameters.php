<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity;


use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Exception;
use JsonSerializable;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpInternalServerErrorException;

/**
 * Class SocialNetworkParameters
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 *
 * @OA\Schema(
 *     description="Spécifications du modèle des paramètres des réseaux sociaux.",
 *     title="Paramètres réseaux sociaux",
 *	   @OA\Xml(
 *        name="SocialNetworkParameters"
 *     )
 * )
 */
#[Entity, Table(name: 'social_network_parameters')]
class SocialNetworkParameters implements JsonSerializable {

    /**
     * Email parameters identifier  as autoincremented integer from database
     *
     * @OA\Property(
     *     description="Identifiant des paramètres des réseaux sociaux",
     *     title="Identifier",
     *     property="id",
     *     type="integer"
     * )
     * @var integer
     */
    #[Id, Column(type: Types::INTEGER), GeneratedValue(strategy: 'AUTO')]
    private int $id;

    /**
     * Facebook social network URL
     *
     * @OA\Property(
     *     description="URL du compte Facebook de la collectivité",
     *     title="Facebook URL",
     *     property="facebookUrl",
     *     type="string"
     * )
     * @var string|null
     */
    #[Column(name: 'facebook_url', type: Types::TEXT, nullable: true)]
    private ?string $facebookUrl = '';

    /**
     * Twitter social network URL
     *
     * @OA\Property(
     *     description="URL du compte Twitter de la collectivité",
     *     title="Twitter URL",
     *     property="twitterUrl",
     *     type="string"
     * )
     * @var string|null
     */
    #[Column(name: 'twitter_url', type: Types::TEXT, nullable: true)]
    private ?string $twitterUrl = '';

    /**
     * Instagram social network URL
     *
     * @OA\Property(
     *     description="URL du compte Instagram de la collectivité",
     *     title="Instagram URL",
     *     property="instagramUrl",
     *     type="string"
     * )
     * @var string|null
     */
    #[Column(name: 'instagram_url', type: Types::TEXT, nullable: true)]
    private ?string $instagramUrl = '';

    /**
     * Get facebook social network URL
     * @return  string|null
     */
    public function getFacebookUrl(): ?string {
        return $this->facebookUrl;
    }

    /**
     * Set facebook social network URL
     * @param  string  $facebookUrl  Facebook social network URL
     * @return  self
     */
    public function setFacebookUrl(string $facebookUrl): self {
        $this->facebookUrl = $facebookUrl;
        return $this;
    }

    /**
     * Get twitter social network URL
     * @return  string|null
     */
    public function getTwitterUrl(): ?string {
        return $this->twitterUrl;
    }

    /**
     * Set twitter social network URL
     * @param  string  $twitterUrl  Twitter social network URL
     * @return  self
     */
    public function setTwitterUrl(string $twitterUrl): self {
        $this->twitterUrl = $twitterUrl;
        return $this;
    }

    /**
     * Get instagram social network URL
     * @return string|null
     */
    public function getInstagramUrl(): ?string {
        return $this->instagramUrl;
    }

    /**
     * Set instagram social network URL
     * @param  string  $instagramUrl  Instagram social network URL
     * @return  self
     */
    public function setInstagramUrl(string $instagramUrl): self {
        $this->instagramUrl = $instagramUrl;
        return $this;
    }

    /**
     * To API function to format result for API.
     *
     * @param ServerRequestInterface $request Current HTTP request from Slim framework
     * @return array
     * @throws HttpInternalServerErrorException
     */
    public function __toApi(ServerRequestInterface $request): array {
        try {
            return [
                'id' => $this->id,
                'facebookUrl' => $this->getFacebookUrl(),
                'twitterUrl' => $this->getTwitterUrl(),
                'instagramUrl' => $this->getInstagramUrl(),
            ];
        } catch (Exception $e) {
            throw new HttpInternalServerErrorException($request, 'Une erreur interne s\'est produite lors de la récupération des paramètres des réseaux sociaux.', $e);
        }
    }

    /**
     * JSON serialization
     * @return array
     */
    public function jsonSerialize(): array {
        return get_object_vars($this);
    }
}
