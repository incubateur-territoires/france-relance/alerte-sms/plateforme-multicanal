<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Exception;
use JsonSerializable;
use Multicanal\App\Helper;
use finfo;

/**
 * Class MulticanalFile
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 *
 * @OA\Schema(
 *     description="Spécifications du modèle MulticanalFile",
 *     title="MulticanalFile",
 *     required={"name", "location"},
 *	   @OA\Xml(
 *        name="MulticanalFile"
 *     )
 * )
 *
 */
#[Entity, Table(name: 'multicanal_file'), HasLifecycleCallbacks]
class MulticanalFile implements JsonSerializable {

    /**
     * MulticanalFile identifier
     *
     * @OA\Property(
     *     description="Identifiant du fichier multicanal",
     *     title="Identifier",
     *     property="id",
     *     type="string"
     * )
     *
     * @var string
     */
    #[Id, Column(type: Types::INTEGER), GeneratedValue(strategy: 'AUTO')]
    private string $id;

    /**
     * MulticanalFile filename
     *
     * @OA\Property(
     *     description="Nom du fichier multicanal",
     *     title="Filename",
     *     property="name",
     *     type="string"
     * )
     *
     * @var string
     */
    #[Column(type: Types::STRING, nullable: false)]
    private string $name;

    /**
     * FMulticanalFile location
     *
     * @OA\Property(
     *     description="Localisation du fichier multicanal",
     *     title="Location",
     *     property="location",
     *     type="string"
     * )
     *
     * @var string
     */
    #[Column(type: Types::STRING, nullable: false)]
    private string $location;

    /**
     * Multicanal file description
     *
     * @OA\Property(
     *     description="Description du fichier multicanal",
     *     title="Description",
     *     property="description",
     *     type="string"
     * )
     * @var null|string
     */
    #[Column(type: Types::STRING, nullable: true)]
    private ?string $description;

    /**
     * MulticanalFile concerned content
     *
     * @OA\Property(
     *     description="Contenu concerné par le fichier multicanal",
     *     title="Content",
     *     property="content",
     *     ref="#/components/schemas/Content"
     * )
     *
     * @var null|Content
     */
    #[ManyToOne(targetEntity: "Content", cascade: ["persist"], inversedBy: "medias")]
    private ?Content $content;

    /**
     * MulticanalFile download url
     *
     * @OA\Property(
     *     description="URL de téléchargement du fichier multicanal",
     *     title="URL",
     *     property="url",
     *     type="string"
     * )
     *
     * @var string
     */
    #[Column(type: Types::STRING, nullable: false)]
    private string $url;

    /**
     * Fields constructor
     *
     * @param string $name Multicanal file name
     * @param string $location Multicanal file location
     */
    public function __construct(string $name, string $location) {
        $this->setName($name);
        $this->setLocation($location);
        $this->setUrl(substr($location, strpos($location, '/upload')) . '/' . $name);
    }

    /**
     * Get multicanal file identifier
     * @return  string
     */
    public function getId(): string {
        return $this->id;
    }

    /**
     * Set multicanal file identifier
     * @param  string  $id  Multicanal file identifier
     * @return  self
     */
    public function setId(string $id): self {
        $this->id = $id;
        return $this;
    }

    /**
     * Get multicanal file name
     * @return  string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * Set multicanal file name
     * @param  string  $name  Multicanal file name
     * @return  self
     */
    public function setName(string $name): self {
        $this->name = $name;
        return $this;
    }

    /**
     * Get multicanal file location
     * @return  string
     */
    public function getLocation(): string {
        return $this->location;
    }

    /**
     * Set multicanal file location
     * @param  string  $location  Multicanal file location
     * @return  self
     */
    public function setLocation(string $location): self {
        $this->location = $location;
        return $this;
    }

    /**
     * Get Multicanal file location
     * @return  string
     */
    public function getUrl(): string {
        return /*Helper::getAppDomain() . */ $this->url;
    }

    /**
     * Get the value of description
     * @return  string|null
     */
    public function getDescription(): ?string {
        return $this->description;
    }

    /**
     * Set the value of description
     * @param string $description
     * @return  self
     */
    public function setDescription(string $description): self {
        $this->description = $description;
        return $this;
    }

    /**
     * Get Multicanal file location
     * @return  string
     */
    public function setUrl($url): string {
        $this->url = $url;
        return $this;
    }

    /**
     * Get the value of content
     */
    public function getContent(): ?Content {
        return $this->content;
    }

    /**
     * Set the value of content
     *
     * @return  self
     */
    public function setContent($content): self {
        $this->content = $content;

        return $this;
    }

    /**
     * Get the value of content
     */
    public function getPath(): string {
        return $this->location . '/' . $this->name;
    }

    /**
     * Get the file mime type
     */
    public function getMimeType() {
        $fileData  = file_get_contents($this->getLocation() . DIRECTORY_SEPARATOR . $this->getName());
        $finfo     = new finfo(FILEINFO_MIME_TYPE);
        return $finfo->buffer($fileData);
    }

    /**
     * Get extension from the file mime type.
     */
    public function getExtension(): string {
        $mimeType = explode('/', $this->getMimeType());
        return end($mimeType);
    }

    /**
     * Get the file size
     */
    public function getSize() {
        $bytes = filesize($this->getLocation() . DIRECTORY_SEPARATOR . $this->getName());
        if ($bytes > 1099511627776) {
            $size = number_format($bytes / 1073741824, 2) . ' To';
        } elseif ($bytes > 1073741824) {
            $size = number_format($bytes / 1073741824, 2) . ' Go';
        } elseif ($bytes > 1048576) {
            $size = number_format($bytes / 1048576, 2) . ' Mo';
        } elseif ($bytes > 1024) {
            $size = number_format($bytes / 1024, 2) . ' Ko';
        } else {
            $size = $bytes . ' o';
        }

        return $size;
    }

    public function isImage() {
        return strpos($this->getMimeType(), 'image/') === 0;
    }

    public function deleteFile(): bool {
        return unlink($this->location. '/' . $this->name);
    }

    /**
     * Override to string function
     * @return string
     */
    public function __toString(): string {
        try {
            return '/' . $this->location . '/' . $this->name;
        } catch (Exception $e) {
            if (Helper::isDebugEnabled()) {
                return $e->getMessage();
            }
            return '';
        }
    }

    /**
     * JSON serialization
     * @return array
     */
    public function jsonSerialize(): array {
        return get_object_vars($this);
    }
}
