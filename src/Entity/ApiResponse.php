<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity;

use DateTime;
use Multicanal\App\Helper;

/**
 * Class ApiResponse
 *
 * @package Multicanal\Entity
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 *
 * @OA\Schema(
 *     description="Spécifications du modèle de réponse de l'Api Multicanal.",
 *     title="Api response",
 *     required={"code", "message", "success"},
 *	   @OA\Xml(
 *		    name="ApiResponse"
 *	   )
 * )
 */
class ApiResponse implements \JsonSerializable {

    /**
     * ApiResponse code (HTTP status code)
     *
     * @OA\Property(
     *     description="Code statut HTTP",
     *     title="Code",
     *     property="code",
     *     type="integer",
     *     format="int32"
     * )
     * @var int
     */
    private int $code;

    /**
     * ApiResponse message
     *
     * @OA\Property(
     *     description="Message litéral de la réponse",
     *     title="Message",
     *     property="message",
     *     type="string"
     * )
     * @var string
     */
    private string $message;

    /**
     * ApiResponse success flag
     *
     * OA\Property(
     *     description="Indicateur de succes de la réponse",
     *     title="Success",
     *     property="success",
     *     type="boolean"
     * )
     * @var bool
     */
    private ?bool $success;

    /**
     * ApiResponse results for get, list operations
     *
     * OA\Property(
     *     description="Résultat de la réponse",
     *     title="Results",
     *     property="results",
     *     type="string"
     * )
     * @var string|null
     */
    private ?string $results;

    /**
     * ApiResponse execution time
     *
     * @OA\Property(
     *     description="Temps d'exécution de la requête",
     *     title="Time",
     *     property="time",
     *     type="integer",
     *     format="int32"
     * )
     * @var int
     */
    private int $time;

    /**
     * Fields construtor
     *
     * @param int $code ApiResponse code (HTTP status code)
     * @param string $message User ApiResponse message
     * @param bool $success (optional) ApiResponse success flag, true by default
     * @param string $results (optional) ApiResponse results content as stringified JSON array, null by default
     */
    public function __construct(int $code, string $message, ?bool $success = true, ?string $results = null) {
        $this->code        = $code;
        $this->message     = $message;
        $this->success    = $success;
        $this->results     = $results;
    }

    /**
     * Get the ApiResponse code
     * @return  int
     */
    public function getCode(): int {
        return $this->code;
    }

    /**
     * Set the ApiResponse code
     * @param  int  $code ApiResponse code
     * @return  self
     */
    public function setCode(int $code): self {
        $this->code = $code;
        return $this;
    }

    /**
     * Get the ApiResponse message
     * @return  string
     */
    public function getMessage(): string {
        return $this->message;
    }

    /**
     * Set the ApiResponse message
     * @param  string  $message ApiResponse message
     * @return  self
     */
    public function setMessage(string $message): self {
        $this->message = $message;
        return $this;
    }

    /**
     * Get the ApiResponse success flag
     * @return  bool
     */
    public function getSuccess(): bool {
        return $this->success;
    }

    /**
     * Set the ApiResponse success flag
     * @param  bool  $success ApiResponse success flag
     * @return  self
     */
    public function setSuccess(bool $success): self {
        $this->success = $success;
        return $this;
    }

    /**
     * Get the ApiResponse results as JSON array
     * @return  mixed
     */
    public function getResults(): mixed {
        return $this->results;
    }

    /**
     * Set the ApiResponse results as JSON array
     * @param  string  $results ApiResponse JSON array
     * @return  self
     */
    public function setResults(string $results): self {
        $this->results = $results;
        return $this;
    }

    /**
     * Get the ApiReponse execution time
     * @return  int
     */
    public function getTime(): int {
        return $this->time;
    }

    /**
     * Set the ApiReponse execution time
     * @param  int  $time
     * @return  self
     */
    public function setTime(int $time): self {
        $this->time = $time;
        return $this;
    }


    /**
     * Override to string function
     * @return string
     */
    public function __toString(): string {
        try {
            return $this->code . ' - ' . $this->message;
        } catch (\Exception $e) {
            if (Helper::isDebugEnabled()) {
                return $e->getMessage();
            }
            return '';
        }
    }

    /**
     * JSON serialization
     * @return array
     */
    public function jsonSerialize(): array {
        return get_object_vars($this);
    }
}
