<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\Table;
use Exception;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpInternalServerErrorException;

/**
 * Class GeneralParameters
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 *
 * @OA\Schema(
 *     description="Spécifications du modèle GeneralParameters.",
 *     title="Collectivité",
 *     required={"title"},
 *	   @OA\Xml(
 *        name="Parameters"
 *     )
 * )
 */
#[Entity, Table(name: 'general_parameters')]
class GeneralParameters implements \JsonSerializable {

    /**
     * General parameters identifier as autoincremented integer from database
     *
     * @OA\Property(
     *     description="Identifiant des paramètres généraux",
     *     title="Identifier",
     *     property="id",
     *     type="integer"
     * )
     * @var int
     */
    #[Id, Column(type: Types::INTEGER), GeneratedValue(strategy: 'AUTO')]
    private int $id;

    /**
     * General parameters domain name
     *
     * @OA\Property(
     *     description="Nom de domaine de la collectivité, initialisé par défaut à la première connexion d'un utilisateur",
     *     title="Nom de domaine",
     *     property="domainName",
     *     type="string"
     * )
     * @var string
     */
    #[Column(type: Types::STRING, nullable: false)]
    private string $domainName;

    /**
     * General parameters label
     *
     * @OA\Property(
     *     description="Raison sociale de la collectivité",
     *     title="Intitulé",
     *     property="label",
     *     type="string"
     * )
     * @var string
     */
    #[Column(type: Types::STRING, length: 240, nullable: true)]
    private string $label = '';

    /**
     * General parameters logo
     *
     * @OA\Property(
     *     description="Logo de la collectivité",
     *     title="Logo",
     *     property="logo",
     *     ref="#/components/schemas/MulticanalFile"
     * )
     * @var MulticanalFile|null
     */
    #[
        OneToOne(
            targetEntity: "MulticanalFile",
            cascade: ["all"]
        )
    ]
    #[
        JoinColumn(
            name: "logo_file_id",
            referencedColumnName: "id",
            onDelete: "cascade"
        )
    ]
    private ?MulticanalFile $logo = null;

    /**
     * General parameters background color
     *
     * @OA\Property(
     *     description="Couleur de fond du bandeau d'entête de la collectivité",
     *     title="Couleur de fond du bandeau haut",
     *     property="backgroundColor",
     *     type="string"
     * )
     * @var string|null
     */
    #[Column(name: 'background_color', type: Types::TEXT, nullable: true)]
    private ?string $backgroundColor = '';

    /**
     * General parameters gradient color
     *
     * @OA\Property(
     *     description="Couleur de dégradé du bandeau d'entête de la collectivité",
     *     title="Couleur de dégradé du bandeau haut",
     *     property="gradientColor",
     *     type="string"
     * )
     * @var string|null
     */
    #[Column(name: 'gradient_color', type: Types::TEXT, nullable: true)]
    private ?string $gradientColor = '';

    /**
     * General parameters text color
     *
     * @OA\Property(
     *     description="Couleur de texte du bandeau d'entête de la collectivité",
     *     title="Couleur de texte",
     *     property="textColor",
     *     type="string"
     * )
     * @var string|null
     */
    #[Column(name: 'text_color', type: Types::TEXT, nullable: true)]
    private ?string $textColor = '';

    /**
     * General parameters contact email
     *
     * @OA\Property(
     *     description="Email de contact de la collectivité",
     *     title="Email de contact",
     *     property="email",
     *     type="string"
     * )
     * @var string|null
     */
    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $email = '';

    /**
     * General parameters phone number
     *
     * @OA\Property(
     *     description="Numéro de téléphone de contact de la collectivité",
     *     title="Numéro de téléphone",
     *     property="phoneNumber",
     *     type="string"
     * )
     * @var string|null
     */
    #[Column(name: 'phone_number', type: Types::TEXT, nullable: true)]
    private ?string $phoneNumber = '';

    /**
     * General parameters contact Url
     *
     * @OA\Property(
     *     description="UURL du site Internet de la collectivité",
     *     title="Site internet",
     *     property="webSiteUrl",
     *     type="string"
     * )
     * @var string|null
     */
    #[Column(name: 'web_site_url', type: Types::TEXT, nullable: true)]
    private ?string $webSiteUrl = '';

    /**
     * General parameters contact phone number
     *
     * @OA\Property(
     *     description="URL de la page contact du site Internet de la collectivité",
     *     title="URL de contact",
     *     property="contactUrl",
     *     type="string"
     * )
     * @var string|null
     */
    #[Column(name: 'contact_url', type: Types::TEXT, nullable: true)]
    private ?string $contactUrl  = '';

    /**
     * GeneralParameters constructor.
     * @param string $domainName Organization domain name
     */
    public function __construct(string $domainName) {
        $this->domainName = $domainName;
    }

    /**
     * Get the value of id
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * Set the value of id
     * @param  int  $id  Parameters identifier
     * @return  self
     */
    public function setId(int $id): self {
        $this->id = $id;
        return $this;
    }

    /**
     * Get parameters domainName identifier
     * @return  string
     */
    public function getDomainName(): string {
        return $this->domainName;
    }

    /**
     * Set parameters domainName Identifier
     * @param  string  $domainName  Parameters domainName identifier
     * @return  self
     */
    public function setDomainName(string $domainName): self {
        $this->domainName = $domainName;
        return $this;
    }

    /**
     * Get parameters label
     * @return  string
     */
    public function getLabel(): string {
        return $this->label;
    }

    /**
     * Set parameters label
     * @param  string  $label  Parameters label
     * @return  self
     */
    public function setLabel(string $label): self {
        $this->label = $label;
        return $this;
    }

    /**
     * Get parameters logo
     * @return  MulticanalFile
     */
    public function getLogo(): ?MulticanalFile {
        return $this->logo;
    }

    /**
     * Set parameters logo
     * @param  MulticanalFile  $logo  Parameters logo
     * @return  self
     */
    public function setLogo(?MulticanalFile $logo): self {
        $this->logo = $logo;
        return $this;
    }

    /**
     * Get parameters background color
     * @return  string|null
     */
    public function getBackgroundColor(): ?string {
        return $this->backgroundColor;
    }

    /**
     * Set parameters background color
     * @param  string  $backgroundColor  Parameters background color
     * @return  self
     */
    public function setBackgroundColor(string $backgroundColor): self {
        $this->backgroundColor = $backgroundColor;
        return $this;
    }

    /**
     * Get parameters gradient color
     * @return  string|null
     */
    public function getGradientColor(): ?string {
        return $this->gradientColor;
    }

    /**
     * Set parameters gradient color
     * @param  string  $gradientColor  Parameters gradient color
     * @return  self
     */
    public function setGradientColor(string $gradientColor): self {
        $this->gradientColor = $gradientColor;
        return $this;
    }

    /**
     * Get parameters text color
     * @return  string|null
     */
    public function getTextColor(): ?string {
        return $this->textColor;
    }

    /**
     * Set parameters text color
     * @param  string  $textColor  Parameters text color
     * @return  self
     */
    public function setTextColor(string $textColor): self {
        $this->textColor = $textColor;
        return $this;
    }

    /**
     * Get parameters contact email
     * @return  string|null
     */
    public function getEmail(): ?string {
        return $this->email;
    }

    /**
     * Set parameters contact email
     * @param  string  $email  Parameters contact email
     * @return  self
     */
    public function setEmail(string $email): self {
        $this->email = $email;
        return $this;
    }

    /**
     * Get parameters phone number
     * @return  string|null
     */
    public function getPhoneNumber(): ?string {
        return $this->phoneNumber;
    }

    /**
     * Set parameters phone number
     * @param  string  $phoneNumber  Parameters phone number
     * @return  self
     */
    public function setPhoneNumber(string $phoneNumber): self {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

    /**
     * Get parameters contact Url
     * @return  string|null
     */
    public function getWebSiteUrl(): ?string {
        return $this->webSiteUrl;
    }

    /**
     * Set parameters contact Url
     * @param  string  $webSiteUrl  Parameters contact Url
     * @return  self
     */
    public function setWebSiteUrl(string $webSiteUrl): self {
        $this->webSiteUrl = $webSiteUrl;
        return $this;
    }

    /**
     * Get parameters contact phone number
     * @return  string|null
     */
    public function getContactUrl(): ?string {
        return $this->contactUrl;
    }

    /**
     * Set parameters contact phone number
     * @param  string  $contactUrl  Parameters contact phone number
     * @return  self
     */
    public function setContactUrl(string $contactUrl): self {
        $this->contactUrl = $contactUrl;
        return $this;
    }

    /**
     * To API function to format result for API.
     *
     * @param ServerRequestInterface $request Current HTTP request from Slim framework
     * @return array
     * @throws HttpInternalServerErrorException
     */
    public function __toApi(ServerRequestInterface $request): array {
        try {
            return [
                'id' => $this->getId(),
                'label' => $this->getLabel(),
                'email' => $this->getEmail(),
                'backgroundColor' => $this->getBackgroundColor(),
                'gradientColor' => $this->getGradientColor(),
                'textColor' => $this->getTextColor(),
                'phoneNumber' => $this->getPhoneNumber(),
                'contactUrl' => $this->getContactUrl(),
                'webSiteUrl' => $this->getWebSiteUrl(),
            ];
        } catch (Exception $e) {
            throw new HttpInternalServerErrorException($request, 'Une erreur interne s\'est produite lors de la récupération des paramètres généraux.', $e);
        }
    }

    /**
     * JSON serialization
     * @return array
     */
    public function jsonSerialize(): ?array {
        return get_object_vars($this);
    }
}
