<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Exception;
use JsonSerializable;
use Multicanal\App\Helper;
use Multicanal\Entity\Traits\DistributionListAwareTrait;

/**
 * Class MobileSubscription
 *
 * @package Multicanal\Entity
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 *
 * @OA\Schema(
 *     description="Spécifications du modèle décrivant l'envoie de mail à une liste de destinataire.",
 *     title="MobileSubscription",
 *     required={"mobile"},
 *	   @OA\Xml(
 *        name="MobileSubscription"
 *     )
 * )
 */
#[Entity, Table(name: 'mobile_subscription')]
#[UniqueConstraint(name: "mobile_distributionList", columns: ["mobile", "distributionList_id"])]
class MobileSubscription extends Subscription implements JsonSerializable {
    use DistributionListAwareTrait;

    /**
     * Subscription identifier
     *
     * @OA\Property(
     *     description="Identifiant de l'abonnement mobile",
     *     title="Identifier",
     *     property="id",
     *     type="integer"
     * )
     * @var int
     */
    #[Id, Column(type: Types::INTEGER), GeneratedValue(strategy: 'AUTO')]
    protected int $id;

    /**
     * Subscription mobile number
     *
     * @OA\Property(
     *     description="Numéro de téléphone mobile soumis lors de l'abonnement",
     *     title="Subscription mobile",
     *     property="mobile",
     *     type="string"
     * )
     * @var string
     */
    #[Column(type: Types::STRING, nullable: false)]
    protected string $mobile;

    /**
     * Distribution list
     *
     * @OA\Property(
     *     description="Mobile subscription distribution list",
     *     title="Distribution list",
     *     property="DistributionList",
     *     ref="#/components/schemas/DistributionList"
     * )
     * @var DistributionList
     */
    #[
        ManyToOne(
            targetEntity: "DistributionList",
            cascade: ["persist"],
            inversedBy: "mobileSubscriptions"
        )
    ]
    private DistributionList $distributionList;

    /**
     * MobileSubscription constructor.
     *
     * @param string $mobile Mobile phone number as string
     * @param DistributionList $distributionList Distribution list
     */
    public function __construct(string $mobile, DistributionList $distributionList) {
        $this->mobile = $mobile;
        $this->setDistributionList($distributionList);
    }

    /**
     * Get subscription identifier
     * @return  int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * Set subscription identifier
     * @param  int  $id  Subscription identifier
     * @return  self
     */
    public function setId(int $id): self {
        $this->id = $id;
        return $this;
    }

    /**
     * Get subscription mobile
     * @return  string
     */
    public function getMobile(): string {
        return $this->mobile;
    }

    /**
     * Set subscription mobile
     * @param  string  $mobile  Subscription mobile
     * @return  self
     */
    public function setMobile(string $mobile): self {
        $this->mobile = $mobile;
        return $this;
    }

    /**
     * Override to string function
     * @return string
     */
    public function __toString(): string {
        try {
            return $this->mobile;
        } catch (Exception $e) {
            if (Helper::isDebugEnabled()) {
                return $e->getMessage();
            }
            return '';
        }
    }
}
