<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity;


use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;
use JsonSerializable;

/**
 * Class Counter
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 *
 * @OA\Schema(
 *     description="Spécifications du modèle compteur.",
 *     title="Compteur",
 *	   @OA\Xml(
 *        name="counter"
 *     )
 * )
 */
#[Entity, Table(name: 'counter')]
#[UniqueConstraint(name: "organizations_counter", columns: ["year", "organization_id"])]
class Counter implements JsonSerializable {

    /**
     * Year of the counter
     *
     * @OA\Property(
     *     description="Année du compteur",
     *     title="Année",
     *     property="year",
     *     type="integer"
     * )
     * @var integer
     */
    #[Id, Column(type: Types::INTEGER)]
    private int $year;

    /**
     * Organization concerned by the counter
     *
     * @OA\Property(
     *     description="Collectivité du compteur",
     *     title="Collectivité",
     *     property="organization",
     *     ref="#/components/schemas/Organization"
     * )
     *
     * @var null|Organization
     */
    #[Id, ManyToOne(targetEntity: "Organization", cascade: ["persist"], inversedBy: "counters")]
    private ?Organization $organization;

    /**
     * Organization sms counter
     *
     * @var integer
     */
    #[Column(name: 'nb_of_sms', type: Types::BIGINT)]
    private int $nbOfSms = 0;

    /**
     * Organization email counter
     *
     * @var integer
     */
    #[Column(name: 'nb_of_email', type: Types::BIGINT)]
    private int $nbOfEmail = 0;

    /**
     * Get the value of organization
     */
    public function getOrganization(): ?Organization {
        return $this->organization;
    }

    /**
     * Set the value of organization
     * @return void
     */
    public function setOrganization($organization): void {
        $this->organization = $organization;
    }

    /**
     * Get the value of year
     * @return int
     */
    public function getYear(): int {
        return $this->year;
    }

    /**
     * Set the value of year
     * @param int $year
     * @return void
     */
    public function setYear(int $year): void {
        $this->year = $year;
    }

    /**
     * Get the value of nbOfSms
     * @return int
     */
    public function getNbOfSms(): int {
        return $this->nbOfSms;
    }

    /**
     * Set the value of nbOfSms
     * @param int $nbOfSms
     * @return void
     */
    public function setNbOfSms(int $nbOfSms): void {
        $this->nbOfSms = $nbOfSms;
    }

    /**
     * Get the value of nbOfEmail
     * @return int
     */
    public function getNbOfEmail(): int {
        return $this->nbOfEmail;
    }

    /**
     * Set the value of nbOfEmail
     * @param int $nbOfEmail
     * @return void
     */
    public function setNbOfEmail(int $nbOfEmail): void {
        $this->nbOfEmail = $nbOfEmail;
    }

    /**
     * JSON serialization
     * @return array
     */
    public function jsonSerialize(): array {
        return get_object_vars($this);
    }
}
