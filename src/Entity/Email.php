<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity;

use Exception;
use JsonSerializable;
use Multicanal\App\Helper;

/**
 * Class Email
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 *
 * @OA\Schema(
 *     description="Spécifications du modèle décrivant une souscription par mail.",
 *     title="Email",
 *     required={"emails", "object", "body"},
 *	   @OA\Xml(
 *        name="Email"
 *     )
 * )
 */
class Email implements JsonSerializable {

    /**
     * Email's Object
     *
     * @OA\Property(
     *     description="Email object",
     *     title="Object",
     *     property="object",
     *     type="string"
     * )
     * @var string
     */
    private string $object;

    /**
     * Email's body
     *
     * @OA\Property(
     *     description="Email body",
     *     title="Body",
     *     property="body",
     *     type="string"
     * )
     * @var string
     */
    private string $body;

    /**
     * Email's list
     *
     * @OA\Property(
     *     description="Liste des adresses mail.",
     *     title="Adresses mails",
     *     property="emails",
     *     type="array",
     *     @OA\Items(
     *          type="string"
     *     )
     * )
     * @var array
     */
    private array $emails;

    /**
     * Reply to name
     *
     * @OA\Property(
     *     description="Reply to name",
     *     title="Reply to",
     *     property="replyTo",
     *     type="string"
     * )
     * @var string|null
     */
    private ?string $replyTo = '';

    /**
     * Reply to email
     * @OA\Property(
     *     description="Reply to email",
     *     title="Reply to email",
     *     property="replyToEmail",
     *     type="string"
     * )
     * @var string|null
     */
    private ?string $replyToEmail = '';

    /**
     * Media path list email
     * @OA\Property(
     *     description="Media path list Attachement",
     *     title="Medias",
     *     property="medias",
     *     type="array",
     *     @OA\Items(
     *          type="string"
     *     )
     * )
     * @var array|null
     */
    private ?array $medias = [];

    /**
     * Email constructor.
     *
     * @param array $emails
     * @param string $object
     * @param string $body
     */
    public function __construct(array $emails, string $object, string $body) {
        $this->setEmails($emails);
        $this->setObject($object);
        $this->setBody($body);
    }

    /**
     * @return array
     */
    public function getEmails(): array {
        return $this->emails;
    }

    /**
     * @param array $emails
     */
    public function setEmails(array $emails): void {
        $this->emails = $emails;
    }

    /**
     * @return string
     */
    public function getObject(): string {
        return $this->object;
    }

    /**
     * @param string $object
     */
    public function setObject(string $object): void {
        $this->object = $object;
    }

    /**
     * @return string
     */
    public function getBody(): string {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody(string $body): void {
        $this->body = $body;
    }

    /**
     * @return string|null
     */
    public function getReplyTo(): ?string {
        return $this->replyTo;
    }

    /**
     * @param string|null $replyTo
     */
    public function setReplyTo(?string $replyTo): void {
        $this->replyTo = $replyTo;
    }

    /**
     * @return string|null
     */
    public function getReplyToEmail(): ?string {
        return $this->replyToEmail;
    }

    /**
     * @param string|null $replyToEmail
     */
    public function setReplyToEmail(?string $replyToEmail): void {
        $this->replyToEmail = $replyToEmail;
    }

    /**
     * @return array|null
     */
    public function getMedias(): ?array {
        return $this->medias;
    }

    /**
     * @param array|null $medias
     */
    public function setMedias(?array $medias): void {
        $this->medias = $medias;
    }

    /**
     * Override to string function
     * @return string
     */
    public function __toString(): string {
        try {
            return $this->object . ' - ' . $this->body;
        } catch (Exception $e) {
            if (Helper::isDebugEnabled()) {
                return $e->getMessage();
            }
            return '';
        }
    }

    /**
     * JSON serialization
     * @return array
     */
    public function jsonSerialize(): mixed {
        return get_object_vars($this);
    }
}
