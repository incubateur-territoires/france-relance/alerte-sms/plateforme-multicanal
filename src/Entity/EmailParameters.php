<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Exception;
use JsonSerializable;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpInternalServerErrorException;

use function serialize;

/**
 * Class EmailParameters
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 *
 * @OA\Schema(
 *     description="Spécifications du modèle des paramètres d'emailing.",
 *     title="Paramètres d'emailing",
 *     required={"emailSender", "emailReply"},
 *	   @OA\Xml(
 *        name="EmailParameters"
 *     )
 * )
 */
#[Entity, Table(name: 'email_parameters')]
class EmailParameters implements JsonSerializable {

    /**
     * Email parameters identifier as autoincremented integer from database
     *
     * @OA\Property(
     *     description="Identifiant du paramètre d'emailing",
     *     title="Identifier",
     *     property="id",
     *     type="integer"
     * )
     * @var int
     */
    #[Id, Column(type: Types::INTEGER), GeneratedValue(strategy: 'AUTO')]
    private int $id;

    /**
     * Sending service name
     *
     * @OA\Property(
     *     description="Sending service name",
     *     title="Nominator",
     *     property="nominator",
     *     type="string"
     * )
     * @var string|null
     */
    #[Column(type: Types::TEXT, nullable: true)]
    private ?string $nominator = '';

    /**
     * Sending service email
     *
     * @OA\Property(
     *     description="Sending service email",
     *     title="Nominator email",
     *     property="nominatorEmail",
     *     type="string"
     * )
     * @var string|null
     */
    #[Column(name: 'nominator_email', type: Types::TEXT, nullable: true)]
    private ?string $nominatorEmail = '';

    /**
     * Reply to name
     *
     * @OA\Property(
     *     description="Reply to name",
     *     title="Reply to",
     *     property="replyTo",
     *     type="string"
     * )
     * @var string|null
     */
    #[Column(name: 'reply_to', type: Types::TEXT, nullable: true)]
    private ?string $replyTo = '';

    /**
     * Reply to email
     * @OA\Property(
     *     description="Reply to email",
     *     title="Reply to email",
     *     property="replyToEmail",
     *     type="string"
     * )
     * @var string|null
     */
    #[Column(name: 'reply_to_email', type: Types::TEXT, nullable: true)]
    private ?string $replyToEmail = '';

    /**
     * Get email parameters identifier
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * Set email parameters identifier
     * @param int $id
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * Get sending service name
     * @return  string|null
     */
    public function getNominator(): ?string {
        return $this->nominator;
    }

    /**
     * Set sending service name
     * @param  string  $nominator  Sending service name
     * @return  self
     */
    public function setNominator(string $nominator): self {
        $this->nominator = $nominator;
        return $this;
    }

    /**
     * Get sending service email
     * @return  string|null
     */
    public function getNominatorEmail(): ?string {
        return $this->nominatorEmail;
    }

    /**
     * Set sending service email
     * @param  string  $nominatorEmail  Sending service email
     * @return  self
     */
    public function setNominatorEmail(string $nominatorEmail): self {
        $this->nominatorEmail = $nominatorEmail;
        return $this;
    }

    /**
     * Get reply to name
     * @return  string|null
     */
    public function getReplyTo(): ?string {
        return $this->replyTo;
    }

    /**
     * Set reply to name
     * @param  string  $replyTo  Reply to name
     * @return  self
     */
    public function setReplyTo(string $replyTo): self {
        $this->replyTo = $replyTo;
        return $this;
    }

    /**
     * Get reply to email
     * @return  string|null
     */
    public function getReplyToEmail(): ?string {
        return $this->replyToEmail;
    }

    /**
     * Set reply to email
     * @param  string  $replyToEmail  Reply to email
     * @return  self
     */
    public function setReplyToEmail(string $replyToEmail): self {
        $this->replyToEmail = $replyToEmail;
        return $this;
    }

    /**
     * To API function to format result for API.
     *
     * @param ServerRequestInterface $request Current HTTP request from Slim framework
     * @return array
     * @throws HttpInternalServerErrorException
     */
    public function __toApi(ServerRequestInterface $request): array {
        try {
            return [
                'id' => $this->id,
                'nominator' => $this->nominator,
                'nominatorEmail' => $this->nominatorEmail,
                'replyTo' => $this->replyTo,
                'replyToEmail' => $this->replyToEmail,
            ];
        } catch (Exception $e) {
            throw new HttpInternalServerErrorException($request, 'Une erreur interne s\'est produite lors de la récupération des paramètres d\'emailing.', $e);
        }
    }

    /**
     * JSON serialization
     * @return mixed|string
     */
    public function jsonSerialize(): array {
        return get_object_vars($this);
    }
}
