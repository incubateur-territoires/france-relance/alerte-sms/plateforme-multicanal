<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Exception;
use JsonSerializable;
use Multicanal\App\Helper;
use Multicanal\Entity\Traits\DistributionListAwareTrait;

/**
 * Class EmailSubscription
 *
 * @package Multicanal\Entity
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 * @OA\Schema(
 *     description="Spécifications du modèle décrivant une souscription par mail.",
 *     title="EmailSubscription",
 *     required={"email"},
 *	   @OA\Xml(
 *        name="EmailSubscription"
 *     )
 * )
 */
#[Entity, Table(name: 'email_subscription')]
#[UniqueConstraint(name: "email_distributionList", columns: ["email", "distributionList_id"])]
class EmailSubscription extends Subscription implements JsonSerializable {
    use DistributionListAwareTrait;

    /**
     * Subscription identifier
     *
     * @OA\Property(
     *     description="Identifiant unique de l'abonnement",
     *     title="Identifiant de l'abonnement",
     *     property="id",
     *     type="integer"
     * )
     * @var int
     */
    #[Id, Column(type: Types::INTEGER), GeneratedValue(strategy: 'AUTO')]
    protected int $id;

    /**
     * Subscription email
     *
     * @OA\Property(
     *     description="Email soumis lors de l'abonnement.",
     *     title="Email de l'abonné",
     *     property="email",
     *     type="string"
     * )
     * @var string
     */
    #[Column(type: Types::STRING, nullable: false)]
    protected string $email;


    /**
     * Liste de diffusion
     *
     * @OA\Property(
     *     property="DistributionList",
     *     ref="#/components/schemas/DistributionList"
     * )
     * @var DistributionList
     */
    #[
        ManyToOne(
            targetEntity: "DistributionList",
            inversedBy: "emailSubscriptions"
        )
    ]
    private DistributionList $distributionList;

    /**
     * EmailSubscription constructor.
     *
     * @param string $email Email subscriber
     * @param DistributionList $distributionList
     */
    public function __construct(string $email, DistributionList $distributionList) {
        $this->setEmail($email);
        $this->setDistributionList($distributionList);
    }

    /**
     * Get subscription identifier
     * @return  int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * Set subscription identifier
     * @param  int  $id  Subscription identifier
     * @return  self
     */
    public function setId(int $id): self {
        $this->id = $id;
        return $this;
    }

    /**
     * Get subscription email
     * @return  string
     */
    public function getEmail(): string {
        return $this->email;
    }

    /**
     * Set subscription email
     * @param  string  $email  Subscription email
     * @return  self
     */
    public function setEmail(string $email): self {
        $this->email = $email;
        return $this;
    }

    /**
     * Override to string function
     * @return string
     */
    public function __toString(): string {
        try {
            return $this->email;
        } catch (Exception $e) {
            if (Helper::isDebugEnabled()) {
                return $e->getMessage();
            }
            return '';
        }
    }
}
