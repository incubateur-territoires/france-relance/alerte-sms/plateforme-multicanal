<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity;

use DateTime;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\Table;
use Exception;
use JsonSerializable;
use Multicanal\App\Helper;
use Multicanal\Entity\Traits\ContentsAwareTrait;
use Multicanal\Entity\Traits\CreatorAwareTrait;
use Multicanal\Entity\Traits\DistributionListsAwareTrait;
use Multicanal\Entity\Traits\OrganizationAwareTrait;

/**
 * Class Category
 *
 * @package Multicanal\Entity
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 *
 * @OA\Schema(
 *     description="Spécifications du modèle décrivant une catégorie.",
 *     title="Catégorie",
 *     required={"name"},
 *	   @OA\Xml(
 *        name="Category"
 *     )
 * )
 */
#[Entity, Table(name: 'category'), HasLifecycleCallbacks]
class Category implements JsonSerializable {
    use OrganizationAwareTrait,
        CreatorAwareTrait,
        DistributionListsAwareTrait,
        ContentsAwareTrait;

    /**
     * Category identifier as autoincremented integer from database
     *
     * @OA\Property(
     *     description="Identifiant de la catégorie",
     *     title="Identifier",
     *     property="id",
     *     type="integer"
     * )
     * @var int
     */
    #[Id, Column(type: Types::INTEGER), GeneratedValue(strategy: 'AUTO')]
    private int $id;

    /**
     * Category label name
     *
     * @OA\Property(
     *     description="Libellé de la catégorie",
     *     title="Name",
     *     property="name",
     *     type="string"
     * )
     * @var string
     */
    #[Column(type: Types::STRING, nullable: false)]
    private string $name;

    /**
     * Category user creator
     *
     * @OA\Property(
     *     description="Créateur de la catégorie",
     *     title="Creator",
     *     property="creator",
     *     ref="#/components/schemas/User"
     * )
     * @var string|null|User
     */
    #[ManyToOne(targetEntity: "User", cascade: ["persist"], inversedBy: "categories")]
    private string|null|User $creator;

    /**
     * Category creation date
     *
     * @OA\Property(
     *     description="Date de création de la catégorie",
     *     title="Creation date",
     *     property="creationDate",
     *     type="string",
     *     format="date"
     * )
     * @var DateTime
     */
    #[Column(name: 'creation_date', type: Types::DATETIME_MUTABLE)]
    private DateTime $creationDate;

    /**
     * Organization to which the category is attached
     *
     * @OA\Property(
     *     description="Collectivité à laquelle est rattaché la catégorie",
     *     title="Organization",
     *     property="organization",
     *     ref="#/components/schemas/Organization"
     * )
     * @var null|Organization
     */
    #[
        ManyToOne(
            targetEntity: "Organization",
            cascade: ["persist"],
            inversedBy: "categories"
        )
    ]
    private ?Organization $organization;

    /**
     * DistributionList collection
     *
     * @OA\Property(
     *     description="Listes de diffusion associées à la catégorie",
     *     title="List of distributionList",
     *     property="distributionList",
     *     type="array",
     *     @OA\Items(
     *          ref="#/components/schemas/DistributionList"
     *     )
     * )
     * @var Collection
     */
    #[
        OneToMany(
            mappedBy: "category",
            targetEntity: "DistributionList",
            cascade: ["persist", "merge"],
            orphanRemoval: true
        )
    ]
    private Collection $distributionLists;

    /**
     * Contents collection
     *
     * @OA\Property(
     *     description="Liste des contenus associés à la catégorie",
     *     title="List of content",
     *     property="contents",
     *     type="array",
     *     @OA\Items(
     *          ref="#/components/schemas/Content"
     *     )
     * )
     * @var Collection
     */
    #[
        OneToMany(
            mappedBy: "category",
            targetEntity: "Content",
            cascade: ["persist", "remove", "merge"],
            orphanRemoval: true
        )
    ]
    private Collection $contents;

    /**
     * Fields construtor
     *
     * @param string $name Category name
     */
    public function __construct(string $name) {
        $this->setName($name);
        $this->initializeDistributionListsCollection();
        $this->initializeContentsCollection();
    }

    /**
     * Get category identifier
     * @return  int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * Set category identifier
     * @param int $id Category identifier
     * @return  self
     */
    public function setId(int $id): self {
        $this->id = $id;
        return $this;
    }

    /**
     * Get category label name
     * @return  string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * Set category label name
     * @param string $name Category label name
     * @return  self
     */
    public function setName(string $name): self {
        $this->name = $name;
        return $this;
    }

    /**
     * Get category creation date
     * @return  DateTime
     */
    public function getCreationDate(): ?DateTime {
        return $this->creationDate;
    }

    /**
     * Set category creation date
     * @param DateTime $creationDate Category creation date
     * @return  self
     */
    public function setCreationDate(DateTime $creationDate): self {
        $this->creationDate = $creationDate;
        return $this;
    }

    /**
     * Set creationDate on insert operation
     * @ORM\PrePersist
     */
    #[PrePersist]
    public function onPrePersist() {
        $this->creationDate = new DateTime('NOW');
    }

    /**
     * Override to string function
     * @return string
     */
    public function __toString(): string {
        try {
            return $this->id . ' ' . $this->name;
        } catch (Exception $e) {
            if (Helper::isDebugEnabled()) {
                return $e->getMessage();
            }
            return '';
        }
    }

    /**
     * JSON serialization
     * @return array
     */
    public function jsonSerialize(): array {
        return get_object_vars($this);
    }
}
