<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Traits;

use Multicanal\Entity\User;

/**
 * Creator aware trait
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
trait CreatorAwareTrait {

    /**
     * Get user as creator
     * @return User
     */
    public function getCreator(): User {
        return $this->creator;
    }

    /**
     * Set a user as creator
     * @param String|User $creator
     * @return void
     */
    public function setCreator(User|string $creator): void {
        $this->creator = $creator;
    }
}
