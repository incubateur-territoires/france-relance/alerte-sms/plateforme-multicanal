<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Multicanal\Entity\Counter;

/**
 * Counters aware trait
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
trait CountersAwareTrait {

    /**
     * Initialize empty counters collection
     */
    public function initializeCountersCollection(): void {
        $this->counters = new ArrayCollection();
    }

    /**
     * Get counters collection
     * @return Collection
     */
    public function getCounters(): ?Collection {
        return $this->counters;
    }

    /**
     * Check if counters collection contains a specific counter
     * @param Counter $counter
     * @return bool
     */
    public function hasCounter(Counter $counter): bool {
        return $this->counters && $this->counters->contains($counter);
    }

    /**
     * Add a counter in counters collection
     * @param Counter $counter
     */
    public function addCounter(Counter $counter): void {
        if (true !== $this->hasCounter($counter)) {
            $this->counters->add($counter);
        }
    }

    /**
     * Remove a counter from counters collection
     * @param Counter $counter
     */
    public function removeCounter(Counter $counter): void {
        if (true === $this->hasCounter($counter)) {
            $this->counters->removeElement($counter);
        }
    }
}
