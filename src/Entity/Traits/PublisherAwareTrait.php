<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Traits;

use Multicanal\Entity\User;

/**
 * Publisher aware trait
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
trait PublisherAwareTrait {

    /**
     * Get user as publisher
     * @return User|null
     */
    public function getPublisher(): ?User {
        return $this->publisher;
    }

    /**
     * Set an user as publisher
     * @param User|null $publisher
     * @return void
     */
    public function setPublisher(?User $publisher): void {
        $this->publisher = $publisher;
    }
}
