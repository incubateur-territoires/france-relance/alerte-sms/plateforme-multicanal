<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Traits;

use Multicanal\Entity\MulticanalFile;

/**
 * Image aware trait
 *
 * @package Multicanal\Entity\trait
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
trait ImageAwareTrait {

    /**
     * Get MulticanalFile
     * @return  MulticanalFile image
     */
    public function getImage(): MulticanalFile {
        return $this->image;
    }

    /**
     * Set MulticanalFile
     * @param MulticanalFile $image image
     * @return void
     */
    public function setImage(MulticanalFile $image): void {
        $this->image = $image;
    }
}
