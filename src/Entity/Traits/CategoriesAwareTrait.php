<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Multicanal\Entity\Category;

/**
 * Categories aware trait
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
trait CategoriesAwareTrait {

    /**
     * Initialize empty categories collection
     */
    public function initializeCategoriesCollection(): void {
        $this->categories = new ArrayCollection();
    }

    /**
     * Get categories collection
     * @return Collection
     */
    public function getCategories(): Collection {
        return $this->categories;
    }

    /**
     * Check if categories collection contains a specific category
     * @param Category $category
     * @return bool
     */
    public function hasCategory(Category $category): bool {
        return $this->categories->contains($category);
    }

    /**
     * Add category in categories collection
     * @param Category $category
     */
    public function addCategory(Category $category): void {
        if (true !== $this->hasCategory($category)) {
            $this->categories->add($category);
        }
    }

    /**
     * Remove category from categories collection
     * @param Category $category
     */
    public function removeCategory(Category $category): void {
        if (true === $this->hasCategory($category)) {
            $this->categories->removeElement($category);
        }
    }
}
