<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Multicanal\Entity\DistributionList;

/**
 * DistributionLists aware trait
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
trait DistributionListsAwareTrait {

    /**
     * Initialize empty distribution lists collection
     */
    public function initializeDistributionListsCollection(): void {
        $this->distributionLists = new ArrayCollection();
    }

    /**
     * Get distributionLists collection
     * @return Collection
     */
    public function getDistributionLists(): Collection {
        return $this->distributionLists;
    }

    /**
     * Check if distributionLists collection contains a specific distributionList
     * @param DistributionList $distributionList
     * @return bool
     */
    public function hasDistributionList(DistributionList $distributionList): bool {
        return $this->distributionLists->contains($distributionList);
    }

    /**
     * Add a distributionList in distributionLists collection
     * @param DistributionList $distributionList
     */
    public function addDistributionList(DistributionList $distributionList): void {
        if (true !== $this->hasDistributionList($distributionList)) {
            $this->distributionLists->add($distributionList);
        }
    }

    /**
     * Remove a distributionList from distributionLists collection
     * @param DistributionList $distributionList
     */
    public function removeDistributionList(DistributionList $distributionList): void {
        if (true === $this->hasDistributionList($distributionList)) {
            $this->distributionLists->removeElement($distributionList);
        }
    }
}
