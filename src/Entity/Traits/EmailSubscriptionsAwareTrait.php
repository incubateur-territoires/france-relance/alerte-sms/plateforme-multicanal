<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Multicanal\Entity\EmailSubscription;

/**
 * EmailSubscription aware trait
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
trait EmailSubscriptionsAwareTrait {

    /**
     * Initialize empty email subscriptions collection
     */
    public function initializeEmailSubscriptionsCollection(): void {
        $this->emailSubscriptions = new ArrayCollection();
    }

    /**
     * Get emailSubscriptions collection
     * @return Collection
     */
    public function getEmailSubscriptions(): Collection {
        return $this->emailSubscriptions;
    }

    /**
     * Check if emailSubscriptions collection contains a specific emailSubscription
     * @param EmailSubscription $emailSubscription
     * @return bool
     */
    public function hasEmailSubscription(EmailSubscription $emailSubscription): bool {
        return $this->emailSubscriptions->contains($emailSubscription);
    }

    /**
     * Add an emailSubscription in emailSubscriptions collection
     * @param EmailSubscription $emailSubscription
     */
    public function addEmailSubscription(EmailSubscription $emailSubscription): void {
        if (true !== $this->hasEmailSubscription($emailSubscription)) {
            $this->emailSubscriptions->add($emailSubscription);
        }
    }

    /**
     * Remove an emailSubscription from emailSubscriptions collection
     * @param EmailSubscription $emailSubscription
     */
    public function removeEmailSubscription(EmailSubscription $emailSubscription): void {
        if (true === $this->hasEmailSubscription($emailSubscription)) {
            $this->emailSubscriptions->removeElement($emailSubscription);
        }
    }

    /**
     * Get email subscribers as email array
     * @return array
     */
    public function getEmailSubscribersList(): array {
        return array_unique(array_map(function (EmailSubscription $emailSubscription) {
            return $emailSubscription->getEmail();
        }, $this->getEmailSubscriptions()->getValues()));
    }
}
