<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Multicanal\Entity\MobileSubscription;

/**
 * MobileSubscription aware trait
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
trait MobileSubscriptionsAwareTrait {

    /**
     * Initialize empty mobile subscriptions collection
     */
    public function initializeMobileSubscriptionsCollection(): void {
        $this->mobileSubscriptions = new ArrayCollection();
    }

    /**
     * Get mobileSubscriptions collection
     * @return Collection
     */
    public function getMobileSubscriptions(): Collection {
        return $this->mobileSubscriptions;
    }

    /**
     * Check if mobileSubscriptions collection contains a specific mobileSubscription
     * @param MobileSubscription $mobileSubscription
     * @return bool
     */
    public function hasMobileSubscription(MobileSubscription $mobileSubscription): bool {
        return $this->mobileSubscriptions->contains($mobileSubscription);
    }

    /**
     * Add a mobileSubscription in mobileSubscriptions collection
     * @param MobileSubscription $mobileSubscription
     */
    public function addMobileSubscription(MobileSubscription $mobileSubscription): void {
        if (true !== $this->hasMobileSubscription($mobileSubscription)) {
            $this->mobileSubscriptions->add($mobileSubscription);
        }
    }

    /**
     * Remove a mobileSubscription from mobileSubscriptions collection
     * @param MobileSubscription $mobileSubscription
     */
    public function removeMobileSubscription(MobileSubscription $mobileSubscription): void {
        if (true === $this->hasMobileSubscription($mobileSubscription)) {
            $this->mobileSubscriptions->removeElement($mobileSubscription);
        }
    }

    /**
     * Get mobile subscribers as mobile array
     * @return array
     */
    public function getMobileSubscribersList(): array {
        return array_unique(array_map(function (MobileSubscription $mobileSubscription) {
            return $mobileSubscription->getMobile();
        }, $this->getMobileSubscriptions()->getValues()));
    }
}
