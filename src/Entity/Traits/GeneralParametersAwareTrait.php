<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Traits;

use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Multicanal\Entity\GeneralParameters;

/**
 * GeneralParameters aware trait
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
trait GeneralParametersAwareTrait {

    /**
     * Organization's general parameters
     *
     * @OA\Property(
     *     description="Paramètres généraux de la collectivité",
     *     title="GeneralParameters",
     *     property="generalParameters",
     *     ref="#/components/schemas/GeneralParameters"
     * )
     * @var GeneralParameters
     */
    #[
        OneToOne(
            targetEntity: "GeneralParameters",
            cascade: ["all"],
            orphanRemoval: true
        )
    ]
    #[
        JoinColumn(
            name: "general_parameters_id",
            referencedColumnName: "id",
            onDelete: "cascade"
        )
    ]
    private GeneralParameters $generalParameters;


    /**
     * Get organization's general parameters
     * @return GeneralParameters
     */
    public function getGeneralParameters(): GeneralParameters {
        return $this->generalParameters;
    }

    /**
     * Set organization's general parameters
     * @param GeneralParameters $generalParameters
     * @return void
     */
    public function setGeneralParameters(GeneralParameters $generalParameters): void {
        $this->generalParameters = $generalParameters;
    }
}
