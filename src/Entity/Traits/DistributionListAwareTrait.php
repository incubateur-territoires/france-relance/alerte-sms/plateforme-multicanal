<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Traits;

use Multicanal\Entity\DistributionList;

/**
 * DistributionList aware trait
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
trait DistributionListAwareTrait {

    /**
     * Get distribution list
     * @return DistributionList|null
     */
    public function getDistributionList(): ?DistributionList {
        return $this->distributionList;
    }

    /**
     * Set a distribution list
     * @param DistributionList|null $distributionList
     */
    public function setDistributionList(?DistributionList $distributionList): void {
        $this->distributionList = $distributionList;
    }
}
