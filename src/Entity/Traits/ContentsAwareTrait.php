<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Multicanal\Entity\Content;

/**
 * Content aware trait
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
trait ContentsAwareTrait {

    /**
     * Initialize empty contents collection
     */
    public function initializeContentsCollection(): void {
        $this->contents = new ArrayCollection();
    }

    /**
     * Get contents collection
     * @return Collection
     */
    public function getContents(): ?Collection {
        return $this->contents;
    }

    /**
     * Check if contents collection contains a specific content
     * @param Content $content
     * @return bool
     */
    public function hasContent(Content $content): bool {
        return $this->contents->contains($content);
    }

    /**
     * Add a content in contents collection
     * @param Content $content
     */
    public function addContent(Content $content): void {
        if (true !== $this->hasContent($content)) {
            $this->contents->add($content);
        }
    }

    /**
     * Remove a content from contents collection
     * @param Content $content
     */
    public function removeContent(Content $content): void {
        if (true === $this->hasContent($content)) {
            $this->contents->removeElement($content);
        }
    }
}
