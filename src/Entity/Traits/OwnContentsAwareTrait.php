<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Multicanal\Entity\Content;

/**
 * OwnContent aware trait
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
trait OwnContentsAwareTrait {

    /**
     * Initialize empty user own contents collection
     */
    public function initializeOwnContentsCollection(): void {
        $this->ownContents = new ArrayCollection();
    }

    /**
     * Get ownContents collection
     * @return Collection
     */
    public function getOwnContents(): Collection {
        return $this->ownContents;
    }

    /**
     * Check if ownContents collection contains a specific content
     * @param Content $ownContents
     * @return bool
     */
    public function hasOwnContent(Content $ownContents): bool {
        return $this->ownContents->contains($ownContents);
    }

    /**
     * Add a content in ownContents collection
     * @param Content $ownContents
     */
    public function addOwnContent(Content $ownContents): void {
        if (true !== $this->hasOwnContent($ownContents)) {
            $this->ownContents->add($ownContents);
        }
    }

    /**
     * Remove a content from ownContents collection
     * @param Content $ownContents
     */
    public function removeOwnContent(Content $ownContents): void {
        if (true === $this->hasOwnContent($ownContents)) {
            $this->ownContents->removeElement($ownContents);
        }
    }
}
