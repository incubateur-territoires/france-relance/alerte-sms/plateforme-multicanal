<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Multicanal\Entity\User;

/**
 * Authors aware trait
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
trait AuthorsAwareTrait {

    /**
     * Initialize empty authors collection
     */
    public function initializeAuthorsCollection(): void {
        $this->authors = new ArrayCollection();
    }

    /**
     * Get authors collection
     * @return Collection
     */
    public function getAuthors(): Collection {
        return $this->authors;
    }

    /**
     * Check if authors collection contains a specific user as author
     * @param User $author
     * @return bool
     */
    public function hasAuthor(User $author): bool {
        return $this->authors->contains($author);
    }

    /**
     * Add user as author in collection
     * @param User $author
     */
    public function addAuthor(User $author): void {
        if (true !== $this->hasAuthor($author)) {
            $this->authors->add($author);
        }
    }

    /**
     * Remove user from auhors collection
     * @param User $author
     */
    public function removeAuthor(User $author): void {
        if (true === $this->hasAuthor($author)) {
            $this->authors->removeElement($author);
        }
    }
}
