<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Traits;

use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Multicanal\Entity\EmailParameters;

/**
 * EmailParameters aware trait
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
trait EmailParametersAwaresTrait {

    /**
     * Organization's email parameters
     *
     * @OA\Property(
     *     description="Paramètres d'emailing de la collectivité",
     *     title="EmailParameters",
     *     property="emailParameters",
     *     ref="#/components/schemas/EmailParameters"
     * )
     * @var EmailParameters
     */
    #[
        OneToOne(
            targetEntity: "EmailParameters",
            cascade: ["persist"],
            orphanRemoval: true
        )
    ]
    #[
        JoinColumn(
            name: "email_parameters_id",
            referencedColumnName: "id",
            onDelete: "cascade"
        )
    ]
    private EmailParameters $emailParameters;


    /**
     * Get organization's email settings
     * @return  EmailParameters Organization's email settings
     */
    public function getEmailParameters(): EmailParameters {
        return $this->emailParameters;
    }

    /**
     * Set organization's email settings
     * @param EmailParameters $emailParameters Organization's email settings
     * @return void
     */
    public function setEmailParameters(EmailParameters $emailParameters): void {
        $this->emailParameters = $emailParameters;
    }
}
