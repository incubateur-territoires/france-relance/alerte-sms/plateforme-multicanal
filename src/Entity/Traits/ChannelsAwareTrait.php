<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Multicanal\Entity\SocialNetwork\Channel;

/**
 * Channels aware trait
 *
 * @package Multicanal\Entity
 * @author  Nathanael KHODL <consulting@khodl.com>
 */
trait ChannelsAwareTrait {

    /**
     * Initialize empty channels collection
     */
    public function initializeChannelsCollection(): void {
        $this->channels = new ArrayCollection();
    }

    /**
     * Get channels collection
     * @return Collection
     */
    public function getChannels(): Collection {
        return $this->channels;
    }

    /**
     * Get channels collection if not archived
     * @return Collection
     */
    public function getActiveChannels(): Collection {
        if (!is_null($this->channels)) {
            return $this->channels->filter(function (Channel $channel) {
                return $channel->isArchived() === false;
            });
        } else {
            return new ArrayCollection();
        }
    }

    /**
     * Check if channels collection contains a specific channel
     * @param Channel $channel
     * @return bool
     */
    public function hasChannel(Channel $channel): bool {
        return $this->channels->contains($channel);
    }

    /**
     * Add channel in channels collection
     * @param Channel $channel
     */
    public function addChannel(Channel $channel): void {
        if (true !== $this->hasChannel($channel)) {
            $this->channels->add($channel);
        }
    }

    /**
     * Remove channel from channels collection
     * @param Channel $channel
     */
    public function removeChannel(Channel $channel): void {
        if (true === $this->hasChannel($channel)) {
            $this->channels->removeElement($channel);
        }
    }
}
