<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Traits;

use Multicanal\Entity\Organization;

/**
 * Organization aware trait
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
trait OrganizationAwareTrait {

    /**
     * Get organization
     * @return Organization
     */
    public function getOrganization(): Organization {
        return $this->organization;
    }

    /**
     * Set organization
     * @param Organization $organization
     * @return void
     */
    public function setOrganization(Organization $organization): void {
        $this->organization = $organization;
    }
}
