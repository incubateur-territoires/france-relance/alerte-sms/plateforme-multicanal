<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Multicanal\Entity\MulticanalFile;

/**
 * Media aware trait
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
trait MediasAwareTrait {

    /**
     * Initialize empty medias collection
     */
    public function initializeMediasCollection(): void {
        $this->medias = new ArrayCollection();
    }

    /**
     * Get medias collection
     * @return Collection
     */
    public function getMedias(): Collection {
        return $this->medias;
    }

    /**
     * Check if medias collection contains a specific media
     * @param MulticanalFile $media
     * @return bool
     */
    public function hasMedia(MulticanalFile $media): bool {
        return $this->medias->contains($media);
    }

    /**
     * Add a media in medias collection
     * @param MulticanalFile $media
     */
    public function addMedia(MulticanalFile $media): void {
        if (true !== $this->hasMedia($media)) {
            $this->medias->add($media);
        }
    }

    /**
     * Remove a media from medias collection
     * @param MulticanalFile $media
     */
    public function removeMedia(MulticanalFile $media): void {
        if (true === $this->hasMedia($media)) {
            $this->medias->removeElement($media);
        }
    }

    /**
     * return Media path list
     * @return array
     */
    public function getMediasPath(): array {
        return array_unique(array_map(function (MulticanalFile $multicanalFile) {
            return $multicanalFile->getPath();
        }, $this->getMedias()->getValues()));
    }
}
