<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Exception;
use Multicanal\Entity\User;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpInternalServerErrorException;

/**
 * User aware trait
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
trait UsersAwareTrait {

    /**
     * Initialize empty users collection
     */
    public function initializeUsersCollection(): void {
        $this->users = new ArrayCollection();
    }

    /**
     * Get users collection
     * @return Collection
     */
    public function getUsers(): Collection {
        return $this->users;
    }

    /**
     * Check if users collection contains a specific user
     * @param User $user
     * @return bool
     */
    public function hasUser(User $user): bool {
        return $this->users->contains($user);
    }

    /**
     * Add an user in users collection
     * @param User $user
     */
    public function addUser(User $user): void {
        if (true !== $this->hasUser($user)) {
            $this->users->add($user);
        }
    }

    /**
     * Remove an user from users collection
     * @param User $user
     */
    public function removeUser(User $user): void {
        if (true === $this->hasUser($user)) {
            $this->users->removeElement($user);
        }
    }

    /**
     * To API function to format result for API.
     *
     * @param ServerRequestInterface $request Current HTTP request from Slim framework
     * @return array
     * @throws HttpInternalServerErrorException
     */
    public function __usersToApi(ServerRequestInterface $request): array {
        try {
            $users = [];
            /** @var User $user */
            foreach ($this->users as $user) {
                $users[] = $user->getEmail();
            }
            return $users;
        } catch (Exception $e) {
            throw new HttpInternalServerErrorException($request, 'Une erreur interne s\'est produite lors de la récupération de la liste des utilisateurs.', $e);
        }
    }
}
