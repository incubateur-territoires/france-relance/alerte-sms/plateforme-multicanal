<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Traits;

use Multicanal\Entity\Content;

/**
 * Content aware trait
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
trait ContentAwareTrait {


    /**
     * Get content
     * @return Content
     */
    public function getContent(): Content {
        return $this->content;
    }

    /**
     * Set a content
     * @param Content|null $content
     */
    public function setContent(?Content $content): void {
        $this->content = $content;
    }
}
