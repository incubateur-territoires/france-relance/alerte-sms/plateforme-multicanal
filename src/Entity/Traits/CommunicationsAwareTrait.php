<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Multicanal\Entity\Communication;

/**
 * Communication aware trait
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
trait CommunicationsAwareTrait {

    /**
     * Initialize empty communications collection
     */
    public function initializeCommunicationsCollection(): void {
        $this->communications = new ArrayCollection();
    }

    /**
     * Get communications collection
     * @return Collection
     */
    public function getCommunications(): Collection {
        return $this->communications;
    }

    /**
     * Check if communications collection contains a specific communication
     * @param Communication $communication
     * @return bool
     */
    public function hasCommunication(Communication $communication): bool {
        return $this->communications->contains($communication);
    }

    /**
     * Add a communication in communications collection
     * @param Communication $communication
     */
    public function addCommunication(Communication $communication): void {
        if (true !== $this->hasCommunication($communication)) {
            $this->communications->add($communication);
        }
    }

    /**
     * Remove a communication from communications collection
     * @param Communication $communication
     */
    public function removeCommunication(Communication $communication): void {
        if (true === $this->hasCommunication($communication)) {
            $this->communications->removeElement($communication);
        }
    }
}
