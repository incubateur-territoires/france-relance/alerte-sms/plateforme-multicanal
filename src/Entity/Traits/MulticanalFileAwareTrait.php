<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Traits;

use Multicanal\Entity\MulticanalFile;

/**
 * MulticanalFile aware trait
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
trait MulticanalFileAwareTrait {


    /**
     * Get MulticanalFile
     * @return  MulticanalFile MulticanalFile
     */
    public function getMulticanalFile(): MulticanalFile {
        return $this->multicanalFile;
    }

    /**
     * Set MulticanalFile
     * @param MulticanalFile $multicanalFile MulticanalFile
     * @return void
     */
    public function setMulticanalFile(MulticanalFile $multicanalFile): void {
        $this->multicanalFile = $multicanalFile;
    }
}
