<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Traits;

use Multicanal\Entity\Category;

/**
 * Category aware trait
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
trait CategoryAwareTrait {

    /**
     * Get category
     * @return Category|null
     */
    public function getCategory(): ?Category {
        return $this->category;
    }

    /**
     * Set a category
     * @param Category|null $category
     */
    public function setCategory(?Category $category): void {
        $this->category = $category;
    }
}
