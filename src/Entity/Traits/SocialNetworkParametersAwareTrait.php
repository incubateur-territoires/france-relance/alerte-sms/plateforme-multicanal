<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Traits;

use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Multicanal\Entity\SocialNetworkParameters;

/**
 * SocialNetworkParameters aware trait
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
trait SocialNetworkParametersAwareTrait {

    /**
     * Organization's social network parameters
     *
     * @OA\Property(
     *     description="Paramètres des réseaux sociaux de la collectivité",
     *     title="SocialNetworkParameters",
     *     property="socialNetworkParameters",
     *     ref="#/components/schemas/SocialNetworkParameters"
     * )
     * @var SocialNetworkParameters
     */
    #[
        OneToOne(
            targetEntity: "SocialNetworkParameters",
            cascade: ["all"],
            orphanRemoval: true
        )
    ]
    #[
        JoinColumn(
            name: "social_network_parameters_id",
            referencedColumnName: "id",
            onDelete: "cascade"
        )
    ]
    private SocialNetworkParameters $socialNetworkParameters;


    /**
     * Get organization's social network parameters
     * @return  SocialNetworkParameters
     */
    public function getSocialNetworkParameters(): SocialNetworkParameters {
        return $this->socialNetworkParameters;
    }

    /**
     * Set organization's social network parameters
     * @param SocialNetworkParameters $socialNetworkParameters Organization's social network parameters
     * @return void
     */
    public function setSocialNetworkParameters(SocialNetworkParameters $socialNetworkParameters): void {
        $this->socialNetworkParameters = $socialNetworkParameters;
    }
}
