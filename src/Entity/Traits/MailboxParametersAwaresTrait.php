<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Traits;

use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Multicanal\Entity\MailboxParameters;

/**
 * MailboxParameters aware trait
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
trait MailboxParametersAwaresTrait {

    /**
     * Organization's mailbox parameters
     * @var MailboxParameters
     */
    #[
        OneToOne(
            targetEntity: "MailboxParameters",
            cascade: ["persist"],
            orphanRemoval: true
        )
    ]
    #[
        JoinColumn(
            name: "mailbox_parameters_id",
            referencedColumnName: "id",
            onDelete: "cascade"
        )
    ]
    private ?MailboxParameters $mailboxParameters;


    /**
     * Get organization's mailbox settings
     * @return MailboxParameters|null Organization's mailbox settings
     */
    public function getMailboxParameters(): ?MailboxParameters {
        return $this->mailboxParameters;
    }

    /**
     * Set organization's mailbox settings
     * @param MailboxParameters $mailboxParameters Organization's mailbox settings
     * @return void
     */
    public function setMailboxParameters(MailboxParameters $mailboxParameters): void {
        $this->mailboxParameters = $mailboxParameters;
    }
}
