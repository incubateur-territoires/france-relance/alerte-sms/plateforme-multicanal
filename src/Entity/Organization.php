<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\PreRemove;
use Doctrine\ORM\Mapping\Table;
use Exception;
use JsonSerializable;
use Multicanal\App\Helper;
use Multicanal\Entity\SocialNetwork\Channel;
use Multicanal\Entity\Traits\CategoriesAwareTrait;
use Multicanal\Entity\Traits\ChannelsAwareTrait;
use Multicanal\Entity\Traits\CountersAwareTrait;
use Multicanal\Entity\Traits\DistributionListsAwareTrait;
use Multicanal\Entity\Traits\EmailParametersAwaresTrait;
use Multicanal\Entity\Traits\GeneralParametersAwareTrait;
use Multicanal\Entity\Traits\MailboxParametersAwaresTrait;
use Multicanal\Entity\Traits\SocialNetworkParametersAwareTrait;
use Multicanal\Entity\Traits\UsersAwareTrait;

/**
 * Class Organization
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 *
 * @OA\Schema(
 *     description="Spécifications du modèle décrivant une collectivité.",
 *     title="Collectivité",
 *     required={"domain"},
 *	   @OA\Xml(
 *        name="Organization"
 *     )
 * )
 */
#[Entity, Table(name: 'organization')]
class Organization implements JsonSerializable {
    use DistributionListsAwareTrait,
        GeneralParametersAwareTrait,
        EmailParametersAwaresTrait,
        SocialNetworkParametersAwareTrait,
        MailboxParametersAwaresTrait,
        CountersAwareTrait,
        UsersAwareTrait,
        CategoriesAwareTrait,
        ChannelsAwareTrait;

    /**
     * Organization identifier as autoincremented integer from database
     *
     * @OA\Property(
     *     description="Identifiant de la collectivité",
     *     title="Identifier",
     *     property="id",
     *     type="integer"
     * )
     * @var integer
     */
    #[Id, Column(type: Types::INTEGER), GeneratedValue(strategy: 'AUTO')]
    private int $id;

    /**
     * Organization's domain name
     *
     * @OA\Property(
     *     description="Nom de domaine de la collectivité, exemple girondenumerique.fr pour Gironde Numérique.",
     *     title="Domain name",
     *     property="domain",
     *     type="string"
     * )
     * @var string
     */
    #[Column(type: Types::STRING, unique: true)]
    private string $domain;

    /**
     * Organization's distribution list
     *
     * @OA\Property(
     *     description="Listes de diffusion de la collectivité",
     *     title="Distribution lists",
     *     property="distributionLists",
     *     type="array",
     *     @OA\Items(
     *          ref="#/components/schemas/DistributionList"
     *     )
     * )
     * @var Collection
     */
    #[
        OneToMany(
            mappedBy: "organization",
            targetEntity: "DistributionList",
            cascade: ["all"],
            orphanRemoval: true
        )
    ]
    private Collection $distributionLists;

    /**
     * Organization's users list
     *
     * @OA\Property(
     *     description="Liste des utilisateurs de la collectivité",
     *     title="Users",
     *     property="users",
     *     type="array",
     *     @OA\Items(
     *          ref="#/components/schemas/User"
     *     )
     * )
     * @var Collection
     */
    #[
        OneToMany(
            mappedBy: "organization",
            targetEntity: "User",
            cascade: ["all"],
            orphanRemoval: true
        )
    ]
    private Collection $users;

    /**
     * Organization's categories list
     *
     * @OA\Property(
     *     description="Liste des catégories associées à la collectivité",
     *     title="Categories list",
     *     property="categories",
     *     type="array",
     *     @OA\Items(
     *          ref="#/components/schemas/Category"
     *     )
     * )
     * @var Collection
     */
    #[
        OneToMany(
            mappedBy: "organization",
            targetEntity: "Category",
            cascade: ["remove"],
            orphanRemoval: true
        )
    ]
    private Collection $categories;

    /**
     * Organization counter list
     * @var Collection
     */
    #[
        OneToMany(
            mappedBy: "organization",
            targetEntity: "Counter",
            cascade: ["all"],
            orphanRemoval: true
        )
    ]
    private Collection $counters;

    /**
     * Organization's channels list
     * @var Collection
     */
    #[
        OneToMany(
            mappedBy: "organization",
            targetEntity: Channel::class,
            cascade: ["all"],
            orphanRemoval: true
        )
    ]
    private ?Collection $channels = null;

    /**
     * Organization constructor.
     * @param string $domain
     */
    public function __construct(string $domain) {
        $this->domain = $domain;
        $this->setGeneralParameters(new GeneralParameters($domain));
        $this->setEmailParameters(new EmailParameters());
        $this->setSocialNetworkParameters(new SocialNetworkParameters());
        $this->setMailboxParameters(new MailboxParameters());
        $this->initializeCountersCollection();
        $this->initializeDistributionListsCollection();
        $this->initializeCategoriesCollection();
        $this->initializeUsersCollection();
        $this->initializeChannelsCollection();
    }

    /**
     * Get the value of id
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * Set the value of id
     * @return  self
     */
    public function setId(int $id): self {
        $this->id = $id;
        return $this;
    }

    /**
     * Get organization's domain
     * @return  string
     */
    public function getDomain(): string {
        return $this->domain;
    }

    /**
     * Set organization's domain
     * @param string $domain Organization's domain
     * @return  self
     */
    public function setDomain(string $domain): self {
        $this->domain = $domain;
        return $this;
    }

    /**
     * Removes physical illustration file on entity deletion
     */
    #[PreRemove]
    public function onPreRemove() {
        if ($this->getGeneralParameters()->getLogo() instanceof MulticanalFile) {
            $logo = $this->getGeneralParameters()->getLogo();
            unlink($logo->getLocation() . DIRECTORY_SEPARATOR . $logo->getName());
        }
    }

    /**
     * To string
     */
    public function __toString(): string {
        try {
            return $this->domain;
        } catch (Exception $e) {
            if (Helper::isDebugEnabled()) {
                return $e->getMessage();
            }
            return '';
        }
    }

    /**
     * JSON serialization
     * @return array
     */
    public function jsonSerialize(): array {
        return get_object_vars($this);
    }
}
