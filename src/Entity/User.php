<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;
use Exception;
use Multicanal\App\Helper;
use Multicanal\Entity\Enum\Rank;
use Multicanal\Entity\Traits\AuthorsAwareTrait;
use Multicanal\Entity\Traits\CategoriesAwareTrait;
use Multicanal\Entity\Traits\CommunicationsAwareTrait;
use Multicanal\Entity\Traits\ContentsAwareTrait;
use Multicanal\Entity\Traits\CreatorAwareTrait;
use Multicanal\Entity\Traits\DistributionListsAwareTrait;
use Multicanal\Entity\Traits\OrganizationAwareTrait;
use Multicanal\Entity\Traits\OwnContentsAwareTrait;

/**
 * Class User
 *
 * @package Multicanal\Entity
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 * @OA\Schema(
 *     description="Spécifications du modèle décrivant un utilisateur.",
 *     title="User",
 *     required={"email"},
 *	   @OA\Xml(
 *        name="User"
 *     )
 * )
 */
#[Entity, Table(name: 'user')]
class User implements \JsonSerializable {
    use DistributionListsAwareTrait,
        ContentsAwareTrait,
        CategoriesAwareTrait,
        AuthorsAwareTrait,
        CreatorAwareTrait,
        OwnContentsAwareTrait,
        OrganizationAwareTrait,
        CommunicationsAwareTrait;

    /**
     * User identifier
     * @OA\Property(
     *     description = "User identifier",
     *     title = "identifier",
     *     property = "id",
     *     type = "integer"
     * )
     * @var int
     */
    #[Id, Column(type: Types::INTEGER), GeneratedValue(strategy: 'AUTO')]
    private int $id;

    /**
     * User login
     * @var string
     */
    #[Column(type: Types::STRING, unique: false, nullable: false)]
    private string $login;

    /**
     * Content authors Collection list
     * @var Collection
     */
    #[ManyToMany(targetEntity: "Content", mappedBy: "authors")]
    private Collection $contents;

    /**
     * Content authors Collection list
     * @var Collection
     */
    #[OneToMany(
        mappedBy: "creator",
        targetEntity: "Content"
    )]
    private Collection $ownContents;

    /**
     * DistributionList authors Collection list
     * @var Collection
     */
    #[OneToMany(
        mappedBy: "creator",
        targetEntity: "DistributionList",
        cascade: ["persist", "merge"],
        orphanRemoval: false
    )]
    private Collection $distributionLists;


    /**
     * DistributionList authors Collection list
     * @var Collection
     */
    #[OneToMany(
        mappedBy: "creator",
        targetEntity: "Category"
    )]
    private Collection $categories;

    /**
     * Content communications
     * @var Collection
     */
    #[
        OneToMany(
            mappedBy: "publisher",
            targetEntity: "Communication"
        )
    ]
    private Collection $communications;

    /**
     * Organization
     * @var Organization
     */
    #[ManyToOne(targetEntity: "Organization", cascade: ["persist"], inversedBy: "users")]
    private Organization $organization;

    /**
     * User password
     * @var string
     */
    private string $password;

    /**
     * User firstname
     * @var string
     */
    private string $firstname;

    /**
     * User lastname
     * @var string
     */
    private string $lastname;

    /**
     * User email
     * @OA\Property(
     *     description = "User email",
     *     title = "email",
     *     property = "email",
     *     type = "string"
     * )
     * @var string
     */
    #[Column(type: Types::STRING, unique: true, nullable: false)]
    private string $email;

    /**
     * User rank
     * @var Rank
     */
    private ?Rank $rank;

    /**
     * User type
     * @var string
     */
    private string $type;

    /**
     * User applicationsArray
     * @var array
     */
    private array $applicationsArray;

    /**
     * User description
     * @var string
     */
    private string $description;

    /**
     * User organization's name
     * @var string
     */
    private string $organizationName;

    /**
     * User date of last connection
     * @var string
     */
    private string $lastConnection;

    /**
     * User id token
     * @var string|null
     */
    private ?string $idToken;

    /**
     * User access token
     * @var string
     */
    private string $accessToken;

    /**
     * User refresh token
     * @var string
     */
    private string $refreshToken;

    /**
     * Fields constructor
     *
     * @param string $login User login
     */
    public function __construct(string $login) {
        $this->login = strtolower($login);
        $this->initializeCategoriesCollection();
        $this->initializeContentsCollection();
        $this->initializeContentsCollection();
        $this->initializeOwnContentsCollection();
        $this->initializeCommunicationsCollection();
    }

    /**
     * Get user identifier
     * @return  int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * Set user identifier
     * @param int $id User identifier
     * @return  self
     */
    public function setId(int $id): self {
        $this->id = $id;
        return $this;
    }

    /**
     * Get user login
     * @return  string
     */
    public function getLogin(): string {
        return $this->login;
    }

    /**
     * Set user login
     * @param string $login User identifier
     * @return  self
     */
    public function setLogin(string $login): self {
        $this->login = $login;
        return $this;
    }

    /**
     * Get user password
     * @return  string
     */
    public function getPassword(): string {
        return $this->password;
    }

    /**
     * Set user password
     * @param string $password User password
     * @return  self
     */
    public function setPassword(string $password): self {
        $this->password = $password;
        return $this;
    }

    /**
     * Get user firstname
     * @return  string
     */
    public function getFirstname(): string {
        return $this->firstname;
    }

    /**
     * Set user firstname
     * @param string $firstname User firstname
     * @return  self
     */
    public function setFirstname(?string $firstname): self {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * Get user lastname
     * @return  string
     */
    public function getLastname(): string {
        return $this->lastname;
    }

    /**
     * Set user lastname
     * @param string $lastname User lastname
     * @return  self
     */
    public function setLastname(?string $lastname): self {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * Get user name
     * @return  string
     */
    public function getName(): string {
        return $this->firstname . ' ' . $this->lastname;
    }

    /**
     * Get user email
     * @return  string
     */
    public function getEmail(): string {
        return $this->email;
    }

    /**
     * Set user email
     * @param string $email User email
     * @return  self
     */
    public function setEmail(?string $email): self {
        $this->email = $email;
        return $this;
    }

    /**
     * Get user rank
     * @return  Rank
     */
    public function getRank(): ?Rank {
        return $this->rank;
    }

    /**
     * Set user rank
     * @param Rank $rank User rank
     * @return  self
     */
    public function setRank(Rank $rank): self {
        $this->rank = $rank;
        return $this;
    }

    /**
     * Get user type
     * @return  string
     */
    public function getType(): string {
        return $this->type;
    }

    /**
     * Set user type
     * @param string $type User type
     * @return  self
     */
    public function setType(string $type): self {
        $this->type = $type;
        return $this;
    }

    /**
     * Get user applicationsArray
     * @return  array
     */
    public function getApplicationsArray(): array {
        return $this->applicationsArray;
    }

    /**
     * Set user applicationsArray
     * @param string $applicationsArray User applicationsArray
     */
    public function setApplicationsArray(string $applicationsArray): self {
        $this->applicationsArray = $applicationsArray;
        return $this;
    }

    /**
     * Get user description
     * @return  string
     */
    public function getDescription(): string {
        return $this->description;
    }

    /**
     * Set user description
     * @param string $description User description
     * @return  self
     */
    public function setDescription(string $description): self {
        $this->description = $description;
        return $this;
    }

    /**
     * Get user organization's name
     * @return  string
     */
    public function getOrganizationName(): string {
        return $this->organizationName;
    }


    /**
     * Get user domain name without tld extension
     * @return  string
     */
    public function getDomainName(): string {
        return explode('.', $this->organizationName)[0];
    }


    /**
     * Set user organization's name
     * @param string $organizationName organization's name
     */
    public function setOrganizationName(string $organizationName): self {
        $this->organizationName = $organizationName;
        return $this;
    }

    /**
     * Get user date of last connection
     * @return  string
     */
    public function getLastConnection(): string {
        return $this->lastConnection;
    }

    /**
     * Set user date of last connection
     * @param string $lastConnection date of last connection
     * @return  self
     */
    public function setLastConnection(string $lastConnection): self {
        $this->lastConnection = $lastConnection;
        return $this;
    }

    /**
     * Get OIDC IdToken
     * @return  string
     */
    public function getIdToken(): ?string {
        return $this->idToken;
    }

    /**
     * Set OIDC IdToken
     * @param string $organizationName organization's name
     * @return  self
     */
    public function setIdToken(string $idToken): self {
        $this->idToken = $idToken;
        return $this;
    }

    /**
     * Get OIDC accessToken
     * @return  string
     */
    public function getAccessToken(): string {
        return $this->accessToken;
    }

    /**
     * Set OIDC accessToken
     * @param string $accessToken OIDC accessToken
     * @return  self
     */
    public function setAccessToken(string $accessToken): self {
        $this->accessToken = $accessToken;
        return $this;
    }

    /**
     * Get OIDC refreshToken
     * @return  string
     */
    public function getRefreshToken(): string {
        return $this->refreshToken;
    }

    /**
     * Set OIDC refreshToken
     * @param string $refreshToken OIDC refreshToken
     * @return  self
     */
    public function setRefreshToken(string $refreshToken): self {
        $this->refreshToken = $refreshToken;
        return $this;
    }

    /**
     * Override to string function
     * @return string
     */
    public function __toString(): string {
        try {
            return $this->firstname . ' ' . $this->lastname . ' (' . $this->email . ')';
        } catch (Exception $e) {
            if (Helper::isDebugEnabled()) {
                return $e->getMessage();
            }
            return '';
        }
    }

    /**
     * JSON serialization
     * @return array
     */
    public function jsonSerialize(): array {
        return get_object_vars($this);
    }
}
