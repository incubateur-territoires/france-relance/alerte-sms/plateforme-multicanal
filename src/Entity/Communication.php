<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity;

use DateTime;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use JsonSerializable;
use Multicanal\Entity\Enum\ChannelType;
use Multicanal\Entity\SocialNetwork\Channel;
use Multicanal\Entity\Traits\ContentAwareTrait;
use Multicanal\Entity\Traits\PublisherAwareTrait;

/**
 * Class Communication
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 *
 * @OA\Schema(
 *     description="Spécifications du modèle décrivant une Communication.",
 *     title="Communication",
 *     required={"content", "channel"},
 *	   @OA\Xml(
 *        name="Communication"
 *     )
 * )
 */
#[Entity, Table(name: 'communication')]
class Communication implements JsonSerializable {
    use ContentAwareTrait,
        PublisherAwareTrait;

    /**
     * Communication identifier as autoincremented integer from database
     *
     * @OA\Property(
     *     description="Identifiant de la communication",
     *     title="Identifier",
     *     property="id",
     *     type="integer"
     * )
     * @var int
     */
    #[Id, Column(type: Types::INTEGER), GeneratedValue(strategy: 'AUTO')]
    private int $id;

    /**
     * Communication object used for email channel
     *
     * @OA\Property(
     *     description="Sujet de la communication (employé pour le canal email)",
     *     title="Object",
     *     property="object",
     *     type="string"
     * )
     * @var null|string
     */
    #[Column(type: Types::STRING, length: 240)]
    private ?string $object;

    /**
     * Communication text
     *
     * @OA\Property(
     *     description="Texte de la communication",
     *     title="Text",
     *     property="text",
     *     type="string"
     * )
     * @var string|null
     */
    #[Column(type: Types::TEXT, length: 400)]
    private ?string $text;

    /**
     * Communication sender's email
     *
     * @OA\Property(
     *     description="Adresse email de l'expéditeur pour le reply-to dans un envoi email",
     *     title="Sender's email",
     *     property="senderMail",
     *     type="string"
     * )
     * @var string|null
     */
    #[Column(name: 'sender_mail', type: Types::TEXT, length: 150, nullable: true)]
    private ?string $senderMail;

    /**
     * Communication sent flag
     *
     * @OA\Property(
     *     description="Indicateur d'envoi de la communication",
     *     title="Sent",
     *     property="sent",
     *     type="boolean"
     * )
     * @var bool
     */
    #[Column(type: Types::BOOLEAN)]
    private bool $sent = false;

    /**
     * Communication channel
     *
     * @OA\Property(
     *     description="Canal de la communication",
     *     title="Channel",
     *     property="channel",
     *     type="string"
     * )
     * @var string
     */
    #[Column(type: 'enum_channel_type')]
    private string $channel;

    /**
     * Channel associated with the communication
     * @var null|Channel
     */
    #[
        ManyToOne(
            targetEntity: Channel::class,
            //inversedBy: "channels" // TODO: add that
        )
    ]
    #[JoinColumn(nullable: true, name: 'channel_id')]
    private ?Channel $organisationChannel;

    /**
     * Communication user publisher
     *
     * @OA\Property(
     *     description="Publieur de la communication",
     *     title="Publisher",
     *     property="publisher",
     *     ref="#/components/schemas/User"
     * )
     * @var string|User|null
     */
    #[ManyToOne(targetEntity: "User", cascade: ["persist", "merge"], inversedBy: "communications")]
    private User|string|null $publisher;

    /**
     * Communication schedule date
     *
     * @OA\Property(
     *     description="Date planifiée d'envoi de la communication",
     *     title="Schedule date",
     *     property="scheduleDate",
     *     type="string",
     *     format="date"
     * )
     * @var ?DateTime
     */
    #[Column(name: 'schedule_date', type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTime $scheduleDate;

    /**
     * Communication sending date
     *
     * @OA\Property(
     *     description="Date d'envoi de la communication",
     *     title="Sending date",
     *     property="sendingDate",
     *     type="string",
     *     format="date"
     * )
     * @var ?DateTime
     */
    #[Column(name: 'sending_date', type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTime $sendingDate;


    /**
     * Content concerned by the communication
     *
     * @OA\Property(
     *     description="Contenu concerné par la communication",
     *     title="Content",
     *     property="content",
     *     ref="#/components/schemas/Content"
     * )
     *
     * @var null|Content
     */
    #[ManyToOne(targetEntity: "Content", cascade: ["persist"], inversedBy: "communications")]
    private ?Content $content;

    /**
     * Medias used in the communication
     *
     * @OA\Property(
     *     description="Medias concernés par la communication",
     *     title="Medias",
     *     property="medias",
     *     ref="#/components/schemas/MulticanalFile"
     * )
     *
     * @var Collection $medias
     */
    #[ManyToMany(targetEntity: MulticanalFile::class)]
    private Collection $medias;

    /**
     * Fields constructor
     *
     * @param Content $content Concerned content by the communication
     * @param Channel $channel Communication channel (EMAIL, SMS, WEBSITE, FACEBOOK or INSTAGRAM)
     * @param string|null $object Communication object used for email channel
     * @param string|null $text Content text
     */
    public function __construct(Content $content, string $channelName = null, ?string $object = '', ?string $text = '', $channel = null) {
        if (!empty($object)) {
            $this->setObject($object);
        } else {
            $this->setObject($content->getTitle());
        }
        if (!empty($text)) {
            $this->setText($text);
        } else {
            $this->setText($content->getText());
        }

        if ($channelName) {
            $this->setChannel($channelName);
        } elseif ($channel) {
            $this->setChannel(ChannelType::CHANNEL);
            $this->setOrganisationChannel($channel);
        } else {
            throw new \LogicException("Channel name or channel object must be provided");
        }
        $this->setContent($content);
    }

    /**
     * Get communication identifier as autoincremented integer from database
     * @return  int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * Set communication identifier as autoincremented integer from database
     * @param  int  $id  Communication identifier as autoincremented integer from database
     * @return  self
     */
    public function setId(int $id): self {
        $this->id = $id;
        return $this;
    }

    /**
     * Get communication object used for email channel
     * @return  null|string
     */
    public function getObject(): ?string {
        return $this->object;
    }

    /**
     * Set communication object used for email channel
     * @param  null|string  $object  Communication object used for email channel
     * @return  self
     */
    public function setObject(?string $object): self {
        $this->object = $object;
        return $this;
    }

    /**
     * Get communication text
     * @return  string|null
     */
    public function getText(): ?string {
        return $this->text;
    }

    /**
     * Get communication raw text
     * @return  string|null
     */
    public function getRawText(): ?string {
        if ($rawText = $this->getText()) {
            return strip_tags($rawText);
        }
        return null;
    }

    /**
     * Set communication text
     * @param  string|null  $text  Communication text
     * @return  self
     */
    public function setText(?string $text): self {
        $this->text = $text;
        return $this;
    }

    /**
     * Get communication sender's email
     * @return  string
     */
    public function getSenderMail(): string {
        return $this->senderMail;
    }

    /**
     * Set communication sender's email
     * @return  self
     */
    public function setSenderMail(string $senderMail): self {
        $this->senderMail = $senderMail;
        return $this;
    }

    /**
     * Get communication sent flag
     * @return  bool
     */
    public function getSent(): bool {
        return $this->sent;
    }

    /**
     * Set communication sent flag
     * @param  bool  $sent  Communication sent flag
     * @return  self
     */
    public function setSent(bool $sent): self {
        $this->sent = $sent;
        return $this;
    }

    /**
     * Get communication channel
     * @return  string
     */
    public function getChannel(): string {
        return $this->channel;
    }

    /**
     * Set communication channel
     * @param  string  $channel  Communication channel
     * @return  self
     */
    public function setChannel(string $channel): self {
        $this->channel = $channel;
        return $this;
    }

    /**
     * Get communication user publisher
     * @return  string|User|null
     */
    public function getPublisher(): ?User {
        return $this->publisher;
    }

    /**
     * Set communication user publisher
     * @param  string|User|null  $publisher  Communication user publisher
     * @return  self
     */
    public function setPublisher(?User $publisher): self {
        $this->publisher = $publisher;
        return $this;
    }

    /**
     * Get communication schedule date
     * @return  ?DateTime
     */
    public function getScheduleDate(): ?DateTime {
        return $this->scheduleDate;
    }

    /**
     * Set communication schedule date
     * @param  ?DateTime  $scheduleDate  Communication schedule date
     * @return  self
     */
    public function setScheduleDate(?DateTime $scheduleDate): self {
        $this->scheduleDate = $scheduleDate;
        return $this;
    }


    /**
     * Get communication sending date
     *
     * @return  ?DateTime
     */
    public function getSendingDate(): ?DateTime {
        return $this->sendingDate;
    }

    /**
     * Set communication sending date
     * @param  ?DateTime  $sendingDate  Communication sending date
     * @return  self
     */
    public function setSendingDate(?DateTime $sendingDate): self {
        $this->sendingDate = $sendingDate;
        return $this;
    }

    /**
     * Get content concerned by the communication
     * @return  null|Content
     */
    public function getContent(): ?Content {
        return $this->content;
    }

    /**
     * Return the channel of the organisation
     * @return Channel|null
     */
    public function getOrganisationChannel(): ?Channel {
        return $this->organisationChannel;
    }

    /**
     * Set the channel of the organisation
     * @param Channel|null $organisationChannel
     */
    public function setOrganisationChannel(?Channel $organisationChannel): void {
        $this->organisationChannel = $organisationChannel;
    }



    /**
     * Set content concerned by the communication
     *
     * @param  null|Content  $content  Content concerned by the communication
     * @return  self
     */
    public function setContent(?Content $content): self {
        $this->content = $content;
        return $this;
    }

    /**
     * Get medias used in the communication
     *
     * @return  Collection
     */
    public function getMedias(): Collection {
        return $this->medias;
    }

    /**
     * JSON serialization
     * @return array
     */
    public function jsonSerialize(): array {
        return get_object_vars($this);
    }
}
