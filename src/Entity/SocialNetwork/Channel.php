<?php

namespace Multicanal\Entity\SocialNetwork;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Multicanal\Entity\Organization;
use Multicanal\Entity\Traits\OrganizationAwareTrait;
use Multicanal\Lib\ChannelPublisher\ChannelFormConfig;
use OpenApi\Annotations as OA;

/**
 * Class Channel
 *
 * @package Multicanal\Entity\SocialNetwork
 * @author  Nathanael KHODL <consulting@khodl.com>
 */
#[Entity, Table(name: 'channel_channel')]
#[InheritanceType('JOINED')]
#[DiscriminatorColumn(name: 'discr', type: 'string')]
abstract class Channel implements \JsonSerializable {

    use OrganizationAwareTrait;

    /**
     * Channel identifier as autoincremented integer from database
     * @var int
     */
    #[Id, Column(type: Types::INTEGER), GeneratedValue(strategy: 'AUTO')]
    private ?int $id;


    /**
     * Organization to which the channel is attached
     * @var null|Organization
     */
    #[
        ManyToOne(
            targetEntity: Organization::class,
            cascade: ["persist"],
            inversedBy: "channels"
        )
    ]
    private ?Organization $organization;

    /**
     * Is the channel archived or not
     * @var bool
     */
    #[Column(type: Types::BOOLEAN, options: ['default' => false])]
    private bool $isArchived = false;

    private ChannelFormConfig $formConfig;

    /**
     * Channel constructor.
     *
     * @param Organization $organization
     */
    public function __construct(Organization $organization) {
        $this->setOrganization($organization);
    }

    /**
     * JSON serialization
     * @return array
     */
    public function jsonSerialize(): array {
        // TODO: Warning: sensitive information might appear here, such as token
        return get_object_vars($this);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int {
        return $this->id;
    }

    abstract public function getChannelShortName(): string;

    abstract public function getChannelName(): string;

    /**
     * @return bool
     */
    public function isArchived(): bool {
        return $this->isArchived;
    }

    /**
     * @param bool $isArchived
     */
    public function setIsArchived(bool $isArchived): void {
        $this->isArchived = $isArchived;
    }

    public function __toString(): string {
        return $this->getChannelName();
    }
}
