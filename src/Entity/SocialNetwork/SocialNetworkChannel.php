<?php

namespace Multicanal\Entity\SocialNetwork;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Multicanal\Entity\Organization;

/**
 * Abstract class SocialNetworkChannel
 *
 * @package Multicanal\Entity\SocialNetwork
 * @author  Nathanael KHODL <consulting@khodl.com>
 */
#[Entity, Table(name: 'channel_socialnetwork')]
abstract class SocialNetworkChannel extends Channel {

    /**
     * Refresh token
     * @var string
     */
    #[Column(type: Types::STRING, nullable: false)]
    protected string $refreshToken;

    /**
     * Identifier on the platform
     * @var string
     */
    #[Column(type: Types::STRING, nullable: false)]
    protected string $identifier;

    abstract public function getProfileUrl();

    public function __construct(Organization $organization, string $identifier, string $refreshToken) {
        parent::__construct($organization);
        $this->refreshToken = $refreshToken;
        $this->identifier = $identifier;
    }

    public function getIdentifier(): string {
        return $this->identifier;
    }

    /**
     * @return string
     */
    public function getRefreshToken(): string {
        return $this->refreshToken;
    }

    /**
     * @param string $refreshToken
     */
    public function setRefreshToken(string $refreshToken): self {
        $this->refreshToken = $refreshToken;
        return $this;
    }
}
