<?php

namespace Multicanal\Entity\SocialNetwork;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Multicanal\Entity\Organization;

/**
 * Class Facebook Channel
 *
 * @package Multicanal\Entity\SocialNetwork
 * @author  Nathanael KHODL <consulting@khodl.com>
 */
#[Entity, Table(name: 'channel_socialnetwork_facebook')]
class FacebookChannel extends SocialNetworkChannel {

    /**
     * Facebook name
     * @var string
     */
    #[Column(type: Types::STRING, nullable: false)]
    protected string $name;

    public function __construct(Organization $organization, string $identifier, string $refreshToken, string $name) {
        parent::__construct($organization, $identifier, $refreshToken);
        $this->name = $name;
    }

    public function getProfileUrl() {
        return 'https://facebook.com/' . $this->getIdentifier();
    }

    public function getChannelShortName(): string {
        return $this->name;
    }

    public function getChannelName(): string {
        return 'Facebook : ' . $this->name;
    }
}
