<?php

namespace Multicanal\Entity\SocialNetwork;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Multicanal\Entity\Organization;

/**
 * Class Twitter Channel
 *
 * @package Multicanal\Entity\SocialNetwork
 * @author  Nathanael KHODL <consulting@khodl.com>
 */
#[Entity, Table(name: 'channel_socialnetwork_twitter')]
class TwitterChannel extends SocialNetworkChannel {

    /**
     * Twitter handle
     * @var string
     */
    #[Column(type: Types::STRING, nullable: false)]
    protected string $handle;

    public function __construct(Organization $organization, string $identifier, string $refreshToken, string $handle) {
        parent::__construct($organization, $identifier, $refreshToken);
        $this->handle = $handle;
    }

    public function getProfileUrl() {
        return 'https://twitter.com/' . $this->handle;
    }

    public function getChannelShortName(): string {
        return '@' . $this->handle;
    }

    public function getChannelName(): string {
        return 'Twitter : @' . $this->handle;
    }
}
