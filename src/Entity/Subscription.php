<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity;

use DateTime;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\PrePersist;

/**
 * Abstract class Subscription
 *
 * @package Multicanal\Entity
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 *
 * @OA\Schema(
 *      description="Spécification du modèle souscription email et téléphone.",
 *	    title="Abonnement email, téléphone mobile",
 *	    @OA\Xml(
 *		    name="Subscription"
 *	    )
 * )
 */
#[MappedSuperclass, HasLifecycleCallbacks]
abstract class Subscription {

    /**
     * Subscription date
     *
     * @OA\Property(
     *     description="Date d'enregistrement de l'abonnement",
     *     title="Subscription date",
     *     property="subscriptionDate",
     *     type="string",
     *     format="date"
     * )
     * @var DateTime
     */
    #[Column(name: 'subscription_date', type: Types::DATETIME_MUTABLE)]
    protected DateTime $subscriptionDate;

    /**
     * Get subscription date
     * @return  DateTime
     */
    public function getSubscriptionDate(): ?DateTime {
        return $this->subscriptionDate;
    }

    /**
     * Set subscription date
     * @param  DateTime  $subscriptionDate  Subscription date
     * @return  self
     */
    public function setSubscriptionDate(DateTime $subscriptionDate): self {
        $this->subscriptionDate = $subscriptionDate;
        return $this;
    }

    /**
     * Set subscription date on insert operation
     * @ORM\PrePersist
     */
    #[PrePersist]
    public function onPrePersist() {
        $this->subscriptionDate = new DateTime('NOW');
    }

    /**
     * JSON serialization
     * @return array
     */
    public function jsonSerialize(): array {
        return get_object_vars($this);
    }
}
