<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Exception;
use JsonSerializable;
use Multicanal\Entity\Traits\DistributionListAwareTrait;

/**
 * Class SMS
 *
 * @package Multicanal\Entity
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 * @OA\Schema(
 *     description="Spécifications du modèle décrivant l'envoie de SMS à une liste de destinataire.",
 *     title="SMS",
 *     required={"sms","text"},
 *	   @OA\Xml(
 *        name="SMS"
 *     )
 * )
 */
class SMS {
    /**
     * SMS Mobile's list
     *
     * @OA\Property(
     *     description="Liste des numéros de mobile des destinataires.",
     *     title="Numéro de mobile",
     *     property="mobiles",
     *     type="array",
     *     @OA\Items(
     *          type="string"
     *     )
     * )
     * @var array
     */
    private array $mobiles;

    /**
     * SMS's message
     *
     * @OA\Property(
     *     description="Message à destination des SMS",
     *     title="Message",
     *     property="message",
     *     type="string"
     * )
     * @var string
     */
    private string $message;

    /**
     * @param array $mobiles
     * @param string $message
     */
    public function __construct(array $mobiles, string $message) {
        $this->setMobiles($mobiles);
        $this->setMessage($message);
    }

    /**
     * @return array
     */
    public function getMobiles(): array {
        return $this->mobiles;
    }

    /**
     * @param array $mobiles
     */
    public function setMobiles(array $mobiles): void {
        $this->mobiles = $mobiles;
    }

    /**
     * @return string
     */
    public function getMessage(): string {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void {
        $this->message = $message;
    }

    /**
     * JSON serialization
     */
    public function jsonSerialize(): mixed {
        return get_object_vars($this);
    }
}
