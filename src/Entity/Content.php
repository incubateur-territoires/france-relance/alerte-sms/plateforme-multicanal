<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity;

use DateTime;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\InverseJoinColumn;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\PreUpdate;
use Doctrine\ORM\Mapping\Table;
use Exception;
use JsonSerializable;
use Multicanal\App\Helper;
use Multicanal\Entity\Exception\MulticanalException;
use Multicanal\Entity\Traits\AuthorsAwareTrait;
use Multicanal\Entity\Traits\CategoryAwareTrait;
use Multicanal\Entity\Traits\CommunicationsAwareTrait;
use Multicanal\Entity\Traits\CreatorAwareTrait;
use Multicanal\Entity\Traits\DistributionListAwareTrait;
use Multicanal\Entity\Traits\MediasAwareTrait;
use Multicanal\Utils\UrlMinifierUtils;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpInternalServerErrorException;
use OpenApi\Attributes\OpenApi as OA;

/**
 * Class Content
 *
 * @package Multicanal\Entity
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 *
 * @OA\Schema(
 *     description="Spécifications du modèle décrivant un contenu.",
 *     title="Contenu",
 *     required={"title"},
 *     @OA\Xml(
 *        name="Content"
 *     )
 * )
 */
#[Entity, Table(name: 'content'), HasLifecycleCallbacks]
class Content implements JsonSerializable {
    use DistributionListAwareTrait;
    use AuthorsAwareTrait;
    use CommunicationsAwareTrait;
    use CategoryAwareTrait;
    use CreatorAwareTrait;
    use MediasAwareTrait;

    /**
     * Content identifier as autoincremented integer from database
     *
     * @OA\Property(
     *     description="Identifiant du contenu",
     *     title="Identifier",
     *     property="id",
     *     type="integer"
     * )
     * @var int
     */
    #[Id, Column(type: Types::INTEGER), GeneratedValue(strategy: 'AUTO')]
    private ?int $id;

    /**
     * Content title
     *
     * @OA\Property(
     *     description="Titre du contenu",
     *     title="Title",
     *     property="title",
     *     type="string"
     * )
     * @var string
     */
    #[Column(type: Types::STRING, length: 240, nullable: false)]
    private string $title;

    /**
     * Content text
     *
     * @OA\Property(
     *     description="Texte du contenu",
     *     title="Text",
     *     property="text",
     *     type="string"
     * )
     * @var string|null
     */
    #[Column(type: Types::TEXT)]
    private ?string $text;

    /**
     * Content minified URL
     *
     * @OA\Property(
     *     description="URL minifié du contenu",
     *     title="Short URL",
     *     property="shortUrl",
     *     type="string"
     * )
     * @var string|null
     */
    #[Column(name: 'short_url', type: Types::TEXT, nullable: true)]
    private ?string $shortUrl;

    /**
     * Content creation date
     *
     * @OA\Property(
     *     description="Date de création du contenu",
     *     title="Creation date",
     *     property="creationDate",
     *     type="string",
     *     format="date"
     * )
     * @var DateTime
     */
    #[Column(name: 'creation_date', type: Types::DATETIME_MUTABLE)]
    private DateTime $creationDate;

    /**
     * Content update date
     *
     * @OA\Property(
     *     description="Date de mise à jour du contenu",
     *     title="Update date",
     *     property="updateDate",
     *     type="string",
     *     format="date"
     * )
     * @var ?DateTime
     */
    #[Column(name: 'update_date', type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTime $updateDate;

    /**
     * Content status
     *
     * @OA\Property(
     *     description="Statut du contenu dans le flux de travail",
     *     title="Status",
     *     property="status",
     *     type="string"
     * )
     * @var string
     */
    #[Column(type: 'enum_content_status', nullable: true)]
    private string $status;

    /**
     * Content media collection
     *
     * @OA\Property(
     *     description="Liste des médias du contenu",
     *     title="Medias",
     *     property="medias",
     *     type="array",
     *     @OA\Items(
     *          ref="#/components/schemas/MulticanalFile"
     *     )
     * )
     * @var Collection
     */
    #[
        OneToMany(
            mappedBy: "content",
            targetEntity: "MulticanalFile",
            cascade: ["persist", "remove"],
            orphanRemoval: true
        )
    ]
    private Collection $medias;

    /**
     * Content illustration image file
     *
     * @OA\Property(
     *     description="Illustration du contenu",
     *     title="Illustration",
     *     property="illustration",
     *     ref="#/components/schemas/MulticanalFile"
     * )
     * @var MulticanalFile
     */
    #[
        OneToOne(
            targetEntity: "MulticanalFile",
            cascade: ["all"],
            orphanRemoval: true
        )
    ]
    #[
        JoinColumn(
            name: "illustration_file_id",
            referencedColumnName: "id",
            onDelete: "cascade"
        )
    ]
    private ?MulticanalFile $illustration;

    /**
     * Content user creator
     *
     * @OA\Property(
     *     description="Créateur du contenu",
     *     title="Creator",
     *     property="creator",
     *     ref="#/components/schemas/User"
     * )
     * @var User|null
     */
    #[ManyToOne(targetEntity: "User", cascade: ["persist"], inversedBy: "ownContents")]
    private ?User $creator;

    /**
     * Content list of author(s)
     *
     * @OA\Property(
     *     description="Liste des auteurs du contenu",
     *     title="Authors",
     *     property="authors",
     *     type="array",
     *     @OA\Items(
     *          ref="#/components/schemas/User"
     *     )
     * )
     * @var Collection
     */
    #[ManyToMany(targetEntity: "User", inversedBy: "contents", cascade: ["persist"])]
    #[JoinTable(name: "authors_contents")]
    #[JoinColumn(name: "author_id", referencedColumnName: "id")]
    #[InverseJoinColumn(name: "content_id", referencedColumnName: "id")]
    private Collection $authors;

    /**
     * Content distribution list
     *
     * @OA\Property(
     *     description="Liste de diffusion du contenu",
     *     title="Distribution list",
     *     property="distributionList",
     *     ref="#/components/schemas/DistributionList"
     * )
     * @var DistributionList|null
     */
    #[
        ManyToOne(
            targetEntity: "DistributionList",
            cascade: ["persist"],
            inversedBy: "contents"
        )
    ]
    private ?DistributionList $distributionList;

    /**
     * Content category
     * @OA\Property(
     *     description="Catégorie du contenu",
     *     title="Category",
     *     property="category",
     *     ref="#/components/schemas/Category"
     * )
     * @var Category
     */
    #[ManyToOne(targetEntity: "Category", cascade: ["persist"], inversedBy: "contents")]
    private Category $category;

    /**
     * Content priority (1 to 4)
     *
     * @OA\Property(
     *     description="Priorité du contenu",
     *     title="Priority",
     *     property="priority",
     *     type="integer"
     * )
     * @var integer
     */
    #[Column(type: Types::INTEGER)]
    private int $priority;

    /**
     * Content deadline date
     *
     * @OA\Property(
     *     description="Date prévisionnel d'envoi du contenu",
     *     title="Deadline",
     *     property="deadline",
     *     type="string",
     *     format="date"
     * )
     * @var DateTime|null
     */
    #[Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTime $deadline;

    /**
     * Content communications collection
     *
     * @OA\Property(
     *     description="Liste des communications du contenu, 1 communication par canal sélectionné",
     *     title="Communications",
     *     property="communications",
     *     type="array",
     *     @OA\Items(
     *          ref="#/components/schemas/Communication"
     *     )
     * )
     * @var Collection
     */
    #[
        OneToMany(
            mappedBy: "content",
            targetEntity: "Communication",
            cascade: ["persist", "remove"],
            orphanRemoval: true
        )
    ]
    private Collection $communications;

    /**
     * Fields construtor
     *
     * @param string $title Content title
     * @param string|null $text Content text
     */
    public function __construct(string $title, ?string $text) {
        $this->setTitle($title);
        $this->setText($text);
        $this->initializeCommunicationsCollection();
        $this->initializeAuthorsCollection();
        $this->initializeMediasCollection();
    }

    /**
     * Get content identifier
     * @return  int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * Set content identifier
     * @param int|null $id Content identifier
     * @return  self
     */
    public function setId(?int $id): self {
        $this->id = $id;
        return $this;
    }

    /**
     * Get Organization
     *
     * @return Organization|null
     */
    public function getOrganization(): ?Organization {
        if ($this->getCategory() instanceof  Category) {
            return $this->getCategory()->getOrganization();
        }
        return null;
    }

    /**
     * Get content title
     * @return  string
     */
    public function getTitle(): string {
        return $this->title;
    }

    /**
     * Set content title
     * @param  string  $title  Content title
     * @return  self
     */
    public function setTitle(string $title): self {
        $this->title = $title;
        return $this;
    }

    /**
     * Get content text
     * @return  string
     */
    public function getText(): string {
        return $this->text;
    }

    /**
     * Get content raw text
     * @return  string
     */
    public function getRawText(): string {
        return html_entity_decode(strip_tags($this->getText()));
    }

    /**
     * Set content text
     * @param  string  $text  Content text
     * @return  self
     */
    public function setText(string $text): self {
        $this->text = $text;
        return $this;
    }

    /**
     * Get content short url
     * @return string|null
     */
    public function getShortUrl(): ?string {
        return $this->shortUrl;
    }

    /**
     * Set content short url
     * @param  string  $shortUrl  Content short url
     * @return  self
     */
    public function setShortUrl(string $shortUrl): self {
        $this->shortUrl = $shortUrl;
        return $this;
    }

    /**
     * Update ShortUrl
     * @return void
     * @throws MulticanalException
     */
    public function updateShortUrlQrCodeFile(): void {
        $url = Helper::getAppDomain() . '/contenu/' . $this->getId();
        $this->setShortUrl(UrlMinifierUtils::getReduceUrl($url));
    }

    /**
     * Get parameters illustration
     * @return MulticanalFile|null
     */
    public function getIllustration(): ?MulticanalFile {
        return $this->illustration;
    }

    /**
     * Set illustration image file
     * @param  MulticanalFile  $illustration  Parameters illustration
     * @return  self
     */
    public function setIllustration(MulticanalFile $illustration): self {
        $this->illustration = $illustration;
        return $this;
    }

    /**
     * Set illustration image file
     * @param  MulticanalFile  $illustration  Parameters illustration
     * @return  self
     */
    public function unsetIllustration(): self {
        $this->illustration = null;
        return $this;
    }

    /**
     * Get content creation date
     * @return DateTime|null
     */
    public function getCreationDate(): ?DateTime {
        return $this->creationDate;
    }

    /**
     * Set content creation date
     * @param  DateTime  $creationDate  Content creation date
     * @return  self
     */
    public function setCreationDate(DateTime $creationDate): self {
        $this->creationDate = $creationDate;
        return $this;
    }

    /**
     * Get content update date
     * @return DateTime|null
     */
    public function getUpdateDate(): ?DateTime {
        return $this->updateDate;
    }

    /**
     * Set content update date
     * @param  DateTime  $updateDate  Content update date
     * @return  self
     */
    public function setUpdateDate(DateTime $updateDate): self {
        $this->updateDate = $updateDate;
        return $this;
    }

    /**
     * Get content deadline
     * @return DateTime|null
     */
    public function getDeadline(): ?DateTime {
        return $this->deadline;
    }

    /**
     * Set content deadline
     * @param DateTime|null $deadline Content deadline
     * @return  self
     */
    public function setDeadline(?DateTime $deadline): self {
        $this->deadline = $deadline;
        return $this;
    }

    /**
     * Get content priority
     * @return  int
     */
    public function getPriority(): int {
        return $this->priority;
    }

    /**
     * Set content priority
     * @param integer $priority  Content priority
     * @return  self
     */
    public function setPriority(int $priority): self {
        $this->priority = $priority;
        return $this;
    }

    /**
     * Get content status
     * @return string|null
     */
    public function getStatus(): ?string {
        return $this->status;
    }

    /**
     * Set content status
     * @param  string  $status  Content status
     * @return  self
     */
    public function setStatus(string $status): self {
        $this->status = $status;
        return $this;
    }

    /**
     * Set creationDate on insert operation
     * @ORM\PrePersist
     */
    #[PrePersist]
    public function onPrePersist() {
        $this->creationDate = new DateTime('NOW');
    }

    /**
     * Set updateDate on every update operations
     */
    #[PreUpdate]
    public function onPreUpdate() {
        $this->updateDate = new DateTime('NOW');
    }

    /**
     * Override to string function
     * @return string
     */
    public function __toString(): string {
        try {
            return $this->id . ' ' . $this->title;
        } catch (Exception $e) {
            if (Helper::isDebugEnabled()) {
                return $e->getMessage();
            }
            return '';
        }
    }

    /**
     * Override to array function
     * @return array
     */
    public function __toArray(): array {
        try {
            return $this->contentToArray();
        } catch (Exception $e) {
            return [];
        }
    }

    /**
     * To API function to format result for API.
     *
     * @param ServerRequestInterface $request Current HTTP request from Slim framework
     * @return array
     * @throws HttpInternalServerErrorException
     */
    public function __toApi(ServerRequestInterface $request): array {
        try {
            return $this->contentToArray();
        } catch (Exception $e) {
            throw new HttpInternalServerErrorException($request, 'Une erreur interne s\'est produite lors de la récupération du contenu.', $e);
        }
    }

    /**
     * Transform a content object to an array
     *
     * @return array
     */
    private function contentToArray(): array {
        $content =  [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'text' => $this->getText(),
            'creationDate' => $this->getCreationDate(),
            'updateDate' => $this->getUpdateDate(),
            'priority' => $this->getPriority(),
            'status' => $this->getStatus(),
            'deadline' => $this->getDeadline(),
        ];
        if ($this->getDistributionList() instanceof  DistributionList) {
            $content['distributionList'] = $this->getDistributionList()->jsonSerialize();
        }
        if ($this->getCategory() instanceof  Category) {
            $content['category'] = $this->getCategory()->jsonSerialize();
        }
        if ($this->getCreator() instanceof  User) {
            $content['creator'] = $this->getCreator()->jsonSerialize();
        }
        if ($this->getDistributionList() instanceof  DistributionList) {
            $content['distributionList'] = $this->getDistributionList()->jsonSerialize();
        }
        if ($this->getCommunications() instanceof  Collection) {
            $content['channels'] = $this->getCommunications()->getValues();
        }
        if ($this->getAuthors() instanceof  Collection) {
            $content['authors'] = $this->getAuthors()->getValues();
        }

        return $content;
    }

    /**
     * JSON serialization
     * @return array
     */
    public function jsonSerialize(): array {
        return get_object_vars($this);
    }
}
