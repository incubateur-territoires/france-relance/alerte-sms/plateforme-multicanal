<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Exception;

use Exception;
use Throwable;

/**
 * Class Multicanal Exception to avoid throwing generic exception.
 *
 * @package Multicanal\Entity\Exception
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class MulticanalException extends Exception {

    public function __construct(string $message, ?int $code = 500, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }

    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}
