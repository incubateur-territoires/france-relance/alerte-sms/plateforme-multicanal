<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Exception;

use Exception;

/**
 * Class Upload Exception to throw file upload exception
 *
 * @package Multicanal\Entity\Exception
 * @author  William BLANC DIT JOLICOEUR <w.blanc-dit-jolicoeur@girondenumerique.fr>
 */
class UploadException extends Exception {

    public function __construct(int $code) {
        $message = $this->codeToMessage($code);
        parent::__construct($message, $code);
    }

    /**
     * Translate an error code to a technical message.
     *
     * @param int $code JS uploader widget error code
     * @author  William BLANC DIT JOLICOEUR <w.blanc-dit-jolicoeur@girondenumerique.fr>
     * @return string
     * @access private
     */
    private function codeToMessage(int $code): string {
        return match ($code) {
            UPLOAD_ERR_INI_SIZE   => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
            UPLOAD_ERR_FORM_SIZE  => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
            UPLOAD_ERR_PARTIAL    => 'The uploaded file was only partially uploaded',
            UPLOAD_ERR_NO_FILE    => 'No file was uploaded',
            UPLOAD_ERR_NO_TMP_DIR => 'Missing a temporary folder',
            UPLOAD_ERR_CANT_WRITE => 'Failed to write file to disk',
            UPLOAD_ERR_EXTENSION  => 'File upload stopped by extension',
            default               => 'Unknown upload error',
        };
    }
}
