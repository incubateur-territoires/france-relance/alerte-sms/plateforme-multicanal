<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Enum;

/**
 * Enum class ContentStatus
 *
 * @package Multicanal\Entity\Enum
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class ContentStatusType extends EnumType {

    /**
     * Published status
     * @var string
     */
    const PUBLISHED = 'PUBLI&Eacute;';

    /**
     * Publishing status
     * @var string
     */
    const PUBLISHING = 'PUBLICATION...';

    /**
     * To be published status
     * @var string
     */
    const TO_PUBLISH = '&Agrave; PUBLIER';

    /**
     * Draft status
     * @var string
     */
    const DRAFT = 'BROUILLON';

    /**
     * Deleted status
     * @var string
     */
    const DELETED = 'SUPPRIM&Eacute;';

    /**
     * Archived status
     * @var string
     */
    const ARCHIVED = 'ARCHIV&Eacute;';

    /**
     * @var string
     */
    protected string $name = 'enum_content_status';
}
