<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Enum;

use MyCLabs\Enum\Enum;

/**
 * Enum class Rank
 * Implements MyCLabs\Enum\Enum, usefull to get all values for templates with function Enum::toArray()
 *
 * @package Multicanal\Entity\Enum
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class Rank extends Enum {

	/**
	 * Administrator of gironde numérique rank
	 * @var string
	 */
	const ADMINGN = 'Administrateur GN';

	/**
	 * Administrator rank
	 * @var string
	 */
	const ADMIN = 'Administrateur';

	/**
	 * Agent rank
	 * @var string
	 */
	const AGENT = 'Utilisateur';
}
