<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Enum;

/**
 * Enum class ChannelType
 *
 * @package Multicanal\Entity\Enum
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class ChannelType extends EnumType {

    /**
     * Email channel
     * @var string
     */
    const EMAIL = 'Par email';

    /**
     * SMS channel
     * @var string
     */
    const SMS = 'Par SMS';

    /**
     * WEBSITE channel
     * @var string
     */
    const WEBSITE = 'Sur site Internet';

    /**
     * Facebook channel
     * @var string
     */
    const FACEBOOK = 'Via Facebook';

    /**
     * Instagram channel
     * @var string
     */
    const INSTAGRAM = 'Via Instagram';

    /**
     * Twitter channel
     * @var string
     */
    const TWITTER = 'Via Twitter';


    /**
     * Channel entity used instead of one of the above
     * @var string
     */
    const CHANNEL = 'CHANNEL';

    /**
     * @var string
     */
    protected string $name = 'enum_channel_type';
}
