<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Entity\Enum;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use InvalidArgumentException;
use ReflectionClass;

abstract class EnumType extends Type {

    protected string $name;

    protected array  $values = [];

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string {
        $this->values = array_map(function ($val) {
            return "'" . $val . "'";
        }, $this->getStatusOptions());

        return "ENUM(" . implode(", ", $this->values) . ")";
    }

    public function convertToPHPValue($value, AbstractPlatform $platform) {
        return $value;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform) {
        if (!in_array($value, $this->getStatusOptions())) {
            throw new InvalidArgumentException("Invalid '" . $this->name . "' value.");
        }
        return $value;
    }

    public function getName(): string {
        return $this->name;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool {
        return true;
    }

    public function getStatusOptions(): array {
        $reflector = new ReflectionClass(get_class($this));
        return array_keys($reflector->getConstants());
    }

    public function getStatusLabel(string $status): string {
        $reflector = new ReflectionClass(get_class($this));
        if (isset($reflector->getConstants()[$status])) {
            return $reflector->getConstants()[$status];
        }
        return '';
    }
}
