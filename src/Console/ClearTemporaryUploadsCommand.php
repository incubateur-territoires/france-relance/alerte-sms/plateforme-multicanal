<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Console;

use DirectoryIterator;
use Multicanal\App\Helper;
use Multicanal\Entity\Exception\MulticanalException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'multicanal:clear-tmp-uploads',
    description: 'Delete temporary file uploads older than 3600 seconds',
    aliases: ['clear-tmp-uploads'],
    hidden: false
)]
final class ClearTemporaryUploadsCommand extends Command {

    const FILE_AGE_MAX_SECONDS = 3600;

    private $uploadTempDir;

    public function __construct() {
        $this->uploadTempDir = Helper::getUploadDirectoryTmp();
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {

        if (empty($this->uploadTempDir) || '/' === $this->uploadTempDir) {
            throw new MulticanalException('UploadTempDir should not be empty or /');
        }
        $nbItems = 0;
        $uploadTempDirIterator = new DirectoryIterator($this->uploadTempDir);

        $output->writeln('<info>Current Time is ' . date('Y-m-d H:i:s') . '</info>');
        $output->writeln('<info>Listing directories in ' . $this->uploadTempDir . '</info>');

        foreach ($uploadTempDirIterator as $fileInfo) {
            if ($fileInfo->isDot()) {
                continue;
            }

            $nbItems++;
            if ($fileInfo->isFile()) {
                $output->writeln('<comment>file : ' . $fileInfo->getFilename() . '</comment>');
            }
            if ($fileInfo->isDir()) {
                $output->writeln('<comment>directory: ' . $fileInfo->getFilename() . ' (' . date('Y-m-d H:i:s', $fileInfo->getCTime()) . ')</comment>');
            }
            if ($fileInfo->isDir() && ($_SERVER['REQUEST_TIME'] - $fileInfo->getCTime()) > self::FILE_AGE_MAX_SECONDS) {
                foreach (new DirectoryIterator($fileInfo->getPathname()) as $subfileInfo) {
                    if (!$subfileInfo->isDot()) {
                        unlink($subfileInfo->getPathname());
                    }
                }
                rmdir($fileInfo->getPathname());
                $output->writeln('<error>directory (older than 1h) and all its contained files have been deleted.</error>');
            } else {
                $output->writeln('<info>directory is too fresh and has been preserved</info>');
            }
        }

        if (!$nbItems) {
            $output->writeln('<info>No element found in uploadTempDir</info>');
        }
        return Command::SUCCESS;
    }
}
