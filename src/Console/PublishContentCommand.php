<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Console;

use DateTime;
use Doctrine\ORM\EntityManager;
use Multicanal\App\Settings\SettingsInterface;
use Multicanal\Entity\Communication;
use Multicanal\Entity\Counter;
use Multicanal\Entity\Organization;
use Multicanal\Repository\CommunicationRepository;
use Multicanal\Repository\CounterRepository;
use Multicanal\Utils\ChannelPublisherUtils;
use Multicanal\Utils\MailGeneratorUtils;
use Multicanal\Utils\SMSUtils;
use Psr\Container\ContainerInterface;
use Slim\Views\Twig;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'multicanal:send-content-publish',
    description: 'publish all content not sent but published',
    aliases: ['multicanal-publish-content'],
    hidden: false
)]
final class PublishContentCommand extends Command {
    protected EntityManager $entityManager;

    protected CommunicationRepository $communicationRepository;


    protected CounterRepository $counterRepository;

    protected SettingsInterface $settings;

    protected Twig $twig;

    public function __construct(ContainerInterface $container) {
        parent::__construct();
        $this->settings                = $container->get(SettingsInterface::class);
        $this->entityManager           = $container->get(EntityManager::class);
        $this->communicationRepository = $container->get(CommunicationRepository::class);
        $this->counterRepository       = $container->get(CounterRepository::class);
        $this->twig                    = $container->get('view');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $status         = Command::SUCCESS;
        $communications = $this->communicationRepository->findPublishedNotSent();
        /** @var Communication $communication */
        foreach ($communications as $communication) {
            if ($communication->getScheduleDate() > new DateTime('NOW')) {
                if ($communication->getChannel() === 'EMAIL') {
                    $mailGenerator = new MailGeneratorUtils($this->twig);
                    if ($mailGenerator->publishContentByEmail($communication)) {
                        $this->updateCounter($communication);
                    } else {
                        $status = Command::FAILURE;
                    }
                } elseif ($communication->getChannel() === 'SMS') {
                    if (SMSUtils::publishContentBySMS($communication)) {
                        $this->updateCounter($communication);
                    } else {
                        $status = Command::FAILURE;
                    }
                } elseif ($communication->getChannel() === 'CHANNEL') {
                    if (ChannelPublisherUtils::sendCommunication($communication)) {
                        $this->updateCounter($communication);
                    } else {
                        $status = Command::FAILURE;
                    }
                }
            }
        }

        return $status;
    }

    /**
     * Update organization counter for SMS and email communications.
     *
     * @param Communication $communication Current communication
     * @return void
     * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
     */
    private function updateCounter(Communication $communication): void {
        /** @var Organization $organization */
        $organization = $communication->getContent()->getOrganization();
        /** @var ?Counter $counter */
        $counter      = $this->counterRepository->findByYearOrganization(date('Y'), $organization);
        if ($communication->getChannel() === 'EMAIL') {
            $counter->setNbOfEmail($counter->getNbOfEmail() + count($communication->getContent()->getDistributionList()->getEmailSubscribersList()));
        } elseif ($communication->getChannel() === 'SMS') {
            $counter->setNbOfSms($counter->getNbOfSms() + count($communication->getContent()->getDistributionList()->getMobileSubscribersList()));
        }
        $this->counterRepository->save($counter);
        $this->communicationRepository->setSent($communication);
    }
}
