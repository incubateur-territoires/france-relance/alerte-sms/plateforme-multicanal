<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\Action;

use Multicanal\App\Helper;
use Multicanal\Controller\Action\ActionController;
use Multicanal\Entity\Category;
use Multicanal\Entity\EmailParameters;
use Multicanal\Entity\Enum\Rank;
use Multicanal\Entity\GeneralParameters;
use Multicanal\Entity\MailboxParameters;
use Multicanal\Entity\MulticanalFile;
use Multicanal\Entity\Organization;
use Multicanal\Entity\SocialNetworkParameters;
use Multicanal\Utils\ArrayUtils;
use Multicanal\Utils\MulticanalFilesUtils;
use Multicanal\Utils\StringUtils;
use PhpImap\Exceptions\ConnectionException;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Psr7\UploadedFile;

/**
 * Class OrganizationActionController manager
 *
 * @package Multicanal\Controller\Action
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
class OrganizationActionController extends ActionController {

    private string $directory;

    public function __construct(ContainerInterface $container) {
        parent::__construct($container);
        $this->directory = Helper::getUploadDirectory() . '/' . $this->user->getDomainName() . '/logo';
    }

    /**
     * Save general parameters in an existing organization action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function saveGeneralParameters(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url             = ArrayUtils::get($params, 'current-url');
            $label           = ArrayUtils::get($params, 'label');
            $backgroundColor = ArrayUtils::get($params, 'background-color');
            $gradientColor   = ArrayUtils::get($params, 'gradient-color');
            $textColor       = ArrayUtils::get($params, 'text-color');
            $websiteUrl      = ArrayUtils::get($params, 'website-url');
            $email           = ArrayUtils::get($params, 'email');
            $contactUrl      = ArrayUtils::get($params, 'contact-url');
            $phone           = ArrayUtils::get($params, 'phone-number');
            $facebookUrl     = ArrayUtils::get($params, 'facebook-url');
            $twitterUrl      = ArrayUtils::get($params, 'twitter-url');

            // Define organization name
            if ($this->user->getRank() == Rank::ADMINGN) {
                $organizationDomainName = ArrayUtils::get($params, 'domain-name');
            } else {
                $organizationDomainName = $this->user->getOrganizationName();
            }
            $organization = $this->organizationRepository->createIfNotExist($organizationDomainName);

            // Create or get existing general parameter
            if (is_null($organization->getGeneralParameters())) {
                $organization->setGeneralParameters(new GeneralParameters($organizationDomainName));
            }

            // Handle single input with single file upload
            $uploadedFiles = $request->getUploadedFiles();
            $logoFile      = $uploadedFiles['logo'];
            // If a logo is uploaded
            if ($this->isUploadOK($logoFile)) {
                $this->uploadOrganizationLogo($organization, $logoFile);
            }

            $organization->getGeneralParameters()->setLabel($label);
            $organization->getGeneralParameters()->setBackgroundColor($backgroundColor);
            $organization->getGeneralParameters()->setGradientColor($gradientColor);
            $organization->getGeneralParameters()->setTextColor($textColor);
            $organization->getGeneralParameters()->setWebSiteUrl($websiteUrl);
            $organization->getGeneralParameters()->setEmail($email);
            $organization->getGeneralParameters()->setContactUrl($contactUrl);
            $organization->getGeneralParameters()->setPhoneNumber($phone);
            $this->entityManager->persist($organization->getGeneralParameters());
            $this->entityManager->flush();

            if (!empty($facebookUrl) || !empty($twitterUrl)) {
                $organization->setSocialNetworkParameters(new SocialNetworkParameters());
                $organization->getSocialNetworkParameters()->setFacebookUrl($facebookUrl);
                $organization->getSocialNetworkParameters()->setTwitterUrl($twitterUrl);
                $this->entityManager->persist($organization->getSocialNetworkParameters());
                $this->entityManager->flush();
            }
            $this->organizationRepository->save($organization);

            $this->flash->addMessage('success', 'Les param&egrave;tres g&eacute;n&eacute;raux de la collectivit&eacute; ont bien &eacute;t&eacute; sauvegard&eacute;s.');
        }

        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * Save emailling parameters in an existing organization action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function saveEmailingParameters(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url         = ArrayUtils::get($params, 'current-url');
            $nameSender  = ArrayUtils::get($params, 'name-sender');
            $emailSender = ArrayUtils::get($params, 'email-sender');
            $nameReply   = ArrayUtils::get($params, 'name-reply');
            $emailReply  = ArrayUtils::get($params, 'email-reply');

            if ($this->user->getRank() == Rank::ADMINGN) {
                $organizationName = ArrayUtils::get($params, 'domain-name');
            } else {
                $organizationName = $this->user->getOrganizationName();
            }
            $organization = $this->organizationRepository->createIfNotExist($organizationName);

            // Create EmailParameters
            $emailParameters = new EmailParameters();
            $emailParameters->setNominator($nameSender);
            $emailParameters->setNominatorEmail($emailSender);
            $emailParameters->setReplyTo($nameReply);
            $emailParameters->setReplyToEmail($emailReply);

            $organization->setEmailParameters($emailParameters);
            $this->organizationRepository->save($organization);

            $this->flash->addMessage('success', 'Les param&egrave;tres <em>emailing</em> de la collectivit&eacute; ont bien &eacute;t&eacute; sauvegard&eacute;s.');
        }

        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * Save mailbox parameters in an existing organization action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function saveMailboxParameters(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url      = ArrayUtils::get($params, 'current-url');
            $mailbox  = ArrayUtils::get($params, 'mailbox');
            $folder   = ArrayUtils::get($params, 'folder');
            $user     = ArrayUtils::get($params, 'user');
            $password = ArrayUtils::get($params, 'password');
            $test     = (bool) ArrayUtils::get($params, 'test-configuration');

            if ($this->user->getRank() == Rank::ADMINGN) {
                $organizationName = ArrayUtils::get($params, 'domain-name');
            } else {
                $organizationName = $this->user->getOrganizationName();
            }
            $organization = $this->organizationRepository->createIfNotExist($organizationName);
            if ($this->user->getRank() != Rank::ADMINGN) {
                $organization->addUser($this->dbUser);
            }

            // Create Mailbox Parameters
            $mailboxParameters = new MailboxParameters();
            $mailboxParameters->setMailbox($mailbox);
            $mailboxParameters->setFolder($folder);
            $mailboxParameters->setUser($user);
            $mailboxParameters->setPassword($password);

            $organization->setMailboxParameters($mailboxParameters);
            $this->organizationRepository->save($organization);

            if ($test) {
                try {
                    $this->getMailboxManager()->checkConfiguration();
                    $this->flash->addMessage('success', 'Connexion &agrave; la bo&icirc;te email d\'information r&eacute;ussie');
                } catch (ConnectionException $e) {
                    $message = 'La configuration de la bo&icirc;te email d\'information est incorrecte';
                    if ($this->settings->get('env.debug')) {
                        $message .= ' :<br />' . $e->getMessage();
                    }
                    $this->flash->addMessage('error', $message);
                }
            } else {
                $this->flash->addMessage('success', 'Les param&egrave;tres de la boite mail de la collectivit&eacute; ont bien &eacute;t&eacute; sauvegard&eacute;s.');
            }
        }

        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * Add a new organization action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function add(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url    = ArrayUtils::get($params, 'current-url');
            $domain = ArrayUtils::get($params, 'organization-name');

            $organization = $this->organizationRepository->findByDomainName($domain);
            if ($organization instanceof Organization) {
                $this->flash->addMessage('warning', 'La collectivit&eacute; <em>' . $domain . '</em> a d&eacute;j&agrave; &eacute;t&eacute; param&eacute;tr&eacute;e');
            } else {
                $url = '/administration/' . $domain;
            }
        }

        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * Delete an organization action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function delete(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url    = ArrayUtils::get($params, 'current-url');
            $domain = ArrayUtils::get($params, 'organization-domain');
            $organization = $this->organizationRepository->findByDomainName($domain);
            $this->organizationRepository->delete($organization);

            $this->flash->addMessage('success', 'La collectivit&eacute; <em>' . $domain . '</em> a bien &eacute;t&eacute; supprim&eacute;e');
        }

        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * Load organizations list in JSON format through an AJAX HTTP resquest.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function load(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $organizations = $this->organizationRepository->findAll();
        $rows          = [];
        /** @var Organization $organization */
        foreach ($organizations as $organization) {
            $domain = $organization->getDomain();
            $rows[] = [
                'domain'         => $domain,
                'label'          => $organization->getGeneralParameters()->getLabel(),
                'communications' => count($organization->getDistributionLists())
            ];
        }
        $data = ['total' => sizeOf($rows), 'totalNotFiltered' => sizeOf($rows), 'rows' => $rows];
        $response->getBody()->write(json_encode($data, JSON_THROW_ON_ERROR));

        return $response->withHeader('Content-Type', self::JSON);
    }

    /**
     * Load organizations count list in JSON format through an AJAX HTTP resquest.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function count(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        $domain = ArrayUtils::get($args, 'organization');
        $rows   = [];
        if (!empty($domain)) {
            $organization = $this->organizationRepository->findByDomainName($domain);
            $this->setOrganizationCountingInformation($rows, $organization);
        } else {
            $organizations = $this->organizationRepository->findAll();
            /** @var Organization $organization */
            foreach ($organizations as $organization) {
                $this->setOrganizationCountingInformation($rows, $organization);
            }
        }

        $data = ['total' => sizeOf($rows), 'totalNotFiltered' => sizeOf($rows), 'rows' => $rows];
        $response->getBody()->write(json_encode($data, JSON_THROW_ON_ERROR));

        return $response->withHeader('Content-Type', self::JSON);
    }

    /**
     * Set organization counting information in an array.
     *
     * @param array $rows Information array by reference
     * @param Organization $organization Organization counting information
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @access private
     */
    private function setOrganizationCountingInformation(array &$rows, Organization $organization): void {
        $counters = $this->counterRepository->findByOrganization($organization);
        foreach ($counters as $counter) {
            $rows[] = [
                'year'      => !is_null($counter) ? $counter->getYear() : date('Y'),
                'domain'    => $organization->getDomain(),
                'sms'       => !is_null($counter) ? $counter->getNbOfSms() : 0,
                'email'     => !is_null($counter) ? $counter->getNbOfEmail() : 0
            ];
        }
    }

    /**
     * Upload organization logo with MulticanalFile update processing.
     *
     * @param Organization $organization Current organization to manage
     * @param UploadedFile $logoFile Organization logo to upload
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @access private
     */
    private function uploadOrganizationLogo(Organization $organization, UploadedFile $logoFile): void {
        $uploadSettings = $this->settings->get('genericImageUploader');
        MulticanalFilesUtils::validateUploadedFile($logoFile, $uploadSettings);
        $filename = MulticanalFilesUtils::moveUploadedFile($this->directory, $logoFile, StringUtils::normalize($organization->getGeneralParameters()->getDomainName(), '_', true));
        $logo = new MulticanalFile($filename, $this->directory);
        if (is_null($organization->getGeneralParameters()->getLogo())) {
            $organization->getGeneralParameters()->setLogo($logo);
        } else {
            $organization->getGeneralParameters()->getLogo()->deleteFile();
            $organization->getGeneralParameters()->getLogo()->setName($logo->getName());
            $organization->getGeneralParameters()->getLogo()->setUrl($logo->getUrl());
        }
    }
}
