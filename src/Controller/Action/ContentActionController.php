<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\Action;

use DateTime;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Exception\ORMException;
use Multicanal\App\Helper;
use Multicanal\Controller\Action\ActionController;
use Multicanal\Entity\Communication;
use Multicanal\Entity\Content;
use Multicanal\Entity\Enum\ChannelType;
use Multicanal\Entity\MulticanalFile;
use Multicanal\Utils\ArrayUtils;
use Multicanal\Utils\MailGeneratorUtils;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpNotFoundException;
use Slim\Psr7\UploadedFile;

/**
 * ContentActionController class to manage content
 *
 * @package Multicanal\Controller\Action
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class ContentActionController extends ActionController {

    /**
     * Get a specific content by its identifier through an AJAX HTTP request.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @throws HttpNotFoundException if content does not exist
     * @access public
     */
    public function get(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $id = ArrayUtils::get($params, 'content-id');

            /** @var Content $content */
            $content = $this->contentRepository->findById((int) $id);
            if (is_null($content)) {
                throw new HttpNotFoundException($request, 'Contenu inexistant ou non trouvé pour l\'identifiant ' . $id);
            } else {
                $response->getBody()->write(json_encode($content->__toArray()));
            }
        }

        return $response->withHeader('Content-Type', self::JSON);
    }

    /**
     * Add a new content action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function add(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();

        if ($this->checkParams($request, $params)) {
            $content = $this->getContentFromForm($request);

            if ($this->contentRepository->save($content)) {
                $content->updateShortUrlQrCodeFile();
                $this->contentRepository->save($content);
                $this->flash->addMessage('success', 'Le contenu <em>' . $content->getTitle() . '</em> a bien &eacute;t&eacute; ajout&eacute;');
            } else {
                $this->flash->addMessage('error', 'Une erreur est survenue lors de la tentative d\'ajout du contenu <em>' . $content->getTitle() . '</em>');
            }
        }

        return $response->withStatus(302)->withHeader('Location', '/contenus');
    }

    /**
     * Update an existing content action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function update(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url                = ArrayUtils::get($params, 'current-url');

            $content = $this->getContentFromForm($request);

            if ($this->contentRepository->save($content)) {
                $this->flash->addMessage('success', 'Le contenu <em>' . $content->getTitle() . '</em> a bien &eacute;t&eacute; modifi&eacute;');
            } else {
                $this->flash->addMessage('error', 'Une erreur est survenue lors de la tentative de modification du contenu <em>' . $content->getTitle() . '</em>');
            }
        }

        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * Save and publish an existing content action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function saveAndPublish(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url     = ArrayUtils::get($params, 'current-url');

            $content = $this->getContentFromForm($request);
            $content->setStatus('TO_PUBLISH');

            if ($this->contentRepository->save($content)) {
                $this->flash->addMessage('success', 'Le contenu <em>' . $content->getTitle() . '</em> a bien &eacute;t&eacute; modifi&eacute;');
                $url = '/contenus/publier/' . $content->getId();
            } else {
                $this->flash->addMessage('error', 'Une erreur est survenue lors de la tentative de modification du contenu <em>' . $content->getTitle() . '</em>');
            }
        }

        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * Duplicate an existing content to a new one action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function duplicate(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url = ArrayUtils::get($params, 'current-url');
            $id  = ArrayUtils::get($params, 'content-id');

            $content       = $this->contentRepository->findById((int) $id);
            $copyOfContent = clone $content;
            $copyOfContent->setId(null);
            $copyOfContent->setCreator($this->dbUser);
            $copyOfContent->setCreationDate(new DateTime('NOW'));
            $copyOfContent->setStatus('DRAFT');
            $copyOfContent->setPriority(1);
            $copyOfContent->initializeAuthorsCollection();
            $copyOfContent->setTitle($content->getTitle() . ' - copie');

            // Remove content illustration, medias are also "lost" because medias storage is not handled by content
            $copyOfContent->unsetIllustration();

            if ($this->contentRepository->save($copyOfContent)) {
                $this->flash->addMessage('success', 'Le contenu <em>' . $content->getTitle() . '</em> a bien &eacute;t&eacute; dupliqu&eacute;');
            } else {
                $this->flash->addMessage('error', 'Une erreur est survenue lors de la tentative de duplication du contenu <em>' . $content->getTitle() . '</em>');
            }
        }

        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * Archive a existing content action (status change to ARCHIVED).
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function archive(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url = ArrayUtils::get($params, 'current-url');
            $id  = ArrayUtils::get($params, 'content-id');

            $content = $this->contentRepository->findById((int) $id);
            $content->setStatus('ARCHIVED');

            if ($this->contentRepository->save($content)) {
                $this->flash->addMessage('success', 'Le contenu <em>' . $content->getTitle() . '</em> a bien &eacute;t&eacute; archiv&eacute;');
            } else {
                $this->flash->addMessage('error', 'Une erreur est survenue lors de la tentative d\'archivage du contenu <em>' . $content->getTitle() . '</em>');
            }
        }

        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * Delete a content action (status changed to DELETED).
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function delete(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url = ArrayUtils::get($params, 'current-url');
            $id  = ArrayUtils::get($params, 'content-id');

            $content = $this->contentRepository->findById((int) $id);
            $content->setStatus('DELETED');
            if ($this->contentRepository->save($content)) {
                $this->flash->addMessage('success', 'Le statut du contenu <em>' . $content->getTitle() . '</em> a bien &eacute;t&eacute; chang&eacute; &agrave; supprim&eacute;');
            } else {
                $this->flash->addMessage('error', 'Une erreur est survenue lors de la tentative de suppression du contenu <em>' . $content->getTitle() . '</em>');
            }
        }

        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * Get contents lists from an optional category filter in JSON format through an AJAX HTTP request.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function load(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $categoryId   = (int) ArrayUtils::get($params, 'category');
            $organization = $this->dbUser->getOrganization();

            if (is_int($categoryId) && $categoryId > 0) {
                $categories = array($this->categoryRepository->findById((int) $categoryId));
            } else {
                $categories = $organization->getCategories();
            }

            if (!empty($categories)) {
                $draftContents     = [];
                $toPublishContents = [];
                $publishedContents = [];
                $archivedContents  = [];
                $deletedContents   = [];

                $this->setContentsArrayByStatus($categories, $draftContents, $toPublishContents, $publishedContents, $archivedContents, $deletedContents);

                $data = [
                    'draft'     => [
                        'total' => sizeOf($draftContents), 'totalNotFiltered' => sizeOf($draftContents), 'rows' => $draftContents
                    ],
                    'toPublish' => [
                        'total' => sizeOf($toPublishContents), 'totalNotFiltered' => sizeOf($toPublishContents), 'rows' => $toPublishContents
                    ],
                    'archived'  => [
                        'total' => sizeOf($archivedContents), 'totalNotFiltered' => sizeOf($archivedContents), 'rows' => $archivedContents
                    ],
                    'deleted'   => [
                        'total' => sizeOf($deletedContents), 'totalNotFiltered' => sizeOf($deletedContents), 'rows' => $deletedContents
                    ],
                    'published' => [
                        'total' => sizeOf($publishedContents), 'totalNotFiltered' => sizeOf($publishedContents), 'rows' => $publishedContents
                    ]
                ];
                $response->getBody()->write(json_encode($data));
            }
        }
        return $response->withHeader('Content-Type', self::JSON);
    }

    /**
     * Share a content by email.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function shareByEmail(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url = ArrayUtils::get($params, 'current-url');
            $contentId = ArrayUtils::get($params, 'share-content-id');
            $emails = ArrayUtils::get($params, 'share-email', []);
            $message = nl2br(ArrayUtils::get($params, 'share-email-message'));

            $recipient = explode(',', $emails);
            $content = $this->contentRepository->findById($contentId);

            $mailGenerator = new MailGeneratorUtils($this->twig);
            if ($mailGenerator->shareContentByEmail($recipient, $this->dbUser->getOrganization(), $this->user, $content, $message)) {
                $this->flash->addMessage('success', 'Le contenu a bien &eacute;t&eacute; partag&eacute; par email');
            } else {
                $this->flash->addMessage('error', 'Une erreur est survenue lors du partage du contenu par email');
            }
        }
        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * Get a content object from form, for add and update action
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return Content
     * @access private
     */
    private function getContentFromForm(ServerRequestInterface $request): Content {
        $params = $request->getParsedBody();
        $id                 = ArrayUtils::get($params, 'content-id');
        $title              = ArrayUtils::get($params, 'content-title');
        $text               = ArrayUtils::get($params, 'content-text');
        $distributionListId = ArrayUtils::get($params, 'content-distributionList');
        $categoryId         = ArrayUtils::get($params, 'content-category');
        $priority           = ArrayUtils::get($params, 'content-priority');
        $sendDate           = ArrayUtils::get($params, 'content-send-date');
        $sendTime           = ArrayUtils::get($params, 'content-send-time');
        $channels           = ArrayUtils::get($params, 'content-channel');
        $ready              = ArrayUtils::get($params, 'content-ready');
        $mediaIdsToDelete   = array_keys(ArrayUtils::get($params, 'delete-media', []));
        $uploadedMedias     = ArrayUtils::get($params, 'uploaded-files');
        $mediaToUpdate      = ArrayUtils::get($params, 'edit-media', []);

        if (is_numeric($id) && intval($id) > 0) {
            $content = $this->contentRepository->findById((int) $id);
            $content->setTitle($title);
            $content->setText($text);

            $this->deleteUpdateMediasFromContent($content, $mediaIdsToDelete, $mediaToUpdate);
        } else {
            $content = new Content($title, $text);
            $content->setCreator($this->dbUser);
        }
        $this->setContent($content, $priority, $sendDate, $sendTime, $categoryId, $distributionListId, $channels, $ready);

        // Content illustration
        if (!empty($request->getUploadedFiles()['content-illustration'])) {
            $contentIllustrationFile = $request->getUploadedFiles()['content-illustration'];
            $illustration = $this->getMulticanalFile($contentIllustrationFile, $content->getTitle(), 'contents');
            if ($illustration instanceof MulticanalFile) {
                $content->setIllustration($illustration);
            }
        }

        if (!is_null($uploadedMedias)) {
            /** Add uploaded multicanal files  */
            foreach ($uploadedMedias as $uploadId => $uploadedFileItem) {
                $mFile = $this->handlePreUploadedMediaFiles($uploadId, $uploadedFileItem);
                $mFile->setContent($content);
                $content->addMedia($mFile);
            }
        }

        return $content;
    }

    /**
     * Set a content object from HTTP request form extracted data
     *
     * @param Content &$content
     * @param mixed $priority
     * @param mixed $sendDate
     * @param mixed $sendTime
     * @param mixed $categoryId
     * @param mixed $distributionListId
     * @param mixed $channels
     * @param mixed $ready
     * @author Thierry CEREYON <t.cereyon@groupeonepoint.com>
     * @return void
     * @throws ORMException
     * @access private
     */
    private function setContent(
        Content &$content,
        mixed   $priority,
        mixed   $sendDate,
        mixed   $sendTime,
        mixed   $categoryId,
        mixed   $distributionListId,
        mixed   $channels,
        mixed   $ready
    ): void {
        $content->setCreationDate(new DateTime('NOW'));
        // Done set Channels
        if (!empty($channels)) {
            $channelType = new ChannelType;
            $allowedChannels = $channelType->getStatusOptions();
            $channelEntities = [];

            $organisation = $this->dbOrganization; // TODO: ask if it's ok that way
            foreach ($organisation->getChannels() as $channel) {
                $key = 'CHANNEL-' . $channel->getId();
                $allowedChannels[] = $key;
                $channelEntities[$key] = $channel;
            }

            $submittedChannels = array_filter($channels, function ($channel) use ($allowedChannels) {
                return in_array($channel, $allowedChannels);
            });

            $existingChannels = [];
            // Remove existing communication that are unchecked
            foreach ($content->getCommunications() as &$communication) {
                if (!in_array($communication->getChannel(), $submittedChannels)) {
                    $content->removeCommunication($communication);
                } else {
                    array_push($existingChannels, $communication->getChannel());
                }
            }
            unset($communication);

            $channelsToAdd = array_diff($submittedChannels, $existingChannels);
            foreach ($channelsToAdd as $channelName) {
                if ($channel = $channelEntities[$channelName] ?? null) {
                    $channelContent = new Communication($content, channel: $channel);
                } else {
                    $channelContent = new Communication($content, channelName: $channelName);
                }
                $content->addCommunication($channelContent);
            }
        } else {
            foreach ($content->getCommunications() as &$communication) {
                $content->removeCommunication($communication);
            }
            unset($communication);
        }
        $content->addAuthor($this->dbUser);
        $content->setPriority($priority != null ? $priority : 1);
        $content->setDeadline($this->getDeadlineAsDateTime($sendDate, $sendTime));
        $content->setStatus('DRAFT');
        if (!is_null($categoryId) && $categoryId > 0) {
            $content->setCategory($this->categoryRepository->findById((int) $categoryId));
        }
        if (!is_null($distributionListId) && $distributionListId > 0) {
            $content->setDistributionList($this->distributionListRepository->findById((int) $distributionListId));
        }
        if (!is_null($ready)) {
            $content->setStatus('TO_PUBLISH');
        } else {
            $content->setStatus('DRAFT');
        }
    }


    /**
     * handlePreUploadedMediaFiles
     *
     * In "add" and "update" content, transform pre-uploaded files with ajax dmUploader into real MulticanalFile,
     * move it to its final destination $upload_directory, removes it from tmp upload dir $upload_tmp_directory.
     * @param mixed $uploadId
     * @param mixed $uploadedFileItem
     * @return MulticanalFile
     * @access private
     */
    private function handlePreUploadedMediaFiles($uploadId, $uploadedFileItem): MulticanalFile {
        $uploadDirectoryTemp = Helper::getUploadDirectoryTmp();
        /** @var string $upload_directory */
        $uploadDirectory = Helper::getUploadDirectory() . '/' . $this->user->getDomainName() . '/contents';
        $fileTmpUploadDir = $uploadDirectoryTemp . '/' . $uploadId;
        $tmpFilepath      = $fileTmpUploadDir . '/' . $uploadedFileItem['filename'];

        if (!is_dir($uploadDirectory)) {
            mkdir($uploadDirectory, 0755, true);
        }

        $uploadedFile = new UploadedFile($tmpFilepath, $uploadedFileItem['filename']);
        $uploadedFile->moveTo($uploadDirectory . '/' . $uploadedFile->getClientFilename());
        rmdir($fileTmpUploadDir);
        $mFile = new MulticanalFile($uploadedFile->getClientFilename(), $uploadDirectory);
        $mFile->setDescription($uploadedFileItem['description']);

        return $mFile;
    }

    /**
     * Set contents array by status from categories list.
     * @param array|Collection $categories Categories array with contents
     * @param array $draftContents Draft contents array by reference
     * @param array $toPublishContents To publish contents array by reference
     * @param array $publishedContents Published contents array by reference
     * @param array $archivedContents Archived contents array by reference
     * @param array $deletedContents Deleted contents array by reference
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return void
     * @access private
     */
    private function setContentsArrayByStatus(array|Collection $categories, array &$draftContents, array &$toPublishContents, array &$publishedContents, array &$archivedContents, array &$deletedContents): void {
        foreach ($categories as $category) {
            $contents = $category->getContents();
            if (!empty($contents)) {
                /** @var Content $content */
                foreach ($contents as $content) {
                    if ($content->getStatus() == 'DRAFT') {
                        $draftContents[] = $content->__toArray();
                    } elseif ($content->getStatus() == 'TO_PUBLISH' || $content->getStatus() == 'PUBLISHING') {
                        $toPublishContents[] = $content->__toArray();
                    } elseif ($content->getStatus() == 'PUBLISHED') {
                        $publishedContents[] = $content->__toArray();
                    } elseif ($content->getStatus() == 'ARCHIVED') {
                        $archivedContents[] = $content->__toArray();
                    } elseif ($content->getStatus() == 'DELETED') {
                        $deletedContents[] = $content->__toArray();
                    }
                }
            }
        }
    }

    /**
     * Delete medias from an existing content in update action.
     * @param Content $content Existing content
     * @param array $mediaIdsToDelete Medias identifiers to delete
     * @param array $mediaToUpdate Medias to update
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return void
     * @access private
     */
    private function deleteUpdateMediasFromContent(Content $content, array $mediaIdsToDelete, array $mediaToUpdate): void {
        foreach ($content->getMedias() as &$media) {
            if (in_array($media->getId(), $mediaIdsToDelete)) {
                $content->removeMedia($media);
            }
            if (isset($mediaToUpdate[$media->getId()])) {
                $media->setDescription($mediaToUpdate[$media->getId()]);
                $this->entityManager->flush($media);
            }
        }
    }
}
