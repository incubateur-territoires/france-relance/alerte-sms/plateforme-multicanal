<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\Action;

use Exception;
use Jumbojett\OpenIDConnectClient;
use Multicanal\Controller\Action\ActionController;
use Multicanal\Entity\Enum\Rank;
use Multicanal\Entity\Exception\MulticanalException;
use Multicanal\Entity\Organization;
use Multicanal\Entity\User;
use Multicanal\Lib\UserDAO;
use Multicanal\Utils\ArrayUtils;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * AuthenticationActionController class to manage authentication and logout
 *
 * @package Multicanal\Controller\Action
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class AuthenticationActionController extends ActionController {

    const OIDC_EXTERNAL_PARAM = 'oidc.external';

    private OpenIDConnectClient $oidc;

    protected UserDAO $dao;

    public function __construct(ContainerInterface $container) {
        parent::__construct($container);
        $this->dao      = $container->get(UserDAO::class);
        $this->oidc     = $container->get(OpenIDConnectClient::class);
    }

    /**
     * User login to access protected pages.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function login(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $login    = strtolower(ArrayUtils::get($params, 'login'));
            $password = ArrayUtils::get($params, 'password');
            $url      = ArrayUtils::get($params, 'current-url');

            if (!empty($login) && strpos($login, '@') !== false && !empty($password)) {
                $emailArray       = explode('@', $login);
                $login            = $emailArray[0];
                $organizationName = $emailArray[1];

                try {
                    $user = $this->dao->searchUser($organizationName, $login, $password);
                    $isNewOrganization = $this->getUserFromDatabaseAndCreateIfNotExists($user);
                    $this->session->regenerateId();
                    $this->session->set('user', \serialize($user));
                    $url = $this->getLoginRedirectionPath($user, $isNewOrganization);
                } catch (\Exception $e) {
                    $this->flash->addMessage('error', $e->getMessage());
                }
            } else {
                $this->flash->addMessage('warning', 'Veuillez saisir votre identifiant et votre mot de passe');
            }
        }

        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * User login to access protected pages through OIDC protocol.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @param mixed $args Mixed parameters
     * @return ResponseInterface
     * @access public
     */
    public function openIdConnect(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        if ($this->settings->get(self::OIDC_EXTERNAL_PARAM)) {
            $this->doOIDCAuth();
        } else {
            try {
                $this->doGNOIDCAuth($args);
            } catch (MulticanalException $e) {
                if ($this->user && $this->user->getIdToken()) {
                    $this->flash->addMessage('error', $e->getMessage());
                    $this->oidc->signOut($this->user->getIdToken(), $request->getUri()->withPath('/')->withQuery('')->__toString());
                }
            }
        }
        $url = $this->getLoginRedirectionPath($this->user);

        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * Check user authentication through OIDC protocol.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function checkSso(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        try {
            if ($this->settings->get(self::OIDC_EXTERNAL_PARAM)) {
                $this->doOIDCAuth();
            } else {
                $this->doGNOIDCAuth();
            }
            // If we are here, user is SSO logged, and instanciated, we can reload "parent" frame
            $response->getBody()->write('<html><body><script>window.parent.location.reload();</script></body></html>');
        } catch (\Exception $e) {
            if ($this->settings->get('env.debug')) {
                $response->getBody()->write('<html><body><script>console.log(\'' . $e->getMessage() . '\');</script></body></html>');
            }
        }

        return $response;
    }

    /**
     * Refresh sso session (extends expiration)
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function refreshSso(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        try {
            $tokenExpiration = $this->session->get('exp');
            $iframeContent = '';
            $serverParams = $request->getServerParams();
            $expiresIn = $tokenExpiration - $serverParams['REQUEST_TIME'];

            $refreshIn = max($expiresIn - 20, 5);

            if ($expiresIn < 60) {
                if ($this->settings->get(self::OIDC_EXTERNAL_PARAM)) {
                    $this->doOIDCAuth();
                } else {
                    $this->doGNOIDCAuth();
                }
                $refreshIn = 240;
            }
            if ($this->settings->get('env.debug')) {
                $iframeContent = '<script>console.log(\'Next Background SSO refresh in ' . $refreshIn . ' seconds\');</script></body>';
            }
            $response->getBody()->write('<html><body>' . $iframeContent . '</body></html>');
        } catch (Exception $e) {
            $response->getBody()->write('<html><body><script>console.log(\'' . $e->getMessage() . '\');</script></body></html>');
        }

        $refreshHeaderValue = sprintf('%s;url=%s', $refreshIn, $request->getUri()->withQuery('')->__toString());
        return $response->withHeader('Refresh', $refreshHeaderValue);
    }

    /**
     * User logout.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function logout(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        if ($this->settings->get('oidc.connexion_enabled')) {
            if ($this->user && $this->user->getIdToken()) {
                $this->session->destroy();
                $this->session->start();
                $this->session->regenerateId();
                $this->oidc->signOut($this->user->getIdToken(), $request->getUri()->withPath('/')->__toString());
            }
        } else {
            $this->session->destroy();
            $this->session->start();
            $this->session->regenerateId();
        }
        return $response->withStatus(302)->withHeader('Location', '/');
    }

    /**
     * Get User from Database after authentication (LDAP or SSO) and create him if not exists with his organization.
     *
     * @param User $user User from authentication processing
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return bool if a new organization is created
     * @access private
     */
    private function getUserFromDatabaseAndCreateIfNotExists(User $user): bool {
        $newCreatedOrganization = false;
        $dbUser = $this->userRepository->findByEmail($user->getEmail());
        if (is_null($dbUser)) {
            /**
             * Clone User, because organization::$generalParameters will trigger the Error
             * Cannot assign __PHP_Incomplete_Class to property Multicanal\Entity\Organization::$generalParameters
             * when unserialising user in Middleware
             */
            $clonedUser = clone $user;

            /**
             * Allow only ADMINGN and ADMIN to auto-create domain on first login.
             */
            if (
                $this->settings->get(self::OIDC_EXTERNAL_PARAM)
                || $clonedUser->getRank()->equals(Rank::ADMINGN())
                || $clonedUser->getRank()->equals(Rank::ADMIN())
            ) {
                $organization = $this->organizationRepository->createIfNotExist($clonedUser->getOrganizationName());
                $newCreatedOrganization = true;
            } else {
                $organization = $this->organizationRepository->findByDomainName($clonedUser->getOrganizationName());
            }
            if ($organization instanceof Organization) {
                $clonedUser->setOrganization($organization);
                $this->entityManager->persist($clonedUser);
                $this->entityManager->flush();
            } else {
                $message = sprintf("Le domaine %s n'a pas été configuré par l'administrateur de l'application.
                        Vous ne pouvez pas encore vous y connecter.", $clonedUser->getOrganizationName());
                throw new MulticanalException($message);
            }
        }

        return $newCreatedOrganization;
    }

    /**
     * Perform generic authentication through external OIDC protocol.
     *
     * @return void
     * @throws MulticanalException
     * @access private
     */
    private function doOIDCAuth(): void {
        $this->oidc->authenticate();
        $userInfos = (array) $this->oidc->requestUserInfo();

        /* Array destructuring  [] si shortend for list() */
        [
            'given_name'         => $firstname,
            'family_name'        => $lastname,
            'preferred_username' => $login,
            'email'              => $email
        ] = $userInfos;

        [, $organizationName] = explode('@', $email);

        $data = $this->oidc->introspectToken($this->oidc->getIdToken());
        $this->session->set('exp', $data->exp);

        $idToken      = $this->oidc->getIdToken();
        $accessToken  = $this->oidc->getAccessToken();
        $refreshToken = $this->oidc->getRefreshToken();

        $user = new User($login);
        $user->setFirstname($firstname);
        $user->setLastname($lastname);
        $user->setEmail($email);
        $user->setOrganizationName($organizationName);
        $user->setIdToken($idToken);
        $user->setAccessToken($accessToken);
        $user->setRefreshToken($refreshToken);

        // Set User Level
        $user->setRank(Rank::AGENT());
        $user->setDescription('-');
        $this->getUserFromDatabaseAndCreateIfNotExists($user);
        $this->session->regenerateId();
        $this->session->set('user', \serialize($user));
        $this->user = $user;
    }

    /**
     * Perform authentication through GN OIDC protocol.
     *
     * @param mixed $args Mixed parameters
     * @return void
     * @throws MulticanalException
     * @access private
     */
    private function doGNOIDCAuth($args = []): void {
        $targetOrganization = ArrayUtils::get($args, 'targetOrganization');
        $this->oidc->addAuthParam(['target_organization' => $targetOrganization]);
        $this->oidc->authenticate();
        $userInfos = (array) $this->oidc->requestUserInfo();

        /* Array destructuring  [] si shortend for list() */
        [
            'given_name'         => $firstname,
            'family_name'        => $lastname,
            'preferred_username' => $login,
            'email'              => $email,
            'clientRank'         => $rank,
            'clientAccess'       => $access,
            'clientIdentifier'   => $clientIdentifier
        ] = $userInfos;

        $data = $this->oidc->introspectToken($this->oidc->getIdToken());
        $this->session->set('exp', $data->exp);

        $idToken      = $this->oidc->getIdToken();
        $accessToken  = $this->oidc->getAccessToken();
        $refreshToken = $this->oidc->getRefreshToken();

        // Extract the domain from the clientIdentifier
        [, $organizationName] = explode('@', $clientIdentifier);
        if (empty($organizationName)) {
            throw new MulticanalException('Aucune organisation trouvée.');
        }

        $user = new User($login);
        $user->setFirstname($firstname);
        $user->setLastname($lastname);
        $user->setEmail($email);
        $user->setOrganizationName($organizationName == 'admin.gn' ? 'girondenumerique.fr' : $organizationName);
        $user->setIdToken($idToken);
        $user->setAccessToken($accessToken);
        $user->setRefreshToken($refreshToken);

        // Set User Level
        if ($rank > 0) {
            if (in_array($user->getOrganizationName(), ['admin.gn', 'girondenumerique.fr']) && $rank == 2) {
                $user->setRank(Rank::ADMINGN());
            } else {
                $user->setRank(Rank::ADMIN());
            }
        } else {
            $user->setRank(Rank::AGENT());
        }
        // Set User Application specific
        if (null !== $access) {
            $user->setDescription($access);
        } else {
            $user->setDescription('-');
        }

        /**
         * Set $this->user before getUserFromDatabase... where is checked Organization existence for non-adminGN user
         * so we can logOut user from SSO before redirecting home with a message if no organization configured
         * MulicanalException thrown in getUserFromDatabaseAndCreateIfNotExists
         */
        $this->user = $user;
        $this->getUserFromDatabaseAndCreateIfNotExists($user);
        $this->session->regenerateId();
        $this->session->set('user', \serialize($user));
    }

    /**
     * Get redirection path in login action from user rank and newly created organization flag.
     *
     * @param User $user Authenticated user
     * @param bool $isNewOrganization (optional) Flag which indicate if organization is newly created for a first login
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return string
     * @access private
     */
    private function getLoginRedirectionPath(User $user, ?bool $isNewOrganization = false): ?string {
        $url = null;
        if ($user->getRank() == Rank::ADMINGN()) {
            $url = '/administration';
        } elseif ($isNewOrganization && $user->getRank() == Rank::ADMIN()) {
            $url = '/collectivite';
            $this->flash->addMessage('info', 'Bienvenu sur la plateforme de communication multicanal, veuillez param&eacute;trer votre collectivit&eacute; afin de proc&eacute;der &agrave; vos communications.');
        } else {
            $url = '/contenus';
        }

        return $url;
    }
}
