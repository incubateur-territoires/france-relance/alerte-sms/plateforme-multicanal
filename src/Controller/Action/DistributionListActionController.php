<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\Action;

use Doctrine\Common\Collections\Collection;
use Multicanal\Controller\Service\DistributionListService;
use Multicanal\Entity\DistributionList;
use Multicanal\Entity\Enum\Rank;
use Multicanal\Utils\ArrayUtils;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpNotFoundException;

/**
 * Class DistributionListActionController manager
 *
 * @package Multicanal\Controller\Action
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class DistributionListActionController extends DistributionListService {

    /**
     * Get a specific distribution list in JSON format through an AJAX HTTP request.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @throws HttpNotFoundException if distribution list does not exist
     * @access public
     */
    public function get(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $id = ArrayUtils::get($params, 'distribution-id');

            $distributionList = $this->distributionListRepository->findById((int)$id);
            if (is_null($distributionList)) {
                throw new HttpNotFoundException($request, 'Liste de diffusion inexistante ou non trouvé pour l\id ' . $id);
            } else {
                $response->getBody()->write(json_encode($distributionList));
            }
        }
        return $response->withHeader('Content-Type', self::JSON);
    }

    /**
     * Load distribution lists for a specific organization in JSON format through an AJAX HTTP request.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function loadByOrganisation(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params            = $request->getParsedBody();
        $distributionLists = [];
        if ($this->user->getRank() == Rank::ADMINGN && ArrayUtils::get($params, 'organization')) {
            $organization      = $this->organizationRepository->findByDomainName(ArrayUtils::get($params, 'organization'));
            $distributionLists = $organization->getDistributionLists();
        }
        $data = $this->getDistributionListAsArray($distributionLists);
        $response->getBody()->write(json_encode($data, JSON_THROW_ON_ERROR));

        return $response->withHeader('Content-Type', self::JSON);
    }

    /**
     * Load distribution lists in JSON format through an AJAX HTTP request.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function load(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->user->getOrganizationName() == ArrayUtils::get($params, 'organization')) {
            $distributionLists = $this->dbUser->getOrganization()->getDistributionLists();
        }
        $data = $this->getDistributionListAsArray($distributionLists);
        $response->getBody()->write(json_encode($data, JSON_THROW_ON_ERROR));

        return $response->withHeader('Content-Type', self::JSON);
    }

    /**
     * Get a specific distribution list in HTML format through an AJAX HTTP request.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function preview(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $id = ArrayUtils::get($params, 'distribution-id');

            $params = [
                'inactive'      => true,
                'organization'  => $this->dbOrganization,
                'communication' => $this->distributionListRepository->findById((int) $id)
            ];
            $response->getBody()->write($this->twig->fetchBlock('public/communication-details.html', 'communication', $params));
        }

        return $response->withHeader('Content-Type', self::HTML);
    }

    /**
     * Add a new distribution lists action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function add(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url         = ArrayUtils::get($params, 'current-url');
            $title       = ArrayUtils::get($params, 'distribution-title');
            $description = ArrayUtils::get($params, 'distribution-description');
            $categoryId  = ArrayUtils::get($params, 'distribution-category');

            $distributionList = $this->addDistribitionListService($params, $categoryId, $title, $description, $request);

            // Save distribution list to get identifier
            if ($this->distributionListRepository->save($distributionList)) {
                $distributionList->updateShortUrlQrCodeFile();
                // Save distribution list to persist shortened url and QR code
                $this->distributionListRepository->save($distributionList);
                $this->flash->addMessage('success', 'La liste de diffusion <em>' . $distributionList->getTitle() . '</em> a bien &eacute;t&eacute; ajout&eacute;e');
            } else {
                $this->flash->addMessage('error', 'Une erreur est survenue lors de la tentative d\'ajout de la liste de diffusion <em>' . $distributionList->getTitle() . '</em>');
            }
        }

        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * Update an existing distribution lists action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function update(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url         = ArrayUtils::get($params, 'current-url');
            $id          = ArrayUtils::get($params, 'distribution-id');
            $title       = ArrayUtils::get($params, 'distribution-title');
            $description = ArrayUtils::get($params, 'distribution-description');
            $categoryId  = ArrayUtils::get($params, 'distribution-category');

            $distributionList = $this->distributionListRepository->findById((int) $id);

            if (empty($distributionList->getQrCodeFile())) {
                $distributionList->updateShortUrlQrCodeFile();
            }

            $category         = $this->categoryRepository->findById((int) $categoryId);
            if ($distributionList instanceof DistributionList) {
                $this->updateDistributionListService($distributionList, $title, $description, $category, $request);

                if ($this->distributionListRepository->save($distributionList)) {
                    $this->flash->addMessage('success', 'La liste de diffusion <em>' . $distributionList->getTitle() . '</em> a bien &eacute;t&eacute; modifi&eacute;e');
                } else {
                    $this->flash->addMessage('error', 'Une erreur est survenue lors de la tentative de modification de la liste de diffusion <em>' . $distributionList->getTitle() . '</em>');
                }
            } else {
                $this->flash->addMessage('error', 'Une erreur est survenue. La liste de diffusion <em>' . $title . " n'a pas été trouvé</em>");
            }
        }
        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * Delete a distribution lists action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function delete(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url = ArrayUtils::get($params, 'current-url');
            $id  = ArrayUtils::get($params, 'distributionList-id');

            $distributionList = $this->distributionListRepository->findById((int) $id);
            if ($this->distributionListRepository->delete($distributionList)) {
                $this->flash->addMessage('success', 'La liste de diffusion <em>' . $distributionList->getTitle() . '</em> a bien &eacute;t&eacute; supprim&eacute;e');
            } else {
                $this->flash->addMessage('error', 'Une erreur est survenue lors de la tentative de suppression de la liste de diffusion <em>' . $distributionList->getTitle() . '</em>');
            }
        }

        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * Set a distributionLists array.
     *
     * @param Collection $distributionLists DistributionLists as collection
     * @author Thierry CEREYON <t.cereyon@groupeonepoint.com>
     * @return array
     * @access private
     */
    private function getDistributionListAsArray(Collection $distributionLists): array {
        /** @var DistributionList $distributionList */
        $rows = [];
        foreach ($distributionLists as $distributionList) {
            $rows[] = [
                'id'                => $distributionList->getId(),
                'title'             => $distributionList->getTitle(),
                'emailsSubscribers' => count($distributionList->getEmailSubscriptions()),
                'phonesSubscribers' => count($distributionList->getMobileSubscriptions()),
            ];
        }
        return ['total' => sizeOf($rows), 'totalNotFiltered' => sizeOf($rows), 'rows' => $rows];
    }
}
