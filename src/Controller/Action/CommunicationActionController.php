<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\Action;

use DateTime;
use Multicanal\Controller\Action\ActionController;
use Multicanal\Entity\Communication;
use Multicanal\Entity\Content;
use Multicanal\Entity\DistributionList;
use Multicanal\Repository\ChannelRepository;
use Multicanal\Repository\CommunicationRepository;
use Multicanal\Utils\ArrayUtils;
use Multicanal\Utils\ChannelPublisherUtils;
use Multicanal\Utils\MailGeneratorUtils;
use Multicanal\Utils\SMSUtils;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpNotFoundException;
use _PHPStan_a3459023a\Symfony\Component\Console\Exception\LogicException;

/**
 * Class CommunicationActionController manager to manage content communication through channel
 *
 * @package Multicanal\Controller\Action
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class CommunicationActionController extends ActionController {

    const SAVE_COMMUNICATION_ERROR = 'Une erreur est survenue lors de la tentative d\'enregistrement de la communication';

    private CommunicationRepository $communicationRepository;

    public function __construct(ContainerInterface $container) {
        parent::__construct($container);
        $this->communicationRepository = $container->get(CommunicationRepository::class);
    }

    /**
     * Save an existing communication action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function save(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url = ArrayUtils::get($params, 'current-url');
            $id  = ArrayUtils::get($params, 'communication-id');

            $communication = $this->communicationRepository->findById($id);
            if ($this->saveCommunication($communication, $request, $params)) {
                $this->flash->addMessage('success', 'La communication a bien &eacute;t&eacute; enregistr&eacute;e');
            } else {
                $this->flash->addMessage('error', self::SAVE_COMMUNICATION_ERROR);
            }
            return $response->withStatus(302)->withHeader('Location', $url);
        }
    }

    /**
     * Schedule a communication for a later publication action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @throws HttpNotFoundException if communication does not exist
     * @access public
     */
    public function schedule(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url         = ArrayUtils::get($params, 'current-url');
            $id          = ArrayUtils::get($params, 'communication-id');
            $sendingDate = ArrayUtils::get($params, 'sending-date');
            $sendingTime = ArrayUtils::get($params, 'sending-time');

            $scheduleDate = $this->getDeadlineAsDateTime($sendingDate, $sendingTime);

            if ($scheduleDate > new DateTime('NOW')) {
                $communication = $this->communicationRepository->findById($id);
                if ($communication instanceof Communication) {
                    $communication->setScheduleDate($scheduleDate);
                    if ($this->communicationRepository->save($communication)) {
                        $this->flash->addMessage('success', 'La communication a bien &eacute;t&eacute; planifi&eacute;e');
                    } else {
                        $this->flash->addMessage('error', 'Une erreur est survenue lors de la tentative de planification de la communication');
                    }
                } else {
                    throw new HttpNotFoundException($request, 'La communication pour l\'identifiant ' . $id . ' n\'existe pas');
                }
            } else {
                $this->flash->addMessage('warning', 'La date d\'envoi doit &ecirc;tre sup&eacute;rieure &agrave; la date du jour');
            }
        }
        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * Cancel a communication scheduling action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @throws HttpNotFoundException if communication does not exist
     * @access public
     */
    public function cancelScheduling(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url         = ArrayUtils::get($params, 'current-url');
            $id          = ArrayUtils::get($params, 'communication-id');

            $communication = $this->communicationRepository->findById($id);
            if ($communication instanceof Communication) {
                $communication->setScheduleDate(null);
                if ($this->communicationRepository->save($communication)) {
                    $this->flash->addMessage('success', 'La planification de la communication a bien &eacute;t&eacute; d&eacute;programm&eacute;e');
                } else {
                    $this->flash->addMessage('error', 'Une erreur est survenue lors de la tentative d\'annulation de la planification de la communication');
                }
            } else {
                throw new HttpNotFoundException($request, 'La communication pour l\'identifiant ' . $id . ' n\'existe pas');
            }
        }
        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * Publish a communication by email action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function publishByEmail(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url = ArrayUtils::get($params, 'current-url');
            $id  = ArrayUtils::get($params, 'communication-id');

            $communication = $this->communicationRepository->findById($id);
            if ($this->saveCommunication($communication, $request, $params)) {
                $mailGenerator = new MailGeneratorUtils($this->twig);
                if ($mailGenerator->publishContentByEmail($communication)) {
                    $this->updateInformations($communication);
                    $this->flash->addMessage('success', 'Le contenu a bien &eacute;t&eacute; publi&eacute; par email');
                } else {
                    $this->flash->addMessage('error', 'Une erreur est survenue lors de la publication du contenu par email');
                }
            } else {
                $this->flash->addMessage('error', self::SAVE_COMMUNICATION_ERROR);
            }

            return $response->withStatus(302)->withHeader('Location', $url);
        }
    }

    /**
     * Publish a communication by SMS action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function publishBySMS(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url = ArrayUtils::get($params, 'current-url');
            $id  = ArrayUtils::get($params, 'communication-id');

            $communication = $this->communicationRepository->findById($id);
            if ($this->saveCommunication($communication, $request, $params)) {
                if (SMSUtils::publishContentBySMS($communication)) {
                    $this->updateInformations($communication);
                    $this->flash->addMessage('success', 'Le contenu a bien &eacute;t&eacute; publi&eacute; par SMS');
                } else {
                    $this->flash->addMessage('error', 'Une erreur est survenue lors de la tentative de publication du contenu par SMS.');
                }
            } else {
                $this->flash->addMessage('error', self::SAVE_COMMUNICATION_ERROR);
            }

            return $response->withStatus(302)->withHeader('Location', $url);
        }
    }


    /**
     * Publish a communication by channel action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function publishByChannel(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $id  = ArrayUtils::get($params, 'communication-id');
            $url = ArrayUtils::get($params, 'current-url');

            $params['communication-object'] = ArrayUtils::get($params, 'communication-object-' . $id);
            $params['communication-content'] = ArrayUtils::get($params, 'communication-content-' . $id);

            $communication = $this->communicationRepository->findById($id);
            $channel = $communication->getOrganisationChannel();
            if ($this->saveCommunication($communication, $request, $params)) {
                if (ChannelPublisherUtils::sendCommunication($communication)) {
                    $this->updateInformations($communication);
                    $this->flash->addMessage('success', 'Le contenu a bien &eacute;t&eacute; publi&eacute; via ' . $channel->getChannelName() . '.');
                } else {
                    $this->flash->addMessage('error', 'Une erreur est survenue lors de la tentative de publication du contenu via ' . $channel->getChannelName() . '.');
                }
            } else {
                $this->flash->addMessage('error', self::SAVE_COMMUNICATION_ERROR);
            }

            return $response->withStatus(302)->withHeader('Location', $url);
        }
    }

    public function previewEmail(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $id  = ArrayUtils::get($params, 'communication-id');
            $communication = $this->communicationRepository->findById($id);
            $this->saveCommunication($communication, $request, $params);
            $htmlPreview = $this->twig->fetchBlock(
                'email/content-publication.html',
                'preview',
                ['communication' => $communication]
            );
        }
        $response->withHeader('Content-type', 'text/html');
        $response->getBody()->write($htmlPreview);
        return $response;
    }

    /**
     * Save a communication informations in database.
     *
     * @param Communication|null $communication Communication to save
     * @param ServerRequestInterface $request Current HTTP request
     * @param array $params Form values from current HTTP request
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return bool
     * @throws HttpNotFoundException if communication does not exist
     * @access private
     */
    private function saveCommunication(?Communication &$communication, ServerRequestInterface $request, array $params): bool {
        $id                 = ArrayUtils::get($params, 'communication-id');
        $object             = ArrayUtils::get($params, 'communication-object');
        $text               = ArrayUtils::get($params, 'communication-content') ?? '';
        $senderMail         = ArrayUtils::get($params, 'communication-sender-email');
        $distributionListId = ArrayUtils::get($params, 'communication-distributionList');
        $distributionList   = $distributionListId ? $this->distributionListRepository->findById($distributionListId) : null;

        if ($communication instanceof Communication) {
            $communication->getMedias()->clear();
            foreach ($communication->getContent()->getMedias() as $medium) {
                if (ArrayUtils::get($params, 'communication-media-' . $medium->getId())) {
                    $communication->getMedias()->add($medium);
                }
            }

            if (!empty($distributionList) && $distributionList instanceof DistributionList) {
                $communication->getContent()->setDistributionList($distributionList);
            }
            if (!empty($senderMail)) {
                $communication->setSenderMail($senderMail);
            }
            $this->contentRepository->save($communication->getContent());

            if (!is_null($object)) {
                $communication->setObject($object);
            }
            $communication->setText($text);

            return $this->communicationRepository->save($communication);
        } else {
            throw new HttpNotFoundException($request, 'La communication pour l\'identifiant ' . $id . ' n\'existe pas');
        }
    }

    /**
     * Update organization counter and publisher information for a communication.
     *
     * @param Communication $communication SMS or email communication
     * @return void
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @access private
     */
    private function updateInformations(Communication $communication): void {
        $this->updateCounter($communication);
        $this->savePublisherInformationInCommunication($communication);
    }

    /**
     * Save publisher information after sending a content through a communication. Update the content if it's all published.
     *
     * @param Communication $communication Concerned communication
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return void
     * @access private
     */
    private function savePublisherInformationInCommunication(Communication &$communication): void {
        $communication->setSent(true);
        $communication->setPublisher($this->dbUser);
        $communication->setSendingDate(new DateTime('NOW'));
        $this->communicationRepository->save($communication);

        if ($this->isContentFullyPublished($communication->getContent())) {
            $communication->getContent()->setStatus('PUBLISHED');
            $this->entityManager->flush();
        } elseif ($communication->getContent()->getStatus() !== 'PUBLISHING') {
            $communication->getContent()->setStatus('PUBLISHING');
            $this->entityManager->flush();
        }
    }

    /**
     * Check if a content has been fully published on all channels except WEBSITE.
     *
     * @param Content $content Content to verify
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return bool
     * @access private
     */
    private function isContentFullyPublished(Content $content): bool {
        $published      = false;
        $communications = $content->getCommunications();
        if (!empty($communications)) {
            $published = true;
            foreach ($communications as $communication) {
                if ($communication->getChannel() != 'WEBSITE' && !$communication->getSent()) {
                    $published = false;
                }
            }
        }
        return $published;
    }
}
