<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\Action;

use Exception;
use Multicanal\App\Helper;
use Multicanal\Controller\Action\ActionController;
use Multicanal\Entity\Exception\UploadException;
use Multicanal\Utils\ArrayUtils;
use Multicanal\Utils\MulticanalFilesUtils;
use Multicanal\Utils\StringUtils;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * DmUploaderActionController class to manage content
 *
 * @package Multicanal\Controller\Action
 * @author  William BLANC DIT JOLICOEUR <w.blanc-dit-jolicoeur@girondenumerique.fr>
 */

final class DmUploaderActionController extends ActionController {

    /**
     * Upload file callback method used by DmUploader js widget.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function uploadFile(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $statusCode = 200;
        $result = [];
        $directory = Helper::getUploadDirectoryTmp();
        $params = $request->getParsedBody();
        $uploadId = ArrayUtils::get($params, 'upload_id');
        $uploadTempDir = $directory . DIRECTORY_SEPARATOR . $uploadId;
        $createDir = mkdir($uploadTempDir, 0755, true);
        $uploadedFiles = $request->getUploadedFiles();
        $uploadedFile = $uploadedFiles['file'];
        try {
            if ($uploadedFile->getError() === UPLOAD_ERR_OK && $createDir) {
                $uploadSettings = $this->settings->get('dmUploader');
                MulticanalFilesUtils::validateUploadedFile($uploadedFile, $uploadSettings);

                $slugifiedName = StringUtils::slug(pathinfo($uploadedFile->getClientFilename(), PATHINFO_FILENAME));
                $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
                $filename = sprintf('%s.%0.8s', $slugifiedName, $extension);
                $targetFilename = $uploadTempDir . DIRECTORY_SEPARATOR . $filename;
                $uploadedFile->moveTo($targetFilename);
                $result['status'] = 'OK';
                $result['message'] = 'fichier t&eacute;l&eacute;vers&eacute;.';
                $result['filename'] = $filename;
            } else {
                throw new UploadException($uploadedFile->getError());
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $result['status'] = 'ERROR';
            $result['message'] = 'Une erreur est survenue.';
            $result['error'] = $e->getMessage();
        }
        $response->getBody()->write(json_encode($result));
        return $response->withStatus($statusCode);
    }
}
