<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\Action;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Exception;
use Multicanal\Controller\Action\ActionController;
use Multicanal\Entity\DistributionList;
use Multicanal\Entity\EmailSubscription;
use Multicanal\Entity\Enum\Rank;
use Multicanal\Entity\MobileSubscription;
use Multicanal\Repository\SubscriptionRepository;
use Multicanal\Utils\ArrayUtils;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class SubscriptionActionController manager
 *
 * @package Multicanal\Controller\Action
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class SubscriptionActionController extends ActionController {

    private SubscriptionRepository $subscriptionRepository;

    public function __construct(ContainerInterface $container) {
        parent::__construct($container);
        $this->subscriptionRepository = $container->get(SubscriptionRepository::class);
    }

    /**
     * Load mobile subscriptions list in JSON format through an AJAX HTTP resquest.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function loadMobileSubscriptions(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params            = $request->getParsedBody();
        $distributionLists = $this->getDistributionListsFromRequestContext($params, $request);
        $rows              = [];
        /** @var DistributionList $distributionList */
        foreach ($distributionLists as $distributionList) {
            /** @var MobileSubscription $mobileSubscription */
            foreach ($distributionList->getMobileSubscriptions() as $mobileSubscription) {
                $rows[] = [
                    'id'               => $mobileSubscription->getId(),
                    'communication_id' => $distributionList->getId(),
                    'title'            => $distributionList->getTitle(),
                    'sms'              => $mobileSubscription->getMobile(),
                ];
            }
        }
        $data = ['total' => sizeOf($rows), 'totalNotFiltered' => sizeOf($rows), 'rows' => $rows];
        $response->getBody()->write(json_encode($data, JSON_THROW_ON_ERROR));

        return $response->withHeader('Content-Type', self::JSON);
    }

    /**
     * Load email subscriptions list in JSON format through an AJAX HTTP resquest.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function loadEmailSubscriptions(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params            = $request->getParsedBody();
        $distributionLists = $this->getDistributionListsFromRequestContext($params, $request);
        $rows              = [];
        /** @var DistributionList $distributionList */
        foreach ($distributionLists as $distributionList) {
            /** @var EmailSubscription $emailsSubscriber */
            foreach ($distributionList->getEmailSubscriptions() as $emailSubscription) {
                $rows[] = [
                    'id'               => $emailSubscription->getId(),
                    'communication_id' => $distributionList->getId(),
                    'title'            => $distributionList->getTitle(),
                    'email'            => $emailSubscription->getEmail(),
                ];
            }
        }
        $data = ['total' => sizeOf($rows), 'totalNotFiltered' => sizeOf($rows), 'rows' => $rows];
        $response->getBody()->write(json_encode($data, JSON_THROW_ON_ERROR));

        return $response->withHeader('Content-Type', self::JSON);
    }

    /**
     * Unsubscribe action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function unsubscribe(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url                = ArrayUtils::get($params, 'current-url');
            $distributionListId = ArrayUtils::get($params, 'communication-id');
            $email              = ArrayUtils::get($params, 'communication-email');
            $mobile             = ArrayUtils::get($params, 'communication-mobile');

            $distributionList = $this->distributionListRepository->findById((int) $distributionListId);
            $emailSubscriber  = $this->subscriptionRepository->findEmailByDistributionList($email, $distributionList);
            $mobileSubscriber = $this->subscriptionRepository->findMobileByDistributionList($mobile, $distributionList);

            $this->deleteSubscription([$email, $mobile], $distributionList, $emailSubscriber, $mobileSubscriber);

            return $response->withStatus(302)->withHeader('Location', $url);
        }
        return $response->withStatus(302)->withHeader('Location', '/');
    }

    /**
     * Delete a specific subscription action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function unsubscribeById(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url                = ArrayUtils::get($params, 'current-url');
            $distributionListId = ArrayUtils::get($params, 'communication-id');
            $email              = ArrayUtils::get($params, 'email-id');
            $mobile             = ArrayUtils::get($params, 'mobile-id');

            $distributionList = $this->distributionListRepository->findById((int) $distributionListId);
            $emailSubscriber  = $this->subscriptionRepository->findEmailById((int) $email);
            $mobileSubscriber = $this->subscriptionRepository->findMobileById((int) $mobile);

            $this->deleteSubscription([$email, $mobile], $distributionList, $emailSubscriber, $mobileSubscriber);

            return $response->withStatus(302)->withHeader('Location', $url);
        }
        return $response->withStatus(302)->withHeader('Location', '/');
    }

    /**
     * Subscribe action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function subscribe(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url                = ArrayUtils::get($params, 'current-url');
            $distributionListId = ArrayUtils::get($params, 'communication-id');
            $email              = ArrayUtils::get($params, 'communication-email');
            $mobile             = ArrayUtils::get($params, 'communication-mobile');

            $distributionList        = $this->distributionListRepository->findById((int) $distributionListId);
            $subscriptionSuccessfull = false;
            if ($distributionList instanceof DistributionList) {
                $this->subscribeEmailToDiffusionList($distributionList, $email, $subscriptionSuccessfull);

                $this->subscribeMobileToDiffusionList($distributionList, $mobile, $subscriptionSuccessfull);

                if ($subscriptionSuccessfull) {
                    try {
                        $this->entityManager->flush();
                        $this->flash->addMessage('success', 'Votre demande d\'abonnement a bien &eacute;t&eacute; prise en compte.');
                    } catch (OptimisticLockException | ORMException $e) {
                        $this->flash->addMessage('error', $this->setErrorMessage($e, 'Une erreur est survenue lors de l\'enregistrement de l\'abonnement'));
                    }
                }
            } else {
                $this->flash->addMessage('warning', 'Cette communication semble ne pas exister.');
            }
        }

        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * Subscribe emails from "my organization" administration page action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function subscribeEmails(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url                = ArrayUtils::get($params, 'current-url');
            $distributionListId = ArrayUtils::get($params, 'communication-id');
            $emailsAsString     = ArrayUtils::get($params, 'email');

            $distributionList   = $this->distributionListRepository->findById((int) $distributionListId);
            if ($distributionList instanceof DistributionList) {
                $emails = explode("\n", chop($emailsAsString));
                $subscriptionSuccessfull = false;
                foreach ($emails as $email) {
                    $this->subscribeEmailToDiffusionList($distributionList, trim($email), $subscriptionSuccessfull);
                }

                if ($subscriptionSuccessfull) {
                    $this->flushSubscriptions($distributionList, 'EMAIL', $emails);
                }
            } else {
                $this->flash->addMessage('warning', 'Aucune liste de diffusion enregistr&eacute;e pour l\'identifiant ' . $distributionListId);
            }
        }
        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * Subscribe mobile phone numbers from "my organization" administration page action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function subscribeMobiles(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url                = ArrayUtils::get($params, 'current-url');
            $distributionListId = ArrayUtils::get($params, 'communication-id');
            $mobilesAsString     = ArrayUtils::get($params, 'mobile');

            $distributionList   = $this->distributionListRepository->findById((int) $distributionListId);
            if ($distributionList instanceof DistributionList) {
                $mobiles = explode('\n', chop($mobilesAsString));
                $subscriptionSuccessfull = false;
                foreach ($mobiles as $mobile) {
                    $this->subscribeMobileToDiffusionList($distributionList, trim($mobile), $subscriptionSuccessfull);
                }

                if ($subscriptionSuccessfull) {
                    $this->flushSubscriptions($distributionList, 'SMS', $mobiles);
                }
            } else {
                $this->flash->addMessage('warning', 'Aucune liste de diffusion enregistr&eacute;e pour l\'identifiant ' . $distributionListId);
            }
        }
        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * Get distributionLists collection from HTTP request context
     *
     * @param object|array|null $params
     * @param ServerRequestInterface $request
     * @return array|Collection
     * @access private
     */
    private function getDistributionListsFromRequestContext(object|array|null $params, ServerRequestInterface $request): array|Collection {
        $distributionLists = [];
        // Mes communications page
        if ($this->user->getOrganizationName() == ArrayUtils::get($params, 'organization')) {
            $organization = $this->organizationRepository->findByDomainName($this->user->getOrganizationName());
            if (!empty($organization)) {
                $distributionLists = $organization->getDistributionLists();
            }
        } // Administration
        elseif ($this->user->getRank() == Rank::ADMINGN && $this->checkParams($request, $params) && !empty(ArrayUtils::get($params, 'organization'))) {
            $organization = $this->organizationRepository->findByDomainName(ArrayUtils::get($params, 'organization'));
            if (!empty($organization)) {
                $distributionLists = $organization->getDistributionLists();
            }
        } // even GN's administrators
        elseif ($this->user->getRank() == Rank::ADMINGN) {
            $distributionLists = $this->distributionListRepository->findAll();
        }
        return $distributionLists;
    }

    /**
     * Delete subscription from HTTP request context
     *
     * @param array $params
     * @param DistributionList|null $distributionList
     * @param EmailSubscription|null $emailSubscriber
     * @param MobileSubscription|null $mobileSubscriber
     * @return void
     * @throws Exception
     * @author Thierry CEREYON <t.cereyon@groupeonepoint.com>
     * @access private
     */
    private function deleteSubscription(array $params, ?DistributionList $distributionList, ?EmailSubscription $emailSubscriber, ?MobileSubscription $mobileSubscriber): void {
        if ($distributionList instanceof DistributionList) {
            if (!is_null($emailSubscriber)) {
                if ($this->subscriptionRepository->deleteEmail($emailSubscriber)) {
                    $this->flash->addMessage('success', 'Votre email a bien &eacute;t&eacute; supprim&eacute; de la liste de diffusion <em>' . $distributionList->getTitle() . '</em>');
                } else {
                    $this->flash->addMessage('error', 'Une erreur est survenue lors de la tentative de d&eacute;sabonnement de votre adresse email de la liste de diffusion <em>' . $distributionList->getTitle() . '</em>');
                }
            } elseif (!empty($params[0])) {
                $this->flash->addMessage('error', 'L\'adresse email saisie n\'existe pas dans la liste de diffusion <em>' . $distributionList->getTitle() . '</em>');
            }
            if (!is_null($mobileSubscriber)) {
                if ($this->subscriptionRepository->deleteMobile($mobileSubscriber)) {
                    $this->flash->addMessage('success', 'Votre num&eacute;ro a bien &eacute;t&eacute; supprim&eacute; de la liste de diffusion <em>' . $distributionList->getTitle() . '</em>');
                } else {
                    $this->flash->addMessage('error', 'Une erreur est survenue lors de la tentative de suppression de votre num&eacute;ro de mobile de la liste de diffusion <em>' . $distributionList->getTitle() . '</em>');
                }
            } elseif (!empty($params[1])) {
                $this->flash->addMessage('error', 'Le num&eacute;ro saisi n\'existe pas dans la liste de diffusion <em>' . $distributionList->getTitle() . '</em>');
            }
        } else {
            $this->flash->addMessage('error', 'La liste de diffusion n\'existe pas ou n\'a pas &eacute;t&eacute; retrouv&eacute;. Veuillez reessayer plus tard...</em>');
        }
    }

    /**
     * Subscribe an email to a diffusion list if it has not already been subscribed.
     *
     * @param DistributionList $distributionList Diffusion list object
     * @param string $email Email to subscribe
     * @param bool $subscriptionSuccessfull Subscription flag
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return void
     * @access private
     */
    private function subscribeEmailToDiffusionList(DistributionList $distributionList, string $email, bool &$subscriptionSuccessfull): void {
        if (is_null($this->subscriptionRepository->findEmailByDistributionList($email, $distributionList)) && !empty($email)) {
            $subscriberEmail = new EmailSubscription($email, $distributionList);
            $this->entityManager->persist($subscriberEmail);
            $subscriptionSuccessfull = true;
        } elseif (!empty($email)) {
            $this->flash->addMessage('info', 'L\'adresse email est d&eacute;j&agrave; entregistr&eacute;e pour cette liste de diffusion.');
        }
    }

    /**
     * Subscribe a mobile phone number to a diffusion list if it has not already been subscribed.
     *
     * @param DistributionList $distributionList Diffusion list object
     * @param string $mobile Mobile phone number to subscribe
     * @param bool $subscriptionSuccessfull Subscription flag
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return void
     * @access private
     */
    private function subscribeMobileToDiffusionList(DistributionList $distributionList, string $mobile, bool &$subscriptionSuccessfull): void {
        if (is_null($this->subscriptionRepository->findMobileByDistributionList($mobile, $distributionList)) && !empty($mobile)) {
            $phoneSubscriber = new MobileSubscription($mobile, $distributionList);
            $this->entityManager->persist($phoneSubscriber);
            $subscriptionSuccessfull = true;
        } elseif (!empty($mobile)) {
            $this->flash->addMessage('info', 'Le num&eacute;ro de t&eacute;l&eacute;phone est d&eacute;j&agrave; entregistr&eacute; pour cette liste de diffusion.');
        }
    }

    /**
     * Flush subscription and display correct flash message.
     *
     * @param DistributionList $distributionList Diffusion list object
     * @param string $type Subscription type (EMAIL or SMS)
     * @param array $contacts List of emails or mobile phone contacts
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return void
     * @access private
     */
    private function flushSubscriptions(DistributionList $distributionList, string $type, array $contacts) {
        if ($type == 'EMAIL') {
            if (sizeof($contacts) > 1) {
                $message = 'Les adresses emails ont bien &eacute;t&eacute; inscrites ';
            } else {
                $message = 'L\'adresse email a bien &eacute;t&eacute; inscrite ';
            }
        } elseif ($type = 'SMS') {
            if (sizeof($contacts) > 1) {
                $message = 'Les num&eacute;ros de t&eacute;l&eacute;phone mobile ont bien &eacute;t&eacute; inscrits ';
            } else {
                $message = 'Le num&eacute;ro de t&eacute;l&eacute;phone mobile a bien &eacute;t&eacute; inscrit ';
            }
        }
        $message .= '&agrave; la liste de diffusion ' . $distributionList->getTitle();

        try {
            $this->entityManager->flush();
            $this->flash->addMessage('success', $message);
        } catch (OptimisticLockException | ORMException $e) {
            $message = 'Une erreur est survenue lors de l\'enregistrement';
            if ($this->settings->get('env.debug')) {
                $message .= '<br /><em>' . $e->getMessage() . '</em>';
            }
            $this->flash->addMessage('error', $message);
        }
    }
}
