<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\Action;

use Multicanal\Controller\Controller;
use Psr\Container\ContainerInterface;
use Slim\Flash\Messages;

/**
 * ActionController interface to load common objects to all action controller
 *
 * @package Multicanal\Controller\Action
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class ActionController extends Controller {

    protected Messages $flash;

    public function __construct(ContainerInterface $container) {

        parent::__construct($container);

        // Flash messages
        $this->flash = $this->container->get(Messages::class);
    }
}
