<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\Action;

use League\OAuth2\Client\Provider\Facebook;
use Multicanal\App\Helper;
use Multicanal\Controller\Action\ActionController;
use Multicanal\Entity\SocialNetwork\Channel;
use Multicanal\Entity\SocialNetwork\FacebookChannel;
use Multicanal\Entity\SocialNetwork\InstagramChannel;
use Multicanal\Entity\SocialNetwork\TwitterChannel;
use Multicanal\Lib\ChannelPublisher\FacebookChannelPublisher;
use Multicanal\Repository\ChannelRepository;
use Multicanal\Utils\ArrayUtils;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Smolblog\OAuth2\Client\Provider\Twitter;

/**
 * Class ChannelActionController manager to manage channels
 *
 * @package Multicanal\Controller\Action
 * @author  Nathanael KHODL <consulting@khodl.com>
 */
class ChannelActionController extends ActionController {

    private ChannelRepository $channelRepository;

    public function __construct(ContainerInterface $container) {
        parent::__construct($container);
        $this->channelRepository = $container->get(ChannelRepository::class);
        $this->twig = $container->get('view');
    }

    /**
     * Delete a channel action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function deleteChannel(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {

        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $id = ArrayUtils::get($params, 'channel-id');
            $channel = $this->channelRepository->findById((int)$id);
            $channel->setIsArchived(true);
            if ($this->channelRepository->save($channel)) {
                $this->flash->addMessage('success', 'Le canal <em>' . $channel->getChannelName() . '</em> a bien &eacute;t&eacute; supprim&eacute;');
            } else {
                $this->flash->addMessage('error', 'Une erreur est survenue lors de la tentative de suppression du canal <em>' . $channel->getChannelName() . '</em>');
            }
        }

        return $response->withStatus(302)->withHeader('Location', '/collectivite');
    }

    /**
     * Manage authentication redirection processing for Twitter.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function redirectTwitter(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {

        $provider = new Twitter([
            'clientId'          => Helper::config('twitter.application_key'),
            'clientSecret'      => Helper::config('twitter.application_secret'),
            'redirectUri'       => Helper::config('app.domain') . '/channels/auth/twitter',
        ]);

        // Check if redirection is required
        $code = $request->getQueryParams()['code'] ?? null;
        $state = $request->getQueryParams()['state'] ?? null;
        if (!$code) {
            $authUrl = $provider->getAuthorizationUrl([
                'scope' => [
                    'users.read',
                    'tweet.read',
                    'tweet.write',
                    'offline.access',
                ],
            ]);

            $this->session->set('oauth2state', $provider->getState());
            $this->session->set('oauth2verifier', $provider->getPkceVerifier());

            return $response->withStatus(302)->withHeader('Location', $authUrl);
        } elseif (empty($state) || ($state !== $this->session->get('oauth2state'))) {
            $this->session->remove('oauth2state');
            throw new \LogicException('Authentification Twitter invalide');
        }

        // Authentication: get access token and save data
        $token = $provider->getAccessToken('authorization_code', [
            'code' => $code,
            'code_verifier' => $_SESSION['oauth2verifier'] ?? null,
        ]);

        $user = $provider->getResourceOwner($token);

        $twitterId = $user->getId();
        $twitterHandle = $user->toArray()['username'];

        $channel = new TwitterChannel($this->dbOrganization, $twitterId, $token->getRefreshToken(), $twitterHandle);

        return $this->successfullChannelResponse($channel, $response);
    }

    /**
     * Manage authentication redirection processing for Facebook.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function redirectFacebook(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {

        $provider = new Facebook([
            'clientId'          => Helper::config('facebook.application_key'),
            'clientSecret'      => Helper::config('facebook.application_secret'),
            'redirectUri'       => Helper::config('app.domain') . '/channels/auth/facebook',
            'graphApiVersion'   => 'v16.0',
        ]);

        $userToken = $this->session->get('facebookUserToken') ?? null;
        $pageId = $request->getQueryParams()['page'] ?? null;
        $code = $request->getQueryParams()['code'] ?? null;
        $state = $request->getQueryParams()['state'] ?? null;

        if ($pageId) {
            $pages = FacebookChannelPublisher::getUserPages($userToken);
            foreach ($pages as $page) {
                if ($page['id'] === $pageId) {
                    $channel = new FacebookChannel(
                        $this->dbOrganization,
                        $page['id'],
                        $page['access_token'],
                        $page['name']
                    );

                    if ($instagramData = $page['instagram_business_account'] ?? false) {
                        $instagramChannel = $this->getInstagramChannel($page['access_token'], $instagramData['id']);
                        if ($instagramChannel) {
                            $this->saveChannel($instagramChannel);
                        }
                    }

                    return $this->successfullChannelResponse($channel, $response);
                }
            }

            throw new \LogicException('Page Facebook invalide : ' . $pageId);
        } elseif ($userToken) {
            return $response->withStatus(302)->withHeader('Location', '/collectivite/' . $userToken);
        } elseif (!$code) {
            $authUrl = $provider->getAuthorizationUrl([
                'auth_type' => 'rerequest',
                'scope' => [
                    "pages_show_list",
                    "pages_manage_posts",
                    "pages_read_engagement",
                    "public_profile",
                    "pages_manage_metadata",
                    "business_management",
                    "instagram_content_publish",
                    "instagram_basic",
                ],
            ]);
            $this->session->set('oauth2state', $provider->getState());

            return $response->withStatus(302)->withHeader('Location', $authUrl);
        } elseif (empty($state) || ($state !== $this->session->get('oauth2state'))) {
            $this->session->remove('oauth2state');
            throw new \LogicException('Invalid Facebook state');
        }

        $token = $provider->getAccessToken('authorization_code', [
            'code' => $code,
        ]);

        $this->session->set('facebookUserToken', $token->getToken());

        return $response->withStatus(302)->withHeader('Location', '/channels/auth/facebook');
    }

    /**
     * Manage successfull social network authentication redirection response.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @author  Nathanael KHODL <consulting@khodl.com>
     * @return ResponseInterface
     * @access private
     */
    private function successfullChannelResponse(Channel $channel, ResponseInterface $response): ResponseInterface {
        $this->channelRepository->save($channel);
        $this->flash->addMessage('success', 'La canal <em>' . $channel->getChannelName() . '</em> a bien &eacute;t&eacute; ajout&eacute;');

        return $response->withStatus(302)->withHeader('Location', '/collectivite');
    }

    /**
     * Save channel processing.
     *
     * @param Channel $channel Channel object to save
     * @author  Nathanael KHODL <consulting@khodl.com>
     * @return void
     * @access private
     */
    private function saveChannel(Channel $channel): void {
        $this->channelRepository->save($channel);
        $this->flash->addMessage('success', 'La canal <em>' . $channel->getChannelName() . '</em> a bien &eacute;t&eacute; ajout&eacute;');
    }

    /**
     * Get Instagram channel from Facebook.
     *
     * @param string $facebookToken Facebook token
     * @param string $instagramId Instagram account identifier
     * @author  Nathanael KHODL <consulting@khodl.com>
     * @return InstagramChannel|null
     * @access private
     */
    private function getInstagramChannel(string $facebookToken, string $instagramId): ?InstagramChannel {

        $instagramData = FacebookChannelPublisher::getInstagramAccount($facebookToken, $instagramId);
        if (!$instagramData) {
            return null;
        }

        return new InstagramChannel($this->dbOrganization, $instagramId, $facebookToken, $instagramData['username']);
    }
}
