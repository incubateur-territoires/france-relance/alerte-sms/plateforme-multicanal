<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\Action;

use Multicanal\Controller\Action\ActionController;
use Multicanal\Entity\Category;
use Multicanal\Entity\DistributionList;
use Multicanal\Utils\ArrayUtils;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class CategoryActionController manager
 *
 * @package Multicanal\Controller\Action
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 *
 */
class CategoryActionController extends ActionController {

    /**
     * Add a new category action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function add(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url      = ArrayUtils::get($params, 'current-url');
            $name     = ArrayUtils::get($params, 'category-name');
            $category = new Category($name);
            $category->setCreator($this->dbUser);
            $category->setOrganization($this->dbOrganization);

            if ($this->categoryRepository->save($category)) {
                $this->flash->addMessage('success', 'La cat&eacute;gorie <em>' . $category->getName() . '</em> a bien &eacute;t&eacute; ajout&eacute;e');
            } else {
                $this->flash->addMessage('error', 'Une erreur est survenue lors de la tentative d\'ajout de la cat&eacute;gorie <em>' . $category->getName() . '</em>');
            }
        }

        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * Update an existing category action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function update(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url  = ArrayUtils::get($params, 'current-url');
            $id   = ArrayUtils::get($params, 'category-id');
            $name = ArrayUtils::get($params, 'category-name');

            $category = $this->categoryRepository->findById((int) $id);
            if ($category instanceof Category) {
                $category->setName($name);
            }
            if ($this->categoryRepository->save($category) && $category instanceof Category) {
                $this->flash->addMessage('success', 'La cat&eacute;gorie <em>' . $category->getName() . '</em> a bien &eacute;t&eacute; renomm&eacute;e');
            } else {
                $this->flash->addMessage('error', 'Une erreur est survenue lors de la tentative de modification de la cat&eacute;gorie <em>' . $category->getName() . '</em>');
            }
        }

        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * Delete an existing category action.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function delete(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url = ArrayUtils::get($params, 'current-url');
            $id  = ArrayUtils::get($params, 'category-id');

            $category = $this->categoryRepository->findById((int) $id);
            $distributionLists = $category->getDistributionLists();
            $category->initializeDistributionListsCollection();
            /** @var DistributionList $distributionList */
            foreach ($distributionLists as $distributionList) {
                $distributionList->setCategory(null);
                $this->entityManager->persist($distributionList);
                $this->entityManager->flush($distributionList);
            }
            if (sizeOf($category->getContents()) > 0 || sizeOf($category->getDistributionLists())) {
                $this->flash->addMessage('warning', 'Impossible de supprimer la catégorie <em>' . $category->getName() . '</em> puisqu\'elle contient des contenus ou des listes de diffusion');
            } else {
                if ($category instanceof Category && $this->categoryRepository->delete($category)) {
                    $this->flash->addMessage('success', 'La cat&eacute;gorie <em>' . $category->getName() . '</em> a bien &eacute;t&eacute; supprim&eacute;e');
                } else {
                    $this->flash->addMessage('error', 'Une erreur est survenue lors de la tentative de suppression de la cat&eacute;gorie <em>' . $category->getName() . '</em>');
                }
            }
        }

        return $response->withStatus(302)->withHeader('Location', $url);
    }
}
