<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\Action;

use Multicanal\App\Helper;
use Multicanal\Controller\Action\ActionController;
use Multicanal\Entity\Content;
use Multicanal\Entity\Exception\MulticanalException;
use Multicanal\Entity\MulticanalFile;
use Multicanal\Utils\ArrayUtils;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * InformationActionController class to manage information
 *
 * @package Multicanal\Controller\Action
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class InformationActionController extends ActionController {
    public function __construct(ContainerInterface $container) {
        parent::__construct($container);
        $this->twig = $container->get('view');
    }

    /**
     * Transform an email to a new content in a specific category.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function transform(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url         = ArrayUtils::get($params, 'current-url');
            $emailNumber = ArrayUtils::get($params, 'email-number');
            $categoryId  = ArrayUtils::get($params, 'information-category');

            $emailManager = $this->getMailboxManager();
            $content = new Content(
                (string) $emailManager->getSubject($emailNumber),
                (string) $emailManager->getTransformedBody($emailNumber)
            );
            $content->setCreator($this->dbUser);
            $content->setStatus('DRAFT');
            $content->setPriority(1);
            if (!is_null($categoryId) && $categoryId > 0) {
                $content->setCategory($this->categoryRepository->findById((int) $categoryId));
            }
            foreach ($this->mailAttachmentHandler($emailNumber) as $multicanalFile) {
                $multicanalFile->setContent($content);
                $content->addMedia($multicanalFile);
            }

            if ($this->contentRepository->save($content)) {
                $this->flash->addMessage('success', 'L\'email <em>' . $content->getTitle() . '</em> a bien &eacute;t&eacute; transform&eacute; en contenu');
                $url = '/contenus/modifier/' . $content->getId();
            } else {
                $this->flash->addMessage('error', 'Une erreur est survenue lors de la tentative de transformation de l\'email <em>' . $content->getTitle() . '</em>');
            }
        }
        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * Concat an email to an existing content.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function concat(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        if ($this->checkParams($request, $params)) {
            $url         = ArrayUtils::get($params, 'current-url');
            $emailNumber = ArrayUtils::get($params, 'email-number');
            $contentId   = ArrayUtils::get($params, 'information-content');

            $content = $this->contentRepository->findById((int) $contentId);
            if ($content instanceof Content) {
                $emailManager = $this->getMailboxManager();
                $text = (string) $emailManager->getTransformedBody($emailNumber);
                $content->setText($content->getText() . '<br />' . $text);
                // Attachments
                foreach ($this->mailAttachmentHandler($emailNumber) as $multicanalFile) {
                    $multicanalFile->setContent($content);
                    $content->addMedia($multicanalFile);
                }

                if ($this->contentRepository->save($content)) {
                    $this->flash->addMessage('success', 'L\'email a bien &eacute;t&eacute; ajout&eacute; au contenu <em>' . $content->getTitle() . '</em>');
                    $url = '/contenus/modifier/' . $content->getId();
                } else {
                    $this->flash->addMessage('error', 'Une erreur est survenue lors de la tentative de concat&eacute;nation de l\'email au contenu <em>' . $content->getTitle() . '</em>');
                }
            } else {
                $this->flash->addMessage('warning', 'Impossible de r&eacute;cup&eacute;rer le contenu');
            }
        }
        return $response->withStatus(302)->withHeader('Location', $url);
    }

    /**
     * Load a mailbox email list in JSON format in an AJAX HTTP request.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function load(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $params = $request->getParsedBody();
        $data   = ['total' => 0, 'rows' => []];
        if ($this->checkParams($request, $params)) {
            $emailManager = $this->getMailboxManager();
            $mails       = $emailManager->getEmailList();
            $data        = ['total' => count($mails), 'rows' => $mails];
        }
        $response->getBody()->write(json_encode($data, JSON_THROW_ON_ERROR));

        return $response->withHeader('Content-Type', self::JSON);
    }

    /**
     * Load email content in JSON format in an AJAX HTTP request.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function details(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $emailNumber = $request->getAttribute('emailNumber');
        $emailData = $this->getMailboxManager()->getEmailData($emailNumber);

        return $this->twig->render($response, 'information-detail.html', [
            'email' => $emailData
        ]);
    }

    public function attachment(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $emailNumber = $request->getAttribute('emailNumber');
        $attachmentId = $request->getAttribute('attachmentId');

        $emailManager = $this->getMailboxManager();

        $attachment = $emailManager->getAttachment($emailNumber, $attachmentId);
        if (!$attachment) {
            throw new MulticanalException('Attachment not found');
        }

        $response->getBody()->write($attachment->getContents());

        return $response->withHeader('Content-Type', $attachment->mimeType);
    }

    /**
     * Save attachments for a specific email.
     *
     * @param int $emailNumber Email number
     * @return array
     * @throws ContainerExceptionInterface|NotFoundExceptionInterface
     * @access private
     */
    private function mailAttachmentHandler(int $emailNumber): array {
        $emailManager       = $this->getMailboxManager();
        $multicanalFiles    = [];
        if ($emailManager->getNbOfAttachments($emailNumber) > 0) {
            $attachments = $emailManager->getAttachments($emailNumber);
            foreach ($attachments as $attachment) {
                $directory = Helper::getUploadDirectory() . '/' . $this->user->getDomainName() . '/contents';
                $emailManager->saveAttachment($attachment, $directory);
                $multicanalFiles[] = new MulticanalFile($attachment->name, $directory);
            }
        }

        return $multicanalFiles;
    }
}
