<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\Service;

use Doctrine\ORM\Exception\ORMException;
use Multicanal\App\Helper;
use Multicanal\Controller\Action\ActionController;
use Multicanal\Entity\Enum\Rank;
use Multicanal\Entity\Organization;
use Multicanal\Utils\ArrayUtils;
use Psr\Container\ContainerInterface;

/**
 * Class OrganizationActionController manager
 *
 * @package Multicanal\Controller\Action
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
class OrganizationService extends ActionController {

    protected string $directory;

    public function __construct(ContainerInterface $container) {
        parent::__construct($container);
        $this->directory = Helper::getUploadDirectory();
    }

    /**
     * @param object|array|null $params
     * @return Organization
     * @throws ORMException
     */
    public function getOrganization(object|array|null $params): Organization {
        // Define organization name
        if ($this->user->getRank() == Rank::ADMINGN) {
            $organizationDomainName = ArrayUtils::get($params, 'domain-name');
        } else {
            $organizationDomainName = $this->user->getOrganizationName();
        }
        return $this->organizationRepository->createIfNotExist($organizationDomainName);
    }
}
