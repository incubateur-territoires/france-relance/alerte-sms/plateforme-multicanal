<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\Service;

use Doctrine\ORM\Exception\ORMException;
use Multicanal\Controller\Controller;
use Multicanal\Entity\Category;
use Multicanal\Entity\DistributionList;
use Multicanal\Entity\Enum\Rank;
use Multicanal\Entity\Exception\MulticanalException;
use Multicanal\Entity\MulticanalFile;
use Multicanal\Utils\ArrayUtils;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Interface Controller for loading common objects.
 *
 * @package Multicanal\Controller
 * @author  Thierry CEREYON <t.cereyon@groupeonepoint.com>
 */
class DistributionListService {

    /**
     * Service to add a new distribution list.
     *
     * @param mixed $params Mixed params containing domain name
     * @param mixed $categoryId Category id
     * @param mixed $title Distribution list title
     * @param mixed $description Distribution list description
     * @param ServerRequestInterface $request HTTP request from Slim framework
     * @return DistributionList
     * @throws ORMException|MulticanalException
     */
    public function addDistribitionListService(mixed $params, mixed $categoryId, mixed $title, mixed $description, ServerRequestInterface $request): DistributionList {
        if ($this->user->getRank() == Rank::ADMINGN && !is_null(ArrayUtils::get($params, 'domain-name'))) {
            $organizationName = ArrayUtils::get($params, 'domain-name');
        } else {
            $organizationName = $this->user->getOrganizationName();
        }
        $organization = $this->organizationRepository->createIfNotExist($organizationName);
        $category     = $this->categoryRepository->findById((int) $categoryId);


        $distributionList = new DistributionList($title, $description, $organization, $category);
        $distributionList->setCreator($this->dbUser);

        $uploadedFiles  = $request->getUploadedFiles();
        $imageFile      = $uploadedFiles['distribution-image'];
        $multicanalFile = $this->getMulticanalFile($imageFile, $distributionList->getTitle(), 'distribution-lists');
        if ($multicanalFile instanceof MulticanalFile) {
            $distributionList->setImage($multicanalFile);
        }

        return $distributionList;
    }

    /**
     * Service to update a distribution list.
     *
     * @param DistributionList $distributionList Distribution list to update
     * @param mixed $title New distribution list title
     * @param mixed $description New distribution list description
     * @param Category|null $category Distribution list category
     * @param ServerRequestInterface $request HTTP request from Slim framework
     * @return void
     * @throws MulticanalException
     */
    public function updateDistributionListService(DistributionList &$distributionList, mixed $title, mixed $description, Category|null $category, ServerRequestInterface $request): void {
        $distributionList->update($title, $description, $category);
        $uploadedFiles  = $request->getUploadedFiles();
        $imageFile      = $uploadedFiles['distribution-image'];
        $multicanalFile = $this->getMulticanalFile($imageFile, $distributionList->getTitle(), 'distribution-lists');
        if ($multicanalFile instanceof MulticanalFile) {
            $distributionList->setImage($multicanalFile);
        }
    }
}
