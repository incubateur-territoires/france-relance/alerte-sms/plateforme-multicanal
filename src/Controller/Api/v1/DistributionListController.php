<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\Api\v1;

use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Exception;
use JsonException;
use Multicanal\Controller\Service\DistributionListService;
use Multicanal\Entity\DistributionList;
use Multicanal\Entity\EmailSubscription;
use Multicanal\Entity\Exception\MulticanalException;
use Multicanal\Entity\MobileSubscription;
use Multicanal\Entity\Organization;
use Multicanal\Repository\SubscriptionRepository;
use Multicanal\Utils\APIUtils;
use Multicanal\Utils\ArrayUtils;
use Multicanal\Utils\StringUtils;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpInternalServerErrorException;
use Slim\Exception\HttpNotFoundException;

/**
 * DistributionList controller class to get, list, create, update or delete distributionList.
 *
 * @package Multicanal\Controller\Api\v1
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class DistributionListController extends DistributionListService {

    private const DISTRIBUTIONLIST_ID_MANDATORY = 'L\'identifiant de la liste de diffusion est obligatoire';

    private const DISTRIBUTIONLIST_ID_MISSING = 'Liste de diffusion inexistante pour l\'identifiant ';

    private const INTERNAL_ERROR = 'Une erreur interne s\'est produite lors du traitement de votre requête.';

    private SubscriptionRepository $subscriptionRepository;

    public function __construct(ContainerInterface $container) {
        parent::__construct($container);
        $this->subscriptionRepository = $container->get(SubscriptionRepository::class);
    }

    /**
     * @OA\Get(
     *     path="/distributionList/{distributionListId}",
     *     tags={"DistributionList"},
     *     summary="Récupération d'une liste de diffusion.",
     *     description="Récupération d'une liste de diffusion par son identifiant.",
     *     operationId="getDistributionListById",
     *     @OA\Parameter(
     *         name="distributionListId",
     *         in="path",
     *         description="Identifiant de la liste de diffusion à récupérer",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Succès, liste de distribution récupérée.",
     *         @OA\JsonContent(ref="#/components/schemas/DistributionList")
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Identifiant de la liste de diffusion invalide."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Informations d'authentification invalides."
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Vous n'êtes pas autorisé à accéder à cette ressource."
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Liste de diffusion non trouvée."
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Une erreur interne s'est produite lors du traitement de votre requête."
     *     ),
     *     security={
     *         {"api_key": {}}
     *     }
     * )
     * @throws HttpBadRequestException|HttpNotFoundException|HttpInternalServerErrorException|JsonException
     */
    public function getDistributionListById(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        $startTime  = $request->getAttribute('startTime');
        try {
            $id = ArrayUtils::get($args, 'distributionListId');
            if (!is_numeric($id) || (int) $id === 0) {
                throw new HttpBadRequestException($request, 'Identifiant de liste de diffusion invalide');
            }
            $distributionList = $this->distributionListRepository->findById((int) $id);
            if (is_null($distributionList)) {
                throw new HttpNotFoundException($request, self::DISTRIBUTIONLIST_ID_MISSING . $id);
            }
            return APIUtils::writeResponse($response, 200, 'Liste de diffusion trouvée', $startTime, true, json_encode($distributionList->__toApi($request), JSON_THROW_ON_ERROR));
        } catch (MulticanalException $e) {
            throw new HttpInternalServerErrorException($request, self::INTERNAL_ERROR, $e);
        }
    }

    /**
     * @OA\Get(
     *     path="/distributionLists",
     *     tags={"DistributionList"},
     *     summary="Récupération des listes de diffusion",
     *     description="Récupération de l'ensemble des listes de diffusion pour l'intégralité des collectivités",
     *     operationId="getDistributionLists",
     *     @OA\Parameter(
     *         name="start",
     *         in="query",
     *         description="Rang du premier élément demandé dans la réponse, défaut 0.",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             default=0
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="size",
     *         in="query",
     *         description="Nombre d'éléments demandés dans la réponse, défaut 100.",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             default=100
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Intégralité des listes de diffusion récupérée.",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/DistributionList")
     *         )
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="Aucune liste de diffusion récupérée."
     *     ),
     *     @OA\Response(
     *         response=206,
     *         description="Partie des listes de diffusion récupérée.",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/DistributionList")
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Identifiant de la liste de diffusion invalide."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Informations d'authentification invalides."
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Vous n'êtes pas autorisé à accéder à cette ressource."
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Une erreur interne s'est produite lors du traitement de votre requête."
     *     ),
     *     security={
     *         {"api_key": {}}
     *     }
     * )
     */
    public function getDistributionLists(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        $start              = (!is_null(ArrayUtils::get($args, 'start'))) ? ArrayUtils::get($args, 'start') : 0;
        $size               = (!is_null(ArrayUtils::get($args, 'size'))) ? ArrayUtils::get($args, 'size') : 100;

        return $this->findDistributionLists([], $size, $start, $request, $response);
    }

    /**
     * @OA\Get(
     *     path="/distributionLists/{organization}",
     *     tags={"DistributionList"},
     *     summary="Récupération des listes de diffusion pour une collectivité.",
     *     description="Récupération de l'ensemble des listes de diffusion pour une collectivité spécifiée par son nom de domaine.",
     *     operationId="getDistributionListsByOrganization",
     *     @OA\Parameter(
     *         name="organization",
     *         in="path",
     *         description="Nom de domain de l'organization",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="start",
     *         in="query",
     *         description="Rang du premier élément demandé dans la réponse, défaut 0.",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             default=0
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="size",
     *         in="query",
     *         description="Nombre d'éléments demandés dans la réponse, défaut 100.",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             default=100
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Intégralité des listes de diffusion récupérée.",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/DistributionList")
     *         )
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="Aucune liste de diffusion récupérée."
     *     ),
     *     @OA\Response(
     *         response=206,
     *         description="Partie des listes de diffusion récupérée.",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/DistributionList")
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Identifiant de la liste de diffusion invalide."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Informations d'authentification invalides."
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Vous n'êtes pas autorisé à accéder à cette ressource."
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Organisation inexistante."
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Une erreur interne s'est produite lors du traitement de votre requête."
     *     ),
     *     security={
     *         {"api_key": {}}
     *     }
     * )
     * @throws HttpBadRequestException|HttpInternalServerErrorException|HttpNotFoundException
     */
    public function getDistributionListsByOrganization(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        $start              = (!is_null(ArrayUtils::get($args, 'start'))) ? ArrayUtils::get($args, 'start') : 0;
        $size               = (!is_null(ArrayUtils::get($args, 'size'))) ? ArrayUtils::get($args, 'size') : 100;
        $organizationDomain = ArrayUtils::get($args, 'organization');
        $criteria           = [];

        if (!is_null($organizationDomain)) {
            $organization = $this->organizationRepository->findByDomainName($organizationDomain);
            if ($organization instanceof Organization) {
                $criteria = ['organization' => $organization];
            } else {
                throw new HttpNotFoundException($request, 'Collectivité inexistante');
            }
        }

        return $this->findDistributionLists($criteria, $size, $start, $request, $response);
    }

    /**
     * @OA\Post(
     *     path="/distributionList/add/{organization}",
     *     tags={"DistributionList"},
     *     summary="Création d'une liste de diffusion",
     *     description="Création d'une liste de diffusion au sein d'une organisation.",
     *     operationId="addDistributionList",
     *     @OA\Parameter(
     *         name="organization",
     *         in="path",
     *         description="Identifiant de l'organization.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 allOf={
     *                     @OA\Schema(ref="#/components/schemas/DistributionList"),
     *                     @OA\Schema(
     *                         @OA\Property(
     *                             description="Item image",
     *                             property="item_image",
     *                             type="string",
     *                             format="binary"
     *                         )
     *                     )
     *                 }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Succès, liste de diffusion créée.",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResponse")
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Informations de la liste de diffusion invalides."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Informations d'authentification invalides."
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Vous n'êtes pas autorisé à accéder à cette ressource."
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Organization inexistante."
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Une erreur interne s'est produite lors du traitement de votre requête."
     *     ),
     *     security={
     *         {"api_key": {}}
     *     }
     * )
     * @throws HttpBadRequestException|HttpInternalServerErrorException
     */
    public function addDistributionList(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        $startTime  = $request->getAttribute('startTime');
        try {
            $organizationDomain = ArrayUtils::get($args, 'organization');
            if (!empty($organizationDomain)) {
                $params = $request->getParsedBody();
                if ($this->checkParams($request, $params)) {
                    $title       = ArrayUtils::get($params, 'title');
                    $description = ArrayUtils::get($params, 'description');
                    $categoryId  = ArrayUtils::get($params, 'categoryId');

                    $distributionList = $this->addDistribitionListService($params, $categoryId, $title, $description, $request);

                    if (!$this->distributionListRepository->save($distributionList)) {
                        throw new HttpInternalServerErrorException($request, self::INTERNAL_ERROR);
                    }
                    $distributionList->updateShortUrlQrCodeFile();
                    // Save distribution list to persist shortened url and QR code
                    if (!$this->distributionListRepository->save($distributionList)) {
                        throw new HttpInternalServerErrorException($request, self::INTERNAL_ERROR);
                    }
                }
            } else {
                throw new HttpBadRequestException($request, 'Le nom de domaine de la collectivité est obligatoire');
            }
        } catch (Exception $e) {
            throw new HttpInternalServerErrorException($request, self::INTERNAL_ERROR, $e);
        }

        return APIUtils::writeResponse($response, 200, 'Liste de diffusion ajoutée', $startTime, true, json_encode($distributionList->__toApi($request), JSON_THROW_ON_ERROR));
    }

    /**
     * @OA\Put(
     *     path="/distributionList/{distributionListId}",
     *     tags={"DistributionList"},
     *     summary="Mise à jour d'une liste de diffusion",
     *     description="Mise à jour des informations d'une liste de diffusion au sein d'une organisation.",
     *     operationId="updateDistributionList",
     *     @OA\Parameter(
     *         name="distributionListId",
     *         in="path",
     *         description="Identifiant de la liste de diffusion à récupérer",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 allOf={
     *                     @OA\Schema(ref="#/components/schemas/DistributionList"),
     *                     @OA\Schema(
     *                         @OA\Property(
     *                             description="Item image",
     *                             property="item_image",
     *                             type="string", format="binary"
     *                         )
     *                     )
     *                 }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Succès, liste de diffusion mise à jour.",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResponse")
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Informations de la liste de diffusion invalides."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Informations d'authentification invalides."
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Vous n'êtes pas autorisé à accéder à cette ressource."
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Liste de diffusion inexistante."
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Une erreur interne s'est produite lors du traitement de votre requête."
     *     ),
     *     security={
     *         {"api_key": {}}
     *     }
     * )
     * @throws HttpBadRequestException
     */
    public function updateDistributionList(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        $startTime  = $request->getAttribute('startTime');
        $distributionListParams = $this->getJsonDecodeParams($request);
        try {
            $id     = ArrayUtils::get($args, 'distributionListId');
            if (StringUtils::checkNumericIdentifier($id)) {
                $params = $request->getParsedBody();
                if ($this->checkParams($request, $params)) {
                    $distributionList = $this->distributionListRepository->findById((int) $id);
                    if (empty($distributionList) || !($distributionList instanceof DistributionList)) {
                        throw new HttpNotFoundException($request, self::DISTRIBUTIONLIST_ID_MISSING . $id);
                    }

                    $this->saveDistributionList($request, $distributionList, $distributionListParams);

                    return APIUtils::writeResponse(
                        $response,
                        200,
                        'Liste de diffusion mise à jour',
                        $startTime,
                        true,
                        json_encode($distributionList->__toApi($request), JSON_THROW_ON_ERROR)
                    );
                }
            } else {
                throw new HttpBadRequestException($request, self::DISTRIBUTIONLIST_ID_MANDATORY);
            }
        } catch (Exception $e) {
            throw new HttpInternalServerErrorException($request, self::INTERNAL_ERROR, $e);
        }
    }

    /**
     * @OA\Delete(
     *     path="/distributionList/{distributionListId}",
     *     tags={"DistributionList"},
     *     summary="Suppression d'une liste de diffusion",
     *     description="Supprime une liste de diffusion dont l'id a été passé en paramètre.",
     *     operationId="deleteDistributionList",
     *     @OA\Parameter(
     *         name="distributionListId",
     *         in="path",
     *         description="L'identifiant de la liste de diffusion à supprimer",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Succès, liste de diffusion supprimée.",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResponse")
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Identifiant de la liste de diffusion invalide"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Informations d'authentification invalides."
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Vous n'êtes pas autorisé à accéder à cette ressource."
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Liste de diffusion non trouvée"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Une erreur interne s'est produite lors du traitement de votre requête."
     *     ),
     *     security={
     *         {"api_key": {}}
     *     }
     * )
     * @throws Exception
     */
    public function deleteDistributionList(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        $startTime = $request->getAttribute('startTime');
        try {
            $id = ArrayUtils::get($args, 'distributionListId');
            if (StringUtils::checkNumericIdentifier($id)) {
                $distributionList = $this->distributionListRepository->findById((int) $id);
                if (empty($distributionList) || !($distributionList instanceof DistributionList)) {
                    throw new HttpNotFoundException($request, self::DISTRIBUTIONLIST_ID_MISSING . $id);
                }

                $distributionList = $this->distributionListRepository->findById((int) $id);
                if (!$this->distributionListRepository->delete($distributionList)) {
                    throw new HttpInternalServerErrorException($request, 'Une erreur est survenue lors de la tentative de suppression de la liste de diffusion ' . $distributionList->getTitle());
                }

                return APIUtils::writeResponse($response, 200, 'Liste de diffusion supprimée', $startTime, true, null);
            } else {
                throw new HttpBadRequestException($request, self::DISTRIBUTIONLIST_ID_MANDATORY);
            }
        } catch (Exception $e) {
            throw new HttpInternalServerErrorException($request, self::INTERNAL_ERROR, $e);
        }
    }

    /**
     * @OA\Put(
     *     path="/distributionList/subscribe/{distributionListId}",
     *     tags={"DistributionList"},
     *     summary="Inscription à une liste de diffusion",
     *     description="Inscription à une liste de diffusion de messages.",
     *     operationId="subscribeToDistributionList",
     *     @OA\Parameter(
     *         name="distributionListId",
     *         in="path",
     *         description="Identifiant de la liste de diffusion.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                   description="Spécification du modèle des moyens de communication d'une liste de diffusion.",
     *                   title="Moyen de communication d'une liste de diffusion",
     *                   @OA\Property(
     *                        property="email",
     *                        description="Adresse email pour la souscription à la réception de message par email.",
     *                        type="string"
     *                   ),
     *                   @OA\Property(
     *                        property="mobile",
     *                        description="Numéro de téléphone mobile pour la souscription à la réception de message par SMS.",
     *                        oneOf={
     *                             @OA\Schema(type="string"),
     *                             @OA\Schema(type="integer")
     *                        }
     *                   )
     *              )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Succès, inscription effectuée.",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResponse")
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Moyens de communication invalides."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Informations d'authentification invalides."
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Vous n'êtes pas autorisé à accéder à cette ressource."
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Liste de diffusion inexistante."
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Une erreur interne s'est produite lors du traitement de votre requête."
     *     ),
     *     security={
     *         {"api_key": {}}
     *     }
     * )
     * @throws HttpNotFoundException|HttpBadRequestException|HttpInternalServerErrorException
     */
    public function subscribeToDistributionList(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        $startTime = $request->getAttribute('startTime');
        $distributionListParams = $this->getJsonDecodeParams($request);
        $id                     = ArrayUtils::get($args, 'distributionListId');
        if (StringUtils::checkNumericIdentifier($id)) {
            if (!empty($distributionListParams)) {
                $distributionList = $this->distributionListRepository->findById((int) $id);
                if (empty($distributionList) || !($distributionList instanceof DistributionList)) {
                    throw new HttpNotFoundException($request, self::DISTRIBUTIONLIST_ID_MISSING . $id);
                }

                $this->subscribeEmail($distributionList, $distributionListParams);

                $this->subscribeMobile($distributionList, $distributionListParams);

                try {
                    $this->entityManager->flush();
                } catch (OptimisticLockException | ORMException $e) {
                    throw new HttpInternalServerErrorException($request, self::INTERNAL_ERROR, $e);
                }

                return APIUtils::writeResponse($response, 200, 'Inscription à la liste de diffusion effectuée', $startTime);
            }
        } else {
            throw new HttpBadRequestException($request, self::DISTRIBUTIONLIST_ID_MANDATORY);
        }
    }

    /**
     * @OA\Put(
     *     path="/distributionList/unsubscribe/{distributionListId}",
     *     tags={"DistributionList"},
     *     summary="Désinscription d'une liste de diffusion",
     *     description="Désinscription d'une liste de diffusion de messages.",
     *     operationId="unsubscribeToDistributionList",
     *     @OA\Parameter(
     *         name="distributionListId",
     *         in="path",
     *         description="Identifiant de la liste de diffusion.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                   description="Spécification du modèle des moyens de communication d'une liste de diffusion.",
     *                   title="Moyen de communication d'une liste de diffusion",
     *                   @OA\Property(
     *                        property="email",
     *                        description="Adresse email pour la souscription à la réception de message par email.",
     *                        type="string"
     *                   ),
     *                   @OA\Property(
     *                        property="mobile",
     *                        description="Numéro de téléphone mobile pour la souscription à la réception de message par SMS.",
     *                        oneOf={
     *                             @OA\Schema(type="string"),
     *                             @OA\Schema(type="integer")
     *                        }
     *                   )
     *              )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Succès, désinscription effectuée.",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResponse")
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Canal de communication invalide."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Informations d'authentification invalides."
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Vous n'êtes pas autorisé à accéder à cette ressource."
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Liste de diffusion inexistante."
     *     ),
     *     @OA\Response(
     *         response=406,
     *         description="Numéro de téléphone ou Email non trouver."
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Une erreur interne s'est produite lors du traitement de votre requête."
     *     ),
     *     security={
     *         {"api_key": {}}
     *     }
     * )
     * @throws HttpNotFoundException|HttpBadRequestException|HttpInternalServerErrorException
     */
    public function unsubscribeToDistributionList(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        $startTime = $request->getAttribute('startTime');
        $distributionListParams = $this->getJsonDecodeParams($request);
        $id                     = ArrayUtils::get($args, 'distributionListId');
        if (StringUtils::checkNumericIdentifier($id)) {
            if (!empty($distributionListParams)) {
                $distributionList = $this->distributionListRepository->findById((int) $id);
                if (empty($distributionList) || !($distributionList instanceof DistributionList)) {
                    throw new HttpNotFoundException($request, self::DISTRIBUTIONLIST_ID_MISSING . $id);
                }

                $this->unsubscribeEmail($request, $distributionList, $distributionListParams);
                $this->unsubscribeMobile($request, $distributionList, $distributionListParams);
            }
        } else {
            throw new HttpBadRequestException($request, self::DISTRIBUTIONLIST_ID_MANDATORY);
        }

        return APIUtils::writeResponse($response, 200, 'Désinscription à la liste de diffusion effectuée', $startTime);
    }

    /**
     * Get distribution list from API request.
     *
     * @param array $criteria Search criteria
     * @param mixed $size Search size
     * @param mixed $start Search offset
     * @param ServerRequestInterface $request HTTP request from Slim framework
     * @param ResponseInterface $response HTTP response from Slim framework
     * @return ResponseInterface
     * @throws HttpNotFoundException|HttpInternalServerErrorException
     * @access private
     */
    private function findDistributionLists(array $criteria, mixed $size, mixed $start, ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $startTime  = $request->getAttribute('startTime');
        $distributionListsFromDb = $this->distributionListRepository->findBySize($criteria, $size, $start);
        if (empty($distributionListsFromDb)) {
            throw new HttpNotFoundException($request, 'Aucune liste de diffusion enregistrée en base de données');
        }

        $distributionLists = [];
        /** @var DistributionList $distributionList */
        foreach ($distributionListsFromDb as $distributionList) {
            $distributionLists[] = $distributionList->__toApi($request);
        }

        return APIUtils::writeResponse($response, 200, count($distributionLists) . ' liste(s) de diffusion retournée(s)', $startTime, true, json_encode($distributionLists, JSON_THROW_ON_ERROR));
    }

    /**
     * Subscribe email in distribution list.
     * @param DistributionList $distributionList Distribution list into subscribe email
     * @param mixed $parameters Email parameters from request
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return void
     * @access private
     */
    private function subscribeEmail(DistributionList $distributionList, mixed $parameters): void {
        $email  = (property_exists($parameters, 'email')) ? $parameters->email : '';
        if (empty($this->subscriptionRepository->findEmailByDistributionList($email, $distributionList)) && !empty($email)) {
            $emailSubscriber = new EmailSubscription($email, $distributionList);
            $this->entityManager->persist($emailSubscriber);
        }
    }

    /**
     * Unsubscribe email from distribution list.
     * @param ServerRequestInterface $request HTTP request from Slim framework
     * @param DistributionList $distributionList Distribution list into unsubscribe email
     * @param mixed $parameters Email parameters from request
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return void
     * @access private
     */
    private function unsubscribeEmail(ServerRequestInterface $request, DistributionList $distributionList, mixed $parameters): void {
        $email  = (property_exists($parameters, 'email')) ? $parameters->email : '';
        $emailSubscriber = $this->subscriptionRepository->findEmailByDistributionList($email, $distributionList);
        if (!empty($emailSubscriber) && !$this->subscriptionRepository->deleteEmail($emailSubscriber)) {
            throw new HttpInternalServerErrorException($request, 'Une erreur est survenue lors de la tentative de désinscription de l\'email ' . $email . 'de la liste de diffusion ' . $distributionList->getTitle());
        }
    }

    /**
     * Subscribe mobile phone number in distribution list.
     * @param DistributionList $distributionList Distribution list into subscribe mobile
     * @param mixed $parameters Mobile parameters from request
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return void
     * @access private
     */
    private function subscribeMobile(DistributionList $distributionList, mixed $parameters): void {
        $mobile = (property_exists($parameters, 'mobile')) ? $parameters->mobile : '';
        if (empty($this->subscriptionRepository->findMobileByDistributionList($mobile, $distributionList)) && !empty($mobile)) {
            $phoneSubscriber = new MobileSubscription($mobile, $distributionList);
            $this->entityManager->persist($phoneSubscriber);
        }
    }

    /**
     * Unsubscribe mobile from distribution list.
     * @param ServerRequestInterface $request HTTP request from Slim framework
     * @param DistributionList $distributionList Distribution list into unsubscribe mobile
     * @param mixed $parameters Mobile parameters from request
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return void
     * @access private
     */
    private function unsubscribeMobile(ServerRequestInterface $request, DistributionList $distributionList, mixed $parameters): void {
        $mobile = (property_exists($parameters, 'mobile')) ? $parameters->mobile : '';
        $mobileSubscriber = $this->subscriptionRepository->findMobileByDistributionList($mobile, $distributionList);
        if (!empty($mobileSubscriber) && !$this->subscriptionRepository->deleteMobile($mobileSubscriber)) {
            throw new HttpInternalServerErrorException($request, 'Une erreur est survenue lors de la tentative de désinscription du numéro ' . $mobile . 'de la liste de diffusion ' . $distributionList->getTitle());
        }
    }

    /**
     * Save distribution list from API update request.
     * @param ServerRequestInterface $request Request from Slim framework
     * @param DistributionList $distributionList Distribution list to update
     * @param mixed $parameters Parameters from request
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return void
     * @access private
     */
    private function saveDistributionList(ServerRequestInterface $request, DistributionList $distributionList, mixed $parameters): void {
        if (isset($parameters['organization']['id'])) {
            $organization = $this->organizationRepository->findById($parameters['organization']['id']);
        } elseif ($parameters['organization']['domain']) {
            $organization = $this->organizationRepository->findById($parameters['organization']['domain']);
        } else {
            $organization = $distributionList->getOrganization();
        }

        if ($organization instanceof Organization) {
            $distributionList->setOrganization($organization);
        }

        $title       = (string) $parameters['title'];
        $description = (string) $parameters['description'];

        $category = null;
        if (isset($parameters['category']['id'])) {
            $category = $this->categoryRepository->findById($parameters['category']['id']);
        } elseif (
            isset($parameters['category'])
            && !empty((string) $parameters['category'])
        ) {
            $category = $this->categoryRepository->findByName($parameters['category']['name']);
        }

        $this->updateDistributionListService($distributionList, $title, $description, $category, $request);

        if (!$this->distributionListRepository->save($distributionList)) {
            throw new HttpInternalServerErrorException(
                $request,
                'Une erreur est survenue lors de la tentative de modification de la liste de diffusion ' . $distributionList->getTitle()
            );
        }
    }
}
