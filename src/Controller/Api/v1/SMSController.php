<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\Api\v1;

use Exception;
use Multicanal\Controller\Controller;
use Multicanal\Entity\Content;
use Multicanal\Entity\DistributionList;
use Multicanal\Entity\Organization;
use Multicanal\Entity\SMS;
use Multicanal\Utils\APIUtils;
use Multicanal\Utils\ArrayUtils;
use Multicanal\Utils\SMSUtils;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpInternalServerErrorException;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpUnauthorizedException;

/**
 * SMS controller class to send SMS through OVH Api.
 *
 * @package Multicanal\Controller\Api\v1
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class SMSController extends Controller {

    private const INTERNAL_ERROR = 'Une erreur interne s\'est produite lors du traitement de votre requête.';

    public function __construct(ContainerInterface $container) {
        parent::__construct($container);
    }

    /**
     * @OA\Post(
     *     path="/send/sms",
     *     tags={"SMS"},
     *     summary="Service d'envoi de SMS.",
     *     description="Service d'envoi de message pour une liste de diffusion par SMS pour un numéro de téléphone mobile.",
     *     operationId="sendSMS",
     *     deprecated=false,
     *     @OA\RequestBody(
     *         description="Message de type SMS avec numéro de téléphone",
     *         required=true,
     *         @OA\JsonContent(
     *              allOf={
     *                  @OA\Schema(ref="#/components/schemas/SMS"),
     *                  @OA\Schema(
     *                      @OA\Property(
     *                          description="Nom de domaine de la collectivité émettrice",
     *                          property="organization",
     *                          type="string"
     *                      )
     *                  )
     *              }
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Succès, SMS envoyé.",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResponse")
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Requête erronée, il se peut que le(s) paramètre(s) soi(en)t mal saisi(s) ou Le nom de domaine de la collectivité émettrice est obligatoire."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Informations d'authentification invalides ou La collectivité n'existe pas ou n'est pas référencé dans l'application Multicanal."
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Vous n'êtes pas autorisé à accéder à cette ressource."
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Service introuvable ou non implémenté."
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Une erreur interne s'est produite lors du traitement de votre requête."
     *     ),
     *     security={
     *         {"api_key": {}}
     *     }
     * )
     * @throws HttpBadRequestException|HttpUnauthorizedException
     */
    public function send(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $startTime  = $request->getAttribute('startTime');
        $smsParams = $this->getJsonDecodeParams($request);
        $domain = $smsParams->organization;
        if (!empty($domain)) {
            $organization = $this->organizationRepository->findByDomainName($domain);
            if ($organization instanceof Organization) {
                $sms = new SMS($smsParams->mobiles, $smsParams->message);
                $this->sendSMS($request, $sms);
                $this->updateCounterByOrganization($organization, 'SMS', sizeof($smsParams->mobiles));

                return APIUtils::writeResponse($response, 200, 'SMS envoyé.', $startTime);
            } else {
                throw new HttpUnauthorizedException($request, 'La collectivité ' . $domain . ' n\'existe pas ou n\'est pas référencé dans l\'application Multicanal');
            }
        } else {
            throw new HttpBadRequestException($request, 'Le nom de domaine de la collectivité émettrice est obligatoire.');
        }
    }

    /**
     * @OA\Post(
     *     path="/send/sms/{distributionListId}",
     *     tags={"SMS"},
     *     summary="Service d'envoi de SMS à travers une liste de diffusion.",
     *     description="Service d'envoi de message pour une liste de diffusion par SMS à travers une liste de diffusion préalablement enregistré dans l'application Multicanal.",
     *     operationId="sendSmsByDistributionList",
     *     deprecated=false,
     *     @OA\Parameter(
     *         name="distributionListId",
     *         in="path",
     *         description="Identifiant de la liste de diffusion",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Message de type SMS avec numéro de téléphone",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/SMS")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Succès, SMS envoyé.",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResponse")
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="Succès, aucun abonnement mobile dans cette liste de diffusion.",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResponse")
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Requête erronée, il se peut que le(s) paramètre(s) soi(en)t mal saisi(s)."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Informations d'authentification invalides."
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Vous n'êtes pas autorisé à accéder à cette ressource."
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Service introuvable ou non implémenté, ou liste de diffusion inexistante."
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Une erreur interne s'est produite lors du traitement de votre requête."
     *     ),
     *     security={
     *         {"api_key": {}}
     *     }
     * )
     * @throws HttpBadRequestException
     */
    public function sendSmsByDistributionList(ServerRequestInterface $request, ResponseInterface $response, mixed $args): ResponseInterface {
        $startTime  = $request->getAttribute('startTime');
        try {
            $id = ArrayUtils::get($args, 'distributionListId');
            if ($this->checkParams($request, [$id])) {
                $distributionList = $this->distributionListRepository->findById((int) $id);

                if ($distributionList instanceof DistributionList) {
                    if (!empty($distributionList->getEmailSubscriptions())) {
                        $smsParams = $this->getJsonDecodeParams($request);
                        $recipients = $smsParams->mobiles;
                        foreach ($distributionList->getMobileSubscriptions() as $mobileSubscription) {
                            $recipients[] = $mobileSubscription->getMobile();
                        }

                        $sms = new SMS($recipients, $smsParams->message);
                        $this->sendSMS($request, $sms);
                        $this->updateCounterByOrganization($distributionList->getOrganization(), 'SMS', sizeof($recipients));

                        return APIUtils::writeResponse($response, 200, 'SMS envoyé.', $startTime);
                    } else {
                        return APIUtils::writeResponse($response, 204, 'Aucun abonnement mobile dans cette liste de diffusion', $startTime);
                    }
                } else {
                    throw new HttpNotFoundException($request, 'Aucune liste de diffusion enregistrée pour l\'identifiant ' . $id);
                }
            }
        } catch (Exception $e) {
            throw new HttpInternalServerErrorException($request, self::INTERNAL_ERROR, $e);
        }
    }

    /**
     * @OA\Post(
     *     path="/communication/sms/{contentId}",
     *     tags={"SMS"},
     *     summary="Service d'envoi d'un contenu par SMS.",
     *     description="Service d'envoi d'un contenu par communication SMS préalablement enregistré dans l'application Multicanal.",
     *     operationId="sendCommunicationBySms",
     *     deprecated=false,
     *     @OA\Parameter(
     *         name="contentId",
     *         in="path",
     *         description="Identifiant du contenu contenant une communication email",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Message de type SMS avec liste des numéros de téléphones",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/SMS")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Succès, SMS envoyé.",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResponse")
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Requête erronée, il se peut que le(s) paramètre(s) soi(en)t mal saisi(s)."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Informations d'authentification invalides."
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Vous n'êtes pas autorisé à accéder à cette ressource."
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Service introuvable ou non implémenté, ou contenu ou communication SMS inexistant."
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Une erreur interne s'est produite lors du traitement de votre requête."
     *     ),
     *     security={
     *         {"api_key": {}}
     *     }
     * )
     * @throws HttpBadRequestException
     */
    public function sendCommunicationBySms(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        $startTime  = $request->getAttribute('startTime');
        try {
            $id = ArrayUtils::get($args, 'contentId');
            if ($this->checkParams($request, [$id])) {
                $content = $this->contentRepository->findById((int) $id);
                if ($content instanceof Content) {
                    if (!empty($content->getCommunications())) {
                        $hasSMSCommunication = $this->publishBySMSAndCount($request, $content);
                    }

                    if (empty($content->getCommunications()) || !$hasSMSCommunication) {
                        throw new HttpNotFoundException($request, 'Le contenu ' . $content->getTitle() . ' ne possède aucune communication SMS');
                    }
                } else {
                    throw new HttpNotFoundException($request, 'Aucun contenu enregistré pour l\'identifiant ' . $id);
                }
            }
        } catch (Exception $e) {
            throw new HttpInternalServerErrorException($request, self::INTERNAL_ERROR, $e);
        }

        return APIUtils::writeResponse($response, 200, 'Communication publiée', $startTime);
    }

    /**
     * Private function to send SMS throught SMSUtils.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param SMS $sms SMS to send
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @throws HttpBadRequestException|HttpInternalServerErrorException
     * @access private
     */
    private function sendSMS(ServerRequestInterface $request, SMS $sms) {
        if (!SMSUtils::checkSMS($sms)) {
            throw new HttpBadRequestException($request, 'Requête erronée, il se peut que le(s) paramètre(s) soi(en)t mal saisi(s).');
        }
        if (!SMSUtils::sendSms($sms)) {
            throw new HttpInternalServerErrorException($request, self::INTERNAL_ERROR);
        }
    }

    /**
     * Private function to send publish content communication by SMS and increment counter.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param Content $content Content with its communication
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return bool
     * @throws HttpInternalServerErrorException
     * @access private
     */
    private function publishBySMSAndCount(ServerRequestInterface $request, Content $content): bool {
        $hasSMSCommunication = false;
        foreach ($content->getCommunications() as $communication) {
            if ($communication->getChannel() === 'SMS') {
                $hasSMSCommunication = true;
                if (SMSUtils::publishContentBySMS($communication)) {
                    $this->updateCounter($communication);
                } else {
                    throw new HttpInternalServerErrorException($request, self::INTERNAL_ERROR);
                }
            }
        }

        return $hasSMSCommunication;
    }
}
