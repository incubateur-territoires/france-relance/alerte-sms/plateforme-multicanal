<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\Api\v1;

use Exception;
use JsonException;
use Multicanal\Controller\Controller;
use Multicanal\Entity\Content;
use Multicanal\Entity\DistributionList;
use Multicanal\Entity\Email;
use Multicanal\Entity\Organization;
use Multicanal\Utils\APIUtils;
use Multicanal\Utils\ArrayUtils;
use Multicanal\Utils\MailGeneratorUtils;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpInternalServerErrorException;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpUnauthorizedException;

/**
 * Email controller class to send SMS through OVH Api.
 *
 * @package Multicanal\Controller\Api\v1
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class EmailController extends Controller {

    private const INTERNAL_ERROR = 'Une erreur interne s\'est produite lors du traitement de votre requête.';

    public function __construct(ContainerInterface $container) {
        parent::__construct($container);
    }

    /**
     * @OA\Post(
     *     path="/send/email",
     *     tags={"Email"},
     *     summary="Service d'envoi d'email en texte enrichie.",
     *     description="Service d'envoi de message pour une communication par email en texte enrichie (HTML).",
     *     operationId="sendEmail",
     *     deprecated=false,
     *     @OA\RequestBody(
     *         description="Message de type Email avec list des adresses de destination",
     *         required=true,
     *         @OA\JsonContent(
     *              allOf={
     *                  @OA\Schema(ref="#/components/schemas/Email"),
     *                  @OA\Schema(
     *                      @OA\Property(
     *                          description="Nom de domaine de la collectivité émettrice",
     *                          property="organization",
     *                          type="string"
     *                      )
     *                  )
     *              }
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Succès, email envoyé.",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResponse")
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Requête erronée, il se peut que le(s) paramètre(s) soi(en)t mal saisi(s)."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Informations d'authentification invalides."
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Vous n'êtes pas autorisé à accéder à cette ressource."
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Service introuvable ou non implémenté."
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Une erreur interne s'est produite lors du traitement de votre requête."
     *     ),
     *     security={
     *         {"api_key": {}}
     *     }
     * )
     * @throws HttpBadRequestException|HttpUnauthorizedException|JsonException
     */
    public function send(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $startTime      = $request->getAttribute('startTime');
        $emailParams    = $this->getJsonDecodeParams($request);
        $domain         = $emailParams->organization;
        if (!empty($domain)) {
            $organization = $this->organizationRepository->findByDomainName($domain);
            if ($organization instanceof Organization) {
                $this->sendEmail($request, $organization, $this->setEmailFromParameters($emailParams->emails, $emailParams));
                $this->updateCounterByOrganization($organization, 'EMAIL', sizeof($emailParams->emails));

                return APIUtils::writeResponse($response, 200, 'Email envoyé', $startTime);
            } else {
                throw new HttpUnauthorizedException($request, 'La collectivité ' . $domain . ' n\'existe pas ou n\'est pas référencé dans l\'application Multicanal');
            }
        } else {
            throw new HttpBadRequestException($request, 'Le nom de domaine de la collectivité émettrice est obligatoire.');
        }
    }

    /**
     * @OA\Post(
     *     path="/send/email/{distributionListId}",
     *     tags={"Email"},
     *     summary="Service d'envoi d'email en texte enrichie à travers une liste de diffusion.",
     *     description="Service d'envoi de message pour une communication par email en texte enrichie (HTML) à travers une liste de diffusion préalablement enregistré dans l'application Multicanal.",
     *     operationId="sendEmailByDistributionList",
     *     deprecated=false,
     *     @OA\Parameter(
     *         name="distributionListId",
     *         in="path",
     *         description="Identifiant de la liste de diffusion",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Message de type Email avec liste optionnelle des adresses de destination",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/Email")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Succès, email envoyé.",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResponse")
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="Succès, aucun abonnement email dans cette liste de diffusion.",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResponse")
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Requête erronée, il se peut que le(s) paramètre(s) soi(en)t mal saisi(s)."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Informations d'authentification invalides."
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Vous n'êtes pas autorisé à accéder à cette ressource."
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Service introuvable ou non implémenté, ou liste de diffusion inexistante."
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Une erreur interne s'est produite lors du traitement de votre requête."
     *     ),
     *     security={
     *         {"api_key": {}}
     *     }
     * )
     * @throws HttpBadRequestException|JsonException
     */
    public function sendEmailByDistributionList(ServerRequestInterface $request, ResponseInterface $response, mixed $args): ResponseInterface {
        $startTime  = $request->getAttribute('startTime');
        try {
            $id = ArrayUtils::get($args, 'distributionListId');
            if ($this->checkParams($request, [$id])) {
                $distributionList = $this->distributionListRepository->findById((int) $id);

                if ($distributionList instanceof DistributionList) {
                    if (!empty($distributionList->getEmailSubscriptions())) {
                        $emailParams = $this->getJsonDecodeParams($request);
                        $recipients = $emailParams->emails;
                        foreach ($distributionList->getEmailSubscriptions() as $emailSubscription) {
                            $recipients[] = $emailSubscription->getEmail();
                        }

                        $this->sendEmail($request, $distributionList->getOrganization(), $this->setEmailFromParameters($recipients, $emailParams));
                        $this->updateCounterByOrganization($distributionList->getOrganization(), 'EMAIL', sizeof($recipients));

                        return APIUtils::writeResponse($response, 200, 'Email envoyé', $startTime);
                    } else {
                        return APIUtils::writeResponse($response, 204, 'Aucun abonnement email dans cette liste de diffusion', $startTime);
                    }
                } else {
                    throw new HttpNotFoundException($request, 'Aucune liste de diffusion enregistrée pour l\'identifiant ' . $id);
                }
            }
        } catch (Exception $e) {
            throw new HttpInternalServerErrorException($request, self::INTERNAL_ERROR, $e);
        }
    }

    /**
     * @OA\Post(
     *     path="/communication/email/{contentId}",
     *     tags={"Email"},
     *     summary="Service d'envoi d'un contenu par email.",
     *     description="Service d'envoi d'un contenu par communication email préalablement enregistré dans l'application Multicanal.",
     *     operationId="sendCommunicationByEmail",
     *     deprecated=false,
     *     @OA\Parameter(
     *         name="contentId",
     *         in="path",
     *         description="Identifiant du contenu contenant une communication email",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Message de type SMS avec ou sans numéro de téléphone",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/Email")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Succès, SMS envoyé.",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResponse")
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Requête erronée, il se peut que le(s) paramètre(s) soi(en)t mal saisi(s) ou Le nom de domaine de la collectivité émettrice est obligatoire."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Informations d'authentification invalides ou La collectivité n'existe pas ou n'est pas référencé dans l'application Multicanal."
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Vous n'êtes pas autorisé à accéder à cette ressource."
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Service introuvable ou non implémenté, ou contenu ou communication email inexistant."
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Une erreur interne s'est produite lors du traitement de votre requête."
     *     ),
     *     security={
     *         {"api_key": {}}
     *     }
     * )
     * @throws HttpBadRequestException|JsonException
     */
    public function sendCommunicationByEmail(ServerRequestInterface $request, ResponseInterface $response, mixed $args): ResponseInterface {
        $startTime  = $request->getAttribute('startTime');
        try {
            $id = ArrayUtils::get($args, 'contentId');
            if ($this->checkParams($request, [$id])) {
                $content = $this->contentRepository->findById((int) $id);
                if ($content instanceof Content) {
                    if (!empty($content->getCommunications())) {
                        $hasEmailCommunication = $this->publishByEmailAndCount($request, $content);
                    }

                    if (empty($content->getCommunications()) || !$hasEmailCommunication) {
                        throw new HttpNotFoundException($request, 'Le contenu ' . $content->getTitle() . ' ne possède aucune communication email');
                    }
                } else {
                    throw new HttpNotFoundException($request, 'Aucun contenu enregistré pour l\'identifiant ' . $id);
                }
            }
        } catch (Exception $e) {
            throw new HttpInternalServerErrorException($request, self::INTERNAL_ERROR, $e);
        }

        return APIUtils::writeResponse($response, 200, 'Communication publiée', $startTime);
    }

    /**
     * Private function to send email through MailGeneratorUtils.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param Organization $organization Sender organization
     * @param Email $email Email to send
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @throws HttpBadRequestException|HttpInternalServerErrorException
     * @access private
     */
    private function sendEmail(ServerRequestInterface $request, Organization $organization, Email $email) {
        $mailGenerator = new MailGeneratorUtils($this->twig);
        if (!MailGeneratorUtils::checkEmail($email)) {
            throw new HttpBadRequestException($request, 'Requête erronée, il se peut que le(s) paramètre(s) soi(en)t mal saisi(s).');
        }
        if (!$mailGenerator->sendEmailWithOrganizationTemplate($email, $organization)) {
            throw new HttpInternalServerErrorException($request, self::INTERNAL_ERROR);
        }
    }

    /**
     * Private function to send publish content communication by email and increment counter.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param Content $content Content with its communication
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return bool
     * @throws HttpInternalServerErrorException
     * @access private
     */
    private function publishByEmailAndCount(ServerRequestInterface $request, Content $content): bool {
        $hasEmailCommunication = false;
        foreach ($content->getCommunications() as $communication) {
            if ($communication->getChannel() === 'EMAIL') {
                $hasEmailCommunication = true;
                $mailGenerator = new MailGeneratorUtils($this->twig);
                if ($mailGenerator->publishContentByEmail($communication)) {
                    $this->updateCounter($communication);
                } else {
                    throw new HttpInternalServerErrorException($request, self::INTERNAL_ERROR);
                }
            }
        }

        return $hasEmailCommunication;
    }

    /**
     * Set email object from request API parameters.
     *
     * @param array $recipients Email recipients
     * @param mixed $parameters Request API parameters
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return Email
     * @access private
     */
    private function setEmailFromParameters(array $recipients, mixed $parameters): Email {
        $email = new Email($recipients, $parameters->object, $parameters->body);
        (isset($parameters->replyTo)) ? $email->setReplyTo($parameters->replyTo) : $email->setReplyTo('');
        (isset($parameters->replyToEmail)) ? $email->setReplyToEmail($parameters->replyToEmail) : $email->setReplyToEmail('');
        (!empty($parameters->medias)) ? $email->setMedias($parameters->medias) : $email->setMedias([]);

        return $email;
    }
}
