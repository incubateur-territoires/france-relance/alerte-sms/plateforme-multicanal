<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\Api\v1;

use JsonException;
use Multicanal\Entity\Exception\MulticanalException;
use Multicanal\Repository\ContentRepository;
use Multicanal\Repository\DistributionListRepository;
use Multicanal\Utils\APIUtils;
use Multicanal\Utils\ArrayUtils;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpInternalServerErrorException;
use Slim\Exception\HttpNotFoundException;

/**
 * Content controller class to get, list, create, update or delete content.
 *
 * @package Multicanal\Controller\Api\v1
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class ContentController {

    private const DISTRIBUTIONLIST_ID_MISSING = 'Liste de diffusion inexistante pour l\'identifiant ';

    private const INTERNAL_ERROR = 'Une erreur interne s\'est produite lors du traitement de votre requête.';

    protected ContentRepository $contentRepository;

    protected DistributionListRepository $distributionListRepository;

    public function __construct(ContainerInterface $container) {
        $this->distributionListRepository   = $container->get(DistributionListRepository::class);
    }

    /**
     * @OA\Get(
     *     path="/contents/{distributionListId}",
     *     tags={"Content"},
     *     summary="Récupération des contenus d'une liste de diffusion.",
     *     description="Récupération des contenus associés à une liste de diffusion par son identifiant.",
     *     operationId="getContentsByDistributionList",
     *     @OA\Parameter(
     *         name="distributionListId",
     *         in="path",
     *         description="Identifiant de la liste de diffusion concernée",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Succès, liste des contenus récupérée.",
     *         @OA\JsonContent(ref="#/components/schemas/Content")
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Identifiant de la liste de diffusion invalide."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Informations d'authentification invalides."
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Vous n'êtes pas autorisé à accéder à cette ressource."
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Liste de diffusion non trouvée."
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Une erreur interne s'est produite lors du traitement de votre requête."
     *     )
     * )
     * @throws HttpBadRequestException|HttpNotFoundException|HttpInternalServerErrorException|JsonException
     */
    public function getContentsByDistributionList(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        $startTime  = $request->getAttribute('startTime');
        try {
            $id = ArrayUtils::get($args, 'distributionListId');
            if (!is_numeric($id) || (int) $id === 0) {
                throw new HttpBadRequestException($request, 'Identifiant de liste de diffusion invalide');
            }
            $distributionList = $this->distributionListRepository->findById((int) $id);
            if (is_null($distributionList)) {
                throw new HttpNotFoundException($request, self::DISTRIBUTIONLIST_ID_MISSING . $id);
            } else {
                $contents = [];
                /** @var DistributionList $distributionList */
                foreach ($distributionList->getContents() as $content) {
                    $contents[] = $content->__toApi($request);
                }
            }
            return APIUtils::writeResponse($response, 200, count($contents) . ' contenu(s) retourné(s)', $startTime, true, json_encode($contents, JSON_THROW_ON_ERROR));
        } catch (MulticanalException $e) {
            throw new HttpInternalServerErrorException($request, self::INTERNAL_ERROR, $e);
        }
    }
}
