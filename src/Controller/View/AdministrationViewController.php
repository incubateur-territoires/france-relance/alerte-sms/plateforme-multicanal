<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\View;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * AdministrationViewController to display alerts administration
 *
 * @package Multicanal\Controller\View
 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class AdministrationViewController extends ViewController {

    /**
     * Rendering administration page controller.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @param mixed $args Mixed parameters
     * @return ResponseInterface
     * @access public
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        return $this->twig->render($response, 'administration.html', [
            'currentUrl'        => $request->getUri()->getPath(),
            'title'             => 'Administration',
            'breadcrumb'        => ['#' => 'Administration'],
            'administration'    => true
        ]);
    }
}
