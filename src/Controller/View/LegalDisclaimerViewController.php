<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\View;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;


/**
 * LegalDisclaimerViewController to display legal disclaimer page
 *
 * @package Multicanal\Controller\View
 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class LegalDisclaimerViewController extends ViewController {

    /**
     * Rendering legal disclaimer page controller.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @param mixed $args Mixed parameters
     * @return ResponseInterface
     * @access public
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        return $this->twig->render($response, 'legal-disclaimer.html', [
            'currentUrl'        => $request->getUri()->getPath(),
            'title'             => 'Mentions l&eacute;gales',
            'breadcrumb'        => ['#' => 'Mentions l&eacute;gales']
        ]);
    }
}
