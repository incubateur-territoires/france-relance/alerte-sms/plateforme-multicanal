<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\View;

use Multicanal\Entity\Enum\Rank;
use Multicanal\Utils\ArrayUtils;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * ConnectionViewController to display login page
 *
 * @package Multicanal\Controller\View
 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class ConnectionViewController extends ViewController {

    /**
     * Rendering connection page controller.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @param mixed $args Mixed parameters
     * @return ResponseInterface
     * @access public
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        $organization = ArrayUtils::get($args, 'organization');
        if ($this->user != null) {
            if (
                !empty($organization)
                && ($this->user->getRank() == Rank::ADMIN() || $this->user->getRank() == Rank::ADMINGN())
            ) {
                $url = '/administration/' . $organization;
            } elseif ($this->user->getRank() == Rank::ADMINGN()) {
                $url = '/administration';
            } else {
                $url = '/contenus';
            }
        } else {
            if ($this->settings->get('oidc.connexion_enabled')) {
                $organization = ArrayUtils::get($args, 'organization');
                if (!empty($organization)) {
                    $url = '/action/openid-connect/' . urlencode($organization);
                } else {
                    $url = '/action/openid-connect';
                }
            } else {
                return $this->twig->render($response, 'connection.html', [
                    'currentUrl'    => $request->getUri()->getPath(),
                    'title'         => 'Connexion',
                    'breadcrumb'    => ['#' => 'Connexion'],
                    'organization'  => $organization
                ]);
            }
        }

        return $response->withStatus(302)->withHeader('Location', $url);
    }
}
