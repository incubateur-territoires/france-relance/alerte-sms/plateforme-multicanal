<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\View;

use Multicanal\Entity\Enum\Rank;
use Multicanal\Lib\ChannelPublisher\FacebookChannelPublisher;
use Multicanal\Utils\ArrayUtils;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * OrganizationViewController to display alerts administration
 *
 * @package Multicanal\Controller\View
 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class OrganizationViewController extends ViewController {

    /**
     * Rendering organization page controller.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @param array $args Facebook token passed as URL parameter
     * @return ResponseInterface
     * @access public
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {

        $token = ArrayUtils::get($args, 'facebookToken');
        if (!empty($token)) {
            $pages =  FacebookChannelPublisher::getUserPages($token);
        }

        $organizationName = $this->user->getOrganizationName();
        $organization     = $this->organizationRepository->createIfNotExist($organizationName);
        if ($this->user->getRank() != Rank::ADMINGN) {
            $organization->addUser($this->dbUser);
        }
        return $this->twig->render($response, 'my-organization.html', [
            'currentUrl'            => $request->getUri()->getPath(),
            'title'                 => 'Ma collectivit&eacute;',
            'breadcrumb'            => ['#' => 'Ma collectivit&eacute;'],
            'organization'          => $organization,
            'categories'            => $organization->getCategories(),
            'distributionLists'     => $organization->getDistributionLists(),
            'fileUploadHintText'    => $this->getFileUploadHintText(),
            'fileUploadAccept'      => $this->getFileUploadAcceptAttribute(),
            'facebookPages'         => $pages
        ]);
    }
}
