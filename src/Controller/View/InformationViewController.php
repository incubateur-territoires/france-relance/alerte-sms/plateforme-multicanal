<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\View;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * InformationViewController to display information page
 *
 * @package Multicanal\Controller\View
 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class InformationViewController extends ViewController {

    /**
     * Rendering information list from email page controller.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @throws LoaderError|RuntimeError|SyntaxError
     * @access public
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {

        $organization = $this->dbUser->getOrganization();
        if (is_null($organization->getMailboxParameters())) {
            $this->flash->addMessage('info', 'Aucune bo&icirc;te email d\'information param&eacute;tr&eacute;e');
        }

        $categories = $organization->getCategories();
        $contents   = $this->contentRepository->findByDomain($organization, ['DRAFT', 'TO_PUBLISH']);

        return $this->twig->render($response, 'information.html', [
            'currentUrl' => $request->getUri()->getPath(),
            'title'      => 'Mes informations',
            'breadcrumb' => ['#' => 'Mes informations'],
            'categories' => $categories,
            'contents' => $contents,
        ]);
    }
}
