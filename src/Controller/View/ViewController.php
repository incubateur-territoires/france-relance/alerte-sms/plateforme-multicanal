<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\View;

use Doctrine\Common\Collections\Collection;
use Multicanal\Controller\Controller;
use Multicanal\Entity\Organization;
use Multicanal\Entity\User;
use Multicanal\Utils\BytesUtils;
use Psr\Container\ContainerInterface;
use Slim\Csrf\Guard;
use Slim\Flash\Messages;

/**
 * Interface ViewController to load common objects to all pages
 *
 * @package Multicanal\Controller\View
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class ViewController extends Controller {

    protected Guard $csrf;

    protected Messages $flash;

    protected Organization $organization;

    public function __construct(ContainerInterface $container) {

        parent::__construct($container);

        // Flash messages
        $this->flash = $this->container->get(Messages::class);

        // Twig view
        $this->twig->getEnvironment()->addGlobal('flash',   $this->flash);
        $this->twig->getEnvironment()->addGlobal('user',    $this->user);

        // General properties
        $this->twig->getEnvironment()->addGlobal('domain',  $this->settings->get('app.domain'));
        $this->twig->getEnvironment()->addGlobal('oidc_connexion_enabled', $this->settings->get('oidc.connexion_enabled'));

        // Flash messages
        $this->twig->getEnvironment()->addGlobal('flash',   $this->flash);

        // Authenticated user
        $this->twig->getEnvironment()->addGlobal('user',    $this->user);

        // CSRF guard
        $this->csrf = $this->container->get('csrf');
        $csrfArray = [
            'keys'    => ['name' => $this->csrf->getTokenNameKey(), 'value' => $this->csrf->getTokenValueKey()],
            'name'    => $this->csrf->getTokenName(),
            'value'   => $this->csrf->getTokenValue()
        ];
        $this->twig->getEnvironment()->addGlobal('csrf', $csrfArray);

        // Assets cache busting
        if (!empty($this->settings->get('app.assets_cache_busting'))) {
            $this->twig->getEnvironment()->addGlobal('assets_cache_busting', '?v=' . $this->settings->get('app.assets_cache_busting'));
        }

        // User organization, only if a user is authenticated
        if ($this->user instanceof User) {
            $this->organization = $this->dbUser->getOrganization();
        }

        // gnSettings (for javascript)
        $this->twig->getEnvironment()->addGlobal('gnSettings', [
            'dmUploader' => $this->settings->get('dmUploader'),
            'isUserLogged' => $this->container->get('user') instanceof User,
            'env' => $this->settings->get('env')
        ]);

        // External authentication flag
        $this->twig->getEnvironment()->addGlobal('externalAuthentication', $this->settings->get('oidc.external'));
    }

    /**
     * Get filtered category list from side menu navigation.
     *
     * @param mixed $categoryId Category id or All string to filter
     * @author Thierry CEREYON <t.cereyon@groupeonepoint.com>
     * @return array|Collection
     * @access protected
     */
    protected function getCategoriesFilter(mixed $categoryId): array|Collection {
        if (isset($categoryId) && is_numeric($categoryId)) {
            $categories = [$this->categoryRepository->findById((int) $categoryId)];
        } elseif ($this->organization instanceof Organization) {
            $categories = $this->organization->getCategories();
        }

        return $categories;
    }

    protected function getFileUploadAcceptAttribute() {
        $uploaderSettings = $this->settings->get('genericImageUploader');
        $genericUploadExtensions = array_map(function ($ext) {
            return '.' . $ext;
        }, $uploaderSettings['allowed_extensions']);

        return implode(',', array_merge($genericUploadExtensions, $uploaderSettings['allowed_mimetypes']));
    }

    protected function getFileUploadHintText() {
        $uploaderSettings = $this->settings->get('genericImageUploader');
        $maxAllowedSize = BytesUtils::formatSize($uploaderSettings['max_filesize']);
        $supportedFormats = implode(', ', $uploaderSettings['allowed_extensions']);
        return sprintf(
            'Taille maximale: %s, Formats supportés : %s.',
            $maxAllowedSize,
            $supportedFormats
        );
    }
}
