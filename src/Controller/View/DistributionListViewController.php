<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\View;

use Multicanal\Entity\DistributionList;
use Multicanal\Entity\Organization;
use Multicanal\Entity\User;
use Multicanal\Utils\ArrayUtils;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpNotFoundException;

/**
 * CommunicationViewController to display communications list, one specific communication or content on an organization custom page
 *
 * @package Multicanal\Controller\View
 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class DistributionListViewController extends ViewController {

    /**
     * Rendering public communications list page controller.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @param mixed $args Mixed parameters
     * @return ResponseInterface
     * @throws HttpNotFoundException
     * @access public
     */
    public function viewCommunicationsList(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        $domain       = ArrayUtils::get($args, 'organization');
        $organization = $this->organizationRepository->findByDomainName($domain);
        if ($organization instanceof Organization) {
            return $this->twig->render($response, 'public/communications-list.html', [
                'currentUrl'     => $request->getUri()->getPath(),
                'title'          => 'Communications de ' . $organization->getGeneralParameters()->getLabel(),
                'breadcrumb'     => $this->user instanceof User ? ['/contenus' => 'Mes contenus', '#' => 'Listes de diffusion de ma collectivit&eacute;'] : null,
                'organization'   => $organization,
                'communications' => $organization->getDistributionLists()
            ]);
        } else {
            throw new HttpNotFoundException($request, 'La collectivité ' . $domain . ' semble n\'avoir aucun canal de communication enregistré.');
        }
    }

    /**
     * Rendering public communication page controller.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @param mixed $args Mixed parameters
     * @return ResponseInterface
     * @throws HttpNotFoundException
     * @access public
     */
    public function viewCommunication(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        $communicationId  = ArrayUtils::get($args, 'communicationId');
        $distributionList = $this->distributionListRepository->findById((int) $communicationId);
        if ($distributionList instanceof DistributionList) {
            return $this->twig->render($response, 'public/communication.html', [
                'currentUrl'    => $request->getUri()->getPath(),
                'title'         => 'Communication de ' . $distributionList->getOrganization()->getGeneralParameters()->getLabel(),
                'breadcrumb'    => $this->user instanceof User ? ['/contenus' => 'Mes contenus', '#' => 'Ma collectivit&eacute; : ' . $distributionList->getTitle()] : null,
                'organization'  => $distributionList->getOrganization(),
                'communication' => $distributionList
            ]);
        } else {
            throw new HttpNotFoundException($request, 'La liste de diffusion dont l\'identifiant est ' . $communicationId . ' n\'existe pas ou semble avoir été supprimée.');
        }
    }

    /**
     * Rendering communication page for printing controller.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @param mixed $args Mixed parameters
     * @return ResponseInterface
     * @throws HttpNotFoundException
     * @access public
     */
    public function print(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        $distributionListId  = ArrayUtils::get($args, 'distributionListId');
        $distributionList = $this->distributionListRepository->findById((int) $distributionListId);
        if ($distributionList instanceof DistributionList) {
            return $this->twig->render($response, 'public/communication-pdf.html', [
                'currentUrl'    => $request->getUri()->getPath(),
                'title'         => 'Abonnement ' . $distributionList->getTitle() . ' - ' . $distributionList->getOrganization()->getDomain(),
                'organization'  => $distributionList->getOrganization(),
                'communication' => $distributionList
            ]);
        } else {
            throw new HttpNotFoundException($request, 'La liste de diffusion dont l\'identifiant est ' . $distributionListId . ' n\'existe pas ou semble avoir &eacute;t&eacute; supprim&eacute;e.');
        }
    }
}
