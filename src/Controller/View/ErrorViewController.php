<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\View;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Classe ErrorViewController to display custom error page for multicana application
 *
 * @package Multicanal\Controller\View
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class ErrorViewController extends ViewController {

    /**
     * Rendering multicanal error page controller.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @return ResponseInterface
     * @access public
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
        $exception = $request->getAttribute('exception');
        switch ($exception->getCode()) {
            case 400:
                $code = 400;
                $message = 'La syntaxe de la requ&ecirc;te est mal formul&eacute;e ou est impossible &agrave; satisfaire.';
                break;
            case 401:
                $code = 401;
                $message = 'Vous n\'&ecirc;tes pas autoris&eacute; &agrave; acc&eacute;der &agrave; cette page.';
                break;
            case 403:
                $code = 403;
                $message = 'Vous n\'&ecirc;tes pas autoris&eacute; &agrave; effectuer cette op&eacute;ration.';
                break;
            case 404:
                $code = 404;
                $message = 'La page demand&eacute;e n\'a pas &eacute;t&eacute; trouv&eacute;e car elle a peut-&ecirc;tre &eacute;t&eacute; d&eacute;plac&eacute;e, ou elle n\'existe plus.';
                break;
            case 405:
                $code = 405;
                $message = 'La m&eacute;thode utilis&eacute;e pour cette requ&ecirc;te n\'est pas support&eacute;e par la ressource cibl&eacute;e.';
                break;
            case 504:
                $code = 504;
                $message = 'La passerelle met trop de temps &agrave; r&eacute;pondre.';
                break;
            default:
                $code = 500;
                $message = 'Erreur interne, le serveur a rencontr&eacute; une condition inattendue qui l\'a emp&ecirc;ch&eacute; de satisfaire la demande.';
        }

        if ($this->settings->get('env.debug')) {
            $message .= '<br /><br /><i>' . $exception->getMessage() . '</i>';
        }

        return $this->twig->render($response, 'error.html', [
            'currentUrl'        => $request->getUri()->getPath(),
            'title'             => 'Erreur ' . $code,
            'breadcrumb'        => null,
            'errorCode'         => $code,
            'errorMessage'      => $message
        ]);
    }
}
