<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\View;

use Multicanal\Entity\Enum\Rank;
use Multicanal\Utils\ArrayUtils;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * OrganizationViewController to display alerts administration
 *
 * @package Multicanal\Controller\View
 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class AdministrationOrganizationViewController extends ViewController {

    /**
     * Rendering organization administration page controller.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @param mixed $args Mixed parameters
     * @return ResponseInterface
     * @access public
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        $organizationName = ArrayUtils::get($args, 'organization');
        $organization     = $this->organizationRepository->createIfNotExist($organizationName);
        if ($this->user->getRank() != Rank::ADMINGN) {
            $organization->addUser($this->dbUser);
        }

        return $this->twig->render($response, 'my-organization.html', [
            'currentUrl'            => $request->getUri()->getPath(),
            'title'                 => 'Ma collectivit&eacute;',
            'breadcrumb'            => [
                '/administration' => 'Administration',
                '#'               => $organizationName,
            ],
            'organization'          => $organization,
            'filter_organisation'   => 'ByOrganization',
            'categories'            => $organization->getCategories(),
            'fileUploadHintText'    => $this->getFileUploadHintText(),
            'fileUploadAccept'      => $this->getFileUploadAcceptAttribute(),
        ]);
    }
}
