<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\View;

use Multicanal\Entity\Content;
use Multicanal\Entity\DistributionList;
use Multicanal\Entity\User;
use Multicanal\Utils\ArrayUtils;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpNotFoundException;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * ContentViewController to display content management page
 *
 * @package Multicanal\Controller\View
 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class ContentViewController extends ViewController {

    const CONTENTS_PATH = '/contenus';

    const CONTENTS_TITLE = 'Mes contenus';

    /**
     * Rendering public contents list page controller for a specific distribution list.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @param mixed $args Mixed parameters
     * @return ResponseInterface
     * @throws HttpNotFoundException
     * @access public
     */
    public function viewContentsList(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        $domain             = ArrayUtils::get($args, 'organization');
        $distributionListId = ArrayUtils::get($args, 'distributionListId');
        $distributionList   = $this->distributionListRepository->findById($distributionListId);
        if ($distributionList instanceof DistributionList) {
            $title = 'Contenus de la liste de diffusion ' . $distributionList->getTitle();
            return $this->twig->render($response, 'public/contents-list.html', [
                'currentUrl'        => $request->getUri()->getPath(),
                'title'             => $title,
                'breadcrumb'        => $this->user instanceof User ? ['/contenus' => 'Mes contenus', '#' => $title] : null,
                'distributionList'  => $distributionList,
                'organization'      => $distributionList->getOrganization(),
                'contents'          => $distributionList->getContents()
            ]);
        } else {
            throw new HttpNotFoundException($request, 'La collectivité ' . $domain . ' semble n\'avoir aucun canal de communication enregistré pour l\'identifiant ' . $distributionListId . '.');
        }
    }

    /**
     * Rendering view content page controller.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @param mixed $args Mixed parameters
     * @return ResponseInterface
     * @throws HttpNotFoundException|LoaderError|RuntimeError|SyntaxError
     * @access public
     */
    public function viewContent(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        $contentId    = ArrayUtils::get($args, 'contentId');
        $content      = $this->contentRepository->findById((int) $contentId);
        if ($content instanceof Content) {
            $organization = $content->getCategory()->getOrganization();
            $title        = $organization->getGeneralParameters()->getLabel() . ' - ' . $content->getTitle();

            return $this->twig->render($response, 'public/content.html', [
                'currentUrl'    => $request->getUri()->getPath(),
                'title'         => $title,
                'breadcrumb'    => $this->user instanceof User ? [self::CONTENTS_PATH => self::CONTENTS_TITLE, '#' => $title] : null,
                'organization'  => $organization,
                'content'       => $content
            ]);
        } else {
            throw new HttpNotFoundException($request, 'Le contenu dont l\'identifiant est ' . $contentId . ' n\'existe pas ou semble avoir été supprimé.');
        }
    }

    /**
     * Rendering list contents page controller.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @param mixed $args Mixed parameters
     * @return ResponseInterface
     * @throws LoaderError|RuntimeError|SyntaxError
     * @access public
     */
    public function listContent(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        $categoryId = ArrayUtils::get($args, 'categoryId');
        $categories = $this->getCategoriesFilter($categoryId);

        return $this->twig->render($response, 'content/contents.html', [
            'currentUrl'          => $request->getUri()->getPath(),
            'title'               => self::CONTENTS_TITLE,
            'breadcrumb'          => ['#' => self::CONTENTS_TITLE],
            'organization'        => $this->dbUser->getOrganization(),
            'organizationName'    => $this->user->getOrganizationName(),
            'currentCategory'     => $categoryId,
            'currentCategoryName' => !empty($categoryId) ? $this->categoryRepository->findById($categoryId)->getName() : null,
            'categories'          => $categories,
            'distributionLists'   => $this->organization->getDistributionLists(),
            'activePage'          => 'contents',
            'quickAccessList'     => $this->getQuickAccesContentList()
        ]);
    }

    /**
     * Rendering edit content page controller.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @param mixed $args Mixed parameters
     * @return ResponseInterface
     * @throws LoaderError|RuntimeError|SyntaxError
     * @access public
     */
    public function editContents(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        $categoryId = ArrayUtils::get($args, 'categoryId');
        $categories = $this->getCategoriesFilter($categoryId);

        return $this->twig->render($response, 'content/edit-publish-content.html', [
            'currentUrl'          => $request->getUri()->getPath(),
            'title'               => '&Eacute;diter - publier mes contenus',
            'breadcrumb'          => [self::CONTENTS_PATH => self::CONTENTS_TITLE, '#' => '&Eacute;diter - publier mes contenus'],
            'organization'        => $this->dbUser->getOrganization(),
            'organizationName'    => $this->user->getOrganizationName(),
            'currentCategory'     => $categoryId,
            'currentCategoryName' => !empty($categoryId) ? $this->categoryRepository->findById($categoryId)->getName() : null,
            'categories'          => $categories,
            'distributionLists'   => $this->organization->getDistributionLists(),
            'activePage'          => 'edit-contents',
            'quickAccessList'     => $this->getQuickAccesContentList()
        ]);
    }

    /**
     * Rendering edit content form page controller.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @param mixed $args Mixed parameters
     * @return ResponseInterface
     * @throws LoaderError|RuntimeError|SyntaxError
     * @access public
     */
    public function editContentForm(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        $contentId = ArrayUtils::get($args, 'contentId');
        $content = null;
        if (!is_null($contentId)) {
            $content = $this->contentRepository->findById((int)$contentId);
        }

        if (is_null($content)) {
            $title = 'Nouveau contenu';
        } else {
            $title = '&Eacute;dition contenu - ' . $content->getTitle();
        }

        return $this->twig->render($response, 'content/content-edition.html', [
            'currentUrl'        => $request->getUri()->getPath(),
            'title'             => $title,
            'breadcrumb'        => [self::CONTENTS_PATH => self::CONTENTS_TITLE, '#' => $title],
            'organization'      => $this->dbUser->getOrganization(),
            'organizationName'  => $this->user->getOrganizationName(),
            'categories'        => $this->organization->getCategories(),
            'distributionLists' => $this->organization->getDistributionLists(),
            'content'           => $content,
            'quickAccessList'   => $this->getQuickAccesContentList(),
            'fileUploadHintText' => $this->getFileUploadHintText(),
            'fileUploadAccept'  => $this->getFileUploadAcceptAttribute(),
        ]);
    }

    /**
     * Rendering distribution list page controller.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @param mixed $args Mixed parameters
     * @return ResponseInterface
     * @throws LoaderError|RuntimeError|SyntaxError
     * @access public
     */
    public function distribution(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        $categoryId = ArrayUtils::get($args, 'categoryId');
        $categories = $this->getCategoriesFilter($categoryId);

        return $this->twig->render($response, 'content/distribution.html', [
            'currentUrl'          => $request->getUri()->getPath(),
            'title'               => 'Listes de diffusion',
            'breadcrumb'          => [self::CONTENTS_PATH => self::CONTENTS_TITLE, '#' => 'Listes de diffusion'],
            'organization'        => $this->dbUser->getOrganization(),
            'organizationName'    => $this->user->getOrganizationName(),
            'currentCategory'     => $categoryId,
            'currentCategoryName' => !empty($categoryId) ? $this->categoryRepository->findById($categoryId)->getName() : null,
            'categories'          => $categories,
            'distributionLists'   => $this->organization->getDistributionLists(),
            'activePage'          => 'distribution',
            'fileUploadHintText'  => $this->getFileUploadHintText(),
            'fileUploadAccept'    => $this->getFileUploadAcceptAttribute()
        ]);
    }

    /**
     * Rendering content publication page controller.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @param mixed $args Mixed parameters
     * @return ResponseInterface
     * @throws LoaderError|RuntimeError|SyntaxError
     * @access public
     */
    public function publish(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        $contentId = ArrayUtils::get($args, 'contentId');
        $content   = $this->contentRepository->findById((int) $contentId);

        if ($content instanceof Content) {
            return $this->twig->render($response, 'content/publication/publish.html', [
                'currentUrl'        => $request->getUri()->getPath(),
                'title'             => 'Publier un contenu',
                'breadcrumb'        => [self::CONTENTS_PATH => self::CONTENTS_TITLE, '#' => 'Publier un contenu'],
                'organization'      => $this->dbUser->getOrganization(),
                'organizationName'  => $this->user->getOrganizationName(),
                'categories'        => $this->getCategoriesFilter(null),
                'distributionLists' => $this->organization->getDistributionLists(),
                'content'           => $content
            ]);
        } else {
            throw new HttpNotFoundException($request, 'Contenu inexistant pour l\'identifiant ' . $contentId);
        }
    }

    /**
     * Get quick access content list for draft, to publish and publish status.
     *
     * @author Thierry CEREYON <t.cereyon@groupeonepoint.com>
     * @return array
     * @access private
     */
    private function getQuickAccesContentList(): array {
        return [
            'draft'      => $this->contentRepository->findByDomain($this->organization, ['DRAFT'], 3),
            'to_publish' => $this->contentRepository->findByDomain($this->organization, ['TO_PUBLISH', 'PUBLISHING'], 3),
            'published'  => $this->contentRepository->findByDomain($this->organization, ['PUBLISHED'], 3),
        ];
    }
}
