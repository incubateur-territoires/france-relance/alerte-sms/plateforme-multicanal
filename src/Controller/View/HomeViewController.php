<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller\View;

use Multicanal\Entity\Enum\Rank;
use Multicanal\Entity\User;
use Multicanal\Lib\OrganizationDAO;
use Multicanal\Utils\ArrayUtils;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;


/**
 * HomeViewController to display home page of alertes SMS
 *
 * @package Multicanal\Controller\View
 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class HomeViewController extends ViewController {

    private OrganizationDAO $organizationDao;

    public function __construct(ContainerInterface $container) {
        parent::__construct($container);
        $this->organizationDao     = $container->get(OrganizationDAO::class);
    }

    /**
     * Rendering home page controller.
     *
     * @param ServerRequestInterface $request Slim HTTP request from HTML page
     * @param ResponseInterface $response Slim HTTP response
     * @param mixed $args Mixed parameters
     * @return ResponseInterface
     * @access public
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $args): ResponseInterface {
        $organization = ArrayUtils::get($args, 'organization');
        if ($this->user != null) {
            if (
                !empty($organization)
                && ($this->user->getRank() == Rank::ADMIN() || $this->user->getRank() == Rank::ADMINGN())
            ) {
                $url = '/administration/' . $organization;
            } elseif ($this->user->getRank() == Rank::ADMINGN()) {
                $url = '/administration';
            } else {
                $url = '/contenus';
            }
            return $response->withStatus(302)->withHeader('Location', $url);
        }

        $breadcrumb = null;
        if ($this->user instanceof User && $this->user->getRank() == Rank::ADMINGN()) {
            $breadcrumb = ['/administration' => 'Domaines'];
        }

        return $this->twig->render($response, 'home.html', [
            'currentUrl'        => $request->getUri()->getPath(),
            'organizations'     => $this->organizationDao->getNameOfOrganizations(),
            'title'             => 'Accueil',
            'breadcrumb'        => $breadcrumb
        ]);
    }
}
