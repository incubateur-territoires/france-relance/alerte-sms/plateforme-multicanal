<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Controller;

use DateTime;
use Doctrine\ORM\EntityManager;
use Exception;
use Monolog\Logger;
use Multicanal\App\Helper;
use Multicanal\App\Settings\SettingsInterface;
use Multicanal\Entity\Communication;
use Multicanal\Entity\Counter;
use Multicanal\Entity\Exception\MulticanalException;
use Multicanal\Entity\MulticanalFile;
use Multicanal\Entity\Organization;
use Multicanal\Entity\User;
use Multicanal\Repository\CategoryRepository;
use Multicanal\Repository\ContentRepository;
use Multicanal\Repository\CounterRepository;
use Multicanal\Repository\DistributionListRepository;
use Multicanal\Repository\OrganizationRepository;
use Multicanal\Repository\UserRepository;
use Multicanal\Utils\ArrayUtils;
use Multicanal\Utils\EmailManagerUtils;
use Multicanal\Utils\MulticanalFilesUtils;
use Odan\Session\SessionInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UploadedFileInterface;
use Psr\Log\LoggerInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpInternalServerErrorException;
use Slim\Views\Twig;

/**
 * Interface Controller for loading common objects.
 *
 * @package Multicanal\Controller
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 *
 * @OA\Info(
 *     description="Service de transmission de message multi-canal pour les collectivités adhérentes girondines du syndicat mixte Gironde numérique.",
 *     version="1.0.0",
 *     title="API Multicanal Gironde numérique",
 *     @OA\License(
 *         name="Apache 2.0",
 *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *     ),
 *     @OA\Contact(
 *         email="x.madiot@girondenumerique.fr"
 *     )
 * )
 * @OA\Tag(
 *     name="Content",
 *     description="Gestion des contenus."
 * )
 * @OA\Tag(
 *     name="DistributionList",
 *     description="Gestion des listes de diffusion."
 * )
 * @OA\Tag(
 *     name="Email",
 *     description="Expédition de messages en texte enrichi (HTML) par email."
 * )
 * @OA\Tag(
 *     name="SMS",
 *     description="Envoi de messages instantanés par SMS."
 * )
 * @OA\SecurityScheme(
 *     securityScheme="api_key",
 *     type="apiKey",
 *     in="header",
 *     name="X-Api-Key"
 * )
 * @see ApiAuthorizationMiddleware to find why we're using X-API-KEY and not api_key as header name
 * @OA\Server(
 *     description="Environnement de production du service Multicanal Gironde numérique.",
 *     url="https://comclic.girondenumerique.fr/api/v1"
 * )
 * @OA\Server(
 *     description="Environnement de test du service Multicanal Gironde numérique.",
 *     url="https://multicanal-dev.girondenumerique.fr/api/v1"
 * )
 */
class Controller {

    protected const JSON = 'application/json';

    protected const HTML = 'text/html';

    protected ContainerInterface $container;

    protected Twig $twig;

    protected ?User $user;

    protected ?User $dbUser;

    protected ?Organization $dbOrganization;

    protected EntityManager $entityManager;

    protected CategoryRepository $categoryRepository;

    protected ContentRepository $contentRepository;

    protected CounterRepository $counterRepository;

    protected DistributionListRepository $distributionListRepository;

    protected OrganizationRepository $organizationRepository;

    protected UserRepository $userRepository;

    protected SettingsInterface $settings;

    protected Logger $logger;

    protected SessionInterface $session;

    public function __construct(ContainerInterface $container) {

        // Slim container
        $this->container = $container;

        // Session
        $this->session = $this->container->get(SessionInterface::class);

        // Authenticated user
        $this->user = $this->container->get('user');

        // Application Settings
        $this->settings = $container->get(SettingsInterface::class);

        // Application logger
        $this->logger = $container->get(LoggerInterface::class);

        // Entity manage and repositories
        $this->entityManager                = $container->get(EntityManager::class);
        $this->categoryRepository           = $container->get(CategoryRepository::class);
        $this->counterRepository            = $container->get(CounterRepository::class);
        $this->contentRepository            = $container->get(ContentRepository::class);
        $this->distributionListRepository   = $container->get(DistributionListRepository::class);
        $this->organizationRepository       = $container->get(OrganizationRepository::class);
        $this->userRepository               = $container->get(UserRepository::class);

        if ($this->user instanceof User) {
            $this->dbUser = $this->userRepository->findByEmail($this->user->getEmail());
            $this->dbOrganization = $this->organizationRepository->findByDomainName($this->user->getOrganizationName());
        }

        // Application Settings
        $this->settings = $container->get(SettingsInterface::class);

        // Twig view
        $this->twig = $this->container->get('view');
    }

    /**
     * Check if a params array from HTML form is empty.
     * @param ServerRequestInterface $request HTTP input request
     * @param array $params HTML form values as array
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return bool
     * @throws HttpBadRequestException When array is null or empty
     * @access protected
     */
    protected function checkParams(ServerRequestInterface $request, array $params): bool {
        if (ArrayUtils::arrayIsNotEmpty($params)) {
            return true;
        } else {
            throw new HttpBadRequestException($request);
        }
    }

    /**
     * Check if a file has been uploaded without error through a HTML form.
     *
     * @param object $file Uploaded file through a HTML form
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return bool
     * @access private
     */
    protected function isUploadOK(object $file): bool {
        return $file->getSize() > 0 && $file->getError() === UPLOAD_ERR_OK;
    }

    /**
     * Set a multicanalFile referenced by Object if an image file has been selected.
     *
     * @param UploadedFileInterface $imageFile
     * @param string $title
     * @param string $folder
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return MulticanalFile|null
     * @throws MulticanalException
     * @access public
     */
    protected function getMulticanalFile(UploadedFileInterface $imageFile, string $title, string $folder): ?MulticanalFile {
        // Handle single input with single file upload
        // If an image has been selected
        $directory = Helper::getUploadDirectory() . '/' . $this->user->getDomainName() . '/' . $folder;
        if ($this->isUploadOK($imageFile)) {
            return new MulticanalFile(
                MulticanalFilesUtils::extractFile($imageFile, $directory, $title),
                $directory
            );
        }
        return null;
    }

    /**
     * Set a DateTime object from a date and an hour as string. If hour is empty, set 10H by default.
     *
     * @param string|null $date Date as string
     * @param string|null $hour Hour as date
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return DateTime|null
     * @access private
     */
    protected function getDeadlineAsDateTime(?string $date, ?string $hour): ?DateTime {
        if (!empty($date)) {
            $deadline = $date;
            if (!empty($hour)) {
                $deadline .= ':' . $hour;
            } else {
                $deadline .= ':10:00';
            }
            return DateTime::createFromFormat('Y-m-d:H:i', $deadline);
        } else {
            return null;
        }
    }

    /**
     * JSON decode HTTP request
     *
     * @param ServerRequestInterface $request HTTP request
     * @author Thierry CEREYON <t.cereyon@groupeonepoint.com>
     * @return mixed
     * @throws HttpInternalServerErrorException
     * @access public
     */
    public function getJsonDecodeParams(ServerRequestInterface $request): mixed {
        try {
            $bodyContent = $request->getBody()->getContents();
            return json_decode($bodyContent);
        } catch (Exception $e) {
            throw new HttpInternalServerErrorException($request, 'Une erreur interne s\'est produite lors du traitement de votre requête.', $e);
        }
    }

    /**
     * Update organization counter for SMS and email communications.
     *
     * @param Communication $communication Current communication
     * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return void
     */
    protected function updateCounter(Communication $communication): void {
        $organization  = $communication->getContent()->getOrganization();
        $numberOfRecipients = 0;
        if ($communication->getChannel() === 'EMAIL') {
            $numberOfRecipients = count($communication->getContent()->getDistributionList()->getEmailSubscribersList());
        } elseif ($communication->getChannel() === 'SMS') {
            $numberOfRecipients = count($communication->getContent()->getDistributionList()->getMobileSubscribersList());
        }
        $this->updateCounterByOrganization($organization, $communication->getChannel(), $numberOfRecipients);
    }

    /**
     * Update organization counter for SMS and email API sending.
     *
     * @param Organization $organization Current communication
     * @param string $type Sending type (EMAIL or SMS)
     * @param int $numberOfRecipients Number of recipients
     * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return void
     */
    protected function updateCounterByOrganization(Organization $organization, string $type, int $numberOfRecipients): void {
        $currentYear = date('Y');
        /** @var ?Counter $counter */
        $counter = $this->counterRepository->findByYearOrganization($currentYear, $organization);
        if (is_null($counter)) {
            $counter = new Counter();
            $counter->setYear($currentYear);
            $counter->setOrganization($organization);
            $this->counterRepository->save($counter);
            $organization->addCounter($counter);
            $this->organizationRepository->save($organization);
        }
        if ($type === 'EMAIL') {
            $counter->setNbOfEmail($counter->getNbOfEmail() + $numberOfRecipients);
        } elseif ($type === 'SMS') {
            $counter->setNbOfSms($counter->getNbOfSms() + $numberOfRecipients);
        }
        $this->counterRepository->save($counter);
    }

    /**
     * Get mailbox manager to interact with information mail box.
     *
     * @return EmailManagerUtils
     * @throws Exception
     * @access protected
     */
    protected function getMailboxManager(): EmailManagerUtils {
        $mailboxParameters = $this->dbUser->getOrganization()->getMailboxParameters();
        return new EmailManagerUtils($mailboxParameters->getHost(), $mailboxParameters->getUser(), $mailboxParameters->getPassword());
    }

    /**
     * Set an error message with exception message in debug environment.
     *
     * @param Exception $e Concerned exception
     * @param string $message Message to display
     * @return string
     * @access protected
     */
    protected function setErrorMessage(Exception $e, string $message): string {
        if ($this->settings->get('env.debug')) {
            $message .= '<br /><em>' . $e->getMessage() . '</em>';
        }

        return $message;
    }
}
