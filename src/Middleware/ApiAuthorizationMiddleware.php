<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Middleware;

use Multicanal\App\Settings\SettingsInterface;
use Multicanal\Entity\Exception\MulticanalException;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Authorization middleware for API access
 *
 * @package Multicanal\Middleware
 * @author William BLANC DIT JOLICOEUR <w.blanc-dit-jolicoeur@girondenumerique.fr>
 */
final class ApiAuthorizationMiddleware implements MiddlewareInterface {

    public const API_KEY_HEADER_NAME = 'X-Api-Key';

    private string $apiKey;

    private int $startTime;

    public function __construct(ContainerInterface $container) {
        $this->apiKey = $container->get(SettingsInterface::class)->get('app.api_key');
        $this->startTime = intval(microtime(true) * 1000);
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
        /**
         * On utilise le header X-API-KEY au lieu de l'historique api_key car avec Apache 2.4 + cgi (php-fpm),
         * les headers contenant des underscores sont éliminés. ( probleme legacy des CGI)
         * De cette manière, le code peut s'executer comme attendu indifférement des contexte d'execution (apache + mod_php,
         * apache + php-fpm, nginx + php-fpm)
         * @see https://dev.to/thesameeric/dont-use-underscores-in-your-http-headers-gfp
         * @see https://www.grouparoo.com/blog/dont-use-underscores-in-http-headers
         *
         */
        if (!$request->hasHeader(self::API_KEY_HEADER_NAME)) {
            throw new MulticanalException('Informations d\'authentification manquantes.', 406);
        }

        if ($request->getHeaderLine(self::API_KEY_HEADER_NAME) !== $this->apiKey) {
            throw new MulticanalException('Informations d\'authentification invalides.', 401);
        }

        $request = $request->withAttribute('startTime', $this->startTime);
        return $handler->handle($request);
    }
}
