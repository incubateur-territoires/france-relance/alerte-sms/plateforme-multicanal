<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Middleware;

use DI\Container;
use Jumbojett\OpenIDConnectClient;
use Multicanal\App\Helper;
use Multicanal\App\Settings\SettingsInterface;
use Odan\Session\SessionInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LoggerInterface;

/**
 * Middleware to initialize session and Flash messages.
 *
 * @package Multicanal\Middleware
 * @author  William Blanc Dit Jolicoeur <w.blanc-dit-jolicoeur@girondenumerique.fr>
 */
class EntryMiddleware implements MiddlewareInterface {
    protected ContainerInterface $container;

    protected SessionInterface $session;

    protected SettingsInterface $settings;

    protected OpenIDConnectClient $oidc;

    protected LoggerInterface $logger;

    public function __construct(ContainerInterface $container, SettingsInterface $settings, SessionInterface $session, OpenIDConnectClient $oidc, LoggerInterface $logger) {
        $this->container = $container;
        $this->session   = $session;
        $this->settings = $settings;
        $this->oidc = $oidc;
        $this->logger = $logger;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {

        if ($this->shouldSessionBeStarted($request)) {
            $this->session->start();
        }

        // Juste for exemple purpose
        // $this->logger->debug('entering the entry middleware (before all middleware process the request');

        assert($this->container instanceof Container);

        $response = $handler->handle($request);
        $this->session->save();

        // Juste for exemple purpose
        // $this->logger->debug('leaving the entry middleware (after all middlewares have processed the request');
        return $response;
    }

    /**
     * We should not start session
     * - if already started
     * - if on an api route
     */
    private function shouldSessionBeStarted(ServerRequestInterface $request): bool {
        $startSession = true;

        if ($this->session->isStarted()) {
            $startSession = false;
        }

        if (Helper::isApiRouteRequest($request)) {
            $startSession = false;
        }

        return $startSession;
    }
}
