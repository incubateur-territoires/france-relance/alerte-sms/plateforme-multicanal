<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Middleware\Authentication;

use Multicanal\Entity\Enum\Rank;
use Multicanal\Lib\SessionUser;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Exception\HttpUnauthorizedException;
use Slim\Flash\Messages;
use Slim\Psr7\Response;

/**
 * Middleware to check user authentication.
 *
 * @package Multicanal\Middleware\Authentication
 * @author  William Blanc Dit Jolicoeur <w.blanc-dit-jolicoeur@girondenumerique.fr>
 */
abstract class BaseAuthMiddleware implements MiddlewareInterface {
    /**
     * @var Rank[]|null
     */
    protected ?array $ranks;

    protected SessionUser $sessionUser;

    protected Messages $flash;

    /**
     * __construct
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container) {
        $this->sessionUser  = $container->get(SessionUser::class);
        $this->flash        = $container->get(Messages::class);
        $this->ranks        = [];
    }

    /**
     * setRanks
     *
     * @param array<int, Rank> $ranks
     *
     * @return void
     */
    protected function setRanks(array $ranks = []): void {
        $this->ranks = $ranks;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {

        if ($this->sessionUser->hasBeenDisconected()) {
            $response = new Response();
            return $response->withStatus(302)->withHeader('Location', '/');
        }

        $user = $this->sessionUser->getUser();
        if (
            $user == null
            || (sizeof($this->ranks) && !in_array($user->getRank(), $this->ranks))
        ) {
            if (strpos($request->getUri()->getPath(), 'action/') !== false) {
                throw new HttpUnauthorizedException($request);
            } else {
                $response = new Response();
                $this->flash->addMessage('info', 'Vous n\'avez pas les droits');
                return $response->withStatus(302)->withHeader('Location', '/');
            }
        }
        return $handler->handle($request);
    }
}
