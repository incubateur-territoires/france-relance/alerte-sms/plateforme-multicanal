<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Middleware\Authentication;

use Multicanal\Entity\Enum\Rank;
use Psr\Container\ContainerInterface;

/**
 * Middleware to check authentication and user rights.
 *
 * @package Multicanal\Middleware\Authentication
 * @author  William Blanc Dit Jolicoeur <w.blanc-dit-jolicoeur@girondenumerique.fr>
 */
class AdminGNAuthMiddleware extends BaseAuthMiddleware {
    public function __construct(ContainerInterface $container) {
        parent::__construct($container);
        $this->setRanks([Rank::ADMINGN()]);
    }
}
