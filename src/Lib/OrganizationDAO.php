<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Lib;

use Multicanal\App\Settings\SettingsInterface;
use Multicanal\Entity\Exception\MulticanalException;

/**
 * DAO class for treatments organizations from Open LDAP server
 *
 * @package Multicanal\Lib
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class OrganizationDAO extends LdapConnection {

    protected $ldapLogin;

    protected $ldapPassword;

    /**
     * Constructor to connect LDAP
     */
    public function __construct(SettingsInterface $settings) {
        parent::__construct($settings);
        $this->ldapLogin = $settings->get('ldap.login');
        $this->ldapPassword = $settings->get('ldap.password');
    }

    /**
     * Function to get organization's names from LDAP
     *
     * @author Alexis ZUCHER <a.zucher@girondenumerique.fr>
     * @return array
     */
    public function getNameOfOrganizations(): array {
        $organizationArray = [];
        // TODO: remove that
        return ["Organisation 1", "Organisation 2"];
        if ($this->openLdap($this->ldapLogin, $this->ldapPassword)) {
            $ldapOrganizations = $this->search('dc=global,dc=gn', 'o=*');
            $this->closeLdap();
            unset($ldapOrganizations['count']);
            foreach ($ldapOrganizations as $ldapOrganization) {
                array_push($organizationArray, $ldapOrganization['o'][0]);
            }
            return $organizationArray;
        } else {
            throw new MulticanalException('Identifiant ou mot de passe erron&eacute;', 401);
        }
    }
}
