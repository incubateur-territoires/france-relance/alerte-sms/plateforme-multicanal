<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Lib\ChannelPublisher;

use Multicanal\App\Helper;
use Multicanal\Entity\Communication;
use Multicanal\Entity\MulticanalFile;
use Multicanal\Entity\SocialNetwork\TwitterChannel;
use Multicanal\Utils\MulticanalFilesUtils;
use Symfony\Component\HttpClient\HttpClient;

/**
 * TwitterChannelPublisher abstract class to access Twitter API
 *
 * @package Multicanal\Lib\ChannelPublisher
 * @author  Nathanael KHODL <consulting@khodl.com>
 */
final class TwitterChannelPublisher extends ChannelPublisher {

    private const TWITTER_API_URL = 'https://api.twitter.com/2/';
    private const TWITTER_UPLOAD_URL = 'https://upload.twitter.com/1.1/';

    /**
     * Publish a communication on Twitter
     *
     * @param Communication $communication
     * @return bool
     */
    public function publishInChannel(Communication $communication): bool {
        $channel = $communication->getOrganisationChannel();
        if ($accessToken = $this->getAccessToken($channel)) {
            return $this->tweetCommunication($accessToken, $communication);
        }
        return false;
    }

    /**
     * Request Twitter API with data as JSON
     *
     * @param string $method POST|GET|PUT|DELETE
     * @param string $path API path
     * @param array $data Data that will be sent as JSON
     * @param array $headers HTTP headers
     * @param string $root API root URL
     * @return mixed
     */
    private function requestAPI(string $method, string $path, $data = [], $headers = [], $root = self::TWITTER_API_URL) {

        $client = HttpClient::create();
        $response = $client->request($method, $root . $path, [
            'json' => $data,
            'headers' => $headers
        ]);

        $result = $response->getContent(); // Will throw an exception if the response is not 200 OK
        return json_decode($result);
    }

    /**
     * Get an access token from the refresth token
     *
     * @param TwitterChannel $channel
     * @return string
     */
    private function getAccessToken(TwitterChannel $channel): string {

        $applicationKey = Helper::config('twitter.application_key');
        $refreshToken = $channel->getRefreshToken();

        $response = $this->requestAPI('POST', 'oauth2/token', [
            'refresh_token' => $refreshToken,
            'grant_type' => 'refresh_token',
            'client_id' => $applicationKey,
        ]);

        $channel->setRefreshToken($response->refresh_token);
        return $response->access_token;
    }

    /**
     * Sends the communication to Twitter
     *
     * @param string $accessToken the access token (not the refresh token)
     * @param Communication $communication a Twitter communication
     * @return bool If the publication was successful
     */
    private function tweetCommunication(string $accessToken, Communication $communication): bool {
        $text = $communication->getObject() . "\n" . $communication->getContent()->getShortUrl();
        $options = [
            'text' => $text
        ];

        if ($fileId = $this->uploadFile($accessToken, $communication->getContent()->getIllustration())) {
            $options['media_ids'] = [
                $fileId
            ];
        }


        try {
            $this->requestAPI('POST', 'tweets', $options, [
                'Authorization' => 'Bearer ' . $accessToken,
                'Content-type' => 'application/json'
            ]);
            return true;
        } catch (\Exception $e) {
            dd($e, $options);
            return false;
        }
    }

    /**
     * WIP: Upload a file to Twitter. Only returns null for now.
     *
     * @param string $accessToken
     * @param MulticanalFile|null $file
     * @return string|null
     */
    private function uploadFile(string $accessToken, ?MulticanalFile $file): ?string {

        if (!$file) {
            return null;
        }

        // TODO: Example of code that might work when Twitter/X API v2 supports media uploads with oAuth2 tokens
        $fileData = file_get_contents(MulticanalFilesUtils::getFileUrl($file));
        $response = $this->requestAPI('POST', 'media/upload', [
            'media_data' => base64_encode($fileData),
            'media_category' => 'TWEET_IMAGE'
        ], [
            'Authorization' => 'Bearer ' . $accessToken,
            'Content-type' => 'multipart/form-data'
        ], root: self::TWITTER_UPLOAD_URL);

        return $response->media_id;
    }
}
