<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Lib\ChannelPublisher;

use Multicanal\Entity\Communication;

/**
 * ChannelPublisher abstract class to access social network publishing process
 *
 * @package Multicanal\Lib\ChannelPublisher
 * @author  Nathanael KHODL <consulting@khodl.com>
 */
abstract class ChannelPublisher {
    abstract public function publishInChannel(Communication $communication): bool;

    final public function publish(Communication $communication): bool {
        if ($communication->getSent()) {
            return false;
        }

        if ($communication->getOrganisationChannel()->isArchived()) {
            return false;
        }

        return $this->publishInChannel($communication);
    }
}
