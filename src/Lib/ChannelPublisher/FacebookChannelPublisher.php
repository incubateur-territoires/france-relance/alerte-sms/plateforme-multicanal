<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Lib\ChannelPublisher;

use FacebookAds\Api;
use Multicanal\App\Helper;
use Multicanal\Entity\Communication;
use Multicanal\Entity\SocialNetwork\FacebookChannel;
use Multicanal\Utils\MulticanalFilesUtils;

/**
 * FacebookChannelPublisher abstract class to access Facebook API
 *
 * @package Multicanal\Lib\ChannelPublisher
 * @author  Nathanael KHODL <consulting@khodl.com>
 */
final class FacebookChannelPublisher extends ChannelPublisher {
    public static function getClient(string $accessToken) {
        return Api::init(
            Helper::config('facebook.application_key'),
            Helper::config('facebook.application_secret'),
            $accessToken
        );
    }

    /**
     * Publish Communication in a Facebook Channel
     *
     * @param Communication $communication
     * @return bool
     */
    public function publishInChannel(Communication $communication): bool {

        /** @var FacebookChannel $channel */
        $channel = $communication->getOrganisationChannel();

        $facebook = self::getClient($channel->getRefreshToken());

        $id = $channel->getIdentifier();
        $text = $communication->getContent()->getRawText();
        $url = $communication->getContent()->getShortUrl();
        $illustration = $illustration = $communication->getContent()->getIllustration();

        try {
            if (MulticanalFilesUtils::isImage($illustration)) {
                // Illustration, with link within text
                $facebook->call('/' . $id . '/photos', 'POST', [
                    'url' => MulticanalFilesUtils::getFileUrl($illustration),
                    'caption' => trim($text . "\n\n" . $url),
                ])->getContent();
            } else {
                // Post link with text
                $facebook->call('/' . $id . '/feed', 'POST', [
                    'message' => $text,
                    'link' => $url,
                ])->getContent();
            }
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Return all the pages owned by a user, including the pages associated with a business owned by said user
     *
     * @param string $userToken
     * @return array
     */
    public static function getUserPages(string $userToken) {
        $client = self::getClient($userToken);

        // Check if user is still valid
        $uid = $client->call('/me', 'GET')->getContent()['id'] ?? null;
        if (!$uid) {
            return [];
        }

        // Load user pages
        $pages = $client->call('/me/accounts', 'GET')->getContent()['data'] ?? [];

        // Load business accounts
        $businesses = $client->call('/' . $uid . '/businesses', 'GET')->getContent()['data'] ?? [];
        foreach ($businesses as $business) {
            // Load business pages
            $businessPages = $client->call('/' . $business['id'] . '/owned_pages', 'GET', [
                'fields' => 'id,name,access_token,instagram_business_account'
            ])->getContent()['data'] ?? [];

            // Add business pages to pages
            $pages = array_merge($pages, $businessPages);
        }

        return self::cleanPages($pages);
    }

    /**
     * Clean the pages list, remove duplicates and prioritize pages with Instagram account
     *
     * @param array $pages An array pages data as returned by the API
     * @return array
     */
    private static function cleanPages(array $pages) {
        // Only keep pages with access (they can still be listed)
        $pages = array_filter($pages, function ($page) {
            return isset($page['access_token']);
        });

        // Sort pages with Instagram accounts first
        usort($pages, function ($a, $b) {
            if ($a['instagram_business_account'] ?? false and !($b['instagram_business_account'] ?? false)) {
                return -1;
            } elseif (!($a['instagram_business_account'] ?? false) and $b['instagram_business_account'] ?? false) {
                return 1;
            } else {
                return 0;
            }
        });

        // Deduplicate pages
        $uniquePages = [];
        foreach ($pages as $page) {
            if (!isset($uniquePages[$page['id']])) {
                $uniquePages[$page['id']] = $page;
            }
        }

        return $uniquePages;
    }

    /**
     * Retrieve the data of the instagram account
     *
     * @param string $pageToken The Facebook page token that owns the Instagram account
     * @param string $instagramId The Instagram id of the account
     * @return array|false
     */
    public static function getInstagramAccount(string $pageToken, string $instagramId) {
        $client = self::getClient($pageToken);
        return $client->call('/' . $instagramId, 'GET', [
            'fields' => 'id,username,name'
        ])->getContent() ?? false;
    }
}
