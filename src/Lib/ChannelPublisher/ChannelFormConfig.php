<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Lib\ChannelPublisher;

/**
 * ChannelFormConfig class to describe publish form through a social network channel
 *
 * @package Multicanal\Lib\ChannelPublisher
 * @author  Nathanael KHODL <consulting@khodl.com>
 */
class ChannelFormConfig {
    public function __construct(
        // Title constraints
        public bool $useTitle = false,
        public bool $isTitleRequired = true,
        public int $minTitleLength = 0, // 0 = no min length
        public int $maxTitleLength = 0, // 0 = no max length
        // Content constraints
        public bool $useContent = false,
        // Media constraints
        public bool $useMedia = false,
    ) {
    }
}
