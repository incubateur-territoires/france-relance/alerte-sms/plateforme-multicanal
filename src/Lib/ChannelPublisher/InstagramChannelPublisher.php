<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Lib\ChannelPublisher;

use FacebookAds\Api;
use Multicanal\App\Helper;
use Multicanal\Entity\Communication;
use Multicanal\Entity\MulticanalFile;
use Multicanal\Entity\SocialNetwork\InstagramChannel;
use Multicanal\Utils\MulticanalFilesUtils;

/**
 * InstagramChannelPublisher abstract class to access Instagram API
 *
 * @package Multicanal\Lib\ChannelPublisher
 * @author  Nathanael KHODL <consulting@khodl.com>
 */
final class InstagramChannelPublisher extends ChannelPublisher {
    public static function getClient(string $accessToken) {
        return Api::init(
            Helper::config('facebook.application_key'),
            Helper::config('facebook.application_secret'),
            $accessToken
        );
    }

    /**
     * Publish Communication to Instagram
     *
     * @param Communication $communication
     * @return bool Status of publication
     */
    public function publishInChannel(Communication $communication): bool {

        /** @var InstagramChannel $channel */
        if (!$communication->getMedias()->count()) {
            return false;
        }

        $channel = $communication->getOrganisationChannel();


        $client = FacebookChannelPublisher::getClient($channel->getRefreshToken());

        // Get all files, with cover first
        $files = [];
        $files[] = $communication->getContent()->getIllustration();
        foreach ($communication->getMedias() as $medium) {
            $files[] = $medium;
        }

        // Remove duplicates
        $files = array_unique($files);

        // Using only the first 10 files
        if (count($files) > 10) {
            $files = array_slice($files, 0, 10);
        }

        // Create media containers
        $containers = [];
        foreach ($files as $file) {
            $containers[] = $this->createMediaContainer($file, $channel);
        }

        // Create post with those containers
        $carouselData = $client->call('/' . $channel->getIdentifier() . '/media', 'POST', [
            'media_type' => 'CAROUSEL',
            'children' => implode(',', $containers),
            'caption' => $communication->getContent()->getRawText(),
        ])->getContent() ?? [];

        if (!$postId = $carouselData['id'] ?? null) {
            return false;
        }

        // Publish post
        $creationData = $client->call('/' . $channel->getIdentifier() . '/media_publish', 'POST', [
            'creation_id' => $postId,
        ])->getContent() ?? [];

        $postId = $creationData['id'] ?? null;

        return (bool)$postId;
    }

    /**
     * Upload the media in a container, that can be used later in a post
     *
     * @param MulticanalFile $medium The file to upload
     * @param InstagramChannel $channel The Instagram Channel
     * @return string|null The container id
     */
    private function createMediaContainer(MulticanalFile $medium, InstagramChannel $channel): ?string {

        $mediumUrl = MulticanalFilesUtils::getFileUrl($medium);

        $profileId = $channel->getIdentifier();
        $client = FacebookChannelPublisher::getClient($channel->getRefreshToken());

        $options = [
            'is_carousel_item' => true,
        ];
        if (MulticanalFilesUtils::isImage($medium)) {
            $options['image_url'] = $mediumUrl;
        } elseif (MulticanalFilesUtils::isVideo($medium)) {
            $options['media_type'] = 'VIDEO';
            $options['video_url'] = $mediumUrl;
        } else {
            return null;
        }

        $containerData = $client->call('/' . $profileId . '/media', 'POST', $options)->getContent() ?? [];
        if (!$containerId = $containerData['id'] ?? null) {
            return null;
        }
        return $containerId;
    }
}
