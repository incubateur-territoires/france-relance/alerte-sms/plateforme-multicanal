<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Lib;

use LDAP\Connection;
use Multicanal\App\Settings\SettingsInterface;
use Multicanal\Entity\Exception\MulticanalException;

/**
 * Connection class to access Ldap
 *
 * @package Multicanal\Lib
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class LdapConnection {

    protected Connection|false $ldapConnection;

    protected $ldapServer;

    protected $ldapPort;

    public function __construct(SettingsInterface $settings) {
        $this->ldapServer = $settings->get('ldap.server');
        $this->ldapPort = $settings->get('ldap.port');
    }

    /**
     * Open a LDAP connection.
     *
     * @param string $login User login
     * @param string $password User password
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return bool
     * @access public
     */
    public function openLdap(string $login, string $password): bool {
        try {
            $this->ldapConnection = ldap_connect($this->ldapServer, $this->ldapPort);
            ldap_set_option($this->ldapConnection, LDAP_OPT_PROTOCOL_VERSION, 3); // Force LDAPv3 (for openldap)

            return @ldap_bind($this->ldapConnection, sprintf($login), $password);
        } catch (\Exception $e) {
            throw new MulticanalException('Erreur lors de la connexion au serveur OpenLDAP : <i>' . $e->getMessage() . '</i>');
        }
    }

    /**
     * Get datas from an open connection Ldap.
     *
     * @param string $base Search base
     * @param string $filter Search filter
     * @param array $attributes Attributes array
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return array|false
     * @access public
     */
    public function search(string $base, string $filter, array $attributes = []): array|false {
        return ldap_get_entries($this->ldapConnection, ldap_search($this->ldapConnection, $base, $filter, $attributes));
    }

    /**
     * Close a connection Ldap.
     *
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @access public
     */
    public function closeLdap(): void {
        ldap_close($this->ldapConnection);
    }

    /**
     * Get connection Ldap.
     *
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return Connection|false
     * @access public
     */
    public function getLdapConnection(): Connection|false {
        return $this->ldapConnection;
    }
}
