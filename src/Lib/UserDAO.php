<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Lib;

use Multicanal\Entity\Enum\Rank;
use Multicanal\Entity\Exception\MulticanalException;
use Multicanal\Entity\User;

/**
 * DAO class for treatments user for LDAP
 *
 * @package Multicanal\Lib
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class UserDAO extends LdapConnection {

    /**
     * Function to search a user in LDAP, use for authentication
     *
     * @param string $organizationName Organization name in which user must be find
     * @param string $login User login
     * @param string $password User password
     * @return User
     * @throws MulticanalException
     * @access public
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     */
    public function searchUser(string $organizationName, string $login, string $password): User {
        if ($this->openLdap('uid=' . $login . ',ou=users,o=' . $organizationName . ',dc=global,dc=gn', $password)) {
            // Get user informations
            $userInformation = $this->search('ou=users,o=' . $organizationName . ',dc=global,dc=gn', 'uid=' . $login);
            if ($userInformation['count'] == 1) {
                $user = new User($userInformation[0]['uid'][0]);
                $user->setFirstname(ucfirst($userInformation[0]['givenname'][0]));
                $user->setLastname(strtoupper($userInformation[0]['sn'][0]));
                $user->setEmail(strtolower($userInformation[0]['mail'][0]));
                $user->setId($userInformation[0]['uidnumber'][0]);
                $user->setOrganizationName($organizationName == 'admin.gn' ? 'girondenumerique.fr' : $organizationName);
                $user->setRank($this->getUserRank($userInformation, $organizationName));

                if (isset($userInformation[0]['description'][0])) {
                    $user->setDescription($userInformation[0]['description'][0]);
                } else {
                    $user->setDescription('-');
                }
            }
            $this->closeLdap();

            return $user;
        } else {
            throw new MulticanalException('Identifiant ou mot de passe erron&eacute;', 401);
        }
    }

    /**
     * Function to search a user in LDAP, use for authentication
     *
     * @param array $information User information from OpenLDAP in array
     * @param string $organizationName User organization name
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return Rank
     * @access private
     */
    private function getUserRank(array $information, string $organizationName): Rank {
        if ($information[0]['employeetype'][0] > 0) {
            if (
                (strcmp($organizationName, 'girondenumerique.fr') === 0 || strcmp($organizationName, 'admin.gn') === 0)
                && $information[0]['employeetype'][0] == 2
            ) {
                return Rank::ADMINGN();
            } else {
                return Rank::ADMIN();
            }
        } else {
            return Rank::AGENT();
        }
    }
}
