<?php

// SPDX-FileCopyrightText: 2023 Syndicat mixte Gironde Numérique
//
// SPDX-License-Identifier: Apache-2.0

namespace Multicanal\Lib;

use Jumbojett\OpenIDConnectClient;
use Multicanal\App\Settings\SettingsInterface;
use Multicanal\Entity\User;
use Odan\Session\SessionInterface;

/**
 * Middleware to initialize session and Flash messages.
 *
 * @package Multicanal\Middleware
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class SessionUser {
    protected SessionInterface $session;
    protected SettingsInterface $settings;
    protected OpenIDConnectClient $oidc;

    protected ?User $user = null;
    protected bool $disconnected = false;


    public function __construct(SessionInterface $session, SettingsInterface $settings, OpenIDConnectClient $oidc) {

        $this->session = $session;
        $this->settings = $settings;
        $this->oidc = $oidc;

        $user = null;

        if ($this->session->has('user')) {
            $user = unserialize($session->get('user'));
        }

        $this->checkUserSessionValidityAndSetUser($user);
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(?User $user): self {
        $this->user = $user;
        return $this;
    }

    public function hasBeenDisconected(): bool {
        return $this->disconnected;
    }

    public function checkUserSessionValidityAndSetUser(?User $user): bool {
        if (!$user instanceof User) {
            $this->setUser(null);
            return false;
        }

        if ($this->settings->get('oidc.connexion_enabled')) {
            // check if token is still valid
            $data = $this->oidc->introspectToken($user->getIdToken());
            if (!$data->active) {
                $this->session->destroy();
                $this->session->start();
                $this->session->regenerateId();
                $this->disconnected = true;
                $this->setUser(null);
                return false;
            }
            $res = $this->oidc->refreshToken($user->getRefreshToken());
            $user->setIdToken($res->id_token);
            $user->setAccessToken($res->access_token);
            $user->setRefreshToken($res->refresh_token);
            $this->session->set('user', serialize($user));

            $data = $this->oidc->introspectToken($user->getIdToken());
            $this->session->set('exp', $data->exp);
        }
        $this->setUser($user);
        return true;
    }
}
