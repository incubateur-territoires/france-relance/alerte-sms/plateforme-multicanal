<?php

/*
 * This file is part of Twig.
 *
 * (c) Fabien Potencier
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Multicanal\Lib;

use Doctrine\Common\Collections\Collection;
use Multicanal\Entity\MulticanalFile;
use Multicanal\Entity\SocialNetwork\Channel;
use Multicanal\Entity\SocialNetwork\FacebookChannel;
use Multicanal\Entity\SocialNetwork\InstagramChannel;
use Multicanal\Entity\SocialNetwork\TwitterChannel;
use Multicanal\Utils\ChannelFormConfigUtils;
use Multicanal\Utils\MulticanalFilesUtils;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

final class TwigExtension extends AbstractExtension {
    public function getFunctions() {
        return [
            new TwigFunction('getFormConfig', [$this, 'getFormConfig']),
            new TwigFunction('dd', [$this, 'dumpDie']),
            new TwigFunction('isImage', [$this, 'isImage']),
        ];
    }

    public function getFilters() {
        return [
            new TwigFilter('channelName', [$this, 'getChannelName']),
            new TwigFilter('channelIcon', [$this, 'getChannelIcon']),
            new TwigFilter('contains', [$this, 'contains']),
        ];
    }

    public function dumpDie(...$data) {
        dd(...$data);
    }

    public function getFormConfig(Channel $channel) {
        return ChannelFormConfigUtils::getChannelFormConfig($channel);
    }

    public function isImage(MulticanalFile $file) {
        return MulticanalFilesUtils::isImage($file);
    }

    public function getChannelIcon(Channel $channel): string {
        $channelName = strtolower($this->getChannelName($channel));

        switch ($channelName) {
            case 'twitter':
                return 'twitter-fill';
            case 'facebook':
                return 'facebook-circle-fill';
            case 'instagram':
                return 'instagram-fill';
            default:
                return 'window-fill';
        }
    }

    public function getChannelName(Channel $channel): string {
        if ($channel instanceof TwitterChannel) {
            return 'Twitter';
        }
        if ($channel instanceof FacebookChannel) {
            return 'Facebook';
        }
        if ($channel instanceof InstagramChannel) {
            return 'Instagram';
        }

        return 'Website';
    }

    public function contains(Collection $haystack, $needle) {
        return $haystack->contains($needle);
    }
}
